<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemPurchasePrice extends Model
{

    protected $fillable = [
       'item_id', 'price'
    ];
}
