<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerDebt extends Model
{

    protected $fillable = [
       'customer_id', 'nominal'
    ];
}
