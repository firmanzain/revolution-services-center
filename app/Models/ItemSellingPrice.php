<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemSellingPrice extends Model
{

    protected $fillable = [
       'item_id', 'price'
    ];
}
