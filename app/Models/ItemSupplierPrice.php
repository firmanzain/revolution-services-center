<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemSupplierPrice extends Model
{

    protected $fillable = [
       'item_id', 'supplier_id', 'price'
    ];

    public function item()
    {
        return $this->hasOne('App\Models\Item', 'id', 'item_id');
    }

    public function supplier()
    {
        return $this->hasOne('App\Models\Supplier', 'id', 'supplier_id');
    }
}
