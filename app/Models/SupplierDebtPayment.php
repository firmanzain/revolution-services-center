<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SupplierDebtPayment extends Model
{

    protected $fillable = [
       'date', 'number', 'supplier_id', 'debt_nominal', 'debt_nominal_payment', 'user_id', 'information'
    ];

    use SoftDeletes;
    protected $dates =['deleted_at'];

    public function supplier()
    {
        return $this->hasOne('App\Models\Supplier', 'id', 'supplier_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
