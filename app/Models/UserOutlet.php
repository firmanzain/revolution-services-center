<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserOutlet extends Model
{

    protected $fillable = [
       'user_id', 'outlet_id',
    ];

    public function user()
    {
        return $this->hasMany('App\User', 'id', 'user_id');
    }

    public function outlet()
    {
        return $this->hasMany('App\Models\Outlet', 'id', 'outlet_id');
    }
}
