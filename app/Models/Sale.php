<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sale extends Model
{

    protected $fillable = [
       'date', 'number', 'outlet_id', 'customer_id', 'type', 'service_id', 'technician_id', 'service_item_name', 'service_complaint', 'service_equipment', 'subtotal', 'fee', 'down_payment', 'discount', 'ppn', 'total', 'payment', 'information', 'user_id', 'status', 'customer_name', 'customer_phone', 'pay', 'change'
    ];

    use SoftDeletes;
    protected $dates =['deleted_at'];

    public function detail()
    {
        return $this->hasMany('App\Models\SaleDetail', 'sale_id', 'id');
    }

    public function outlet()
    {
        return $this->hasOne('App\Models\Outlet', 'id', 'outlet_id');
    }

    public function customer()
    {
        return $this->hasOne('App\Models\Customer', 'id', 'customer_id');
    }

    public function technician()
    {
        return $this->hasOne('App\Models\Technician', 'id', 'technician_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
