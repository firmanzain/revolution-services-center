<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseDetail extends Model
{

    protected $fillable = [
       'purchase_id', 'item_id', 'price', 'quantity', 'discount',
    ];

    use SoftDeletes;
    protected $dates =['deleted_at'];

    public function header()
    {
        return $this->hasOne('App\Models\Purchase', 'id', 'purchase_id');
    }
}
