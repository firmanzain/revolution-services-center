<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPrivilege extends Model
{

    protected $fillable = [
       'user_type_id', 'menu_id', 'create', 'read', 'update', 'delete'
    ];

    use SoftDeletes;
    protected $dates =['deleted_at'];
}
