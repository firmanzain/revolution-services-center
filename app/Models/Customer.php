<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{

    protected $fillable = [
       'customer_type_id', 'number', 'name', 'email', 'phone', 'address'
    ];

    use SoftDeletes;
    protected $dates =['deleted_at'];

    public function customertype()
    {
        return $this->hasOne('App\Models\CustomerType', 'id', 'customer_type_id');
    }

    public function debt()
    {
        return $this->hasOne('App\Models\CustomerDebt', 'customer_id');
    }
}
