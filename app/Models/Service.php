<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model
{

    protected $fillable = [
       'date', 'date_estimation', 'number', 'outlet_id', 'customer_id', 'item_name', 'complaint', 'equipment', 'information', 'user_id', 'status', 'customer_name', 'customer_phone', 'technician_id'
    ];

    use SoftDeletes;
    protected $dates =['deleted_at'];

    public function outlet()
    {
        return $this->hasOne('App\Models\Outlet', 'id', 'outlet_id');
    }

    public function customer()
    {
        return $this->hasOne('App\Models\Customer', 'id', 'customer_id');
    }

    public function technician()
    {
        return $this->hasOne('App\Models\Technician', 'id', 'technician_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
