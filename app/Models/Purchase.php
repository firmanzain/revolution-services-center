<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Purchase extends Model
{

    protected $fillable = [
       'date', 'number', 'outlet_id', 'supplier_id', 'subtotal', 'discount', 'ppn', 'total', 'payment', 'information', 'user_id', 'status',
    ];

    use SoftDeletes;
    protected $dates =['deleted_at'];

    public function detail()
    {
        return $this->hasMany('App\Models\PurchaseDetail', 'purchase_id', 'id');
    }

    public function outlet()
    {
        return $this->hasOne('App\Models\Outlet', 'id', 'outlet_id');
    }

    public function supplier()
    {
        return $this->hasOne('App\Models\Supplier', 'id', 'supplier_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
