<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Operational extends Model
{

    protected $fillable = [
       'date', 'number', 'outlet_id', 'nominal', 'information', 'user_id',
    ];

    use SoftDeletes;
    protected $dates =['deleted_at'];

    public function outlet()
    {
        return $this->hasOne('App\Models\Outlet', 'id', 'outlet_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
