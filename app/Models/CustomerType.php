<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerType extends Model
{

    protected $fillable = [
       'name', 'discount', 'debt_limit'
    ];

    use SoftDeletes;
    protected $dates =['deleted_at'];

    public function customer()
    {
        return $this->hasMany('App\Models\Customer', 'id', 'customer_type_id');
    }
}
