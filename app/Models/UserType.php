<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserType extends Model
{

    protected $fillable = [
       'name',
    ];

    use SoftDeletes;
    protected $dates =['deleted_at'];

    public function userprivilege()
    {
        return $this->hasMany('App\Models\UserPrivilege', 'user_type_id');
    }
}
