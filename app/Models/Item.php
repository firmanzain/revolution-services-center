<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{

    protected $fillable = [
       'category_id', 'brand_id', 'code', 'name', 'image', 'description'
    ];

    use SoftDeletes;
    protected $dates =['deleted_at'];

    public function purchaseprice()
    {
        return $this->hasOne('App\Models\ItemPurchasePrice', 'item_id', 'id');
    }

    public function sellingprice()
    {
        return $this->hasOne('App\Models\ItemSellingPrice', 'item_id', 'id');
    }

    public function category()
    {
        return $this->hasOne('App\Models\Category', 'id', 'category_id');
    }

    public function brand()
    {
        return $this->hasOne('App\Models\Brand', 'id', 'brand_id');
    }
}
