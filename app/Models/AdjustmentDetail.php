<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdjustmentDetail extends Model
{

    protected $fillable = [
       'adjustment_id', 'item_id', 'quantity_old', 'quantity',
    ];

    use SoftDeletes;
    protected $dates =['deleted_at'];

    public function header()
    {
        return $this->hasOne('App\Models\Adjustment', 'id', 'adjustment_id');
    }
}
