<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Adjustment extends Model
{

    protected $fillable = [
       'date', 'number', 'outlet_id', 'information', 'user_id', 'status',
    ];

    use SoftDeletes;
    protected $dates =['deleted_at'];

    public function detail()
    {
        return $this->hasMany('App\Models\AdjustmentDetail', 'adjustment_id', 'id');
    }

    public function outlet()
    {
        return $this->hasOne('App\Models\Outlet', 'id', 'outlet_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
