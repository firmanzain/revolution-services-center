<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerDebtPayment extends Model
{

    protected $fillable = [
       'date', 'number', 'customer_id', 'debt_nominal', 'debt_nominal_payment', 'user_id', 'information'
    ];

    use SoftDeletes;
    protected $dates =['deleted_at'];

    public function customer()
    {
        return $this->hasOne('App\Models\Customer', 'id', 'customer_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
