<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mutation extends Model
{

    protected $fillable = [
       'date', 'number', 'outlet_origin_id', 'outlet_destination_id', 'information', 'user_id', 'status',
    ];

    use SoftDeletes;
    protected $dates =['deleted_at'];

    public function detail()
    {
        return $this->hasMany('App\Models\MutationDetail', 'mutation_id', 'id');
    }

    public function outletorigin()
    {
        return $this->hasOne('App\Models\Outlet', 'id', 'outlet_origin_id');
    }

    public function outletdestination()
    {
        return $this->hasOne('App\Models\Outlet', 'id', 'outlet_destination_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
