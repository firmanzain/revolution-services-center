<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Outlet extends Model
{

    protected $fillable = [
       'name', 'phone', 'address', 'receipt_note',
    ];

    use SoftDeletes;
    protected $dates =['deleted_at'];

    public function useroutlet()
    {
        return $this->hasMany('App\Models\UserOutlet', 'outlet_id');
    }
}
