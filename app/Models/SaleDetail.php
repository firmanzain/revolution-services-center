<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaleDetail extends Model
{

    protected $fillable = [
       'sale_id', 'item_id', 'purchase_price', 'selling_price', 'quantity', 'discount',
    ];

    use SoftDeletes;
    protected $dates =['deleted_at'];

    public function header()
    {
        return $this->hasOne('App\Models\Sale', 'id', 'sale_id');
    }
}
