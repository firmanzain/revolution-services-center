<?php

namespace App\Http\Controllers;

use App\Models\SupplierDebtPayment;
use Illuminate\Http\Request;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\SupplierDebtPaymentTransformer;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Models\SupplierDebt, App\Models\Purchase, App\Models\Supplier;
use App\User;
use App\Transformers\UserTransformer;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class SupplierDebtPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $columns = ['', 'date', 'number', 'supplier_id', 'debt_nominal_payment', 'status'];

        $length = $request->input('length');
        $column = $request->input('column');
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        $query = SupplierDebtPayment::with(['supplier', 'user'])
                                      ->orderBy($columns[$column], $dir);

        if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
                $query->where('date', 'like', '%' . $searchValue . '%')
                      ->orWhere('number', 'like', '%' . $searchValue . '%')
                      ->orWhere('debt_nominal_payment', 'like', '%' . $searchValue . '%')
                      ->orWhere('status', 'like', '%' . $searchValue . '%');
            });
        }

        $data = $query->paginate($length);
        return ['data' => $data, 'draw' => $request->input('draw')];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'date' => 'required|date|max:255',
            'supplier_id' => 'required|integer|max:20',
            'debt_nominal' => 'required|string|max:20',
            'debt_nominal_payment' => 'required|string|max:20',
            'information' => 'nullable|string|max:255',
        ]);

        $number = 'AR'.Carbon::parse($request['date'])->isoFormat('YYYY').Carbon::parse($request['date'])->isoFormat('MM');
        $check = SupplierDebtPayment::select(DB::raw('SUBSTRING(number,9,5) as id'))
                                      ->whereYear('date', Carbon::parse($request['date'])->isoFormat('YYYY'))
                                      ->whereMonth('date', Carbon::parse($request['date'])->isoFormat('MM'))
                                      ->orderBy('number', 'DESC')
                                      ->first();

        $seq = '';
        if ($check) {
            $urut = intval($check->id) + 1;
            $seq = sprintf("%05d",$urut);
        } else {
            $seq = sprintf("%05d",1);
        }

        $number .= $seq;

        $payment = SupplierDebtPayment::create([
            'date' => Carbon::parse($request['date'])->isoFormat('YYYY-MM-DD'),
            'number' => $number,
            'supplier_id' => $request['supplier_id'],
            'debt_nominal' => intval(str_replace(',', '', $request['debt_nominal'])),
            'debt_nominal_payment' => intval(str_replace(',', '', $request['debt_nominal_payment'])),
            'information' => $request['information'],
            'user_id' => $user->id,
            'status' => 0,
        ]);

        $debtNominal = $payment['debt_nominal'] - $payment['debt_nominal_payment'];
        $debt = SupplierDebt::where('supplier_id', $payment['supplier_id'])->delete();
        SupplierDebt::create([
            'supplier_id' => $payment['supplier_id'],
            'nominal' => $debtNominal,
        ]);

        return $debt;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SupplierDebtPayment  $supplierDebtPayment
     * @return \Illuminate\Http\Response
     */
    public function show(SupplierDebtPayment $supplierDebtPayment, $id)
    {
        $supplierDebtPayment = SupplierDebtPayment::where('id', $id)->first();
        return fractal()
               ->item($supplierDebtPayment, new SupplierDebtPaymentTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SupplierDebtPayment  $supplierDebtPayment
     * @return \Illuminate\Http\Response
     */
    public function edit(SupplierDebtPayment $supplierDebtPayment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SupplierDebtPayment  $supplierDebtPayment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SupplierDebtPayment $supplierDebtPayment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SupplierDebtPayment  $supplierDebtPayment
     * @return \Illuminate\Http\Response
     */
    public function destroy(SupplierDebtPayment $supplierDebtPayment, $id)
    {
        $payment = SupplierDebtPayment::find($id);
        if (!$payment) {
            return response()->json([
                'code' => 404,
                'message' => 'Not found.'
            ], 404);
        }

        $debtNominal = 0;
        $debt = SupplierDebt::where('supplier_id', $payment->supplier_id)->first();
        if ($debt) {
            $debtNominal = $debt->nominal;
        }

        SupplierDebt::where('supplier_id', $payment->supplier_id)->delete();
        SupplierDebt::create([
            'supplier_id' => $payment->supplier_id,
            'nominal' => $debtNominal + $payment->debt_nominal_payment,
        ]);
        $payment->delete();
        return ['message' => 'Data Deleted!'];
    }

    public function reportCount(Request $request)
    {
        $user = Auth::user();

        $outletId = $request->outlet;
        if ($outletId == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
            ->item($user, new UserTransformer())
            ->serializeWith(new ArraySerializer())
            ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outletId = $outletUser['outlet']['id'];
                    break;
                }
            }
        }
        $firstDate = Carbon::parse($request->firstDate);
        $lastDate = Carbon::parse($request->lastDate);

        $debt_nominal = 0;
        $debt_paid = 0;
        $debt_remaining = 0;

        $debtNominalQuery = Purchase::whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
        ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'))
        ->where('payment', 2)
        ->get();

        if ($debtNominalQuery) {
            foreach ($debtNominalQuery as $debt) {
                $debt_nominal += $debt->total;
            }
        }

        $debtPaidQuery = SupplierDebtPayment::whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
        ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'))
        ->get();

        if ($debtPaidQuery) {
            foreach ($debtPaidQuery as $debt) {
                $debt_paid += $debt->debt_nominal_payment;
            }
        }

        $debtRemainingQuery = SupplierDebt::get();

        if ($debtRemainingQuery) {
            foreach ($debtRemainingQuery as $debt) {
                $debt_remaining += $debt->nominal;
            }
        }

        $response['debt_nominal'] = $debt_nominal;
        $response['debt_paid'] = $debt_paid;
        $response['debt_remaining'] = $debt_remaining;

        $chart1 = array();

        $suppliers = Supplier::get();

        foreach ($suppliers as $supplier) {
            $debtNominal = 0;
            $debtQuery = SupplierDebt::where('supplier_id', $supplier->id)->get();
            if ($debtQuery) {
                foreach ($debtQuery as $debt) {
                    $debtNominal += $debt->nominal;
                }
            }

            $supplierTemp = array(
            'name' => $supplier->name,
            'data' => array(
            $debtNominal
            ),
            );
            array_push($chart1, $supplierTemp);
        }
        $response['chart1'] = $chart1;

        return $response;
    }

    public function reportTransaction(Request $request)
    {
        $user = Auth::user();

        $outlet = $request->input('outlet');
        if ($outlet == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
                    ->item($user, new UserTransformer())
                    ->serializeWith(new ArraySerializer())
                    ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outlet = $outletUser['outlet']['id'];
                    break;
                }
            }
        }

        $columns = ['', 'date', 'number', 'supplier', 'nominal', 'information'];

        if ($request->firstDate && $request->lastDate) {
            $firstDate = Carbon::parse($request->firstDate);
            $lastDate = Carbon::parse($request->lastDate);
        }

        $length = $request->input('length');
        $column = $request->input('column');
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        $purchase = Purchase::select(DB::raw('date, purchases.number AS number, suppliers.name AS supplier, total AS nominal, "Pembelian Hutang" as type, information'))
                    ->join('suppliers', 'suppliers.id', '=', 'purchases.supplier_id')
                    ->where('outlet_id', $outlet)->where('payment', 2);
        if ($request->firstDate && $request->lastDate) {
            $purchase->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                     ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        if ($searchValue) {
            $purchase->where(function($query) use ($searchValue) {
                $query->where('date', 'like', '%' . $searchValue . '%')
                ->orWhere('number', 'like', '%' . $searchValue . '%')
                ->orWhere('supplier', 'like', '%' . $searchValue . '%')
                ->orWhere('type', 'like', '%' . $searchValue . '%')
                ->orWhere('nominal', 'like', '%' . $searchValue . '%')
                ->orWhere('information', 'like', '%' . $searchValue . '%');
            });
        }

        $debtPayment = SupplierDebtPayment::select(DB::raw('date, supplier_debt_payments.number AS number, suppliers.name AS supplier, debt_nominal_payment AS nominal, "Pembayaran Hutang" as type, information'))
                       ->join('suppliers', 'suppliers.id', '=', 'supplier_debt_payments.supplier_id')
                       ->union($purchase);
        if ($request->firstDate && $request->lastDate) {
            $debtPayment->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                     ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        if ($searchValue) {
            $debtPayment->where(function($query) use ($searchValue) {
                $query->where('date', 'like', '%' . $searchValue . '%')
                ->orWhere('number', 'like', '%' . $searchValue . '%')
                ->orWhere('supplier', 'like', '%' . $searchValue . '%')
                ->orWhere('type', 'like', '%' . $searchValue . '%')
                ->orWhere('nominal', 'like', '%' . $searchValue . '%')
                ->orWhere('information', 'like', '%' . $searchValue . '%');
            });
        }

        $query = $debtPayment;
        $query->orderBy($columns[$column], $dir);

        $data = $query->paginate($length);
        return ['data' => $data, 'draw' => $request->input('draw')];
    }

    public function reportTransactionExcel(Request $request)
    {
        $user = Auth::user();

        $outlet = $request->input('outlet');
        if ($outlet == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
            ->item($user, new UserTransformer())
            ->serializeWith(new ArraySerializer())
            ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outlet = $outletUser['outlet']['id'];
                    break;
                }
            }
        }

        if ($request->firstDate && $request->lastDate) {
            $firstDate = Carbon::parse($request->firstDate);
            $lastDate = Carbon::parse($request->lastDate);
        }

        $purchase = Purchase::select(DB::raw('date, purchases.number AS number, suppliers.name AS supplier, total AS nominal, "Pembelian Hutang" as type, information'))
                    ->join('suppliers', 'suppliers.id', '=', 'purchases.supplier_id')
                    ->where('outlet_id', $outlet)->where('payment', 2);
        if ($request->firstDate && $request->lastDate) {
            $purchase->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                     ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        $debtPayment = SupplierDebtPayment::select(DB::raw('date, supplier_debt_payments.number AS number, suppliers.name AS supplier, debt_nominal_payment AS nominal, "Pembayaran Hutang" as type, information'))
                       ->join('suppliers', 'suppliers.id', '=', 'supplier_debt_payments.supplier_id')
                       ->union($purchase);
        if ($request->firstDate && $request->lastDate) {
            $debtPayment->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                     ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        $query = $debtPayment;
        $query->orderBy('date', 'ASC');
        $data = $query->paginate();

        $spreadsheet = new Spreadsheet();
        $styleArray1 = array(
            'font'  => array(
                'bold'  => true
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
        );
        $styleArray2 = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );
        $styleArray3 = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
        );
        $styleArray4 = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            ),
        );
        $styleArray5 = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            ),
        );


        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1',env('APP_NAME'))
            ->setCellValue('A2','Laporan Hutang');
        //STYLING TITLE
        $spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray1);
        $spreadsheet->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray1);
        $spreadsheet->getActiveSheet()->mergeCells('A1:F1');
        $spreadsheet->getActiveSheet()->mergeCells('A2:F2');

        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A4','TANGGAL')
            ->setCellValue('B4','NOMOR REFERENSI')
            ->setCellValue('C4','PEMASOK')
            ->setCellValue('D4','NOMINAL')
            ->setCellValue('E4','TIPE')
            ->setCellValue('F4','KETERANGAN');
        //STYLING TITLE
        $spreadsheet->getActiveSheet()->getStyle('A4:F4')->applyFromArray($styleArray1);
        $spreadsheet->getActiveSheet()->getStyle('A4:F4')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

        if ($data) {
          $idx = 5; $no = 1;
            foreach ($data as $value) {
                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A'.$idx,date('d/m/Y', strtotime($value['date'])))
                    ->setCellValue('B'.$idx,$value['number'])
                    ->setCellValue('C'.$idx,$value['supplier'])
                    ->setCellValue('D'.$idx,$value['nominal'])
                    ->setCellValue('E'.$idx,$value['type'])
                    ->setCellValue('F'.$idx,$value['information']);
                $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':F'.$idx)->applyFromArray($styleArray2);
                $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':F'.$idx)->applyFromArray($styleArray5);
                $spreadsheet->getActiveSheet()->getStyle('D'.$idx)->applyFromArray($styleArray4);
                $no++;
                $idx = $idx+1;
            }
        }

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="test.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
    }
}
