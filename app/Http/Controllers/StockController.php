<?php

namespace App\Http\Controllers;

use App\Models\Stock;
use Illuminate\Http\Request;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\StockTransformer;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Item, App\Models\StockCard;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();

        $outlet = $request->input('outlet');
        if ($outlet == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
                    ->item($user, new UserTransformer())
                    ->serializeWith(new ArraySerializer())
                    ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outlet = $outletUser['outlet']['id'];
                    break;
                }
            }
        }

        $columns = ['', 'code', 'name', 'category', 'brand', 'quantity'];

        $length = $request->input('length');
        $column = $request->input('column');
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        $query = Stock::with(['outlet', 'item.category', 'item.brand'])
                        ->where('outlet_id', $outlet);
                        // ->orderBy($columns[$column], $dir);

        if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
                $query->where('quantity', 'like', '%' . $searchValue . '%');
            });
            $query->orWhereHas('item', function($query) use($searchValue) {
                $query->where('code', 'like', '%' . $searchValue . '%')
                      ->orWhere('name', 'like', '%' . $searchValue . '%');
            });
            $query->orWhereHas('item.category', function($query) use($searchValue) {
                $query->where('name', 'like', '%' . $searchValue . '%');
            });
            $query->orWhereHas('item.brand', function($query) use($searchValue) {
                $query->where('name', 'like', '%' . $searchValue . '%');
            });
        }

        $data = $query->paginate($length);
        return ['data' => $data, 'draw' => $request->input('draw')];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function show(Stock $stock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function edit(Stock $stock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stock $stock)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stock $stock)
    {
        //
    }

    public function detail(Request $request, $outletId, $itemId = NULL)
    {
        $query = Stock::with(['outlet', 'item'])->where('outlet_id', $outletId);
        if ($itemId) {
            $query->where('item_id', $itemId);
        }

        $data = $query->paginate(15);

        if ($itemId) {
            $data = $query->first();
            if ($data) {
                return fractal()
                       ->item($data, new StockTransformer())
                       ->serializeWith(new ArraySerializer())
                       ->toArray();
            } else {
                return [];
            }
        } else {
            return fractal()
                   ->collection($data, new StockTransformer())
                   ->serializeWith(new ArraySerializer())
                   ->toArray();
        }
    }

    public function reportCount(Request $request)
    {
        $user = Auth::user();

        $outletId = $request->outlet;
        if ($outletId == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
                    ->item($user, new UserTransformer())
                    ->serializeWith(new ArraySerializer())
                    ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outletId = $outletUser['outlet']['id'];
                    break;
                }
            }
        }
        $firstDate = Carbon::parse($request->firstDate);
        $lastDate = Carbon::parse($request->lastDate);

        $item = 0;
        $itemQuery = Item::count();
        if ($itemQuery) {
          $item = $itemQuery;
        }
        $response['item'] = $item;

        $stock = 0;
        $stockQuery = Stock::where('outlet_id', $outletId)->get();
        if ($stockQuery) {
            foreach ($stockQuery as $item) {
                $stock += $item->quantity;
            }
        }
        $response['stock'] = $stock;

        $chartCategories = array();
        $chartItems = array();

        $startDate = Carbon::parse($request->firstDate);
        $itemQuery = Item::get();
        if ($itemQuery) {
            foreach ($itemQuery as $item) {

                $stockItems = StockCard::whereDate('date', '<', $startDate->isoFormat('YYYY-MM-DD'))
                                         ->where('outlet_id', $outletId)
                                         ->where('item_id', $item->id)
                                         ->orderBy('date', 'desc')
                                         ->limit(1)
                                         ->get();
                $begin = 0;
                if ($stockItems) {
                    foreach ($stockItems as $stock) {
                        $begin = $stock->ending;
                    }
                }

                $chartItemsTemp = array(
                    'id' => $item->id,
                    'name' => $item->name,
                    'last' => $begin,
                    'data' => array(),
                );
                array_push($chartItems, $chartItemsTemp);
            }
        }

        if ($firstDate->diffInDays($lastDate) <= 30) {

            $startDate = Carbon::parse($request->firstDate);
            $endDate = Carbon::parse($request->firstDate);
            $endDate->addDays(6);

            do {
                $strDate = $startDate->isoFormat('DD') . '-' . $endDate->isoFormat('DD/MM/YY');
                array_push($chartCategories, $strDate);

                for ($i = 0; $i < sizeof($chartItems); $i++) {
                    $stockItems = StockCard::whereDate('date', '>=', $startDate->isoFormat('YYYY-MM-DD'))
                                             ->whereDate('date', '<=', $endDate->isoFormat('YYYY-MM-DD'))
                                             ->where('outlet_id', $outletId)
                                             ->where('item_id', $chartItems[$i]['id'])
                                             ->get();

                    $stockMovement = $chartItems[$i]['last'];
                    if ($stockItems) {
                        foreach ($stockItems as $stock) {
                            $stockMovement = $stock->ending;
                            $chartItems[$i]['last'] = $stock->ending;
                        }
                    }

                    array_push($chartItems[$i]['data'], $stockMovement);
                }

                $startDate = Carbon::parse($endDate)->addDays(1);
                $endDate = Carbon::parse($endDate);
                $endDate->addDays(6);

            } while ($endDate->lessThanOrEqualTo($lastDate));

            if ($startDate->lessThan($lastDate) && $endDate->greaterThan($lastDate)) {
                $strDate = $startDate->isoFormat('DD') . '-' . $lastDate->isoFormat('DD/MM/YY');
                array_push($chartCategories, $strDate);

                for ($i = 0; $i < sizeof($chartItems); $i++) {
                    $stockItems = StockCard::whereDate('date', '>=', $startDate->isoFormat('YYYY-MM-DD'))
                                             ->whereDate('date', '<=', $endDate->isoFormat('YYYY-MM-DD'))
                                             ->where('outlet_id', $outletId)
                                             ->where('item_id', $chartItems[$i]['id'])
                                             ->get();

                    $stockMovement = $chartItems[$i]['last'];
                    if ($stockItems) {
                        foreach ($stockItems as $stock) {
                            $stockMovement = $stock->ending;
                            $chartItems[$i]['last'] = $stock->ending;
                        }
                    }

                    array_push($chartItems[$i]['data'], $stockMovement);
                }
            }

        } else {
            $startDate = Carbon::parse($request->firstDate);

            do {
                $strDate = $startDate->isoFormat('MMM');
                array_push($chartCategories, $strDate);

                for ($i = 0; $i < sizeof($chartItems); $i++) {
                    $stockItems = StockCard::whereMonth('date', $startDate->isoFormat('MM'))
                                             ->whereYear('date', $startDate->isoFormat('YYYY'))
                                             ->where('outlet_id', $outletId)
                                             ->where('item_id', $chartItems[$i]['id'])
                                             ->get();

                    $stockMovement = $chartItems[$i]['last'];
                    if ($stockItems) {
                        foreach ($stockItems as $stock) {
                            $stockMovement = $stock->ending;
                            $chartItems[$i]['last'] = $stock->ending;
                        }
                    }

                    array_push($chartItems[$i]['data'], $stockMovement);
                }

                $startDate->addMonths();

            } while ($startDate->lessThanOrEqualTo($lastDate));
        }
        $response['chart_categories'] = $chartCategories;
        $response['chart_items'] = $chartItems;


        $chart2 = array();
        $totalQty = 0;
        $chart2Query = Stock::select('stocks.*', 'items.name')
                             ->join('items', function ($join) {
                                 $join->on('stocks.item_id', 'items.id');
                             })
                             ->where('stocks.outlet_id', $outletId)
                             ->orderBy('stocks.quantity', 'DESC')
                             // ->groupBy('items.id')
                             ->limit(10)
                             ->get();

        if ($chart2Query) {
            foreach ($chart2Query as $item) {
                $itemTemp = array(
                    'name' => $item->name.' '.'('.number_format($item->quantity,0,'.',',').')',
                    'y' => 0,
                    'quantity' => intval($item->quantity),
                );
                array_push($chart2, $itemTemp);
                $totalQty += $item->quantity;
            }
        }

        for ($i = 0; $i < sizeof($chart2); $i++) {
            $chart2[$i]['y'] = ($chart2[$i]['quantity'] / $totalQty) * 100;
            if ($i == 0) {
                $chart2[$i]['sliced'] = true;
                $chart2[$i]['selected'] = true;
            }
        }

        $response['chart2'] = $chart2;

        return $response;
    }

    public function stockcard(Request $request)
    {
        $user = Auth::user();

        $item = $request->input('item');
        $outlet = $request->input('outlet');
        if ($outlet == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
            ->item($user, new UserTransformer())
            ->serializeWith(new ArraySerializer())
            ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outlet = $outletUser['outlet']['id'];
                    break;
                }
            }
        }

        $columns = ['', 'date', '', '', 'beginning', 'in', 'out', 'ending', 'information'];

        if ($request->firstDate && $request->lastDate) {
            $firstDate = Carbon::parse($request->firstDate);
            $lastDate = Carbon::parse($request->lastDate);
        }

        $length = $request->input('length');
        $column = $request->input('column');
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        $query = StockCard::with(['outlet', 'item'])
                            ->where('outlet_id', $outlet)
                            ->where('item_id', $item)
                            ->orderBy($columns[$column], $dir);

        if ($request->firstDate && $request->lastDate) {
            $query->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                  ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
                $query->where('date', 'like', '%' . $searchValue . '%')
                ->orWhere('beginning', 'like', '%' . $searchValue . '%')
                ->orWhere('in', 'like', '%' . $searchValue . '%')
                ->orWhere('out', 'like', '%' . $searchValue . '%')
                ->orWhere('ending', 'like', '%' . $searchValue . '%')
                ->orWhere('information', 'like', '%' . $searchValue . '%');
            });
            $query->orWhereHas('item', function($query) use($searchValue) {
                $query->where('code', 'like', '%' . $searchValue . '%')
                      ->orWhere('name', 'like', '%' . $searchValue . '%');
            });
        }

        $data = $query->paginate($length);
        return ['data' => $data, 'draw' => $request->input('draw')];
    }

    public function stockExcel(Request $request)
    {
          $user = Auth::user();

          $outlet = $request->input('outlet');
          if ($outlet == 0) {
              $user = User::where('id', $user->id)->first();
              $user = fractal()
              ->item($user, new UserTransformer())
              ->serializeWith(new ArraySerializer())
              ->toArray();
              foreach ($user['outlet'] as $outletUser) {
                  if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                      $outlet = $outletUser['outlet']['id'];
                      break;
                  }
              }
          }

          $query = Stock::with(['outlet', 'item.category', 'item.brand'])
                          ->where('outlet_id', $outlet);

          $data = $query->paginate();

          $spreadsheet = new Spreadsheet();
          $styleArray1 = array(
              'font'  => array(
                  'bold'  => true
              ),
              'alignment' => array(
                  'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
              ),
          );
          $styleArray2 = array(
              'borders' => array(
                  'allBorders' => array(
                      'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                      'color' => array('argb' => '000000'),
                  ),
              ),
          );
          $styleArray3 = array(
              'alignment' => array(
                  'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
              ),
          );
          $styleArray4 = array(
              'alignment' => array(
                  'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
              ),
          );
          $styleArray5 = array(
              'alignment' => array(
                  'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
              ),
          );


          $spreadsheet->setActiveSheetIndex(0)
              ->setCellValue('A1',env('APP_NAME'))
              ->setCellValue('A2','Laporan Stok');
          //STYLING TITLE
          $spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray1);
          $spreadsheet->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray1);
          $spreadsheet->getActiveSheet()->mergeCells('A1:E1');
          $spreadsheet->getActiveSheet()->mergeCells('A2:E2');

          $spreadsheet->setActiveSheetIndex(0)
              ->setCellValue('A4','KODE BARANG')
              ->setCellValue('B4','NAMA BARANG')
              ->setCellValue('C4','KATEGORI')
              ->setCellValue('D4','MEREK')
              ->setCellValue('E4','KUANTITI');
          //STYLING TITLE
          $spreadsheet->getActiveSheet()->getStyle('A4:E4')->applyFromArray($styleArray1);
          $spreadsheet->getActiveSheet()->getStyle('A4:E4')->applyFromArray($styleArray2);
          $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
          $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
          $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
          $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
          $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

          if ($data) {
            $idx = 5; $no = 1;
              foreach ($data as $value) {
                  $spreadsheet->setActiveSheetIndex(0)
                      ->setCellValue('A'.$idx,$value['item']['code'])
                      ->setCellValue('B'.$idx,$value['item']['name'])
                      ->setCellValue('C'.$idx,$value['item']['category']['name'])
                      ->setCellValue('D'.$idx,$value['item']['brand']['name'])
                      ->setCellValue('E'.$idx,$value['quantity']);
                  $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':E'.$idx)->applyFromArray($styleArray2);
                  $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':E'.$idx)->applyFromArray($styleArray5);
                  $spreadsheet->getActiveSheet()->getStyle('E'.$idx)->applyFromArray($styleArray4);
                  $no++;
                  $idx = $idx+1;
              }
          }

          // Redirect output to a client’s web browser (Xlsx)
          header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
          header('Content-Disposition: attachment;filename="test.xlsx"');
          header('Cache-Control: max-age=0');
          // If you're serving to IE 9, then the following may be needed
          header('Cache-Control: max-age=1');
          // If you're serving to IE over SSL, then the following may be needed
          header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
          header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
          header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
          header('Pragma: public'); // HTTP/1.0
          $writer = new Xlsx($spreadsheet);
          $writer->save('php://output');
    }

    public function stockcardExcel(Request $request)
    {
          $user = Auth::user();

          $item = $request->input('item');
          $outlet = $request->input('outlet');
          if ($outlet == 0) {
              $user = User::where('id', $user->id)->first();
              $user = fractal()
              ->item($user, new UserTransformer())
              ->serializeWith(new ArraySerializer())
              ->toArray();
              foreach ($user['outlet'] as $outletUser) {
                  if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                      $outlet = $outletUser['outlet']['id'];
                      break;
                  }
              }
          }

          if ($request->firstDate && $request->lastDate) {
              $firstDate = Carbon::parse($request->firstDate);
              $lastDate = Carbon::parse($request->lastDate);
          }

          $query = StockCard::with(['outlet', 'item'])
                              ->where('outlet_id', $outlet)
                              ->where('item_id', $item);

          if ($request->firstDate && $request->lastDate) {
              $query->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                    ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
          }

          $query->orderBy('date', 'ASC');
          $data = $query->paginate();

          $spreadsheet = new Spreadsheet();
          $styleArray1 = array(
              'font'  => array(
                  'bold'  => true
              ),
              'alignment' => array(
                  'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
              ),
          );
          $styleArray2 = array(
              'borders' => array(
                  'allBorders' => array(
                      'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                      'color' => array('argb' => '000000'),
                  ),
              ),
          );
          $styleArray3 = array(
              'alignment' => array(
                  'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
              ),
          );
          $styleArray4 = array(
              'alignment' => array(
                  'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
              ),
          );
          $styleArray5 = array(
              'alignment' => array(
                  'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
              ),
          );


          $spreadsheet->setActiveSheetIndex(0)
              ->setCellValue('A1',env('APP_NAME'))
              ->setCellValue('A2','Laporan Kartu Stok');
          //STYLING TITLE
          $spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray1);
          $spreadsheet->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray1);
          $spreadsheet->getActiveSheet()->mergeCells('A1:H1');
          $spreadsheet->getActiveSheet()->mergeCells('A2:H2');

          $spreadsheet->setActiveSheetIndex(0)
              ->setCellValue('A4','TANGGAL')
              ->setCellValue('B4','KODE BARANG')
              ->setCellValue('C4','NAMA BARANG')
              ->setCellValue('D4','AWAL')
              ->setCellValue('E4','MASUK')
              ->setCellValue('F4','KELUAR')
              ->setCellValue('G4','AKHIR')
              ->setCellValue('H4','KETERANGAN');
          //STYLING TITLE
          $spreadsheet->getActiveSheet()->getStyle('A4:H4')->applyFromArray($styleArray1);
          $spreadsheet->getActiveSheet()->getStyle('A4:H4')->applyFromArray($styleArray2);
          $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
          $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
          $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
          $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
          $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
          $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
          $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
          $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

          if ($data) {
            $idx = 5; $no = 1;
              foreach ($data as $value) {
                  $spreadsheet->setActiveSheetIndex(0)
                      ->setCellValue('A'.$idx,date('d/m/Y', strtotime($value['date'])))
                      ->setCellValue('B'.$idx,$value['item']['code'])
                      ->setCellValue('C'.$idx,$value['item']['name'])
                      ->setCellValue('D'.$idx,$value['beginning'])
                      ->setCellValue('E'.$idx,$value['in'])
                      ->setCellValue('F'.$idx,$value['out'])
                      ->setCellValue('G'.$idx,$value['ending'])
                      ->setCellValue('H'.$idx,$value['information']);
                  $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':H'.$idx)->applyFromArray($styleArray2);
                  $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':H'.$idx)->applyFromArray($styleArray5);
                  $spreadsheet->getActiveSheet()->getStyle('D'.$idx.':G'.$idx)->applyFromArray($styleArray4);
                  $no++;
                  $idx = $idx+1;
              }
          }

          // Redirect output to a client’s web browser (Xlsx)
          header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
          header('Content-Disposition: attachment;filename="test.xlsx"');
          header('Cache-Control: max-age=0');
          // If you're serving to IE 9, then the following may be needed
          header('Cache-Control: max-age=1');
          // If you're serving to IE over SSL, then the following may be needed
          header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
          header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
          header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
          header('Pragma: public'); // HTTP/1.0
          $writer = new Xlsx($spreadsheet);
          $writer->save('php://output');
    }
}
