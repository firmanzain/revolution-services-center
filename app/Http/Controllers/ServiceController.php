<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Http\Request;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\ServiceTransformer;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Transformers\UserTransformer;
use PDF;
use App\Models\Sale, App\Models\SaleDetail, App\Models\CustomerDebt,
    App\Models\Stock, App\Models\StockCard, App\Models\Technician;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();

        $outlet = $request->input('outlet');
        if ($outlet == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
                    ->item($user, new UserTransformer())
                    ->serializeWith(new ArraySerializer())
                    ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outlet = $outletUser['outlet']['id'];
                    break;
                }
            }
        }

        $columns = ['', 'date', 'date_estimation', 'number', 'customer_name', 'item_name', 'status'];

        if ($request->firstDate && $request->lastDate) {
            $firstDate = Carbon::parse($request->firstDate);
            $lastDate = Carbon::parse($request->lastDate);
        }

        $length = $request->input('length');
        $column = $request->input('column');
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        $query = Service::with(['outlet', 'customer', 'technician'])
                             ->where('outlet_id', $outlet)
                             ->orderBy($columns[$column], $dir);

        if ($request->firstDate && $request->lastDate) {
            $query->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                  ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
                $query->where('date', 'like', '%' . $searchValue . '%')
                      ->orWhere('date_estimation', 'like', '%' . $searchValue . '%')
                      ->orWhere('number', 'like', '%' . $searchValue . '%')
                      ->orWhere('customer_name', 'like', '%' . $searchValue . '%')
                      ->orWhere('item_name', 'like', '%' . $searchValue . '%')
                      ->orWhere('status', 'like', '%' . $searchValue . '%');
            });
            // $query->orWhereHas('customer', function($query) use($searchValue) {
            //        $query->where('name', 'like', '%' . $searchValue . '%');
            // });
        }

        $data = $query->paginate($length);
        return ['data' => $data, 'draw' => $request->input('draw')];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'date' => 'required|date|max:255',
            'date_estimation' => 'required|date|max:255',
            'outlet_id' => 'required|integer|max:20',
            'customer_id' => 'required|integer|max:20',
            'customer_name' => 'required|string|max:255',
            'customer_phone' => 'required|string|max:255',
            'item_name' => 'nullable|string|max:255',
            'complaint' => 'nullable|string|max:255',
            'equipment' => 'nullable|string|max:255',
            'information' => 'nullable|string|max:255',
        ]);

        $number = 'SC'.Carbon::parse($request['date'])->isoFormat('YYYY').Carbon::parse($request['date'])->isoFormat('MM');
        $check = Service::select(DB::raw('SUBSTRING(number,9,5) as id'))
                       ->whereYear('date', Carbon::parse($request['date'])->isoFormat('YYYY'))
                       ->whereMonth('date', Carbon::parse($request['date'])->isoFormat('MM'))
                       ->orderBy('number', 'DESC')
                       ->first();

        $seq = '';
        if ($check) {
            $urut = intval($check->id) + 1;
            $seq = sprintf("%05d",$urut);
        } else {
            $seq = sprintf("%05d",1);
        }

        $number .= $seq;

        $outletId = $request['outlet_id'];
        if ($outletId == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
                    ->item($user, new UserTransformer())
                    ->serializeWith(new ArraySerializer())
                    ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outletId = $outletUser['outlet']['id'];
                    break;
                }
            }
        }

        $service = Service::create([
            'date' => Carbon::parse($request['date'])->isoFormat('YYYY-MM-DD'),
            'date_estimation' => Carbon::parse($request['date_estimation'])->isoFormat('YYYY-MM-DD'),
            'number' => $number,
            'outlet_id' => $outletId,
            'customer_id' => $request['customer_id'],
            'customer_name' => $request['customer_name'],
            'customer_phone' => $request['customer_phone'],
            'item_name' => $request['item_name'],
            'complaint' => $request['complaint'],
            'equipment' => $request['equipment'],
            'information' => $request['information'],
            'user_id' => $user->id,
            'status' => 1,
        ]);

        return $service;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        return fractal()
               ->item($service, new ServiceTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        $user = Auth::user();

        $this->validate($request, [
            'technician_id' => 'required|integer|max:20',
        ]);

        $service->technician_id = $request->technician_id;
        $service->status = 2;
        $service->save();

        return $service;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        $service->delete();

        $sale = Sale::where('service_id', $service->id)->first();
        if ($sale) {
            $sale->delete();

            if ($sale->payment == 2) {
                $debtNominal = 0;
                $debt = CustomerDebt::where('customer_id', $sale->customer_id)->first();
                if ($debt) {
                    $debtNominal += $debt->nominal;
                    $debt = CustomerDebt::where('customer_id', $sale->customer_id)->delete();
                }
                CustomerDebt::create([
                    'customer_id' => $sale->customer_id,
                    'nominal' => $debtNominal - $sale->total - $sale->down_payment,
                ]);
            }

            $details = SaleDetail::where('sale_id', $sale->id)->get();
            if ($details) {
                foreach ($details as $detail) {

                    $stock = Stock::where('outlet_id', $sale->outlet_id)
                                    ->where('item_id', $detail->item_id)
                                    ->first();

                    $qtyOld = 0;
                    $qtyNew = 0;

                    if ($stock) {
                        $qtyOld = $stock->quantity;

                        $stock = Stock::where('outlet_id', $sale->outlet_id)
                                        ->where('item_id', $detail->item_id)
                                        ->delete();

                        $stock = Stock::create([
                            'outlet_id' => $sale->outlet_id,
                            'item_id' => $detail->item_id,
                            'quantity' => $qtyOld - $detail->quantity,
                        ]);

                        $stockCard = StockCard::create([
                            'date' => date('Y-m-d'),
                            'outlet_id' => $sale->outlet_id,
                            'item_id' => $detail->item_id,
                            'beginning' => $qtyOld,
                            'in' => 0,
                            'out' => $detail->quantity,
                            'ending' => $qtyOld - $detail->quantity,
                            'information' => 'Hapus Penjualan '.$sale->number,
                        ]);
                    }

                }
            }
        }

        return ['message' => 'Data Deleted!'];
    }

    public function autocomplete(Request $request)
    {
        $query = Service::where('number', 'like', '%' . $request->input('search') . '%')
                        ->where('status', 2)
                        ->orderBy('number', 'ASC')
                        ->paginate(15);

        return fractal()
             ->collection($query, new ServiceTransformer())
             ->serializeWith(new ArraySerializer())
             ->toArray();
    }

    public function print(Request $request, $id)
    {
        $service = Service::find($id);
        if (!$service) {
            return response()->json([
              'code' => 404,
            ], 404);
        }

        $data = fractal()
               ->item($service, new ServiceTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();

        // return view("pdf.service")->with($data);
        $pdf = PDF::loadView('pdf.service', $data);
        $pdf->setPaper(array(0,0,684,792), 'landscape');

        return $pdf->stream($data['number'].'.pdf');
    }

    public function reportCount(Request $request)
    {
        $user = Auth::user();

        $outletId = $request->outlet;
        if ($outletId == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
                    ->item($user, new UserTransformer())
                    ->serializeWith(new ArraySerializer())
                    ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outletId = $outletUser['outlet']['id'];
                    break;
                }
            }
        }
        $firstDate = Carbon::parse($request->firstDate);
        $lastDate = Carbon::parse($request->lastDate);

        $service_progress = 0;
        $service_done = 0;
        $service_close = 0;

        // $totalServiceQuery = Service::where('outlet_id', $outletId)->get();

        // if ($totalServiceQuery) {
        //   foreach ($totalServiceQuery as $service) {
        //     if ($service->status == 1) {
        //       $service_progress++;
        //     } else if ($service->status == 0) {
        //       $service_done++;
        //     }
        //   }
        // }

        $serviceQuery = Service::whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                                 ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'))
                                 ->where('outlet_id', $outletId)
                                 ->get();

        if ($serviceQuery) {
            foreach ($serviceQuery as $service) {
                if ($service->status == 1) {
                    $service_progress++;
                } else if ($service->status == 2) {
                    $service_done++;
                } else if ($service->status == 0) {
                    $service_close++;
                }
            }
        }
        $response['service_progress'] = $service_progress;
        $response['service_done'] = $service_done;
        $response['service_close'] = $service_close;

        $service_nominal = 0;
        $saleQuery = Sale::whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                           ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'))
                           ->where('outlet_id', $outletId)
                           ->get();

        if ($saleQuery) {
            foreach ($saleQuery as $sale) {
                if ($sale->type == 2) {
                    $service_nominal += $sale->fee;
                }
            }
        }
        $response['service_nominal'] = $service_nominal;

        $chartCategories = array();
        $chartService = array();
        $chartServiceDone = array();
        $chartServiceClose = array();

        if ($firstDate->diffInDays($lastDate) <= 30) {

            $startDate = Carbon::parse($request->firstDate);
            $endDate = Carbon::parse($request->firstDate);
            $endDate->addDays(6);

            do {
                $strDate = $startDate->isoFormat('DD') . '-' . $endDate->isoFormat('DD/MM/YY');
                array_push($chartCategories, $strDate);

                $chartServiceCount = Service::whereDate('date', '>=', $startDate->isoFormat('YYYY-MM-DD'))
                                              ->whereDate('date', '<=', $endDate->isoFormat('YYYY-MM-DD'))
                                              ->where('outlet_id', $outletId)
                                              ->count();
                array_push($chartService, $chartServiceCount);

                $chartServiceDoneCount = Service::whereDate('updated_at', '>=', $startDate->isoFormat('YYYY-MM-DD'))
                                                ->whereDate('updated_at', '<=', $endDate->isoFormat('YYYY-MM-DD'))
                                                ->where('outlet_id', $outletId)
                                                ->where('status', 2)
                                                ->count();
                array_push($chartServiceDone, $chartServiceDoneCount);

                $chartServiceCloseCount = Service::whereDate('updated_at', '>=', $startDate->isoFormat('YYYY-MM-DD'))
                                                ->whereDate('updated_at', '<=', $endDate->isoFormat('YYYY-MM-DD'))
                                                ->where('outlet_id', $outletId)
                                                ->where('status', 0)
                                                ->count();
                array_push($chartServiceClose, $chartServiceCloseCount);

                $startDate = Carbon::parse($endDate)->addDays(1);
                $endDate = Carbon::parse($endDate);
                $endDate->addDays(6);

            } while ($endDate->lessThanOrEqualTo($lastDate));

            if ($startDate->lessThan($lastDate) && $endDate->greaterThan($lastDate)) {
                $strDate = $startDate->isoFormat('DD') . '-' . $lastDate->isoFormat('DD/MM/YY');
                array_push($chartCategories, $strDate);

                $chartServiceCount = Service::whereDate('date', '>=', $startDate->isoFormat('YYYY-MM-DD'))
                                              ->whereDate('date', '<=', $endDate->isoFormat('YYYY-MM-DD'))
                                              ->where('outlet_id', $outletId)
                                              ->count();
                array_push($chartService, $chartServiceCount);

                $chartServiceDoneCount = Service::whereDate('updated_at', '>=', $startDate->isoFormat('YYYY-MM-DD'))
                                                ->whereDate('updated_at', '<=', $endDate->isoFormat('YYYY-MM-DD'))
                                                ->where('outlet_id', $outletId)
                                                ->where('status', 2)
                                                ->count();
                array_push($chartServiceDone, $chartServiceDoneCount);

                $chartServiceCloseCount = Service::whereDate('updated_at', '>=', $startDate->isoFormat('YYYY-MM-DD'))
                                                ->whereDate('updated_at', '<=', $endDate->isoFormat('YYYY-MM-DD'))
                                                ->where('outlet_id', $outletId)
                                                ->where('status', 0)
                                                ->count();
                array_push($chartServiceClose, $chartServiceCloseCount);
            }

        } else {
            $startDate = Carbon::parse($request->firstDate);

            do {
                $strDate = $startDate->isoFormat('MMM');
                array_push($chartCategories, $strDate);

                $chartServiceCount = Service::whereMonth('date', $startDate->isoFormat('MM'))
                                              ->whereYear('date', $startDate->isoFormat('YYYY'))
                                              ->where('outlet_id', $outletId)
                                              ->count();
                array_push($chartService, $chartServiceCount);

                $chartServiceDoneCount = Service::whereMonth('date', $startDate->isoFormat('MM'))
                                              ->whereYear('date', $startDate->isoFormat('YYYY'))
                                              ->where('outlet_id', $outletId)
                                              ->where('status', 2)
                                              ->count();
                array_push($chartServiceDone, $chartServiceDoneCount);

                $chartServiceCloseCount = Service::whereMonth('date', $startDate->isoFormat('MM'))
                                              ->whereYear('date', $startDate->isoFormat('YYYY'))
                                              ->where('outlet_id', $outletId)
                                              ->where('status', 0)
                                              ->count();
                array_push($chartServiceClose, $chartServiceCloseCount);

                $startDate->addMonths();

            } while ($startDate->lessThanOrEqualTo($lastDate));
        }
        $response['chart_categories'] = $chartCategories;
        $response['chart_service'] = $chartService;
        $response['chart_service_done'] = $chartServiceDone;
        $response['chart_service_close'] = $chartServiceClose;

        return $response;
    }

    public function technician(Request $request)
    {
        $user = Auth::user();

        $outlet = $request->input('outlet');
        if ($outlet == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
                    ->item($user, new UserTransformer())
                    ->serializeWith(new ArraySerializer())
                    ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outlet = $outletUser['outlet']['id'];
                    break;
                }
            }
        }

        $columns = ['', 'name', 'phone', 'address'];

        if ($request->firstDate && $request->lastDate) {
            $firstDate = Carbon::parse($request->firstDate);
            $lastDate = Carbon::parse($request->lastDate);
        }

        $length = $request->input('length');
        $column = $request->input('column');
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        $query = Technician::select(DB::raw('technicians.*, COUNT(services.id) AS count'))
                             ->orderBy($columns[$column], $dir)
                             ->groupBy('technicians.id');

        if ($request->firstDate && $request->lastDate) {
            $query->leftJoin('services', function ($join) use ($firstDate, $lastDate) {
               $join->on('technicians.id', 'services.technician_id')
                    // ->whereNotNull('services.service_id')
                    ->whereDate('services.date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                    ->whereDate('services.date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
            });
        } else {
            $query->leftJoin('services', function ($join) {
               $join->on('technicians.id', 'services.technician_id');
                    // ->whereNotNull('services.service_id');
            });
        }

        if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
                $query->where('name', 'like', '%' . $searchValue . '%')
                      ->orWhere('phone', 'like', '%' . $searchValue . '%')
                      ->orWhere('address', 'like', '%' . $searchValue . '%');
            });
        }

        $data = $query->paginate($length);
        return ['data' => $data, 'draw' => $request->input('draw')];
    }

    public function excel(Request $request)
    {
        $user = Auth::user();

        $outlet = $request->input('outlet');
        if ($outlet == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
            ->item($user, new UserTransformer())
            ->serializeWith(new ArraySerializer())
            ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outlet = $outletUser['outlet']['id'];
                    break;
                }
            }
        }

        if ($request->firstDate && $request->lastDate) {
            $firstDate = Carbon::parse($request->firstDate);
            $lastDate = Carbon::parse($request->lastDate);
        }

        $query = Service::with(['outlet', 'customer', 'technician'])
                          ->where('outlet_id', $outlet)
                          ->orderBy('date', 'ASC');

        if ($request->firstDate && $request->lastDate) {
            $query->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                  ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        $data = $query->paginate();

        $spreadsheet = new Spreadsheet();
        $styleArray1 = array(
            'font'  => array(
                'bold'  => true
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
        );
        $styleArray2 = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );
        $styleArray3 = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
        );
        $styleArray4 = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            ),
        );
        $styleArray5 = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            ),
        );


        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1',env('APP_NAME'))
            ->setCellValue('A2','Laporan Servis');
        //STYLING TITLE
        $spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray1);
        $spreadsheet->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray1);
        $spreadsheet->getActiveSheet()->mergeCells('A1:K1');
        $spreadsheet->getActiveSheet()->mergeCells('A2:K2');

        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A4','NOMOR')
            ->setCellValue('B4','TANGGAL')
            ->setCellValue('C4','ESTIMASI SELESAI')
            ->setCellValue('D4','PELANGGAN')
            ->setCellValue('E4','NO. TELEPON')
            ->setCellValue('F4','NAMA BARANG')
            ->setCellValue('G4','KELUHAN')
            ->setCellValue('H4','KELENGKAPAN')
            ->setCellValue('I4','KETERANGAN')
            ->setCellValue('J4','STATUS')
            ->setCellValue('K4','TEKNISI');
        //STYLING TITLE
        $spreadsheet->getActiveSheet()->getStyle('A4:K4')->applyFromArray($styleArray1);
        $spreadsheet->getActiveSheet()->getStyle('A4:K4')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

        if ($data) {
          $idx = 5; $no = 1;
            foreach ($data as $value) {
                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A'.$idx,$value['number'])
                    ->setCellValue('B'.$idx,date('d/m/Y', strtotime($value['date'])))
                    ->setCellValue('C'.$idx,date('d/m/Y', strtotime($value['date_estimation'])))
                    ->setCellValue('D'.$idx,$value['customer_name'])
                    ->setCellValue('E'.$idx,$value['customer_phone'])
                    ->setCellValue('F'.$idx,$value['item_name'])
                    ->setCellValue('G'.$idx,$value['complaint'])
                    ->setCellValue('H'.$idx,$value['equipment'])
                    ->setCellValue('I'.$idx,$value['information'])
                    ->setCellValue('J'.$idx,$value['status'] == 1 ? 'Belum Selesai' : ($value['status'] == 2 ? 'Selesai Belum Diambil' : 'Sudah Diambil'))
                    ->setCellValue('K'.$idx,$value['technician']['name']);
                $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':K'.$idx)->applyFromArray($styleArray2);
                $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':K'.$idx)->applyFromArray($styleArray5);
                $no++;
                $idx = $idx+1;
            }
        }

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="test.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
    }

    public function technicianExcel(Request $request)
    {
        $user = Auth::user();

        $outlet = $request->input('outlet');
        if ($outlet == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
            ->item($user, new UserTransformer())
            ->serializeWith(new ArraySerializer())
            ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outlet = $outletUser['outlet']['id'];
                    break;
                }
            }
        }

        if ($request->firstDate && $request->lastDate) {
            $firstDate = Carbon::parse($request->firstDate);
            $lastDate = Carbon::parse($request->lastDate);
        }

        $query = Technician::select(DB::raw('technicians.*, COUNT(services.id) AS count'))
                             ->orderBy('name', 'ASC')
                             ->groupBy('technicians.id');

        if ($request->firstDate && $request->lastDate) {
            $query->leftJoin('services', function ($join) use ($firstDate, $lastDate) {
               $join->on('technicians.id', 'services.technician_id')
                    // ->whereNotNull('services.service_id')
                    ->whereDate('services.date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                    ->whereDate('services.date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
            });
        } else {
            $query->leftJoin('services', function ($join) {
               $join->on('technicians.id', 'services.technician_id');
                    // ->whereNotNull('services.service_id');
            });
        }

        $data = $query->paginate();

        $spreadsheet = new Spreadsheet();
        $styleArray1 = array(
            'font'  => array(
                'bold'  => true
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
        );
        $styleArray2 = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );
        $styleArray3 = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
        );
        $styleArray4 = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            ),
        );
        $styleArray5 = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            ),
        );


        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1',env('APP_NAME'))
            ->setCellValue('A2','Laporan Teknisi Servis');
        //STYLING TITLE
        $spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray1);
        $spreadsheet->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray1);
        $spreadsheet->getActiveSheet()->mergeCells('A1:D1');
        $spreadsheet->getActiveSheet()->mergeCells('A2:D2');

        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A4','NAMA')
            ->setCellValue('B4','NO. TELEPON')
            ->setCellValue('C4','ALAMAT')
            ->setCellValue('D4','JUMLAH SERVIS');
        //STYLING TITLE
        $spreadsheet->getActiveSheet()->getStyle('A4:D4')->applyFromArray($styleArray1);
        $spreadsheet->getActiveSheet()->getStyle('A4:D4')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

        if ($data) {
          $idx = 5; $no = 1;
            foreach ($data as $value) {
                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A'.$idx,$value['name'])
                    ->setCellValue('B'.$idx,$value['phone'])
                    ->setCellValue('C'.$idx,$value['address'])
                    ->setCellValue('D'.$idx,$value['count']);
                $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':D'.$idx)->applyFromArray($styleArray2);
                $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':D'.$idx)->applyFromArray($styleArray5);
                $spreadsheet->getActiveSheet()->getStyle('D'.$idx)->applyFromArray($styleArray4);
                $no++;
                $idx = $idx+1;
            }
        }

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="test.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
    }
}
