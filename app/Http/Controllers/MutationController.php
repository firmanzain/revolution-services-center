<?php

namespace App\Http\Controllers;

use App\Models\Mutation, App\Models\MutationDetail;
use Illuminate\Http\Request;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\MutationTransformer;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Models\Stock, App\Models\StockCard;
use App\User;
use App\Transformers\UserTransformer;

class MutationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();

        $outlet = $request->input('outlet');
        if ($outlet == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
                    ->item($user, new UserTransformer())
                    ->serializeWith(new ArraySerializer())
                    ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outlet = $outletUser['outlet']['id'];
                    break;
                }
            }
        }

        $columns = ['', 'date', 'number', 'status'];

        $length = $request->input('length');
        $column = $request->input('column');
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        $query = Mutation::with(['outletorigin', 'outletdestination', 'user', 'detail'])
                           ->where('outlet_origin_id', $outlet)
                           ->orderBy($columns[$column], $dir);

        if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
                $query->where('date', 'like', '%' . $searchValue . '%')
                      ->orWhere('number', 'like', '%' . $searchValue . '%')
                      ->orWhere('status', 'like', '%' . $searchValue . '%');
            });
        }

        $data = $query->paginate($length);
        return ['data' => $data, 'draw' => $request->input('draw')];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'date' => 'required|date|max:255',
            'outlet_origin_id' => 'required|integer|max:20',
            'outlet_destination_id' => 'required|integer|max:20',
            'information' => 'nullable|string|max:255',
        ]);

        $number = 'MT'.Carbon::parse($request['date'])->isoFormat('YYYY').Carbon::parse($request['date'])->isoFormat('MM');
        $check = Mutation::select(DB::raw('SUBSTRING(number,9,5) as id'))
                             ->whereYear('date', Carbon::parse($request['date'])->isoFormat('YYYY'))
                             ->whereMonth('date', Carbon::parse($request['date'])->isoFormat('MM'))
                             ->orderBy('number', 'DESC')
                             ->first();
        $seq = '';
        if ($check) {
            $urut = intval($check->id) + 1;
            $seq = sprintf("%05d",$urut);
        } else {
            $seq = sprintf("%05d",1);
        }

        $number .= $seq;

        $outletId = $request['outlet_origin_id'];
        if ($outletId == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
                    ->item($user, new UserTransformer())
                    ->serializeWith(new ArraySerializer())
                    ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outletId = $outletUser['outlet']['id'];
                    break;
                }
            }
        }

        $mutation = Mutation::create([
            'date' => Carbon::parse($request['date'])->isoFormat('YYYY-MM-DD'),
            'number' => $number,
            'outlet_origin_id' => $outletId,
            'outlet_destination_id' => $request['outlet_destination_id'],
            'information' => $request['information'],
            'user_id' => $user->id,
            'status' => 0,
        ]);

        foreach ($request['details'] as $detail) {
            $qty_old = intval(str_replace(',', '', $detail['quantity_old']));
            $qty = intval(str_replace(',', '', $detail['quantity']));

            $mutationDetail = MutationDetail::create([
                'mutation_id' => $mutation['id'],
                'item_id' => $detail['item_id'],
                'quantity_old' => $qty_old,
                'quantity' => $qty,
            ]);

            $stockOrigin = Stock::where('outlet_id', $mutation['outlet_origin_id'])
                                  ->where('item_id', $detail['item_id'])
                                  ->first();

            $qtyOriginOld = 0;
            $qtyOriginNew = 0;

            if ($stockOrigin) {
                $qtyOriginOld = $stockOrigin->quantity;

                $stockOrigin = Stock::where('outlet_id', $mutation['outlet_origin_id'])
                                      ->where('item_id', $detail['item_id'])
                                      ->delete();

                $stockOrigin = Stock::create([
                    'outlet_id' => $mutation['outlet_origin_id'],
                    'item_id' => $detail['item_id'],
                    'quantity' => $qtyOriginOld - $qty,
                ]);

                $stockOriginCard = StockCard::create([
                    'date' => $mutation['date'],
                    'outlet_id' => $mutation['outlet_origin_id'],
                    'item_id' => $detail['item_id'],
                    'beginning' => $qtyOriginOld,
                    'in' => 0,
                    'out' => $qty,
                    'ending' => $qtyOriginOld - $qty,
                    'information' => 'Mutasi '.$mutation['number'],
                ]);

                $stockDestination = Stock::where('outlet_id', $mutation['outlet_destination_id'])
                                           ->where('item_id', $detail['item_id'])
                                           ->first();

                $qtyDestinationOld = 0;
                $qtyDestinationNew = 0;

                if ($stockDestination) {
                    $qtyDestinationOld = $stockDestination->quantity;

                    $stockDestination = Stock::where('outlet_id', $mutation['outlet_destination_id'])
                                               ->where('item_id', $detail['item_id'])
                                               ->delete();

                    $stockDestination = Stock::create([
                        'outlet_id' => $mutation['outlet_destination_id'],
                        'item_id' => $detail['item_id'],
                        'quantity' => $qtyDestinationOld + $qty,
                    ]);
                } else {
                    $stockDestination = Stock::create([
                        'outlet_id' => $mutation['outlet_destination_id'],
                        'item_id' => $detail['item_id'],
                        'quantity' => $qty,
                    ]);
                }

                $stockDestinationCard = StockCard::create([
                    'date' => $mutation['date'],
                    'outlet_id' => $mutation['outlet_destination_id'],
                    'item_id' => $detail['item_id'],
                    'beginning' => $qtyDestinationOld,
                    'in' => $qty,
                    'out' => 0,
                    'ending' => $qtyDestinationOld + $qty,
                    'information' => 'Mutasi '.$mutation['number'],
                ]);
            }

        }

        return $mutation;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Mutation  $mutation
     * @return \Illuminate\Http\Response
     */
    public function show(Mutation $mutation)
    {
        return fractal()
               ->item($mutation, new MutationTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Mutation  $mutation
     * @return \Illuminate\Http\Response
     */
    public function edit(Mutation $mutation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Mutation  $mutation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mutation $mutation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Mutation  $mutation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mutation $mutation)
    {
        $mutation->delete();
        $details = MutationDetail::where('mutatation_id', $mutation->id)->get();
        if ($details) {
            foreach ($details as $detail) {

                $stockOrigin = Stock::where('outlet_id', $mutation->outlet_origin_id)
                                      ->where('item_id', $detail->item_id)
                                      ->first();

                $qtyOriginOld = 0;
                $qtyOriginNew = 0;

                if ($stockOrigin) {
                    $qtyOriginOld = $stockOrigin->quantity;

                    $stockOrigin = Stock::where('outlet_id', $mutation->outlet_origin_id)
                                          ->where('item_id', $detail->item_id)
                                          ->delete();

                    $stockOrigin = Stock::create([
                        'outlet_id' => $mutation->outlet_origin_id,
                        'item_id' => $detail->item_id,
                        'quantity' => $qtyOriginOld + $detail->quantity,
                    ]);

                    $stockOriginCard = StockCard::create([
                        'date' => date('Y-m-d'),
                        'outlet_id' => $mutation->outlet_origin_id,
                        'item_id' => $detail->item_id,
                        'beginning' => $qtyOriginOld,
                        'in' => $detail->quantity,
                        'out' => 0,
                        'ending' => $qtyOriginOld+ $detail->quantity,
                        'information' => 'Hapus Mutasi '.$mutation->number,
                    ]);
                }

                $stockDestination = Stock::where('outlet_id', $mutation->outlet_destination_id)
                                           ->where('item_id', $detail->item_id)
                                           ->first();

                $qtyDestinationOld = 0;
                $qtyDestinationNew = 0;

                if ($stockDestination) {
                    $qtyDestinationOld = $stockDestination->quantity;

                    $stockDestination = Stock::where('outlet_id', $mutation->outlet_destination_id)
                                               ->where('item_id', $detail->item_id)
                                               ->delete();

                    $stockDestination = Stock::create([
                        'outlet_id' => $mutation->outlet_destination_id,
                        'item_id' => $detail->item_id,
                        'quantity' => $qtyDestinationOld - $detail->quantity,
                    ]);

                    $stockDestinationCard = StockCard::create([
                        'date' => date('Y-m-d'),
                        'outlet_id' => $mutation->outlet_destination_id,
                        'item_id' => $detail->item_id,
                        'beginning' => $qtyDestinationOld,
                        'in' => 0,
                        'out' => $detail->quantity,
                        'ending' => $qtyDestinationOld - $detail->quantity,
                        'information' => 'Hapus Mutasi '.$mutation->number,
                    ]);
                }

            }
        }
        return ['message' => 'Data Deleted!'];
    }
}
