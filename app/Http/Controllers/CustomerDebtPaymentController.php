<?php

namespace App\Http\Controllers;

use App\Models\CustomerDebtPayment;
use Illuminate\Http\Request;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\CustomerDebtPaymentTransformer;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Models\CustomerDebt, App\Models\Sale, App\Models\Customer;
use App\User;
use App\Transformers\UserTransformer;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class CustomerDebtPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $columns = ['', 'date', 'number', 'customer_id', 'debt_nominal_payment', 'status'];

        $length = $request->input('length');
        $column = $request->input('column');
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        $query = CustomerDebtPayment::with(['customer', 'user'])
                                      ->orderBy($columns[$column], $dir);

        if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
                $query->where('date', 'like', '%' . $searchValue . '%')
                      ->orWhere('number', 'like', '%' . $searchValue . '%')
                      ->orWhere('debt_nominal_payment', 'like', '%' . $searchValue . '%')
                      ->orWhere('status', 'like', '%' . $searchValue . '%');
            });
        }

        $data = $query->paginate($length);
        return ['data' => $data, 'draw' => $request->input('draw')];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'date' => 'required|date|max:255',
            'customer_id' => 'required|integer|max:20',
            'debt_nominal' => 'required|string|max:20',
            'debt_nominal_payment' => 'required|string|max:20',
            'information' => 'nullable|string|max:255',
        ]);

        $number = 'AR'.Carbon::parse($request['date'])->isoFormat('YYYY').Carbon::parse($request['date'])->isoFormat('MM');
        $check = CustomerDebtPayment::select(DB::raw('SUBSTRING(number,9,5) as id'))
                                      ->whereYear('date', Carbon::parse($request['date'])->isoFormat('YYYY'))
                                      ->whereMonth('date', Carbon::parse($request['date'])->isoFormat('MM'))
                                      ->orderBy('number', 'DESC')
                                      ->first();

        $seq = '';
        if ($check) {
            $urut = intval($check->id) + 1;
            $seq = sprintf("%05d",$urut);
        } else {
            $seq = sprintf("%05d",1);
        }

        $number .= $seq;

        $payment = CustomerDebtPayment::create([
            'date' => Carbon::parse($request['date'])->isoFormat('YYYY-MM-DD'),
            'number' => $number,
            'customer_id' => $request['customer_id'],
            'debt_nominal' => intval(str_replace(',', '', $request['debt_nominal'])),
            'debt_nominal_payment' => intval(str_replace(',', '', $request['debt_nominal_payment'])),
            'information' => $request['information'],
            'user_id' => $user->id,
            'status' => 0,
        ]);

        $debtNominal = $payment['debt_nominal'] - $payment['debt_nominal_payment'];
        $debt = CustomerDebt::where('customer_id', $payment['customer_id'])->delete();
        CustomerDebt::create([
            'customer_id' => $payment['customer_id'],
            'nominal' => $debtNominal,
        ]);

        return $debt;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CustomerDebtPayment  $customerDebtPayment
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $customerDebtPayment = CustomerDebtPayment::where('id', $id)->first();
        return fractal()
               ->item($customerDebtPayment, new CustomerDebtPaymentTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CustomerDebtPayment  $customerDebtPayment
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerDebtPayment $customerDebtPayment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CustomerDebtPayment  $customerDebtPayment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomerDebtPayment $customerDebtPayment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CustomerDebtPayment  $customerDebtPayment
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomerDebtPayment $customerDebtPayment, $id)
    {
          $payment = CustomerDebtPayment::find($id);
          if (!$payment) {
              return response()->json([
                  'code' => 404,
                  'message' => 'Not found.'
              ], 404);
          }

          $debtNominal = 0;
          $debt = CustomerDebt::where('customer_id', $payment->customer_id)->first();
          if ($debt) {
              $debtNominal = $debt->nominal;
          }

          CustomerDebt::where('customer_id', $payment->customer_id)->delete();
          CustomerDebt::create([
              'customer_id' => $payment->customer_id,
              'nominal' => $debtNominal + $payment->debt_nominal_payment,
          ]);
          $payment->delete();
          return ['message' => 'Data Deleted!'];
    }

    public function reportCount(Request $request)
    {
          $user = Auth::user();

          $outletId = $request->outlet;
          if ($outletId == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
            ->item($user, new UserTransformer())
            ->serializeWith(new ArraySerializer())
            ->toArray();
            foreach ($user['outlet'] as $outletUser) {
              if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                $outletId = $outletUser['outlet']['id'];
                break;
              }
            }
          }
          $firstDate = Carbon::parse($request->firstDate);
          $lastDate = Carbon::parse($request->lastDate);

          $debt_nominal = 0;
          $debt_paid = 0;
          $debt_remaining = 0;

          $debtNominalQuery = Sale::whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
          ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'))
          ->where('payment', 2)
          ->get();

          if ($debtNominalQuery) {
            foreach ($debtNominalQuery as $debt) {
              $debt_nominal += $debt->total;
            }
          }

          $debtPaidQuery = CustomerDebtPayment::whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
          ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'))
          ->get();

          if ($debtPaidQuery) {
            foreach ($debtPaidQuery as $debt) {
              $debt_paid += $debt->debt_nominal_payment;
            }
          }

          $debtRemainingQuery = CustomerDebt::get();

          if ($debtRemainingQuery) {
            foreach ($debtRemainingQuery as $debt) {
              $debt_remaining += $debt->nominal;
            }
          }

          $response['receivable_nominal'] = $debt_nominal;
          $response['receivable_paid'] = $debt_paid;
          $response['receivable_remaining'] = $debt_remaining;

          $chart1 = array();

          $customers = Customer::get();

          foreach ($customers as $customer) {
            $debtNominal = 0;
            $debtQuery = CustomerDebt::where('customer_id', $customer->id)->get();
            if ($debtQuery) {
              foreach ($debtQuery as $debt) {
                $debtNominal += $debt->nominal;
              }
            }

            $customerTemp = array(
              'name' => $customer->name,
              'data' => array(
                $debtNominal
              ),
            );
            array_push($chart1, $customerTemp);
          }
          $response['chart1'] = $chart1;

          return $response;
    }

    public function reportTransaction(Request $request)
    {
        $user = Auth::user();

        $outlet = $request->input('outlet');
        if ($outlet == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
                    ->item($user, new UserTransformer())
                    ->serializeWith(new ArraySerializer())
                    ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outlet = $outletUser['outlet']['id'];
                    break;
                }
            }
        }

        $columns = ['', 'date', 'number', 'customer', 'nominal', 'information'];

        if ($request->firstDate && $request->lastDate) {
            $firstDate = Carbon::parse($request->firstDate);
            $lastDate = Carbon::parse($request->lastDate);
        }

        $length = $request->input('length');
        $column = $request->input('column');
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        $sale = Sale::select(DB::raw('date, sales.number AS number, customers.name AS customer, total AS nominal, "Penjualan Hutang" AS type, information'))
                    ->join('customers', 'customers.id', '=', 'sales.customer_id')
                    ->where('outlet_id', $outlet)->where('payment', 2);
        if ($request->firstDate && $request->lastDate) {
            $sale->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
            ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        if ($searchValue) {
            $sale->where(function($query) use ($searchValue) {
                $query->where('date', 'like', '%' . $searchValue . '%')
                ->orWhere('number', 'like', '%' . $searchValue . '%')
                ->orWhere('customer', 'like', '%' . $searchValue . '%')
                ->orWhere('type', 'like', '%' . $searchValue . '%')
                ->orWhere('nominal', 'like', '%' . $searchValue . '%')
                ->orWhere('information', 'like', '%' . $searchValue . '%');
            });
        }

        $debtPayment = CustomerDebtPayment::select(DB::raw('date, customer_debt_payments.number AS number, customers.name AS customer, debt_nominal_payment AS nominal, "Pembayaran Piutang" AS type, information'))
                       ->join('customers', 'customers.id', '=', 'customer_debt_payments.customer_id')
                       ->union($sale);
        if ($request->firstDate && $request->lastDate) {
            $debtPayment->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
            ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        if ($searchValue) {
            $debtPayment->where(function($query) use ($searchValue) {
                $query->where('date', 'like', '%' . $searchValue . '%')
                ->orWhere('number', 'like', '%' . $searchValue . '%')
                ->orWhere('customer', 'like', '%' . $searchValue . '%')
                ->orWhere('type', 'like', '%' . $searchValue . '%')
                ->orWhere('nominal', 'like', '%' . $searchValue . '%')
                ->orWhere('information', 'like', '%' . $searchValue . '%');
            });
        }

        $query = $debtPayment;
        $query->orderBy($columns[$column], $dir);

        $data = $query->paginate($length);
        return ['data' => $data, 'draw' => $request->input('draw')];
    }

    public function reportTransactionExcel(Request $request)
    {
        $user = Auth::user();

        $outlet = $request->input('outlet');
        if ($outlet == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
            ->item($user, new UserTransformer())
            ->serializeWith(new ArraySerializer())
            ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outlet = $outletUser['outlet']['id'];
                    break;
                }
            }
        }

        if ($request->firstDate && $request->lastDate) {
            $firstDate = Carbon::parse($request->firstDate);
            $lastDate = Carbon::parse($request->lastDate);
        }

        $sale = Sale::select(DB::raw('date, sales.number AS number, customers.name AS customer, total AS nominal, "Penjualan Hutang" AS type, information'))
                    ->join('customers', 'customers.id', '=', 'sales.customer_id')
                    ->where('outlet_id', $outlet)->where('payment', 2);
        if ($request->firstDate && $request->lastDate) {
            $sale->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
            ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        $debtPayment = CustomerDebtPayment::select(DB::raw('date, customer_debt_payments.number AS number, customers.name AS customer, debt_nominal_payment AS nominal, "Pembayaran Piutang" AS type, information'))
                       ->join('customers', 'customers.id', '=', 'customer_debt_payments.customer_id')
                       ->union($sale);
        if ($request->firstDate && $request->lastDate) {
            $debtPayment->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
            ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        $query = $debtPayment;
        $query->orderBy('date', 'ASC');
        $data = $query->paginate();

        $spreadsheet = new Spreadsheet();
        $styleArray1 = array(
            'font'  => array(
                'bold'  => true
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
        );
        $styleArray2 = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );
        $styleArray3 = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
        );
        $styleArray4 = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            ),
        );
        $styleArray5 = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            ),
        );


        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1',env('APP_NAME'))
            ->setCellValue('A2','Laporan Piutang');
        //STYLING TITLE
        $spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray1);
        $spreadsheet->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray1);
        $spreadsheet->getActiveSheet()->mergeCells('A1:F1');
        $spreadsheet->getActiveSheet()->mergeCells('A2:F2');

        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A4','TANGGAL')
            ->setCellValue('B4','NOMOR REFERENSI')
            ->setCellValue('C4','PELANGGAN')
            ->setCellValue('D4','NOMINAL')
            ->setCellValue('E4','TIPE')
            ->setCellValue('F4','KETERANGAN');
        //STYLING TITLE
        $spreadsheet->getActiveSheet()->getStyle('A4:F4')->applyFromArray($styleArray1);
        $spreadsheet->getActiveSheet()->getStyle('A4:F4')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

        if ($data) {
          $idx = 5; $no = 1;
            foreach ($data as $value) {
                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A'.$idx,date('d/m/Y', strtotime($value['date'])))
                    ->setCellValue('B'.$idx,$value['number'])
                    ->setCellValue('C'.$idx,$value['customer'])
                    ->setCellValue('D'.$idx,$value['nominal'])
                    ->setCellValue('E'.$idx,$value['type'])
                    ->setCellValue('F'.$idx,$value['information']);
                $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':F'.$idx)->applyFromArray($styleArray2);
                $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':F'.$idx)->applyFromArray($styleArray5);
                $spreadsheet->getActiveSheet()->getStyle('D'.$idx)->applyFromArray($styleArray4);
                $no++;
                $idx = $idx+1;
            }
        }

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="test.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
    }
}
