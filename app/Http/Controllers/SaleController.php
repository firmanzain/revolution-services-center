<?php

namespace App\Http\Controllers;

use App\Models\Sale, App\Models\SaleDetail;
use Illuminate\Http\Request;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\SaleTransformer;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Models\Stock, App\Models\StockCard;
use App\User;
use App\Transformers\UserTransformer;
use App\Models\CustomerDebt;
use App\Models\Service, App\Models\Item;
use PDF;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();

        $outlet = $request->input('outlet');
        if ($outlet == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
                    ->item($user, new UserTransformer())
                    ->serializeWith(new ArraySerializer())
                    ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outlet = $outletUser['outlet']['id'];
                    break;
                }
            }
        }

        $columns = ['', 'date', 'number', 'customer_id', 'type', 'total', 'payment'];

        if ($request->firstDate && $request->lastDate) {
            $firstDate = Carbon::parse($request->firstDate);
            $lastDate = Carbon::parse($request->lastDate);
        }

        $length = $request->input('length');
        $column = $request->input('column');
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        $query = Sale::with(['outlet', 'customer', 'detail'])
                             ->where('outlet_id', $outlet)
                             ->orderBy($columns[$column], $dir);

        if ($request->firstDate && $request->lastDate) {
            $query->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                  ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
                $query->where('date', 'like', '%' . $searchValue . '%')
                      ->orWhere('number', 'like', '%' . $searchValue . '%')
                      ->orWhere('type', 'like', '%' . $searchValue . '%')
                      ->orWhere('total', 'like', '%' . $searchValue . '%')
                      ->orWhere('payment', 'like', '%' . $searchValue . '%');
            });
            $query->orWhereHas('customer', function($query) use($searchValue) {
                   $query->where('name', 'like', '%' . $searchValue . '%');
            });
        }

        $data = $query->paginate($length);
        return ['data' => $data, 'draw' => $request->input('draw')];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'date' => 'required|date|max:255',
            'outlet_id' => 'required|integer|max:20',
            'customer_id' => 'required|integer|max:20',
            'customer_name' => 'sometimes|string|max:255',
            'customer_phone' => 'nullable|string|max:255',
            'type' => 'required|integer|max:20',
            'service_id' => 'nullable|integer|max:20',
            'technician_id' => 'nullable|integer|max:20',
            'service_item_name' => 'nullable|string|max:255',
            'service_complaint' => 'nullable|string|max:255',
            'service_equipment' => 'nullable|string|max:255',
            'subtotal' => 'required|string|max:20',
            // 'fee' => 'nullable|string|max:20',
            // 'discount' => 'required|string|max:20',
            // 'ppn' => 'required|string|max:20',
            'total' => 'required|string|max:20',
            'payment' => 'required|integer|max:20',
            // 'down_payment' => 'nullable|string|max:20',
            'pay' => 'required|string|max:20',
            'change' => 'required|string|max:20',
            'information' => 'nullable|string|max:255',
        ]);

        $number = 'SO'.Carbon::parse($request['date'])->isoFormat('YYYY').Carbon::parse($request['date'])->isoFormat('MM');
        $check = Sale::select(DB::raw('SUBSTRING(number,9,5) as id'))
                       ->whereYear('date', Carbon::parse($request['date'])->isoFormat('YYYY'))
                       ->whereMonth('date', Carbon::parse($request['date'])->isoFormat('MM'))
                       ->orderBy('number', 'DESC')
                       ->first();

        $seq = '';
        if ($check) {
            $urut = intval($check->id) + 1;
            $seq = sprintf("%05d",$urut);
        } else {
            $seq = sprintf("%05d",1);
        }

        $number .= $seq;

        $outletId = $request['outlet_id'];
        if ($outletId == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
                    ->item($user, new UserTransformer())
                    ->serializeWith(new ArraySerializer())
                    ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outletId = $outletUser['outlet']['id'];
                    break;
                }
            }
        }

        $sale = Sale::create([
            'date' => Carbon::parse($request['date'])->isoFormat('YYYY-MM-DD'),
            'number' => $number,
            'outlet_id' => $outletId,
            'customer_id' => $request['customer_id'],
            'customer_name' => $request['customer_name'],
            'customer_phone' => $request['customer_phone'],
            'type' => $request['type'],
            'technician_id' => $request['technician_id'],
            'service_id' => $request['service_id'],
            'service_item_name' => $request['service_item_name'],
            'service_complaint' => $request['service_complaint'],
            'service_equipment' => $request['service_equipment'],
            'subtotal' => intval(str_replace(',', '', $request['subtotal'])),
            'fee' => intval(str_replace(',', '', $request['fee'])),
            'discount' => intval(str_replace(',', '', $request['discount'])),
            'ppn' => intval(str_replace(',', '', $request['ppn'])),
            'total' => intval(str_replace(',', '', $request['total'])),
            'payment' => intval(str_replace(',', '', $request['payment'])),
            'down_payment' => intval(str_replace(',', '', $request['down_payment'])),
            'pay' => intval(str_replace(',', '', $request['pay'])),
            'change' => intval(str_replace(',', '', $request['change'])),
            'information' => $request['information'],
            'user_id' => $user->id,
            // 'status' => ($request['payment'] == 1) ? 0 : 1,
            'status' => 0,
        ]);

        if ($sale['payment'] == 2) {
            $debtNominal = 0;
            $debt = CustomerDebt::where('customer_id', $sale['customer_id'])->first();
            if ($debt) {
                $debtNominal += $debt->nominal;
                $debt = CustomerDebt::where('customer_id', $sale['customer_id'])->delete();
            }
            CustomerDebt::create([
                'customer_id' => $sale['customer_id'],
                'nominal' => $sale['total'] - $sale['down_payment'],
            ]);
        }

        if ($sale['type'] == 2) {
            $service = Service::where('id', $sale['service_id'])->first();
            if ($service) {
                $service->status = 0;
                $service->save();
            }
        }

        foreach ($request['details'] as $detail) {
            $qtyStock = intval(str_replace(',', '', $detail['quantity_stock']));
            $qty = intval(str_replace(',', '', $detail['quantity']));

            $saleDetail = SaleDetail::create([
                'sale_id' => $sale['id'],
                'item_id' => $detail['item_id'],
                'purchase_price' => intval(str_replace(',', '', $detail['purchase_price'])),
                'selling_price' => intval(str_replace(',', '', $detail['selling_price'])),
                'quantity' => $qty,
                'discount' => intval(str_replace(',', '', $detail['discount_nominal'])),
            ]);

            $stock = Stock::where('outlet_id', $sale['outlet_id'])
                            ->where('item_id', $detail['item_id'])
                            ->first();

            $qtyStock = 0;

            if ($stock) {
                $qtyStock = $stock->quantity;

                $stock = Stock::where('outlet_id', $sale['outlet_id'])
                                ->where('item_id', $detail['item_id'])
                                ->delete();

                $stock = Stock::create([
                    'outlet_id' => $sale['outlet_id'],
                    'item_id' => $detail['item_id'],
                    'quantity' => $qtyStock - $qty,
                ]);
            }

            $statusPenjualan = ($sale['payment'] == 1) ? 'Tunai' : 'Hutang';
            $stockCard = StockCard::create([
                'date' => $sale['date'],
                'outlet_id' => $sale['outlet_id'],
                'item_id' => $detail['item_id'],
                'beginning' => $qtyStock,
                'in' => 0,
                'out' => $qty,
                'ending' => $qtyStock - $qty,
                'information' => 'Penjualan '.$statusPenjualan.' '.$sale['number'],
            ]);
        }

        return $sale;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function show(Sale $sale)
    {
        return fractal()
               ->item($sale, new SaleTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function edit(Sale $sale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sale $sale)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sale $sale)
    {
        $sale->delete();

        if ($sale->type == 2) {
            $service = Service::find($sale->service_id);
            $service->delete();
        }

        if ($sale->payment == 2) {
            $debtNominal = 0;
            $debt = CustomerDebt::where('customer_id', $sale->customer_id)->first();
            if ($debt) {
                $debtNominal += $debt->nominal;
                $debt = CustomerDebt::where('customer_id', $sale->customer_id)->delete();
            }
            CustomerDebt::create([
                'customer_id' => $sale->customer_id,
                'nominal' => $debtNominal - $sale->total - $sale->down_payment,
            ]);
        }

        $details = SaleDetail::where('sale_id', $sale->id)->get();
        if ($details) {
            foreach ($details as $detail) {

                $stock = Stock::where('outlet_id', $sale->outlet_id)
                                ->where('item_id', $detail->item_id)
                                ->first();

                $qtyOld = 0;
                $qtyNew = 0;

                if ($stock) {
                    $qtyOld = $stock->quantity;

                    $stock = Stock::where('outlet_id', $sale->outlet_id)
                                    ->where('item_id', $detail->item_id)
                                    ->delete();

                    $stock = Stock::create([
                        'outlet_id' => $sale->outlet_id,
                        'item_id' => $detail->item_id,
                        'quantity' => $qtyOld + $detail->quantity,
                    ]);

                    $stockCard = StockCard::create([
                        'date' => date('Y-m-d'),
                        'outlet_id' => $sale->outlet_id,
                        'item_id' => $detail->item_id,
                        'beginning' => $qtyOld,
                        'in' => $detail->quantity,
                        'out' => 0,
                        'ending' => $qtyOld + $detail->quantity,
                        'information' => 'Hapus Penjualan '.$sale->number,
                    ]);
                }

            }
        }
        return ['message' => 'Data Deleted!'];
    }

    public function print(Request $request, $type, $id)
    {
        $sale = Sale::find($id);
        if (!$sale) {
            return response()->json([
              'code' => 404,
            ], 404);
        }

        $data = fractal()
               ->item($sale, new SaleTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();

        // return view("pdf.salesSmall")->with($data);
        if ($type == 'large') {
            // $pdf = PDF::loadView('pdf.salesLarge', $data);
            // $pdf->setPaper(array(0,0,600,600), 'landscape');
            return view('pdf.salesLarge', $data);
        } else {
            // $pdf = PDF::loadView('pdf.salesSmall', $data);
            // $pdf->setPaper(array(0,0,175,500));
            return view('pdf.salesSmall', $data);
        }

        return $pdf->stream($data['number'].'.pdf');
    }

    public function reportCount(Request $request)
    {
        $user = Auth::user();

        $outletId = $request->outlet;
        if ($outletId == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
            ->item($user, new UserTransformer())
            ->serializeWith(new ArraySerializer())
            ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outletId = $outletUser['outlet']['id'];
                    break;
                }
            }
        }
        $firstDate = Carbon::parse($request->firstDate);
        $lastDate = Carbon::parse($request->lastDate);

        $sale_count = 0;
        $sale_item = 0;
        $sale_service = 0;
        $sale_nominal = 0;
        $sale_cash = 0;
        $sale_debt = 0;
        $sale_profit = 0;

        $saleQuery = Sale::whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
        ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'))
        ->where('outlet_id', $outletId)
        ->get();

        if ($saleQuery) {
            foreach ($saleQuery as $sale) {
                if ($sale->payment == 1) {
                    $sale_cash += $sale->total;
                } else if ($sale->payment == 2) {
                    $sale_debt += $sale->total;
                }
                if ($sale->type == 1) {
                    $sale_item++;
                } else if ($sale->type == 2) {
                    $sale_service++;
                }
                $sale_count++;
                $sale_nominal += $sale->total;
            }
        }


        $saleDetailQuery = SaleDetail::select('sale_details.*')
        ->join('sales', function ($join) use ($firstDate, $lastDate) {
            $join->on('sales.id', 'sale_details.sale_id')
            ->whereDate('sales.date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
            ->whereDate('sales.date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        })
        ->get();

        if ($saleDetailQuery) {
            foreach ($saleDetailQuery as $detail) {
                $sale_profit += ((($detail->selling_price - $detail->purchase_price) * $detail->quantity) - $detail->discount);
            }
        }

        $response['sale_count'] = $sale_count;
        $response['sale_item'] = $sale_item;
        $response['sale_service'] = $sale_service;
        $response['sale_nominal'] = $sale_nominal;
        $response['sale_cash'] = $sale_cash;
        $response['sale_debt'] = $sale_debt;
        $response['sale_profit'] = $sale_profit;

        $chart1 = array();

        $chart1Query = Item::select(DB::raw('items.*, SUM(sale_details.quantity) AS count'))
        ->leftJoin('sale_details', function ($join) {
            $join->on('items.id', 'sale_details.item_id');
        })
        ->join('sales', function ($join) use ($firstDate, $lastDate) {
            $join->on('sales.id', 'sale_details.sale_id')
            ->whereDate('sales.date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
            ->whereDate('sales.date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        })
        ->orderBy(DB::raw('SUM(sale_details.quantity)'), 'DESC')
        ->groupBy('items.id')
        ->limit(10)
        ->get();

        $totalCount = 0;
        if ($chart1Query) {
            foreach ($chart1Query as $item) {
                $itemTemp = array(
                'name' => $item->name.' '.'('.number_format($item->count,0,'.',',').')',
                'y' => 0,
                'count' => intval($item->count),
                );
                array_push($chart1, $itemTemp);
                $totalCount += $item->count;
            }
        }

        for ($i = 0; $i < sizeof($chart1); $i++) {
            $chart1[$i]['y'] = ($chart1[$i]['count'] / $totalCount) * 100;
            if ($i == 0) {
                $chart1[$i]['sliced'] = true;
                $chart1[$i]['selected'] = true;
            }
        }

        $response['chart1'] = $chart1;

        $chart2Categories = array();
        $chart2Item = array();
        $chart2Service = array();

        if ($firstDate->diffInDays($lastDate) <= 30) {

            $startDate = Carbon::parse($request->firstDate);
            $endDate = Carbon::parse($request->firstDate);
            $endDate->addDays(6);

            do {
                $strDate = $startDate->isoFormat('DD') . '-' . $endDate->isoFormat('DD/MM/YY');
                array_push($chart2Categories, $strDate);

                $chart2ItemCount = Sale::whereDate('date', '>=', $startDate->isoFormat('YYYY-MM-DD'))
                ->whereDate('date', '<=', $endDate->isoFormat('YYYY-MM-DD'))
                ->where('outlet_id', $outletId)
                ->where('type', 1)
                ->count();
                array_push($chart2Item, $chart2ItemCount);

                $chart2ServiceCount = Sale::whereDate('date', '>=', $startDate->isoFormat('YYYY-MM-DD'))
                ->whereDate('date', '<=', $endDate->isoFormat('YYYY-MM-DD'))
                ->where('outlet_id', $outletId)
                ->where('type', 2)
                ->count();
                array_push($chart2Service, $chart2ServiceCount);

                $startDate = Carbon::parse($endDate)->addDays(1);
                $endDate = Carbon::parse($endDate);
                $endDate->addDays(6);

            } while ($endDate->lessThanOrEqualTo($lastDate));

            if ($startDate->lessThan($lastDate) && $endDate->greaterThan($lastDate)) {
                $strDate = $startDate->isoFormat('DD') . '-' . $lastDate->isoFormat('DD/MM/YY');
                array_push($chart2Categories, $strDate);

                $chart2ItemCount = Sale::whereDate('date', '>=', $startDate->isoFormat('YYYY-MM-DD'))
                ->whereDate('date', '<=', $endDate->isoFormat('YYYY-MM-DD'))
                ->where('outlet_id', $outletId)
                ->where('type', 1)
                ->count();
                array_push($chart2Item, $chart2ItemCount);

                $chart2ServiceCount = Sale::whereDate('date', '>=', $startDate->isoFormat('YYYY-MM-DD'))
                ->whereDate('date', '<=', $endDate->isoFormat('YYYY-MM-DD'))
                ->where('outlet_id', $outletId)
                ->where('type', 2)
                ->count();
                array_push($chart2Service, $chart2ServiceCount);
            }

        } else {
            $startDate = Carbon::parse($request->firstDate);

            do {
                $strDate = $startDate->isoFormat('MMM');
                array_push($chart2Categories, $strDate);

                $chart2ItemCount = Sale::whereMonth('date', $startDate->isoFormat('MM'))
                ->whereYear('date', $startDate->isoFormat('YYYY'))
                ->where('outlet_id', $outletId)
                ->where('type', 1)
                ->count();
                array_push($chart2Item, $chart2ItemCount);

                $chart2ServiceCount = Sale::whereMonth('date', $startDate->isoFormat('MM'))
                ->whereYear('date', $startDate->isoFormat('YYYY'))
                ->where('outlet_id', $outletId)
                ->where('type', 2)
                ->count();
                array_push($chart2Service, $chart2ServiceCount);

                $startDate->addMonths();

            } while ($startDate->lessThanOrEqualTo($lastDate));
        }
        $response['chart2_categories'] = $chart2Categories;
        $response['chart2_item'] = $chart2Item;
        $response['chart2_service'] = $chart2Service;

        return $response;
    }

    public function reportExcel(Request $request)
    {
        $user = Auth::user();

        $outlet = $request->input('outlet');
        if ($outlet == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
                    ->item($user, new UserTransformer())
                    ->serializeWith(new ArraySerializer())
                    ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outlet = $outletUser['outlet']['id'];
                    break;
                }
            }
        }

        if ($request->firstDate && $request->lastDate) {
            $firstDate = Carbon::parse($request->firstDate);
            $lastDate = Carbon::parse($request->lastDate);
            $firstDate = $firstDate->isoFormat('YYYY-MM-DD');
            $lastDate = $lastDate->isoFormat('YYYY-MM-DD');
        }

        $query = Sale::where('outlet_id', $outlet)
                       ->orderBy('date', 'ASC');
        if ($request->firstDate && $request->lastDate) {
            $query->whereDate('date', '>=', $firstDate)->whereDate('date', '<=', $lastDate);
        }

        $query = $query->get();

        $data = fractal()
               ->collection($query, new SaleTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();

        $spreadsheet = new Spreadsheet();
        $styleArray1 = array(
            'font'  => array(
                'bold'  => true
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
        );
        $styleArray2 = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );
        $styleArray3 = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
        );
        $styleArray4 = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            ),
        );
        $styleArray5 = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            ),
        );


        $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A1',env('APP_NAME'))
        ->setCellValue('A2','Laporan Penjualan');
        //STYLING TITLE
        $spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray1);
        $spreadsheet->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray1);
        $spreadsheet->getActiveSheet()->mergeCells('A1:K1');
        $spreadsheet->getActiveSheet()->mergeCells('A2:K2');

        if ($data) {
            $idx = 4; $no = 1;
            foreach ($data as $value) {

                if ($value['type'] == 1) {
                  $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A'.$idx,'TANGGAL')
                    ->setCellValue('B'.$idx,'NOMOR REFERENSI')
                    ->setCellValue('C'.$idx,'JENIS PENJUALAN')
                    ->setCellValue('D'.$idx,'PELANGGAN')
                    ->setCellValue('E'.$idx,'TIPE PELANGGAN')
                    ->setCellValue('F'.$idx,'SUB TOTAL')
                    ->setCellValue('G'.$idx,'DISKON')
                    ->setCellValue('H'.$idx,'PPN')
                    ->setCellValue('I'.$idx,'TOTAL')
                    ->setCellValue('J'.$idx,'PEMBAYARAN')
                    ->setCellValue('K'.$idx,'KETERANGAN');
                    //STYLING TITLE
                    $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':K'.$idx.'')->applyFromArray($styleArray1);
                    $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':K'.$idx.'')->applyFromArray($styleArray2);
                    $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                    $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                    $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                    $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                    $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                    $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                    $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
                    $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
                    $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
                    $spreadsheet->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
                    $spreadsheet->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
                } else {
                  $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A'.$idx,'TANGGAL')
                    ->setCellValue('B'.$idx,'NOMOR REFERENSI')
                    ->setCellValue('C'.$idx,'JENIS PENJUALAN')
                    ->setCellValue('D'.$idx,'NOMOR SERVIS')
                    ->setCellValue('E'.$idx,'NAMA BARANG')
                    ->setCellValue('F'.$idx,'KELUHAN')
                    ->setCellValue('G'.$idx,'KELENGKAPAN')
                    ->setCellValue('H'.$idx,'TEKNISI')
                    ->setCellValue('I'.$idx,'PELANGGAN')
                    ->setCellValue('J'.$idx,'TIPE PELANGGAN')
                    ->setCellValue('K'.$idx,'SUB TOTAL')
                    ->setCellValue('L'.$idx,'BIAYA SERVIS')
                    ->setCellValue('M'.$idx,'DISKON')
                    ->setCellValue('N'.$idx,'PPN')
                    ->setCellValue('O'.$idx,'TOTAL')
                    ->setCellValue('P'.$idx,'PEMBAYARAN')
                    ->setCellValue('Q'.$idx,'KETERANGAN');
                  //STYLING TITLE
                  $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':Q'.$idx.'')->applyFromArray($styleArray1);
                  $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':Q'.$idx.'')->applyFromArray($styleArray2);
                  $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                  $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                  $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                  $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                  $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                  $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                  $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
                  $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
                  $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
                  $spreadsheet->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
                  $spreadsheet->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
                  $spreadsheet->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
                  $spreadsheet->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
                  $spreadsheet->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
                  $spreadsheet->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
                  $spreadsheet->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
                  $spreadsheet->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
                }

                $idx = $idx+1;

                if ($value['type'] == 1) {
                  $spreadsheet->setActiveSheetIndex(0)
                  ->setCellValue('A'.$idx,date('d/m/Y', strtotime($value['date'])))
                  ->setCellValue('B'.$idx,$value['number'])
                  ->setCellValue('C'.$idx,($value['type'] == 1) ? 'Barang' : 'Servis')
                  ->setCellValue('D'.$idx,$value['customer']['name'])
                  ->setCellValue('E'.$idx,$value['customer']['customertype']['name'])
                  ->setCellValue('F'.$idx,$value['subtotal'])
                  ->setCellValue('G'.$idx,$value['discount'].'%')
                  ->setCellValue('H'.$idx,$value['ppn'].'%')
                  ->setCellValue('I'.$idx,$value['total'])
                  ->setCellValue('J'.$idx,($value['payment'] == 1) ? 'Tunai' : 'Hutang')
                  ->setCellValue('K'.$idx,$value['information']);
                  $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':K'.$idx)->applyFromArray($styleArray2);
                  $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':K'.$idx)->applyFromArray($styleArray5);
                  $spreadsheet->getActiveSheet()->getStyle('F'.$idx.':I'.$idx)->applyFromArray($styleArray4);
                } else {
                  $spreadsheet->setActiveSheetIndex(0)
                  ->setCellValue('A'.$idx,date('d/m/Y', strtotime($value['date'])))
                  ->setCellValue('B'.$idx,$value['number'])
                  ->setCellValue('C'.$idx,($value['type'] == 1) ? 'Barang' : 'Servis')
                  ->setCellValue('D'.$idx,$value['service']['number'])
                  ->setCellValue('E'.$idx,$value['service_item_name'])
                  ->setCellValue('F'.$idx,$value['service_complaint'])
                  ->setCellValue('G'.$idx,$value['service_equipment'])
                  ->setCellValue('H'.$idx,$value['technician']['name'])
                  ->setCellValue('I'.$idx,$value['customer']['name'])
                  ->setCellValue('J'.$idx,$value['customer']['customertype']['name'])
                  ->setCellValue('K'.$idx,$value['subtotal'])
                  ->setCellValue('L'.$idx,$value['fee'])
                  ->setCellValue('M'.$idx,$value['discount'].'%')
                  ->setCellValue('N'.$idx,$value['ppn'].'%')
                  ->setCellValue('O'.$idx,$value['total'])
                  ->setCellValue('P'.$idx,($value['payment'] == 1) ? 'Tunai' : 'Hutang')
                  ->setCellValue('Q'.$idx,$value['information']);
                  $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':K'.$idx)->applyFromArray($styleArray2);
                  $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':K'.$idx)->applyFromArray($styleArray5);
                  $spreadsheet->getActiveSheet()->getStyle('K'.$idx.':O'.$idx)->applyFromArray($styleArray4);
                }
                $no++;
                $idx = $idx+1;

                if ($value['detail']) {
                  $spreadsheet->setActiveSheetIndex(0)
                  ->setCellValue('C'.$idx,'NO.')
                  ->setCellValue('D'.$idx,'KODE BARANG')
                  ->setCellValue('E'.$idx,'NAMA BARANG')
                  ->setCellValue('F'.$idx,'HARGA')
                  ->setCellValue('G'.$idx,'KUANTITI')
                  ->setCellValue('H'.$idx,'POTONGAN')
                  ->setCellValue('I'.$idx,'TOTAL');
                  //STYLING TITLE
                  $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':I'.$idx.'')->applyFromArray($styleArray1);
                  $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':I'.$idx.'')->applyFromArray($styleArray2);
                  $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                  $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                  $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                  $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                  $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                  $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                  $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
                  $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
                  $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
                  $idx = $idx+1;

                  $noDtl = 1;
                  foreach ($value['detail'] as $detail) {
                      $spreadsheet->setActiveSheetIndex(0)
                      ->setCellValue('C'.$idx,$noDtl)
                      ->setCellValue('D'.$idx,$detail['item']['code'])
                      ->setCellValue('E'.$idx,$detail['item']['name'])
                      ->setCellValue('F'.$idx,$detail['selling_price'])
                      ->setCellValue('G'.$idx,$detail['quantity'])
                      ->setCellValue('H'.$idx,$detail['discount'])
                      ->setCellValue('I'.$idx,($detail['selling_price'] * $detail['quantity'] - $detail['discount']));
                      $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':I'.$idx)->applyFromArray($styleArray2);
                      $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':I'.$idx)->applyFromArray($styleArray5);
                      $spreadsheet->getActiveSheet()->getStyle('F'.$idx.':I'.$idx)->applyFromArray($styleArray4);
                      $spreadsheet->getActiveSheet()->getStyle('C'.$idx)->applyFromArray($styleArray4);
                      $noDtl++;
                      $idx = $idx+1;
                  }
                }
                $idx = $idx+1;
            }
        }

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="test.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
    }
}
