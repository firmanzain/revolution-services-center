<?php

namespace App\Http\Controllers;

use App\Models\Purchase, App\Models\PurchaseDetail;
use Illuminate\Http\Request;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\PurchaseTransformer;
use App\Transformers\PurchaseDetailTransformer;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Models\Stock, App\Models\StockCard;
use App\User;
use App\Transformers\UserTransformer;
use App\Models\SupplierDebt, App\Models\Item;
use PDF;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();

        $outlet = $request->input('outlet');
        if ($outlet == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
                    ->item($user, new UserTransformer())
                    ->serializeWith(new ArraySerializer())
                    ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outlet = $outletUser['outlet']['id'];
                    break;
                }
            }
        }

        $columns = ['', 'date', 'number', 'supplier_id', 'total', 'payment'];

        if ($request->firstDate && $request->lastDate) {
            $firstDate = Carbon::parse($request->firstDate);
            $lastDate = Carbon::parse($request->lastDate);
        }

        $length = $request->input('length');
        $column = $request->input('column');
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        $query = Purchase::with(['outlet', 'supplier', 'detail'])
                             ->where('outlet_id', $outlet)
                             ->orderBy($columns[$column], $dir);

        if ($request->firstDate && $request->lastDate) {
            $query->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                  ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
                $query->where('date', 'like', '%' . $searchValue . '%')
                      ->orWhere('number', 'like', '%' . $searchValue . '%')
                      ->orWhere('total', 'like', '%' . $searchValue . '%')
                      ->orWhere('payment', 'like', '%' . $searchValue . '%');
            });
            $query->orWhereHas('supplier', function($query) use($searchValue) {
                   $query->where('name', 'like', '%' . $searchValue . '%');
            });
        }

        $data = $query->paginate($length);
        return ['data' => $data, 'draw' => $request->input('draw')];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'date' => 'required|date|max:255',
            'outlet_id' => 'required|integer|max:20',
            'supplier_id' => 'required|integer|max:20',
            'subtotal' => 'required|string|max:20',
            'total' => 'required|string|max:20',
            'payment' => 'required|integer|max:20',
            'information' => 'nullable|string|max:255',
        ]);

        $number = 'PO'.Carbon::parse($request['date'])->isoFormat('YYYY').Carbon::parse($request['date'])->isoFormat('MM');
        $check = Purchase::select(DB::raw('SUBSTRING(number,9,5) as id'))
                           ->whereYear('date', Carbon::parse($request['date'])->isoFormat('YYYY'))
                           ->whereMonth('date', Carbon::parse($request['date'])->isoFormat('MM'))
                           ->orderBy('number', 'DESC')
                           ->first();

        $seq = '';
        if ($check) {
            $urut = intval($check->id) + 1;
            $seq = sprintf("%05d",$urut);
        } else {
            $seq = sprintf("%05d",1);
        }

        $number .= $seq;

        $outletId = $request['outlet_id'];
        if ($outletId == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
                    ->item($user, new UserTransformer())
                    ->serializeWith(new ArraySerializer())
                    ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outletId = $outletUser['outlet']['id'];
                    break;
                }
            }
        }

        $purchase = Purchase::create([
            'date' => Carbon::parse($request['date'])->isoFormat('YYYY-MM-DD'),
            'number' => $number,
            'outlet_id' => $outletId,
            'supplier_id' => $request['supplier_id'],
            'subtotal' => intval(str_replace(',', '', $request['subtotal'])),
            'discount' => intval(str_replace(',', '', $request['discount'])),
            'ppn' => intval(str_replace(',', '', $request['ppn'])),
            'total' => intval(str_replace(',', '', $request['total'])),
            'payment' => intval(str_replace(',', '', $request['payment'])),
            'information' => $request['information'],
            'user_id' => $user->id,
            // 'status' => ($request['payment'] == 1) ? 0 : 1,
            'status' => 0,
        ]);

        if ($purchase['payment'] == 2) {
            $debtNominal = 0;
            $debt = SupplierDebt::where('supplier_id', $purchase['supplier_id'])->first();
            if ($debt) {
                $debtNominal += $debt->nominal;
                $debt = SupplierDebt::where('supplier_id', $purchase['supplier_id'])->delete();
            }
            SupplierDebt::create([
                'supplier_id' => $purchase['supplier_id'],
                'nominal' => $purchase['total'],
            ]);
        }

        foreach ($request['details'] as $detail) {
            $qtyStock = intval(str_replace(',', '', $detail['quantity_stock']));
            $qty = intval(str_replace(',', '', $detail['quantity']));

            $purchaseDetail = PurchaseDetail::create([
                'purchase_id' => $purchase['id'],
                'item_id' => $detail['item_id'],
                'price' => intval(str_replace(',', '', $detail['price'])),
                'quantity' => $qty,
                'discount' => intval(str_replace(',', '', $detail['discount_nominal'])),
            ]);

            $stock = Stock::where('outlet_id', $purchase['outlet_id'])
                            ->where('item_id', $detail['item_id'])
                            ->first();

            $qtyStock = 0;

            if ($stock) {
                $qtyStock = $stock->quantity;

                $stock = Stock::where('outlet_id', $purchase['outlet_id'])
                                ->where('item_id', $detail['item_id'])
                                ->delete();

                $stock = Stock::create([
                    'outlet_id' => $purchase['outlet_id'],
                    'item_id' => $detail['item_id'],
                    'quantity' => $qtyStock + $qty,
                ]);
            } else {
                $stock = Stock::create([
                    'outlet_id' => $purchase['outlet_id'],
                    'item_id' => $detail['item_id'],
                    'quantity' => $qty,
                ]);
            }

            $statusPembelian = ($purchase['payment'] == 1) ? 'Tunai' : 'Hutang';
            $stockCard = StockCard::create([
                'date' => $purchase['date'],
                'outlet_id' => $purchase['outlet_id'],
                'item_id' => $detail['item_id'],
                'beginning' => $qtyStock,
                'in' => $qty,
                'out' => 0,
                'ending' => $qtyStock + $qty,
                'information' => 'Pembelian '.$statusPembelian.' '.$purchase['number'],
            ]);
        }

        return $purchase;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function show(Purchase $purchase)
    {
        return fractal()
               ->item($purchase, new PurchaseTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase $purchase)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purchase $purchase)
    {
        $purchase->delete();

        if ($purchase->payment == 2) {
            $debtNominal = 0;
            $debt = SupplierDebt::where('supplier_id', $purchase->supplier_id)->first();
            if ($debt) {
                $debtNominal += $debt->nominal;
                $debt = SupplierDebt::where('supplier_id', $purchase->supplier_id)->delete();
            }
            SupplierDebt::create([
                'supplier_id' => $purchase->supplier_id,
                'nominal' => $debtNominal - $purchase->total,
            ]);
        }

        $details = PurchaseDetail::where('purchase_id', $purchase->id)->get();
        if ($details) {
            foreach ($details as $detail) {

                $stock = Stock::where('outlet_id', $purchase->outlet_id)
                                ->where('item_id', $detail->item_id)
                                ->first();

                $qtyOld = 0;
                $qtyNew = 0;

                if ($stock) {
                    $qtyOld = $stock->quantity;

                    $stock = Stock::where('outlet_id', $purchase->outlet_id)
                                    ->where('item_id', $detail->item_id)
                                    ->delete();

                    $stock = Stock::create([
                        'outlet_id' => $purchase->outlet_id,
                        'item_id' => $detail->item_id,
                        'quantity' => $qtyOld - $detail->quantity,
                    ]);

                    $stockCard = StockCard::create([
                        'date' => date('Y-m-d'),
                        'outlet_id' => $purchase->outlet_id,
                        'item_id' => $detail->item_id,
                        'beginning' => $qtyOld,
                        'in' => 0,
                        'out' => $detail->quantity,
                        'ending' => $qtyOld - $detail->quantity,
                        'information' => 'Hapus Pembelian '.$purchase->number,
                    ]);
                }

            }
        }
        return ['message' => 'Data Deleted!'];
    }

    public function print(Request $request, $type, $id)
    {
        $purchase = Purchase::find($id);
        if (!$purchase) {
            return response()->json([
              'code' => 404,
            ], 404);
        }

        $data = fractal()
               ->item($purchase, new PurchaseTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();

        // return view("pdf.purchasesSmall")->with($data);
        if ($type == 'large') {
            $pdf = PDF::loadView('pdf.purchasesLarge', $data);
            $pdf->setPaper(array(0,0,684,792), 'landscape');
        } else {
            $pdf = PDF::loadView('pdf.purchasesSmall', $data);
            $pdf->setPaper(array(0,0,204,500));
        }

        return $pdf->stream($data['number'].'.pdf');
    }

    public function reportCount(Request $request)
    {
        $user = Auth::user();

        $outletId = $request->outlet;
        if ($outletId == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
                    ->item($user, new UserTransformer())
                    ->serializeWith(new ArraySerializer())
                    ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outletId = $outletUser['outlet']['id'];
                    break;
                }
            }
        }
        $firstDate = Carbon::parse($request->firstDate);
        $lastDate = Carbon::parse($request->lastDate);

        $purchase_count = 0;
        $purchase_nominal = 0;
        $purchase_cash = 0;
        $purchase_debt = 0;

        $purchaseQuery = Purchase::whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                                 ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'))
                                 ->where('outlet_id', $outletId)
                                 ->get();

        if ($purchaseQuery) {
            foreach ($purchaseQuery as $purchase) {
                if ($purchase->payment == 1) {
                    $purchase_cash += $purchase->total;
                } else if ($purchase->payment == 2) {
                    $purchase_debt += $purchase->total;
                }
                $purchase_count++;
                $purchase_nominal += $purchase->total;
            }
        }
        $response['purchase_count'] = $purchase_count;
        $response['purchase_nominal'] = $purchase_nominal;
        $response['purchase_cash'] = $purchase_cash;
        $response['purchase_debt'] = $purchase_debt;

        $chart = array();

        $chartQuery = Item::select(DB::raw('items.*, SUM(purchase_details.quantity) AS count'))
                            ->leftJoin('purchase_details', function ($join) {
                                $join->on('items.id', 'purchase_details.item_id');
                            })
                            ->join('purchases', function ($join) use ($firstDate, $lastDate) {
                                $join->on('purchases.id', 'purchase_details.purchase_id')
                                     ->whereDate('purchases.date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                                     ->whereDate('purchases.date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
                            })
                            ->orderBy(DB::raw('SUM(purchase_details.quantity)'), 'DESC')
                            ->groupBy('items.id')
                            ->limit(10)
                            ->get();

        $totalCount = 0;
        if ($chartQuery) {
            foreach ($chartQuery as $item) {
                $itemTemp = array(
                    'name' => $item->name.' '.'('.number_format($item->count,0,'.',',').')',
                    'y' => 0,
                    'count' => intval($item->count),
                );
                array_push($chart, $itemTemp);
                $totalCount += $item->count;
            }
        }

        for ($i = 0; $i < sizeof($chart); $i++) {
            $chart[$i]['y'] = ($chart[$i]['count'] / $totalCount) * 100;
            if ($i == 0) {
                $chart[$i]['sliced'] = true;
                $chart[$i]['selected'] = true;
            }
        }

        $response['chart'] = $chart;

        return $response;
    }

    public function reportExcel(Request $request)
    {
        $user = Auth::user();

        $outlet = $request->input('outlet');
        if ($outlet == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
                    ->item($user, new UserTransformer())
                    ->serializeWith(new ArraySerializer())
                    ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outlet = $outletUser['outlet']['id'];
                    break;
                }
            }
        }

        if ($request->firstDate && $request->lastDate) {
            $firstDate = Carbon::parse($request->firstDate);
            $lastDate = Carbon::parse($request->lastDate);
            $firstDate = $firstDate->isoFormat('YYYY-MM-DD');
            $lastDate = $lastDate->isoFormat('YYYY-MM-DD');
        }

        $query = Purchase::where('outlet_id', $outlet)
                           ->orderBy('date', 'ASC');
        if ($request->firstDate && $request->lastDate) {
            $query->whereDate('date', '>=', $firstDate)->whereDate('date', '<=', $lastDate);
        }

        $query = $query->get();

        $data = fractal()
               ->collection($query, new PurchaseTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();

        $spreadsheet = new Spreadsheet();
        $styleArray1 = array(
            'font'  => array(
                'bold'  => true
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
        );
        $styleArray2 = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );
        $styleArray3 = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
        );
        $styleArray4 = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            ),
        );
        $styleArray5 = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            ),
        );


        $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A1',env('APP_NAME'))
        ->setCellValue('A2','Laporan Pembelian');
        //STYLING TITLE
        $spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray1);
        $spreadsheet->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray1);
        $spreadsheet->getActiveSheet()->mergeCells('A1:I1');
        $spreadsheet->getActiveSheet()->mergeCells('A2:I2');

        if ($data) {
            $idx = 4; $no = 1;
            foreach ($data as $value) {

                $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A'.$idx,'TANGGAL')
                ->setCellValue('B'.$idx,'NOMOR REFERENSI')
                ->setCellValue('C'.$idx,'PEMASOK')
                ->setCellValue('D'.$idx,'SUB TOTAL')
                ->setCellValue('E'.$idx,'DISKON')
                ->setCellValue('F'.$idx,'PPN')
                ->setCellValue('G'.$idx,'TOTAL')
                ->setCellValue('H'.$idx,'PEMBAYARAN')
                ->setCellValue('I'.$idx,'KETERANGAN');
                //STYLING TITLE
                $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':I'.$idx.'')->applyFromArray($styleArray1);
                $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':I'.$idx.'')->applyFromArray($styleArray2);
                $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
                $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
                $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
                $idx = $idx+1;

                $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A'.$idx,date('d/m/Y', strtotime($value['date'])))
                ->setCellValue('B'.$idx,$value['number'])
                ->setCellValue('C'.$idx,$value['supplier']['name'])
                ->setCellValue('D'.$idx,$value['subtotal'])
                ->setCellValue('E'.$idx,$value['discount'].'%')
                ->setCellValue('F'.$idx,$value['ppn'].'%')
                ->setCellValue('G'.$idx,$value['total'])
                ->setCellValue('H'.$idx,($value['payment'] == 1) ? 'Tunai' : 'Hutang')
                ->setCellValue('I'.$idx,$value['information']);
                $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':I'.$idx)->applyFromArray($styleArray2);
                $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':I'.$idx)->applyFromArray($styleArray5);
                $spreadsheet->getActiveSheet()->getStyle('D'.$idx.':G'.$idx)->applyFromArray($styleArray4);
                $no++;
                $idx = $idx+1;

                $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('C'.$idx,'NO.')
                ->setCellValue('D'.$idx,'KODE BARANG')
                ->setCellValue('E'.$idx,'NAMA BARANG')
                ->setCellValue('F'.$idx,'HARGA')
                ->setCellValue('G'.$idx,'KUANTITI')
                ->setCellValue('H'.$idx,'POTONGAN')
                ->setCellValue('I'.$idx,'TOTAL');
                //STYLING TITLE
                $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':I'.$idx.'')->applyFromArray($styleArray1);
                $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':I'.$idx.'')->applyFromArray($styleArray2);
                $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
                $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
                $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
                $idx = $idx+1;

                $noDtl = 1;
                foreach ($value['detail'] as $detail) {
                    $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('C'.$idx,$noDtl)
                    ->setCellValue('D'.$idx,$detail['item']['code'])
                    ->setCellValue('E'.$idx,$detail['item']['name'])
                    ->setCellValue('F'.$idx,$detail['price'])
                    ->setCellValue('G'.$idx,$detail['quantity'])
                    ->setCellValue('H'.$idx,$detail['discount'].'%')
                    ->setCellValue('I'.$idx,($detail['price'] * $detail['quantity']) - ($detail['price'] * $detail['quantity'] * $detail['discount'] / 100));
                    $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':I'.$idx)->applyFromArray($styleArray2);
                    $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':I'.$idx)->applyFromArray($styleArray5);
                    $spreadsheet->getActiveSheet()->getStyle('F'.$idx.':I'.$idx)->applyFromArray($styleArray4);
                    $spreadsheet->getActiveSheet()->getStyle('C'.$idx)->applyFromArray($styleArray4);
                    $noDtl++;
                    $idx = $idx+1;
                }
                $idx = $idx+1;
            }
        }

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="test.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
    }
}
