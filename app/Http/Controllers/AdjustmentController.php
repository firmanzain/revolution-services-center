<?php

namespace App\Http\Controllers;

use App\Models\Adjustment, App\Models\AdjustmentDetail;
use Illuminate\Http\Request;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\AdjustmentTransformer;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Models\Stock, App\Models\StockCard;
use App\User;
use App\Transformers\UserTransformer;

class AdjustmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();

        $outlet = $request->input('outlet');
        if ($outlet == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
                    ->item($user, new UserTransformer())
                    ->serializeWith(new ArraySerializer())
                    ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outlet = $outletUser['outlet']['id'];
                    break;
                }
            }
        }

        $columns = ['', 'date', 'number', 'status'];

        $length = $request->input('length');
        $column = $request->input('column');
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        $query = Adjustment::with(['outlet', 'user', 'detail'])
                             ->where('outlet_id', $outlet)
                             ->orderBy($columns[$column], $dir);

        if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
                $query->where('date', 'like', '%' . $searchValue . '%')
                      ->orWhere('number', 'like', '%' . $searchValue . '%')
                      ->orWhere('status', 'like', '%' . $searchValue . '%');
            });
        }

        $data = $query->paginate($length);
        return ['data' => $data, 'draw' => $request->input('draw')];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'date' => 'required|date|max:255',
            'outlet_id' => 'required|integer|max:20',
            'information' => 'nullable|string|max:255',
        ]);

        $number = 'AD'.Carbon::parse($request['date'])->isoFormat('YYYY').Carbon::parse($request['date'])->isoFormat('MM');
        $check = Adjustment::select(DB::raw('SUBSTRING(number,9,5) as id'))
                             ->whereYear('date', Carbon::parse($request['date'])->isoFormat('YYYY'))
                             ->whereMonth('date', Carbon::parse($request['date'])->isoFormat('MM'))
                             ->orderBy('number', 'DESC')
                             ->first();

        $seq = '';
        if ($check) {
            $urut = intval($check->id) + 1;
            $seq = sprintf("%05d",$urut);
        } else {
            $seq = sprintf("%05d",1);
        }

        $number .= $seq;

        $outletId = $request['outlet_id'];
        if ($outletId == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
                    ->item($user, new UserTransformer())
                    ->serializeWith(new ArraySerializer())
                    ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outletId = $outletUser['outlet']['id'];
                    break;
                }
            }
        }

        $adjustment = Adjustment::create([
            'date' => Carbon::parse($request['date'])->isoFormat('YYYY-MM-DD'),
            'number' => $number,
            'outlet_id' => $outletId,
            'information' => $request['information'],
            'user_id' => $user->id,
            'status' => 0,
        ]);

        foreach ($request['details'] as $detail) {
            $qty_old = intval(str_replace(',', '', $detail['quantity_old']));
            $qty = intval(str_replace(',', '', $detail['quantity']));

            $adjustmentDetail = AdjustmentDetail::create([
                'adjustment_id' => $adjustment['id'],
                'item_id' => $detail['item_id'],
                'quantity_old' => $qty_old,
                'quantity' => $qty,
            ]);

            $stock = Stock::where('outlet_id', $adjustment['outlet_id'])
                            ->where('item_id', $detail['item_id'])
                            ->first();

            $qtyOld = 0;
            $qtyNew = 0;

            if ($stock) {
                $qtyOld = $stock->quantity;

                $stock = Stock::where('outlet_id', $adjustment['outlet_id'])
                                ->where('item_id', $detail['item_id'])
                                ->delete();

                $stock = Stock::create([
                    'outlet_id' => $adjustment['outlet_id'],
                    'item_id' => $detail['item_id'],
                    'quantity' => $qty,
                ]);
            } else {
                $stock = Stock::create([
                    'outlet_id' => $adjustment['outlet_id'],
                    'item_id' => $detail['item_id'],
                    'quantity' => $qty,
                ]);
            }

            $stockCard = StockCard::create([
                'date' => $adjustment['date'],
                'outlet_id' => $adjustment['outlet_id'],
                'item_id' => $detail['item_id'],
                'beginning' => $qtyOld,
                'in' => $qty,
                'out' => $qtyOld,
                'ending' => $qty,
                'information' => 'Penyesuaian '.$adjustment['number'],
            ]);
        }

        return $adjustment;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Adjustment  $adjustment
     * @return \Illuminate\Http\Response
     */
    public function show(Adjustment $adjustment)
    {
        return fractal()
               ->item($adjustment, new AdjustmentTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Adjustment  $adjustment
     * @return \Illuminate\Http\Response
     */
    public function edit(Adjustment $adjustment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Adjustment  $adjustment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Adjustment $adjustment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Adjustment  $adjustment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Adjustment $adjustment)
    {
        $adjustment->delete();
        $details = AdjustmentDetail::where('adjustment_id', $adjustment->id)->get();
        if ($details) {
            foreach ($details as $detail) {

                $stock = Stock::where('outlet_id', $adjustment->outlet_id)
                                ->where('item_id', $detail->item_id)
                                ->first();

                $qtyOld = 0;
                $qtyNew = 0;

                if ($stock) {
                    $qtyOld = $stock->quantity;

                    $stock = Stock::where('outlet_id', $adjustment->outlet_id)
                                    ->where('item_id', $detail->item_id)
                                    ->delete();

                    $stock = Stock::create([
                        'outlet_id' => $adjustment->outlet_id,
                        'item_id' => $detail->item_id,
                        'quantity' => $qtyOld - $detail->quantity,
                    ]);

                    $stockCard = StockCard::create([
                        'date' => date('Y-m-d'),
                        'outlet_id' => $adjustment->outlet_id,
                        'item_id' => $detail->item_id,
                        'beginning' => $qtyOld,
                        'in' => 0,
                        'out' => $detail->quantity,
                        'ending' => $qtyOld - $detail->quantity,
                        'information' => 'Hapus Penyesuaian '.$adjustment->number,
                    ]);
                }

            }
        }
        return ['message' => 'Data Deleted!'];
    }
}
