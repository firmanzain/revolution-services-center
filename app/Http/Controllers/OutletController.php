<?php

namespace App\Http\Controllers;

use App\Models\Outlet;
use Illuminate\Http\Request;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\OutletTransformer;
use App\Models\UserOutlet;

class OutletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
          $columns = ['', 'name', 'phone', 'address'];

          $length = $request->input('length');
          $column = $request->input('column');
          $dir = $request->input('dir');
          $searchValue = $request->input('search');

          $query = Outlet::where('id', '!=', 1)
                           ->orderBy($columns[$column], $dir);

          if ($searchValue) {
              $query->where(function($query) use ($searchValue) {
                  $query->where('name', 'like', '%' . $searchValue . '%')
                        ->orWhere('phone', 'like', '%' . $searchValue . '%')
                        ->orWhere('address', 'like', '%' . $searchValue . '%');
              });
          }

          $data = $query->paginate($length);
          return ['data' => $data, 'draw' => $request->input('draw')];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'phone' => 'required|string|max:15|unique:outlets',
            'address' => 'required|string|max:255',
            'receipt_note' => 'required|string',
        ]);

        $outlet = Outlet::create([
            'name' => $request['name'],
            'phone' => $request['phone'],
            'address' => $request['address'],
            'receipt_note' => $request['receipt_note'],
        ]);

        UserOutlet::create([
            'user_id' => 1,
            'outlet_id' => $outlet['id'],
        ]);

        return $outlet;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Outlet  $outlet
     * @return \Illuminate\Http\Response
     */
    public function show(Outlet $outlet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Outlet  $outlet
     * @return \Illuminate\Http\Response
     */
    public function edit(Outlet $outlet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Outlet  $outlet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Outlet::findOrFail($id);
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'phone' => 'required|string|max:15|unique:outlets,phone,'.$data->id.'',
            'address' => 'required|string|max:255',
            'receipt_note' => 'required|string',
        ]);

        $data->update($request->all());
        return ['message' => 'Data updated!'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Outlet  $outlet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Outlet $outlet)
    {
        $outlet->delete();
        return ['message' => 'Data Deleted!'];
    }

    public function select(Request $request)
    {
        if ($request->paramsParse) {
            if ($request->paramsParse['outlet_origin'] == NULL) {
                return [];
            }
        }

        $id = NULL;
        if ($request->paramsParse) {
            $id = explode(',', $request->paramsParse['outlet_id']);
        }
        $query = Outlet::select('id AS id', 'name AS text')
                         ->where('id', '!=', 1)
                         ->where('name', 'like', '%' . $request->input('search') . '%')
                         ->orderBy('name', 'ASC');
        if ($id) {
            foreach ($id as $key) {
                $query->where('id', '!=', $key);
            }
        }
        $data = $query->paginate(15);

        return fractal()
           ->collection($data, new OutletTransformer())
           ->serializeWith(new ArraySerializer())
           ->toArray();
    }
}
