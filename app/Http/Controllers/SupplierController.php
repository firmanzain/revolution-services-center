<?php

namespace App\Http\Controllers;

use App\Models\Supplier;
use Illuminate\Http\Request;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\SupplierTransformer;
use App\Models\ItemSupplierPrice;
use App\Transformers\ItemSupplierPriceTransformer;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
          $columns = ['', 'name', 'phone', 'address'];

          $length = $request->input('length');
          $column = $request->input('column');
          $dir = $request->input('dir');
          $searchValue = $request->input('search');

          $query = Supplier::orderBy($columns[$column], $dir);

          if ($searchValue) {
              $query->where(function($query) use ($searchValue) {
                  $query->where('name', 'like', '%' . $searchValue . '%')
                        ->orWhere('phone', 'like', '%' . $searchValue . '%')
                        ->orWhere('address', 'like', '%' . $searchValue . '%');
              });
          }

          $data = $query->paginate($length);
          return ['data' => $data, 'draw' => $request->input('draw')];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'phone' => 'required|string|max:15|unique:outlets',
            'address' => 'required|string|max:255',
        ]);

        return Supplier::create([
            'name' => $request['name'],
            'phone' => $request['phone'],
            'address' => $request['address'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show(Supplier $supplier)
    {
        return fractal()
               ->item($supplier, new SupplierTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit(Supplier $supplier)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Supplier::findOrFail($id);
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'phone' => 'required|string|max:15|unique:suppliers,phone,'.$data->id.'',
            'address' => 'required|string|max:255',
        ]);

        $data->update($request->all());
        return ['message' => 'Data updated!'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supplier $supplier)
    {
        $outlet->delete();
        return ['message' => 'Data Deleted!'];
    }

    public function select(Request $request)
    {
        $query = Supplier::select('id AS id', 'name AS text')
                           ->where('name', 'like', '%' . $request->input('search') . '%')
                           ->orderBy('name', 'ASC')
                           ->paginate(15);

        return fractal()
           ->collection($query, new SupplierTransformer())
           ->serializeWith(new ArraySerializer())
           ->toArray();
    }

    public function price(Request $request, $supplierId, $itemId)
    {
        $query = ItemSupplierPrice::where('supplier_id', $supplierId)
                                    ->where('item_id', $itemId);
        $data = $query->paginate(15);

        $data = $query->first();
        if ($data) {
            return fractal()
                   ->item($data, new ItemSupplierPriceTransformer())
                   ->serializeWith(new ArraySerializer())
                   ->toArray();
        } else {
            return [];
        }
    }
}
