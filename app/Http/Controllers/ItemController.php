<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;
use App\Models\ItemPurchasePrice, App\Models\ItemSellingPrice;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\ItemTransformer;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $columns = ['', 'code', 'name', 'category_id', 'brand_id'];

        $length = $request->input('length');
        $column = $request->input('column');
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        if ($column == 3) {
            $query = Item::with(['purchaseprice', 'sellingprice', 'category' => function ($query) use($dir) {
                return $query->orderBy('name', $dir);
            }, 'brand']);
        } else if ($column == 4) {
            $query = Item::with(['purchaseprice', 'sellingprice', 'category', 'brand' => function ($query) use($dir) {
                return $query->orderBy('name', 'desc');
            }]);
        } else {
            $query = Item::with(['purchaseprice', 'sellingprice', 'category', 'brand'])
                           ->orderBy($columns[$column], $dir);
        }

        if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
                $query->where('code', 'like', '%' . $searchValue . '%')
                      ->orWhere('name', 'like', '%' . $searchValue . '%')
                      ->orWhere('description', 'like', '%' . $searchValue . '%');
            });
            $query->orWhereHas('purchaseprice', function($query) use($searchValue) {
                $query->where('price', 'like', '%' . $searchValue . '%');
            });
            $query->orWhereHas('sellingprice', function($query) use($searchValue) {
                $query->where('price', 'like', '%' . $searchValue . '%');
            });
            $query->orWhereHas('category', function($query) use($searchValue) {
                $query->where('name', 'like', '%' . $searchValue . '%');
            });
        }

        $data = $query->paginate($length);
        return ['data' => $data, 'draw' => $request->input('draw')];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required|integer|max:20',
            'brand_id' => 'required|integer|max:20',
            'code' => 'required|string|max:255',
            'name' => 'required|string|max:255',
            'purchasePrice' => 'required|string|max:10',
            'sellingPrice' => 'required|string|max:10',
        ]);

        $item = Item::create([
            'category_id' => $request['category_id'],
            'brand_id' => $request['brand_id'],
            'code' => $request['code'],
            'name' => $request['name'],
            'description' => $request['description'],
        ]);

        ItemPurchasePrice::create([
            'item_id' => $item['id'],
            'price' => str_replace(',', '', $request['purchasePrice']),
        ]);

        ItemSellingPrice::create([
            'item_id' => $item['id'],
            'price' => str_replace(',', '', $request['sellingPrice']),
        ]);

        return $item;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Item::findOrFail($id);
        $this->validate($request, [
            'category_id' => 'required|integer|max:20',
            'brand_id' => 'required|integer|max:20',
            'code' => 'required|string|max:255',
            'name' => 'required|string|max:255',
            'purchasePrice' => 'required|string|max:10',
            'sellingPrice' => 'required|string|max:10',
        ]);

        $item->category_id =  $request['category_id'];
        $item->brand_id =  $request['brand_id'];
        $item->code =  $request['code'];
        $item->name =  $request['name'];
        $item->description =  $request['description'];
        $item->save();

        $purchasePrice = ItemPurchasePrice::where('item_id', $item->id)->delete();

        ItemPurchasePrice::create([
            'item_id' => $item->id,
            'price' => str_replace(',', '', $request['purchasePrice']),
        ]);

        $sellingPrice = ItemSellingPrice::where('item_id', $item->id)->delete();

        ItemSellingPrice::create([
            'item_id' => $item->id,
            'price' => str_replace(',', '', $request['sellingPrice']),
        ]);

        return ['message' => 'Data updated!'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        $item->delete();
        return ['message' => 'Data Deleted!'];
    }

    public function autocomplete(Request $request)
    {
      $query = Item::where('name', 'like', '%' . $request->input('search') . '%')
                     ->orWhere('code', 'like', '%' . $request->input('search') . '%')
                     ->orderBy('name', 'ASC')
                     ->paginate(15);

      return fractal()
         ->collection($query, new ItemTransformer())
         ->serializeWith(new ArraySerializer())
         ->toArray();
    }

    public function select(Request $request)
    {
        $id = NULL;
        if ($request->paramsParse) {
            $id = explode(',', $request->paramsParse['item_id']);
        }
        $query = Item::select('id AS id', 'name AS text')
                       ->where('name', 'like', '%' . $request->input('search') . '%')
                       ->orderBy('name', 'ASC');

        if ($id) {
            foreach ($id as $key) {
                $query->where('id', '!=', $key);
            }
        }
        $data = $query->paginate(15);

        return fractal()
           ->collection($data, new ItemTransformer())
           ->serializeWith(new ArraySerializer())
           ->toArray();
    }
}
