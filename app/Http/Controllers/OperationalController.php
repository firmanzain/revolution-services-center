<?php

namespace App\Http\Controllers;

use App\Models\Operational;
use Illuminate\Http\Request;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\OperationalTransformer;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Transformers\UserTransformer;

class OperationalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $columns = ['', 'date', 'number', 'nominal', 'information'];

        $length = $request->input('length');
        $column = $request->input('column');
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        $query = Operational::with(['outlet', 'user'])
                              ->orderBy($columns[$column], $dir);

        if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
                $query->where('date', 'like', '%' . $searchValue . '%')
                      ->orWhere('number', 'like', '%' . $searchValue . '%')
                      ->orWhere('nominal', 'like', '%' . $searchValue . '%')
                      ->orWhere('information', 'like', '%' . $searchValue . '%');
            });
        }

        $data = $query->paginate($length);
        return ['data' => $data, 'draw' => $request->input('draw')];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'date' => 'required|date|max:255',
            'nominal' => 'required|string|max:20',
            'information' => 'nullable|string|max:255',
        ]);

        $number = 'OP'.Carbon::parse($request['date'])->isoFormat('YYYY').Carbon::parse($request['date'])->isoFormat('MM');
        $check = Operational::select(DB::raw('SUBSTRING(number,9,5) as id'))
                              ->whereYear('date', Carbon::parse($request['date'])->isoFormat('YYYY'))
                              ->whereMonth('date', Carbon::parse($request['date'])->isoFormat('MM'))
                              ->orderBy('number', 'DESC')
                              ->first();

        $seq = '';
        if ($check) {
            $urut = intval($check->id) + 1;
            $seq = sprintf("%05d",$urut);
        } else {
            $seq = sprintf("%05d",1);
        }

        $number .= $seq;

        $outletId = $request['outlet_id'];
        if ($outletId == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
                    ->item($user, new UserTransformer())
                    ->serializeWith(new ArraySerializer())
                    ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outletId = $outletUser['outlet']['id'];
                    break;
                }
            }
        }

        $payment = Operational::create([
            'date' => Carbon::parse($request['date'])->isoFormat('YYYY-MM-DD'),
            'number' => $number,
            'outlet_id' => $outletId,
            'nominal' => intval(str_replace(',', '', $request['nominal'])),
            'information' => $request['information'],
            'user_id' => $user->id,
        ]);

        return $payment;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Operational  $operational
     * @return \Illuminate\Http\Response
     */
    public function show(Operational $operational)
    {
        return fractal()
               ->item($operational, new OperationalTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Operational  $operational
     * @return \Illuminate\Http\Response
     */
    public function edit(Operational $operational)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Operational  $operational
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Operational $operational)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Operational  $operational
     * @return \Illuminate\Http\Response
     */
    public function destroy(Operational $operational)
    {
        $operational->delete();
        return ['message' => 'Data Deleted!'];
    }
}
