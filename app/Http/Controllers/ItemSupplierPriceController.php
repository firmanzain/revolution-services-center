<?php

namespace App\Http\Controllers;

use App\Models\ItemSupplierPrice;
use Illuminate\Http\Request;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\ItemSupplierPriceTransformer;

class ItemSupplierPriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $columns = ['', 'item_id', 'supplier_id'];

        $length = $request->input('length');
        $column = $request->input('column');
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        $query = ItemSupplierPrice::with(['item', 'supplier'])
                           ->where('supplier_id', $request->input('supplier'))
                           ->orderBy($columns[$column], $dir);

        $data = $query->paginate($length);
        return ['data' => $data, 'draw' => $request->input('draw')];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'item_id' => 'required|integer|max:20',
            'supplier_id' => 'required|integer|max:20',
            'price' => 'required|string|max:20',
        ]);

        return ItemSupplierPrice::create([
            'item_id' => $request['item_id'],
            'supplier_id' => $request['supplier_id'],
            'price' => str_replace(',', '', $request['price']),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ItemSupplierPrice  $itemSupplierPrice
     * @return \Illuminate\Http\Response
     */
    public function show(ItemSupplierPrice $itemSupplierPrice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ItemSupplierPrice  $itemSupplierPrice
     * @return \Illuminate\Http\Response
     */
    public function edit(ItemSupplierPrice $itemSupplierPrice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ItemSupplierPrice  $itemSupplierPrice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItemSupplierPrice $itemSupplierPrice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ItemSupplierPrice  $itemSupplierPrice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $itemId, $supplierId)
    {
        $itemSupplierPrice = ItemSupplierPrice::where('item_id', $itemId)->where('supplier_id', $supplierId)->delete();
        return ['message' => 'Data Deleted!'];
    }
}
