<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User, App\Models\UserType, App\Models\Menu,
    App\Models\UserPrivilege;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\UserTransformer, App\Transformers\MenuTransformer;

class GlobalController extends Controller
{

    public function refreshCsrfToken(){
        return ['csrfToken' => csrf_token()];
    }

    public function authCheck(Request $request)
    {
      $user = Auth::user();
      $menu = [];

      if ($user) {
          $user = User::where('id', $user->id)->first();
          $user = fractal()
                  ->item($user, new UserTransformer())
                  ->serializeWith(new ArraySerializer())
                  ->toArray();

          $privileges = UserPrivilege::where('user_type_id', $user['usertype']['id'])->get();

          $menusTemp = Menu::where('parent', 0)->get();
          $menusTemp = fractal()
                       ->collection($menusTemp, new MenuTransformer())
                       ->serializeWith(new ArraySerializer())
                       // ->parseIncludes('sub')
                       ->toArray();

          $submenusTemp = Menu::where('parent', '!=', 0)->get();
          $submenusTemp = fractal()
                       ->collection($submenusTemp, new MenuTransformer())
                       ->serializeWith(new ArraySerializer())
                       ->toArray();

          foreach ($privileges as $privMenu) {
              foreach ($menusTemp as $m) {
                  if ($privMenu->menu_id == $m['id'] && $privMenu->create == 1) {

                      $sub = [];
                      $subLink = [];
                      foreach ($privileges as $privSub) {
                          foreach ($submenusTemp as $s) {
                              if ($privSub->menu_id == $s['id'] && $privSub->create == 1 && $s['parent'] == $m['id']) {
                                  array_push($sub, $s);
                                  array_push($subLink, '/'.$s['link']);
                              }
                          }
                      }

                      $mParse = [
                        'id' => $m['id'],
                        'name' => $m['name'],
                        'index' => $m['index'],
                        'parent' => $m['parent'],
                        'link' => $m['link'],
                        'icon' => $m['icon'],
                        'subLink' => $subLink,
                        'sub' => $sub,
                      ];
                      array_push($menu, $mParse);
                  }
              }
          }
      }

      return response()->json([
          'code' => 200,
          'data' => [
            'user' => $user,
            'menu' => $menu,
          ]
      ], 200);
    }

    public function test(Request $request)
    {
        $status = 0;
        $name = $status == 1 ? 'Process' : ($status == 2 ? 'Pending' : 'Done');
        return $name;
    }
}
