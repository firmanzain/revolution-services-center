<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use App\Models\CustomerType;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\CustomerTransformer;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $columns = ['', 'number', 'name', 'email', 'phone', 'address'];

        $length = $request->input('length');
        $column = $request->input('column');
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        $query = Customer::with(['customertype', 'debt'])
                               ->orderBy($columns[$column], $dir);

        if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
                $query->where('name', 'like', '%' . $searchValue . '%')
                      ->orWhere('number', 'like', '%' . $searchValue . '%')
                      ->orWhere('email', 'like', '%' . $searchValue . '%')
                      ->orWhere('phone', 'like', '%' . $searchValue . '%')
                      ->orWhere('address', 'like', '%' . $searchValue . '%');
            });
            $query->orWhereHas('customertype', function($query) use($searchValue) {
                   $query->where('name', 'like', '%' . $searchValue . '%');
            });
        }

        $data = $query->paginate($length);
        return ['data' => $data, 'draw' => $request->input('draw')];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'customer_type_id' => 'required|integer|max:20',
            'name' => 'required|string|max:255',
            'email' => 'nullable|email|max:255',
        ]);

        $number = 'RVM'.Carbon::now()->isoFormat('YYYY').Carbon::now()->isoFormat('MM');
        $check = Customer::select(DB::raw('SUBSTRING(number,10,5) as id'))
                             ->whereYear('created_at', Carbon::now()->isoFormat('YYYY'))
                             ->whereMonth('created_at', Carbon::now()->isoFormat('MM'))
                             ->orderBy('number', 'DESC')
                             ->first();

        $seq = '';
        if ($check) {
            $urut = intval($check->id) + 1;
            $seq = sprintf("%05d",$urut);
        } else {
            $seq = sprintf("%05d",1);
        }

        $number .= $seq;

        return Customer::create([
            'customer_type_id' => $request['customer_type_id'],
            'number' => $number,
            'name' => $request['name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'address' => $request['address'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        return fractal()
               ->item($customer, new CustomerTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Customer::findOrFail($id);
        $this->validate($request, [
            'customer_type_id' => 'required|integer|max:20',
            'name' => 'required|string|max:255',
        ]);

        $data->customer_type_id =  $request['customer_type_id'];
        $data->name = $request['name'];
        $data->email = $request['email'];
        $data->phone = $request['phone'];
        $data->address = $request['address'];
        $data->save();
        return ['message' => 'Data updated!'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $customer->delete();
        return ['message' => 'Data Deleted!'];
    }

    public function select(Request $request)
    {
        $id = NULL;
        if ($request->paramsParse) {
            $id = explode(',', $request->paramsParse['customer_id']);
        }
        $query = Customer::select('id AS id', 'name AS text', 'number')
                       ->where('name', 'like', '%' . $request->input('search') . '%')
                       ->orderBy('name', 'ASC');
        if ($id) {
            foreach ($id as $key) {
                $query->where('id', '!=', $key);
            }
        }
        $data = $query->paginate(15);

        return fractal()
           ->collection($data, new CustomerTransformer())
           ->serializeWith(new ArraySerializer())
           ->toArray();
    }
}
