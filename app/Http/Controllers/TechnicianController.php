<?php

namespace App\Http\Controllers;

use App\Models\Technician;
use Illuminate\Http\Request;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\TechnicianTransformer;

class TechnicianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
          $columns = ['', 'name', 'phone', 'address'];

          $length = $request->input('length');
          $column = $request->input('column');
          $dir = $request->input('dir');
          $searchValue = $request->input('search');

          $query = Technician::orderBy($columns[$column], $dir);

          if ($searchValue) {
              $query->where(function($query) use ($searchValue) {
                  $query->where('name', 'like', '%' . $searchValue . '%')
                        ->orWhere('phone', 'like', '%' . $searchValue . '%')
                        ->orWhere('address', 'like', '%' . $searchValue . '%');
              });
          }

          $data = $query->paginate($length);
          return ['data' => $data, 'draw' => $request->input('draw')];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'phone' => 'required|string|max:15|unique:technicians',
            'address' => 'required|string|max:255',
        ]);

        return Technician::create([
            'name' => $request['name'],
            'phone' => $request['phone'],
            'address' => $request['address'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Technician  $technician
     * @return \Illuminate\Http\Response
     */
    public function show(Technician $technician)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Technician  $technician
     * @return \Illuminate\Http\Response
     */
    public function edit(Technician $technician)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Technician  $technician
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Technician::findOrFail($id);
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'phone' => 'required|string|max:15|unique:technicians,phone,'.$data->id.'',
            'address' => 'required|string|max:255',
        ]);

        $data->update($request->all());
        return ['message' => 'Data updated!'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Technician  $technician
     * @return \Illuminate\Http\Response
     */
    public function destroy(Technician $technician)
    {
        $technician->delete();
        return ['message' => 'Data Deleted!'];
    }

    public function select(Request $request)
    {
        $query = Technician::select('id AS id', 'name AS text')
                         ->where('name', 'like', '%' . $request->input('search') . '%')
                         ->orderBy('name', 'ASC')
                         ->paginate(15);

        return fractal()
           ->collection($query, new TechnicianTransformer())
           ->serializeWith(new ArraySerializer())
           ->toArray();
    }
}
