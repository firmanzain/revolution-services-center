<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Stock, App\Models\Customer, App\Models\Sale,
    App\Models\Service, App\Models\Item, App\Models\Supplier,
    App\Models\Purchase, App\Models\SupplierDebtPayment, App\Models\CustomerDebtPayment,
    App\Models\Operational, App\Models\OtherExpense;
use App\User;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();

        $outletId = $request->outlet;
        if ($outletId == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
                    ->item($user, new UserTransformer())
                    ->serializeWith(new ArraySerializer())
                    ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outletId = $outletUser['outlet']['id'];
                    break;
                }
            }
        }
        $firstDate = Carbon::parse($request->firstDate);
        $lastDate = Carbon::parse($request->lastDate);

        $response = array();

        $stock = 0;
        $stockQuery = Stock::where('outlet_id', $outletId)->get();
        if ($stockQuery) {
            foreach ($stockQuery as $item) {
                $stock += $item->quantity;
            }
        }
        $response['stock'] = $stock;

        $customer = 0;
        $customerQuery = Customer::count();
        if ($customerQuery) {
            $customer += $customerQuery;
        }
        $response['customer'] = $customer;

        $sale_item = 0;
        $sale_service = 0;
        $sale_nominal = 0;
        $sale_service_nominal = 0;
        $saleQuery = Sale::whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                           ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'))
                           ->where('outlet_id', $outletId)
                           ->get();

        if ($saleQuery) {
            foreach ($saleQuery as $sale) {
                if ($sale->type == 1) {
                    $sale_item++;
                } else if ($sale->type == 2) {
                    $sale_service_nominal += $sale->fee;
                    $sale_service++;
                }
                $sale_nominal += $sale->total;
            }
        }
        $response['sale_item'] = $sale_item;
        $response['sale_service'] = $sale_service;
        $response['sale_nominal'] = $sale_nominal;
        $response['sale_service_nominal'] = $sale_service_nominal;

        $service_progress = 0;
        $service_done = 0;
        $service_close = 0;
        $serviceQuery = Service::whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                                 ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'))
                                 ->where('outlet_id', $outletId)
                                 ->get();

        if ($serviceQuery) {
            foreach ($serviceQuery as $service) {
                if ($service->status == 1) {
                    $service_progress++;
                } else if ($service->status == 2) {
                    $service_done++;
                } else if ($service->status == 0) {
                    $service_close++;
                }
            }
        }
        $response['service_progress'] = $service_progress;
        $response['service_done'] = $service_done;
        $response['service_close'] = $service_close;

        $item = 0;
        $itemQuery = Item::count();
        if ($itemQuery) {
            $item += $itemQuery;
        }
        $response['item'] = $item;

        $supplier = 0;
        $supplierQuery = Supplier::count();
        if ($supplierQuery) {
            $supplier += $supplierQuery;
        }
        $response['supplier'] = $supplier;

        $purchase_count = 0;
        $purchase_nominal = 0;
        $purchaseQuery = Purchase::whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                               ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'))
                               ->where('outlet_id', $outletId)
                               ->get();

        if ($purchaseQuery) {
            foreach ($purchaseQuery as $purchase) {
                $purchase_count++;
                $purchase_nominal += $purchase->total;
            }
        }
        $response['purchase'] = $purchase_count;
        $response['purchase_nominal'] = $purchase_nominal;

        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function reportCount(Request $request)
    {
        $user = Auth::user();

        $outletId = $request->outlet;
        if ($outletId == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
            ->item($user, new UserTransformer())
            ->serializeWith(new ArraySerializer())
            ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outletId = $outletUser['outlet']['id'];
                    break;
                }
            }
        }
        $firstDate = Carbon::parse($request->firstDate);
        $lastDate = Carbon::parse($request->lastDate);

        $pengeluaran = 0;

        $purchases = Purchase::whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
        ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'))
        ->where('outlet_id', $outletId)
        ->where('payment', 1)
        ->get();

        if ($purchases) {
            foreach ($purchases as $purchase) {
                $pengeluaran += $purchase->total;
            }
        }

        $debtPayments = SupplierDebtPayment::whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
        ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'))
        ->get();

        if ($debtPayments) {
            foreach ($debtPayments as $debtPayment) {
                $pengeluaran += $debtPayment->debt_nominal_payment;
            }
        }

        $operationals = Operational::whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
        ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'))
        ->where('outlet_id', $outletId)
        ->get();

        if ($operationals) {
            foreach ($operationals as $operational) {
                $pengeluaran += $operational->nominal;
            }
        }

        $otherExpenses = OtherExpense::whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
        ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'))
        ->where('outlet_id', $outletId)
        ->get();

        if ($otherExpenses) {
            foreach ($otherExpenses as $otherExpense) {
                $pengeluaran += $operational->nominal;
            }
        }

        $response['pengeluaran'] = $pengeluaran;

        $pemasukkan = 0;

        $sales = Sale::whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
        ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'))
        ->where('outlet_id', $outletId)
        ->where('payment', 1)
        ->get();

        if ($sales) {
            foreach ($sales as $sale) {
                $pemasukkan += $sale->total;
            }
        }

        $receivablePayments = CustomerDebtPayment::whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
        ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'))
        ->get();

        if ($receivablePayments) {
            foreach ($receivablePayments as $receivablePayment) {
                $pemasukkan += $receivablePayment->debt_nominal_payment;
            }
        }

        $response['pemasukkan'] = $pemasukkan;

        $chartCategories = array();
        $chartPengeluaran = array();
        $chartPemasukkan = array();

        if ($firstDate->diffInDays($lastDate) <= 30) {

            $startDate = Carbon::parse($request->firstDate);
            $endDate = Carbon::parse($request->firstDate);
            $endDate->addDays(6);

            do {
                $strDate = $startDate->isoFormat('DD') . '-' . $endDate->isoFormat('DD/MM/YY');
                array_push($chartCategories, $strDate);

                // PENGELUARAN
                $pengeluaran = 0;

                $purchases = Purchase::whereDate('date', '>=', $startDate->isoFormat('YYYY-MM-DD'))
                ->whereDate('date', '<=', $endDate->isoFormat('YYYY-MM-DD'))
                ->where('outlet_id', $outletId)
                ->where('payment', 1)
                ->get();

                if ($purchases) {
                    foreach ($purchases as $purchase) {
                        $pengeluaran += $purchase->total;
                    }
                }

                $debtPayments = SupplierDebtPayment::whereDate('date', '>=', $startDate->isoFormat('YYYY-MM-DD'))
                ->whereDate('date', '<=', $endDate->isoFormat('YYYY-MM-DD'))
                ->get();

                if ($debtPayments) {
                    foreach ($debtPayments as $debtPayment) {
                        $pengeluaran += $debtPayment->debt_nominal_payment;
                    }
                }

                $operationals = Operational::whereDate('date', '>=', $startDate->isoFormat('YYYY-MM-DD'))
                ->whereDate('date', '<=', $endDate->isoFormat('YYYY-MM-DD'))
                ->where('outlet_id', $outletId)
                ->get();

                if ($operationals) {
                    foreach ($operationals as $operational) {
                        $pengeluaran += $operational->nominal;
                    }
                }

                $otherExpenses = OtherExpense::whereDate('date', '>=', $startDate->isoFormat('YYYY-MM-DD'))
                ->whereDate('date', '<=', $endDate->isoFormat('YYYY-MM-DD'))
                ->where('outlet_id', $outletId)
                ->get();

                if ($otherExpenses) {
                    foreach ($otherExpenses as $otherExpense) {
                        $pengeluaran += $operational->nominal;
                    }
                }

                array_push($chartPengeluaran, $pengeluaran);
                // END PENGELUARAN

                // PEMASUKKAN
                $pemasukkan = 0;

                $sales = Sale::whereDate('date', '>=', $startDate->isoFormat('YYYY-MM-DD'))
                ->whereDate('date', '<=', $endDate->isoFormat('YYYY-MM-DD'))
                ->where('outlet_id', $outletId)
                ->where('payment', 1)
                ->get();

                if ($sales) {
                    foreach ($sales as $sale) {
                        $pemasukkan += $sale->total;
                    }
                }

                $receivablePayments = CustomerDebtPayment::whereDate('date', '>=', $startDate->isoFormat('YYYY-MM-DD'))
                ->whereDate('date', '<=', $endDate->isoFormat('YYYY-MM-DD'))
                ->get();

                if ($receivablePayments) {
                    foreach ($receivablePayments as $receivablePayment) {
                        $pemasukkan += $receivablePayment->debt_nominal_payment;
                    }
                }

                array_push($chartPemasukkan, $pemasukkan);
                // END PEMASUKKAN

                $startDate = Carbon::parse($endDate)->addDays(1);
                $endDate = Carbon::parse($endDate);
                $endDate->addDays(6);

            } while ($endDate->lessThanOrEqualTo($lastDate));

            if ($startDate->lessThan($lastDate) && $endDate->greaterThan($lastDate)) {
                $strDate = $startDate->isoFormat('DD') . '-' . $lastDate->isoFormat('DD/MM/YY');
                array_push($chartCategories, $strDate);

                // PENGELUARAN
                $pengeluaran = 0;

                $purchases = Purchase::whereDate('date', '>=', $startDate->isoFormat('YYYY-MM-DD'))
                ->whereDate('date', '<=', $endDate->isoFormat('YYYY-MM-DD'))
                ->where('outlet_id', $outletId)
                ->where('payment', 1)
                ->get();

                if ($purchases) {
                    foreach ($purchases as $purchase) {
                        $pengeluaran += $purchase->total;
                    }
                }

                $debtPayments = SupplierDebtPayment::whereDate('date', '>=', $startDate->isoFormat('YYYY-MM-DD'))
                ->whereDate('date', '<=', $endDate->isoFormat('YYYY-MM-DD'))
                ->get();

                if ($debtPayments) {
                    foreach ($debtPayments as $debtPayment) {
                        $pengeluaran += $debtPayment->debt_nominal_payment;
                    }
                }

                $operationals = Operational::whereDate('date', '>=', $startDate->isoFormat('YYYY-MM-DD'))
                ->whereDate('date', '<=', $endDate->isoFormat('YYYY-MM-DD'))
                ->where('outlet_id', $outletId)
                ->get();

                if ($operationals) {
                    foreach ($operationals as $operational) {
                        $pengeluaran += $operational->nominal;
                    }
                }

                $otherExpenses = OtherExpense::whereDate('date', '>=', $startDate->isoFormat('YYYY-MM-DD'))
                ->whereDate('date', '<=', $endDate->isoFormat('YYYY-MM-DD'))
                ->where('outlet_id', $outletId)
                ->get();

                if ($otherExpenses) {
                    foreach ($otherExpenses as $otherExpense) {
                        $pengeluaran += $operational->nominal;
                    }
                }

                array_push($chartPengeluaran, $pengeluaran);
                // END PENGELUARAN

                // PEMASUKKAN
                $pemasukkan = 0;

                $sales = Sale::whereDate('date', '>=', $startDate->isoFormat('YYYY-MM-DD'))
                ->whereDate('date', '<=', $endDate->isoFormat('YYYY-MM-DD'))
                ->where('outlet_id', $outletId)
                ->where('payment', 1)
                ->get();

                if ($sales) {
                    foreach ($sales as $sale) {
                        $pemasukkan += $sale->total;
                    }
                }

                $receivablePayments = CustomerDebtPayment::whereDate('date', '>=', $startDate->isoFormat('YYYY-MM-DD'))
                ->whereDate('date', '<=', $endDate->isoFormat('YYYY-MM-DD'))
                ->get();

                if ($receivablePayments) {
                    foreach ($receivablePayments as $receivablePayment) {
                        $pemasukkan += $receivablePayment->debt_nominal_payment;
                    }
                }

                array_push($chartPemasukkan, $pemasukkan);
                // END PEMASUKKAN
            }

        } else {
            $startDate = Carbon::parse($request->firstDate);

            do {
                $strDate = $startDate->isoFormat('MMM');
                array_push($chartCategories, $strDate);

                // PENGELUARAN
                $pengeluaran = 0;

                $purchases = Purchase::whereMonth('date', $startDate->isoFormat('MM'))
                ->whereYear('date', $startDate->isoFormat('YYYY'))
                ->where('outlet_id', $outletId)
                ->where('payment', 1)
                ->get();

                if ($purchases) {
                    foreach ($purchases as $purchase) {
                        $pengeluaran += $purchase->total;
                    }
                }

                $debtPayments = SupplierDebtPayment::whereMonth('date', $startDate->isoFormat('MM'))
                ->whereYear('date', $startDate->isoFormat('YYYY'))
                ->get();

                if ($debtPayments) {
                    foreach ($debtPayments as $debtPayment) {
                        $pengeluaran += $debtPayment->debt_nominal_payment;
                    }
                }

                $operationals = Operational::whereMonth('date', $startDate->isoFormat('MM'))
                ->whereYear('date', $startDate->isoFormat('YYYY'))
                ->where('outlet_id', $outletId)
                ->get();

                if ($operationals) {
                    foreach ($operationals as $operational) {
                        $pengeluaran += $operational->nominal;
                    }
                }

                $otherExpenses = OtherExpense::whereMonth('date', $startDate->isoFormat('MM'))
                ->whereYear('date', $startDate->isoFormat('YYYY'))
                ->where('outlet_id', $outletId)
                ->get();

                if ($otherExpenses) {
                    foreach ($otherExpenses as $otherExpense) {
                        $pengeluaran += $operational->nominal;
                    }
                }

                array_push($chartPengeluaran, $pengeluaran);
                // END PENGELUARAN

                // PEMASUKKAN
                $pemasukkan = 0;

                $sales = Sale::whereMonth('date', $startDate->isoFormat('MM'))
                ->whereYear('date', $startDate->isoFormat('YYYY'))
                ->where('outlet_id', $outletId)
                ->where('payment', 1)
                ->get();

                if ($sales) {
                    foreach ($sales as $sale) {
                        $pemasukkan += $sale->total;
                    }
                }

                $receivablePayments = CustomerDebtPayment::whereMonth('date', $startDate->isoFormat('MM'))
                ->whereYear('date', $startDate->isoFormat('YYYY'))
                ->get();

                if ($receivablePayments) {
                    foreach ($receivablePayments as $receivablePayment) {
                        $pemasukkan += $receivablePayment->debt_nominal_payment;
                    }
                }

                array_push($chartPemasukkan, $pemasukkan);
                // END PEMASUKKAN

                $startDate->addMonths();

            } while ($startDate->lessThanOrEqualTo($lastDate));
        }
        $response['chart_categories'] = $chartCategories;
        $response['chart_pengeluaran'] = $chartPengeluaran;
        $response['chart_pemasukkan'] = $chartPemasukkan;

        return $response;
    }

    public function transactionDetail(Request $request)
    {
        $user = Auth::user();

        $outlet = $request->input('outlet');
        if ($outlet == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
            ->item($user, new UserTransformer())
            ->serializeWith(new ArraySerializer())
            ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outlet = $outletUser['outlet']['id'];
                    break;
                }
            }
        }

        $columns = ['', 'date', 'number', 'pengeluaran', 'pemasukkan', 'information'];

        if ($request->firstDate && $request->lastDate) {
            $firstDate = Carbon::parse($request->firstDate);
            $lastDate = Carbon::parse($request->lastDate);
        }

        $length = $request->input('length');
        $column = $request->input('column');
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        $purchase = Purchase::select(DB::raw('date, number, total AS pengeluaran, 0 AS pemasukkan, "Pembelian Barang" AS type, information'))
                    ->where('outlet_id', $outlet)->where('payment', 1);
        if ($request->firstDate && $request->lastDate) {
            $purchase->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                     ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        if ($searchValue) {
            $purchase->where(function($query) use ($searchValue) {
                $query->where('date', 'like', '%' . $searchValue . '%')
                ->orWhere('number', 'like', '%' . $searchValue . '%')
                ->orWhere('total', 'like', '%' . $searchValue . '%')
                ->orWhere('information', 'like', '%' . $searchValue . '%');
            });
        }

        $debtPayment = SupplierDebtPayment::select(DB::raw('date, number, debt_nominal_payment AS pengeluaran, 0 AS pemasukkan, "Pembayaran Hutang" AS type, "-" AS information'));
        if ($request->firstDate && $request->lastDate) {
            $debtPayment->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                     ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        if ($searchValue) {
            $debtPayment->where(function($query) use ($searchValue) {
                $query->where('date', 'like', '%' . $searchValue . '%')
                ->orWhere('number', 'like', '%' . $searchValue . '%')
                ->orWhere('total', 'like', '%' . $searchValue . '%');
            });
        }

        $operational = Operational::select(DB::raw('date, number, nominal AS pengeluaran, 0 AS pemasukkan, "Operasional" AS type, information'))->where('outlet_id', $outlet);
        if ($request->firstDate && $request->lastDate) {
            $operational->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                     ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        if ($searchValue) {
            $operational->where(function($query) use ($searchValue) {
                $query->where('date', 'like', '%' . $searchValue . '%')
                ->orWhere('number', 'like', '%' . $searchValue . '%')
                ->orWhere('total', 'like', '%' . $searchValue . '%')
                ->orWhere('information', 'like', '%' . $searchValue . '%');
            });
        }

        $otherExpense = OtherExpense::select(DB::raw('date, number, nominal AS pengeluaran, 0 AS pemasukkan, "Pengeluaran Lain Lain" AS type, information'))->where('outlet_id', $outlet);
        if ($request->firstDate && $request->lastDate) {
            $otherExpense->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                     ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        if ($searchValue) {
            $otherExpense->where(function($query) use ($searchValue) {
                $query->where('date', 'like', '%' . $searchValue . '%')
                ->orWhere('number', 'like', '%' . $searchValue . '%')
                ->orWhere('total', 'like', '%' . $searchValue . '%')
                ->orWhere('information', 'like', '%' . $searchValue . '%');
            });
        }

        $receivablePayment = CustomerDebtPayment::select(DB::raw('date, number, 0 AS pengeluaran, debt_nominal_payment AS pemasukkan, "Pembayaran Piutang" AS type, "-" AS information'));
        if ($request->firstDate && $request->lastDate) {
            $receivablePayment->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                     ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        if ($searchValue) {
            $receivablePayment->where(function($query) use ($searchValue) {
                $query->where('date', 'like', '%' . $searchValue . '%')
                ->orWhere('number', 'like', '%' . $searchValue . '%')
                ->orWhere('total', 'like', '%' . $searchValue . '%');
            });
        }

        $sale = Sale::select(DB::raw('date, number, 0 AS pengeluaran, total AS pemasukkan, "Penjualan Barang" AS type, information'))
                ->where('outlet_id', $outlet)->where('payment', 1)
                ->union($purchase)
                ->union($debtPayment)
                ->union($operational)
                ->union($otherExpense)
                ->union($receivablePayment);
        if ($request->firstDate && $request->lastDate) {
            $sale->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                 ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        if ($searchValue) {
            $sale->where(function($query) use ($searchValue) {
                $query->where('date', 'like', '%' . $searchValue . '%')
                ->orWhere('number', 'like', '%' . $searchValue . '%')
                ->orWhere('total', 'like', '%' . $searchValue . '%')
                ->orWhere('information', 'like', '%' . $searchValue . '%');
            });
        }

        $query = $sale;
        $query->orderBy($columns[$column], $dir);

        $data = $query->paginate($length);
        return ['data' => $data, 'draw' => $request->input('draw')];
    }

    public function transactionExcel(Request $request)
    {
        $user = Auth::user();

        $outlet = $request->input('outlet');
        if ($outlet == 0) {
            $user = User::where('id', $user->id)->first();
            $user = fractal()
            ->item($user, new UserTransformer())
            ->serializeWith(new ArraySerializer())
            ->toArray();
            foreach ($user['outlet'] as $outletUser) {
                if ($outletUser['outlet']['id'] != 1 || sizeof($user['outlet']) == 1) {
                    $outlet = $outletUser['outlet']['id'];
                    break;
                }
            }
        }

        if ($request->firstDate && $request->lastDate) {
            $firstDate = Carbon::parse($request->firstDate);
            $lastDate = Carbon::parse($request->lastDate);
        }

        $purchase = Purchase::select(DB::raw('date, number, total AS pengeluaran, 0 AS pemasukkan, "Pembelian Barang" AS type, information'))
                    ->where('outlet_id', $outlet)->where('payment', 1);
        if ($request->firstDate && $request->lastDate) {
            $purchase->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                     ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        $debtPayment = SupplierDebtPayment::select(DB::raw('date, number, debt_nominal_payment AS pengeluaran, 0 AS pemasukkan, "Pembayaran Hutang" AS type, "-" AS information'));
        if ($request->firstDate && $request->lastDate) {
            $debtPayment->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                     ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        $operational = Operational::select(DB::raw('date, number, nominal AS pengeluaran, 0 AS pemasukkan, "Operasional" AS type, information'))->where('outlet_id', $outlet);
        if ($request->firstDate && $request->lastDate) {
            $operational->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                     ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        $otherExpense = OtherExpense::select(DB::raw('date, number, nominal AS pengeluaran, 0 AS pemasukkan, "Pengeluaran Lain Lain" AS type, information'))->where('outlet_id', $outlet);
        if ($request->firstDate && $request->lastDate) {
            $otherExpense->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                     ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        $receivablePayment = CustomerDebtPayment::select(DB::raw('date, number, 0 AS pengeluaran, debt_nominal_payment AS pemasukkan, "Pembayaran Piutang" AS type, "-" AS information'));
        if ($request->firstDate && $request->lastDate) {
            $receivablePayment->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                     ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        $sale = Sale::select(DB::raw('date, number, 0 AS pengeluaran, total AS pemasukkan, "Penjualan Barang" AS type, information'))
                ->where('outlet_id', $outlet)->where('payment', 1)
                ->union($purchase)
                ->union($debtPayment)
                ->union($operational)
                ->union($otherExpense)
                ->union($receivablePayment);
        if ($request->firstDate && $request->lastDate) {
            $sale->whereDate('date', '>=', $firstDate->isoFormat('YYYY-MM-DD'))
                 ->whereDate('date', '<=', $lastDate->isoFormat('YYYY-MM-DD'));
        }

        $query = $sale;
        $query->orderBy('date', 'ASC');
        $data = $query->paginate();

        $spreadsheet = new Spreadsheet();
        $styleArray1 = array(
            'font'  => array(
                'bold'  => true
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
        );
        $styleArray2 = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );
        $styleArray3 = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
        );
        $styleArray4 = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            ),
        );


        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1',env('APP_NAME'))
            ->setCellValue('A2','Laporan Statistik');
        //STYLING TITLE
        $spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray1);
        $spreadsheet->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray1);
        $spreadsheet->getActiveSheet()->mergeCells('A1:F1');
        $spreadsheet->getActiveSheet()->mergeCells('A2:F2');

        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A4','TANGGAL')
            ->setCellValue('B4','NOMOR REFERENSI')
            ->setCellValue('C4','PENGELUARAN')
            ->setCellValue('D4','PEMASUKKAN')
            ->setCellValue('E4','JENIS')
            ->setCellValue('F4','KETERANGAN');
        //STYLING TITLE
        $spreadsheet->getActiveSheet()->getStyle('A4:F4')->applyFromArray($styleArray1);
        $spreadsheet->getActiveSheet()->getStyle('A4:F4')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

        if ($data) {
			$idx = 5; $no = 1;
            foreach ($data as $value) {
                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A'.$idx,date('d/m/Y', strtotime($value['date'])))
                    ->setCellValue('B'.$idx,$value['number'])
                    ->setCellValue('C'.$idx,$value['pengeluaran'])
                    ->setCellValue('D'.$idx,$value['pemasukkan'])
                    ->setCellValue('E'.$idx,$value['type'])
                    ->setCellValue('F'.$idx,$value['information']);
                $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':F'.$idx)->applyFromArray($styleArray2);
                $spreadsheet->getActiveSheet()->getStyle('A'.$idx.':F'.$idx)->applyFromArray($styleArray3);
                $spreadsheet->getActiveSheet()->getStyle('C'.$idx.':D'.$idx)->applyFromArray($styleArray4);
                $no++;
                $idx = $idx+1;
            }
        }

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="test.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $writer = new Xlsx($spreadsheet);
		$writer->save('php://output');
    }

}
