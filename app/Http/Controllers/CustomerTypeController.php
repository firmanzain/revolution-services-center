<?php

namespace App\Http\Controllers;

use App\Models\CustomerType;
use Illuminate\Http\Request;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\CustomerTypeTransformer;

class CustomerTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $columns = ['', 'name', 'discount', 'debt_limit'];

        $length = $request->input('length');
        $column = $request->input('column');
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        $query = CustomerType::orderBy($columns[$column], $dir);

        if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
                $query->where('name', 'like', '%' . $searchValue . '%')
                      ->orWhere('discount', 'like', '%' . $searchValue . '%')
                      ->orWhere('debt_limit', 'like', '%' . $searchValue . '%');
            });
        }

        $data = $query->paginate($length);
        return ['data' => $data, 'draw' => $request->input('draw')];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'discount' => 'required|integer|max:10',
            'debt_limit' => 'required|string|max:10',
        ]);

        return CustomerType::create([
            'name' => $request['name'],
            'discount' => str_replace(',', '', $request['discount']),
            'debt_limit' => str_replace(',', '', $request['debt_limit']),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CustomerType  $customerType
     * @return \Illuminate\Http\Response
     */
    public function show(CustomerType $customerType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CustomerType  $customerType
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerType $customerType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CustomerType  $customerType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = CustomerType::findOrFail($id);
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'discount' => 'required|integer|max:10',
            'debt_limit' => 'required|string|max:10',
        ]);

        $request['discount'] = str_replace(',', '', $request['discount']);
        $request['debt_limit'] = str_replace(',', '', $request['debt_limit']);

        $data->update($request->all());
        return ['message' => 'Data updated!'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CustomerType  $customerType
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomerType $customerType)
    {
        $customerType->delete();
        return ['message' => 'Data Deleted!'];
    }

    public function select(Request $request)
    {
        $query = CustomerType::select('id AS id', 'name AS text')
                           ->where('name', 'like', '%' . $request->input('search') . '%')
                           ->orderBy('name', 'ASC')
                           ->paginate(15);

        return fractal()
           ->collection($query, new CustomerTypeTransformer())
           ->serializeWith(new ArraySerializer())
           ->toArray();
    }
}
