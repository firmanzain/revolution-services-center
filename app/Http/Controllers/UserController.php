<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\UserTransformer;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
          $columns = ['', 'name', 'user_type_id', 'phone', 'username'];

          $length = $request->input('length');
          $column = $request->input('column');
          $dir = $request->input('dir');
          $searchValue = $request->input('search');

          $query = User::with(['usertype', 'outlet'])
                         ->where('id', '!=', 1)
                         ->orderBy($columns[$column], $dir);

          if ($searchValue) {
              $query->where(function($query) use ($searchValue) {
                  $query->where('name', 'like', '%' . $searchValue . '%')
                        ->orWhere('phone', 'like', '%' . $searchValue . '%')
                        ->orWhere('username', 'like', '%' . $searchValue . '%');
              });
              $query->orWhereHas('usertype', function($query) use($searchValue) {
                     $query->where('name', 'like', '%' . $searchValue . '%');
              });
          }

          $data = $query->paginate($length);
          return ['data' => $data, 'draw' => $request->input('draw')];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_type_id' => 'required|integer|max:20',
            'name' => 'required|string|max:255',
            'phone' => 'required|string|max:15|unique:users',
            'username' => 'required|string|max:15|unique:users',
            'password' => 'required|string|max:255',
        ]);

        return User::create([
            'user_type_id' => $request['user_type_id'],
            'name' => $request['name'],
            'phone' => $request['phone'],
            'username' => $request['username'],
            'password' => bcrypt($request['password']),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = User::findOrFail($id);
        $this->validate($request, [
            'user_type_id' => 'required|integer|max:20',
            'name' => 'required|string|max:255',
            'phone' => 'required|string|max:15|unique:users,phone,'.$data->id.'',
            'username' => 'required|string|max:15|unique:users,username,'.$data->id.'',
        ]);

        $data->user_type_id =  $request['user_type_id'];
        $data->name = $request['name'];
        $data->phone = $request['phone'];
        $data->username = $request['username'];
        if (strlen($request['password']) > 0) {
            $data->password = bcrypt($request['password']);
        }
        $data->save();
        return ['message' => 'Data updated!'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return ['message' => 'Data Deleted!'];
    }

    public function select(Request $request)
    {
        $id = NULL;
        if ($request->paramsParse) {
            $id = explode(',', $request->paramsParse['user_id']);
        }
        $query = User::select('id AS id', 'name AS text')
                       ->where('id', '!=', 1)
                       ->where('name', 'like', '%' . $request->input('search') . '%')
                       ->orderBy('name', 'ASC');
        if ($id) {
            foreach ($id as $key) {
                $query->where('id', '!=', $key);
            }
        }
        $data = $query->paginate(15);

        return fractal()
           ->collection($data, new UserTransformer())
           ->serializeWith(new ArraySerializer())
           ->toArray();
    }
}
