<?php

namespace App\Http\Controllers;

use App\Models\UserOutlet;
use Illuminate\Http\Request;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\UserOutletTransformer;

class UserOutletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
          $columns = ['', 'user_id', 'outlet_id'];

          $length = $request->input('length');
          $column = $request->input('column');
          $dir = $request->input('dir');
          $searchValue = $request->input('search');

          $query = UserOutlet::with(['user', 'outlet'])
                               ->where('user_id', '!=', 1)
                               ->where('outlet_id', $request->input('outlet'))
                               ->orderBy($columns[$column], $dir);

          $data = $query->paginate($length);
          return ['data' => $data, 'draw' => $request->input('draw')];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|integer|max:20',
            'outlet_id' => 'required|integer|max:20',
        ]);

        return UserOutlet::create([
            'user_id' => $request['user_id'],
            'outlet_id' => $request['outlet_id'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserOutlet  $userOutlet
     * @return \Illuminate\Http\Response
     */
    public function show(UserOutlet $userOutlet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserOutlet  $userOutlet
     * @return \Illuminate\Http\Response
     */
    public function edit(UserOutlet $userOutlet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserOutlet  $userOutlet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserOutlet $userOutlet)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserOutlet  $userOutlet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $userId, $outletId)
    {
        $userOutlet = UserOutlet::where('user_id', $userId)->where('outlet_id', $outletId)->delete();
        return ['message' => 'Data Deleted!'];
    }
}
