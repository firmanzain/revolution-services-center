<?php

namespace App\Http\Controllers;

use App\Models\UserPrivilege;
use Illuminate\Http\Request;

class UserPrivilegeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        for ($i = 0; $i < sizeof($request->privilege); $i++) {
            $priv = UserPrivilege::where('user_type_id', $request->privilege[$i]['user_type_id'])
                                  ->where('menu_id', $request->privilege[$i]['menuId'])
                                  ->first();

            if ($priv) {
                $priv->create = $request->privilege[$i]['checked'];
                $priv->read = $request->privilege[$i]['checked'];
                $priv->update = $request->privilege[$i]['checked'];
                $priv->delete = $request->privilege[$i]['checked'];
                $priv->save();
            } else {
                $priv = UserPrivilege::create([
                    'user_type_id' => $request->privilege[$i]['user_type_id'],
                    'menu_id' => $request->privilege[$i]['menuId'],
                    'create' => $request->privilege[$i]['checked'],
                    'read' => $request->privilege[$i]['checked'],
                    'update' => $request->privilege[$i]['checked'],
                    'delete' => $request->privilege[$i]['checked'],
                ]);
            }
        }

        return ['message' => 'Data updated!'];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserPrivilege  $userPrivilege
     * @return \Illuminate\Http\Response
     */
    public function show(UserPrivilege $userPrivilege)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserPrivilege  $userPrivilege
     * @return \Illuminate\Http\Response
     */
    public function edit(UserPrivilege $userPrivilege)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserPrivilege  $userPrivilege
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserPrivilege $userPrivilege)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserPrivilege  $userPrivilege
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserPrivilege $userPrivilege)
    {
        //
    }
}
