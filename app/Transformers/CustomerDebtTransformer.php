<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\CustomerDebt;

class CustomerDebtTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(CustomerDebt $data)
    {
        return [
            'nominal' => $data->nominal
        ];
    }
}
