<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\AdjustmentDetail;
use App\Models\Item;

class AdjustmentDetailTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'item',
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(AdjustmentDetail $data)
    {
        return [
            'id' => $data->id,
            'quantity_old' => $data->quantity_old,
            'quantity' => $data->quantity,
            'created_at' => $data->created_at,
        ];
    }

    public function includeItem(AdjustmentDetail $data) {
        $data = Item::where('id', $data->item_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new ItemTransformer());
    }
}
