<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Service;
use App\Models\Outlet, App\Models\Customer, App\User, App\Models\Technician;

class ServiceTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'outlet', 'customer', 'user', 'technician',
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Service $data)
    {
        return [
            'id' => $data->id,
            'date' => $data->date,
            'date_estimation' => $data->date_estimation,
            'number' => $data->number,
            'customer_name' => $data->customer_name,
            'customer_phone' => $data->customer_phone,
            'item_name' => $data->item_name,
            'complaint' => $data->complaint,
            'equipment' => $data->equipment,
            'information' => $data->information,
            'status' => $data->status,
            'status_text' => ($data->status == 1) ? 'Open' : 'Close',
            'created_at' => $data->created_at,
        ];
    }

    public function includeOutlet(Service $data) {
        $data = Outlet::where('id', $data->outlet_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new OutletTransformer());
    }

    public function includeCustomer(Service $data) {
        $data = Customer::where('id', $data->customer_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new CustomerTransformer());
    }

    public function includeUser(Service $data) {
        $data = User::where('id', $data->user_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new UserTransformer());
    }

    public function includeTechnician(Service $data) {
        $data = Technician::where('id', $data->technician_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new TechnicianTransformer());
    }
}
