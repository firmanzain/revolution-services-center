<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\CustomerType;

class CustomerTypeTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(CustomerType $data)
    {
        if (strlen($data->name) > 0) {
          return [
              'id' => $data->id,
              'name' => $data->name,
              'discount' => $data->discount,
              'debt_limit' => $data->debt_limit,
          ];
        } else {
          return [
              'id' => $data->id,
              'text' => $data->text,
          ];
        }
    }
}
