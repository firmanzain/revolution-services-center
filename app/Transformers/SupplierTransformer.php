<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Supplier, App\Models\SupplierDebt;

class SupplierTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'debt'
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Supplier $data)
    {
        if (strlen($data->name) > 0) {
          return [
              'id' => $data->id,
              'name' => $data->name,
              'phone' => $data->phone,
              'address' => $data->address,
          ];
        } else {
          return [
              'id' => $data->id,
              'text' => $data->text,
          ];
        }
    }

    public function includeDebt(Supplier $data) {
        $data = SupplierDebt::where('supplier_id', $data->id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new SupplierDebtTransformer());
    }
}
