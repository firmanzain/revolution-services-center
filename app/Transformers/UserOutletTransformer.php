<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\UserOutlet, App\User, App\Models\Outlet;

class UserOutletTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'user'
    ];

    protected $defaultIncludes = [
        'outlet'
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(UserOutlet $data)
    {
        return [];
    }

    public function includeUser(UserOutlet $data) {
        $data = User::where('id', $data->user_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new UserTransformer());
    }

    public function includeOutlet(UserOutlet $data) {
        $data = Outlet::where('id', $data->outlet_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new OutletTransformer());
    }
}
