<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\SaleDetail;
use App\Models\Item;

class SaleDetailTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'item',
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(SaleDetail $data)
    {
        return [
            'id' => $data->id,
            'purchase_price' => $data->purchase_price,
            'selling_price' => $data->selling_price,
            'quantity' => $data->quantity,
            'discount' => $data->discount,
            'created_at' => $data->created_at,
        ];
    }

    public function includeItem(SaleDetail $data) {
        $data = Item::where('id', $data->item_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new ItemTransformer());
    }
}
