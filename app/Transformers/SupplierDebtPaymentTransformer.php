<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\SupplierDebtPayment;
use App\Models\Supplier, App\User;

class SupplierDebtPaymentTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'supplier', 'user',
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(SupplierDebtPayment $data)
    {
        return [
            'id' => $data->id,
            'date' => $data->date,
            'number' => $data->number,
            'debt_nominal' => $data->debt_nominal,
            'debt_nominal_payment' => $data->debt_nominal_payment,
            'information' => $data->information,
            'status' => $data->status,
            'status_text' => ($data->status == 1) ? 'Open' : 'Close',
        ];
    }

    public function includeSupplier(SupplierDebtPayment $data) {
        $data = Supplier::where('id', $data->supplier_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new SupplierTransformer());
    }

    public function includeUser(SupplierDebtPayment $data) {
        $data = User::where('id', $data->user_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new UserTransformer());
    }
}
