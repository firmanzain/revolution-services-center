<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\ItemSellingPrice;

class ItemSellingPriceTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(ItemSellingPrice $data)
    {
          return [
              'price' => $data->price
        ];
    }
}
