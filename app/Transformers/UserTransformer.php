<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\User, App\Models\UserType, App\Models\UserOutlet, App\Models\Outlet;

class UserTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'usertype', 'outlet'
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $data)
    {
        if (strlen($data->name) > 0) {
          return [
              'id' => $data->id,
              'name' => $data->name,
              'phone' => $data->phone,
              'username' => $data->username,
              'image' => $data->image,
          ];
        } else {
          return [
              'id' => $data->id,
              'text' => $data->text,
          ];
        }
    }

    public function includeUsertype(User $data) {
        $data = UserType::where('id', $data->user_type_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new UserTypeTransformer());
    }

    public function includeOutlet(User $data) {
        $data = UserOutlet::where('user_id', $data->id)->get();
        if (!$data) {
            return NULL;
        }

        return $this->collection($data, new UserOutletTransformer());
    }
}
