<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Adjustment;
use App\Models\Outlet, App\User, App\Models\AdjustmentDetail;

class AdjustmentTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'outlet', 'user', 'detail',
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Adjustment $data)
    {
        return [
            'id' => $data->id,
            'date' => $data->date,
            'number' => $data->number,
            'information' => $data->information,
            'status' => $data->status,
            'status_text' => ($data->status == 1) ? 'Open' : 'Close',
            'created_at' => $data->created_at,
        ];
    }

    public function includeOutlet(Adjustment $data) {
        $data = Outlet::where('id', $data->outlet_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new OutletTransformer());
    }

    public function includeUser(Adjustment $data) {
        $data = User::where('id', $data->user_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new UserTransformer());
    }

    public function includeDetail(Adjustment $data) {
        $data = AdjustmentDetail::where('adjustment_id', $data->id)->get();
        if (!$data) {
            return NULL;
        }

        return $this->collection($data, new AdjustmentDetailTransformer());
    }
}
