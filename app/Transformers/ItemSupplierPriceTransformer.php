<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\ItemSupplierPrice;
use App\Models\Item;

class ItemSupplierPriceTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'item'
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(ItemSupplierPrice $data)
    {
          return [
              'price' => $data->price
        ];
    }

    public function includeItem(ItemSupplierPrice $data) {
        $data = Item::where('id', $data->item_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new ItemTransformer());
    }
}
