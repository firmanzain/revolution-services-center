<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Sale;
use App\Models\Outlet, App\Models\Customer, App\Models\Technician,
    App\User, App\Models\SaleDetail, App\Models\Service;

class SaleTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'outlet', 'customer', 'technician', 'user', 'detail', 'service',
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Sale $data)
    {
        return [
            'id' => $data->id,
            'date' => $data->date,
            'number' => $data->number,
            'type' => $data->type,
            'customer_name' => $data->customer_name,
            'customer_phone' => $data->customer_phone,
            'service_item_name' => $data->service_item_name,
            'service_complaint' => $data->service_complaint,
            'service_equipment' => $data->service_equipment,
            'subtotal' => $data->subtotal,
            'fee' => $data->fee,
            'discount' => $data->discount,
            'ppn' => $data->ppn,
            'total' => $data->total,
            'payment' => $data->payment,
            'pay' => $data->pay,
            'change' => $data->change,
            'down_payment' => $data->down_payment,
            'information' => $data->information,
            'status' => $data->status,
            'status_text' => ($data->status == 1) ? 'Open' : 'Close',
            'created_at' => $data->created_at,
        ];
    }

    public function includeOutlet(Sale $data) {
        $data = Outlet::where('id', $data->outlet_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new OutletTransformer());
    }

    public function includeCustomer(Sale $data) {
        $data = Customer::where('id', $data->customer_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new CustomerTransformer());
    }

    public function includeTechnician(Sale $data) {
        $data = Technician::where('id', $data->technician_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new TechnicianTransformer());
    }

    public function includeUser(Sale $data) {
        $data = User::where('id', $data->user_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new UserTransformer());
    }

    public function includeDetail(Sale $data) {
        $data = SaleDetail::where('sale_id', $data->id)->get();
        if (!$data) {
            return NULL;
        }

        return $this->collection($data, new SaleDetailTransformer());
    }

    public function includeService(Sale $data) {
        $data = Service::where('id', $data->service_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new ServiceTransformer());
    }
}
