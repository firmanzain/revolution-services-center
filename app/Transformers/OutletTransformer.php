<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Outlet;

class OutletTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Outlet $data)
    {
        if (strlen($data->name) > 0) {
          return [
              'id' => $data->id,
              'name' => $data->name,
              'phone' => $data->phone,
              'address' => $data->address,
              'receipt_note' => $data->receipt_note,
          ];
        } else {
          return [
              'id' => $data->id,
              'text' => $data->text,
          ];
        }
    }
}
