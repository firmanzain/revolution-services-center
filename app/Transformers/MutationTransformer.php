<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Mutation;
use App\Models\Outlet, App\User, App\Models\MutationDetail;

class MutationTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'outlet_origin', 'outlet_destination', 'user', 'detail',
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Mutation $data)
    {
        return [
            'id' => $data->id,
            'date' => $data->date,
            'number' => $data->number,
            'information' => $data->information,
            'status' => $data->status,
            'status_text' => ($data->status == 1) ? 'Open' : 'Close',
            'created_at' => $data->created_at,
        ];
    }

    public function includeOutletOrigin(Mutation $data) {
        $data = Outlet::where('id', $data->outlet_origin_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new OutletTransformer());
    }

    public function includeOutletDestination(Mutation $data) {
        $data = Outlet::where('id', $data->outlet_destination_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new OutletTransformer());
    }

    public function includeUser(Mutation $data) {
        $data = User::where('id', $data->user_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new UserTransformer());
    }

    public function includeDetail(Mutation $data) {
        $data = MutationDetail::where('mutation_id', $data->id)->get();
        if (!$data) {
            return NULL;
        }

        return $this->collection($data, new MutationDetailTransformer());
    }
}
