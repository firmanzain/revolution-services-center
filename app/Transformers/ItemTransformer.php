<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Item, App\Models\Category, App\Models\Brand;
use App\Models\ItemPurchasePrice, App\Models\ItemSellingPrice;

class ItemTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'category', 'brand', 'purchase_price', 'selling_price'
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Item $data)
    {
        if (strlen($data->name) > 0) {
          return [
              'id' => $data->id,
              'code' => $data->code,
              'name' => $data->name,
              'image' => $data->image,
              'description' => $data->description,
          ];
        } else {
          return [
              'id' => $data->id,
              'text' => $data->text,
          ];
        }
    }

    public function includeCategory(Item $data) {
        $data = Category::where('id', $data->category_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new CategoryTransformer());
    }

    public function includeBrand(Item $data) {
        $data = Brand::where('id', $data->brand_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new BrandTransformer());
    }

    public function includePurchasePrice(Item $data) {
        $data = ItemPurchasePrice::where('item_id', $data->id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new ItemPurchasePriceTransformer());
    }

    public function includeSellingPrice(Item $data) {
        $data = ItemSellingPrice::where('item_id', $data->id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new ItemSellingPriceTransformer());
    }
}
