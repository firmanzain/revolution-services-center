<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\MutationDetail;
use App\Models\Item;

class MutationDetailTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'item',
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(MutationDetail $data)
    {
        return [
            'id' => $data->id,
            'quantity_old' => $data->quantity_old,
            'quantity' => $data->quantity,
            'created_at' => $data->created_at,
        ];
    }

    public function includeItem(MutationDetail $data) {
        $data = Item::where('id', $data->item_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new ItemTransformer());
    }
}
