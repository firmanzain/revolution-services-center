<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\ItemPurchasePrice;

class ItemPurchasePriceTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(ItemPurchasePrice $data)
    {
        return [
            'price' => $data->price
        ];
    }
}
