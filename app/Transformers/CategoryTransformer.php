<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Category;

class CategoryTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Category $data)
    {
        if (strlen($data->name) > 0) {
          return [
              'id' => $data->id,
              'name' => $data->name,
          ];
        } else {
          return [
              'id' => $data->id,
              'text' => $data->text,
          ];
        }
    }
}
