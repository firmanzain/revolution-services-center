<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Menu;

class MenuTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'sub'
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Menu $data)
    {
        return [
            'id' => $data->id,
            'name' => $data->name,
            'index' => $data->index,
            'parent' => $data->parent,
            'link' => $data->link,
            'icon' => $data->icon,
        ];
    }

    public function includeSub(Menu $data)
    {
      $data = Menu::where('parent', $data->id)
                    ->orderBy('index', 'asc')
                    ->get();

      return $this->collection($data, new MenuTransformer());
    }
}
