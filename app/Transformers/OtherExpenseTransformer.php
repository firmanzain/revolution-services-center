<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\OtherExpense;
use App\Models\Outlet, App\User;

class OtherExpenseTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'outlet', 'user',
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(OtherExpense $data)
    {
       return [
           'id' => $data->id,
           'date' => $data->date,
           'number' => $data->number,
           'nominal' => $data->nominal,
           'information' => $data->information,
           'created_at' => $data->created_at,
       ];
    }

    public function includeOutlet(OtherExpense $data) {
       $data = Outlet::where('id', $data->outlet_id)->first();
       if (!$data) {
           return NULL;
       }

       return $this->item($data, new OutletTransformer());
    }

    public function includeUser(OtherExpense $data) {
       $data = User::where('id', $data->user_id)->first();
       if (!$data) {
           return NULL;
       }

       return $this->item($data, new UserTransformer());
    }
}
