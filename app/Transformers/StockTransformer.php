<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Stock, App\Models\Outlet, App\Models\Item;

class StockTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'outlet', 'item',
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Stock $data)
    {
        return [
            'quantity' => $data->quantity,
        ];
    }

    public function includeOutlet(Stock $data) {
        $data = Outlet::where('id', $data->outlet_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new OutletTransformer());
    }

    public function includeItem(Stock $data) {
        $data = Item::where('id', $data->item_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new ItemTransformer());
    }
}
