<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Customer, App\Models\CustomerType, App\Models\CustomerDebt;

class CustomerTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'customertype', 'debt'
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Customer $data)
    {
        if (strlen($data->name) > 0) {
          return [
              'id' => $data->id,
              'number' => $data->number,
              'name' => $data->name,
              'phone' => $data->phone,
              'email' => $data->email,
              'address' => $data->address,
          ];
        } else {
          return [
              'id' => $data->id,
              'text' => $data->number .' - '. $data->text,
          ];
        }
    }

    public function includeCustomertype(Customer $data) {
        $data = CustomerType::where('id', $data->customer_type_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new CustomerTypeTransformer());
    }

    public function includeDebt(Customer $data) {
        $data = CustomerDebt::where('customer_id', $data->id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new CustomerDebtTransformer());
    }
}
