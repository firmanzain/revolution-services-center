<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\CustomerDebtPayment;
use App\Models\Customer, App\User;

class CustomerDebtPaymentTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'customer', 'user',
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(CustomerDebtPayment $data)
    {
        return [
            'id' => $data->id,
            'date' => $data->date,
            'number' => $data->number,
            'debt_nominal' => $data->debt_nominal,
            'debt_nominal_payment' => $data->debt_nominal_payment,
            'information' => $data->information,
            'status' => $data->status,
            'status_text' => ($data->status == 1) ? 'Open' : 'Close',
        ];
    }

    public function includeCustomer(CustomerDebtPayment $data) {
        $data = Customer::where('id', $data->customer_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new CustomerTransformer());
    }

    public function includeUser(CustomerDebtPayment $data) {
        $data = User::where('id', $data->user_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new UserTransformer());
    }
}
