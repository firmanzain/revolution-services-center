<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\SupplierDebt;

class SupplierDebtTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(SupplierDebt $data)
    {
        return [
            'nominal' => $data->nominal
        ];
    }
}
