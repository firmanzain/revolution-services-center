<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Technician;

class TechnicianTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Technician $data)
    {
        if (strlen($data->name) > 0) {
          return [
              'id' => $data->id,
              'name' => $data->name,
          ];
        } else {
          return [
              'id' => $data->id,
              'text' => $data->text,
          ];
        }
    }
}
