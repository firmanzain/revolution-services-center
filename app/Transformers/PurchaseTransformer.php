<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Purchase;
use App\Models\Outlet, App\Models\Supplier, App\User,
    App\Models\PurchaseDetail;

class PurchaseTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'outlet', 'supplier', 'user', 'detail',
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Purchase $data)
    {
       return [
           'id' => $data->id,
           'date' => $data->date,
           'number' => $data->number,
           'subtotal' => $data->subtotal,
           'discount' => $data->discount,
           'ppn' => $data->ppn,
           'total' => $data->total,
           'payment' => $data->payment,
           'information' => $data->information,
           'status' => $data->status,
           'status_text' => ($data->status == 1) ? 'Open' : 'Close',
           'created_at' => $data->created_at,
       ];
    }

    public function includeOutlet(Purchase $data) {
       $data = Outlet::where('id', $data->outlet_id)->first();
       if (!$data) {
           return NULL;
       }

       return $this->item($data, new OutletTransformer());
    }

    public function includeSupplier(Purchase $data) {
       $data = Supplier::where('id', $data->supplier_id)->first();
       if (!$data) {
           return NULL;
       }

       return $this->item($data, new SupplierTransformer());
    }

    public function includeUser(Purchase $data) {
       $data = User::where('id', $data->user_id)->first();
       if (!$data) {
           return NULL;
       }

       return $this->item($data, new UserTransformer());
    }

    public function includeDetail(Purchase $data) {
       $data = PurchaseDetail::where('purchase_id', $data->id)->get();
       if (!$data) {
           return NULL;
       }

       return $this->collection($data, new PurchaseDetailTransformer());
    }
}
