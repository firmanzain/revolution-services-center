<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\PurchaseDetail;
use App\Models\Purchase;
use App\Models\Item;

class PurchaseDetailTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'item',
    ];

    protected $availableIncludes = [
        'header',
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(PurchaseDetail $data)
    {
        return [
            'id' => $data->id,
            'price' => $data->price,
            'quantity' => $data->quantity,
            'discount' => $data->discount,
            'created_at' => $data->created_at,
        ];
    }

    public function includeItem(PurchaseDetail $data) {
        $data = Item::where('id', $data->item_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new ItemTransformer());
    }

    public function includeHeader(PurchaseDetail $data) {
        $data = Purchase::where('id', $data->purchase_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new PurchaseTransformer());
    }
}
