<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return view('layouts.app');
})->name('login');

Route::get('test', 'GlobalController@test')->name('test');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('refreshtoken', 'GlobalController@refreshCsrfToken')->name('token.refresh');
Route::get('auth/check', 'GlobalController@authCheck')->name('auth.check');

Route::group(['middleware' => ['auth']], function () {
  Route::group(['prefix' => 'method/'], function () {
    Route::resource('/dashboard', 'DashboardController')->only([
      'index'
    ]);
    Route::group(['prefix' => '/access'], function () {
      Route::resource('/menus', 'MenuController')->only([
        'index'
      ]);
      Route::resource('/user-types', 'UserTypeController')->only([
        'index', 'store', 'update', 'destroy'
      ]);
      Route::get('user-types/select', 'UserTypeController@select')->name('user-types.select');
      Route::resource('/user-privileges', 'UserPrivilegeController')->only([
        'store'
      ]);
      Route::resource('/users', 'UserController')->only([
        'index', 'store', 'update', 'destroy'
      ]);
      Route::get('users/select', 'UserController@select')->name('users.select');
    });
    Route::group(['prefix' => '/master'], function () {
      Route::resource('/outlets', 'OutletController')->only([
        'index', 'store', 'update', 'destroy'
      ]);
      Route::get('outlets/select', 'OutletController@select')->name('outlets.select');
      Route::resource('/user-outlets', 'UserOutletController')->only([
        'index', 'store', 'update'
      ]);
      Route::delete('user-outlets/{userId}/{outletId}', 'UserOutletController@destroy')->name('outlets.destroy');
      Route::resource('/categories', 'CategoryController')->only([
        'index', 'store', 'update', 'destroy'
      ]);
      Route::get('categories/select', 'CategoryController@select')->name('categories.select');
      Route::resource('/brands', 'BrandController')->only([
        'index', 'store', 'update', 'destroy'
      ]);
      Route::get('brands/select', 'BrandController@select')->name('brands.select');
      Route::resource('/items', 'ItemController')->only([
        'index', 'store', 'update', 'destroy'
      ]);
      Route::get('items/autocomplete', 'ItemController@autocomplete')->name('items.autocomplete');
      Route::get('items/select', 'ItemController@select')->name('items.select');
      Route::get('suppliers/select', 'SupplierController@select')->name('suppliers.select');
      Route::get('suppliers/price/{supplierId}/{itemId}', 'SupplierController@price')->name('suppliers.price');
      Route::resource('/suppliers', 'SupplierController')->only([
        'index', 'store', 'update', 'destroy', 'show'
      ]);
      Route::resource('/item-supplier-prices', 'ItemSupplierPriceController')->only([
        'index', 'store', 'update'
      ]);
      Route::delete('item-supplier-prices/{userId}/{outletId}', 'ItemSupplierPriceController@destroy')->name('outlets.destroy');
      Route::resource('/customer-types', 'CustomerTypeController')->only([
        'index', 'store', 'update', 'destroy'
      ]);
      Route::get('customer-types/select', 'CustomerTypeController@select')->name('customer-types.select');
      Route::get('customers/select', 'CustomerController@select')->name('customers.select');
      Route::resource('/customers', 'CustomerController')->only([
        'index', 'store', 'update', 'destroy', 'show'
      ]);
      Route::resource('/technicians', 'TechnicianController')->only([
        'index', 'store', 'update', 'destroy'
      ]);
      Route::get('technicians/select', 'TechnicianController@select')->name('technicians.select');
    });
    Route::group(['prefix' => '/inventory'], function () {
      Route::get('stocks/excel', 'StockController@stockExcel')->name('stocks.stockExcel');
      Route::get('stocks/{outletId}/{itemId?}', 'StockController@detail')->name('stocks.detail');
      Route::resource('/stocks', 'StockController')->only([
        'index'
      ]);
      Route::resource('/mutations', 'MutationController')->only([
        'index', 'store', 'update', 'destroy', 'show'
      ]);
      Route::resource('/adjustments', 'AdjustmentController')->only([
        'index', 'store', 'update', 'destroy', 'show'
      ]);
      Route::get('purchases/print/{type}/{id}', 'PurchaseController@print')->name('purchases.print');
      Route::resource('/purchases', 'PurchaseController')->only([
        'index', 'store', 'update', 'destroy', 'show'
      ]);
    });
    Route::group(['prefix' => '/transaction'], function () {
      Route::get('services/autocomplete', 'ServiceController@autocomplete')->name('services.autocomplete');
      Route::get('services/print/{id}', 'ServiceController@print')->name('services.print');
      Route::get('services/excel', 'ServiceController@excel')->name('services.excel');
      Route::resource('/services', 'ServiceController')->only([
        'index', 'store', 'update', 'destroy', 'show'
      ]);
      Route::get('sales/print/{type}/{id}', 'SaleController@print')->name('sales.print');
      Route::resource('/sales', 'SaleController')->only([
        'index', 'store', 'update', 'destroy', 'show'
      ]);
    });
    Route::group(['prefix' => '/finance'], function () {
      Route::resource('/debt-payments', 'SupplierDebtPaymentController')->only([
        'index', 'store', 'update', 'destroy', 'show'
      ]);
      Route::resource('/receivable-payments', 'CustomerDebtPaymentController')->only([
        'index', 'store', 'update', 'destroy', 'show'
      ]);
      Route::resource('/operationals', 'OperationalController')->only([
        'index', 'store', 'update', 'destroy', 'show'
      ]);
      Route::resource('/other-expenses', 'OtherExpenseController')->only([
        'index', 'store', 'update', 'destroy', 'show'
      ]);
    });
    Route::group(['prefix' => '/report'], function () {
      Route::group(['prefix' => '/services'], function () {
        Route::get('/count', 'ServiceController@reportCount')->name('services.reportCount');
        Route::get('/technicians', 'ServiceController@technician')->name('services.technician');
        Route::get('/technicians/excel', 'ServiceController@technicianExcel')->name('services.technicianExcel');
      });
      Route::group(['prefix' => '/purchases'], function () {
        Route::get('/count', 'PurchaseController@reportCount')->name('purchases.reportCount');
        Route::get('/excel', 'PurchaseController@reportExcel')->name('purchases.reportExcel');
      });
      Route::group(['prefix' => '/sales'], function () {
        Route::get('/count', 'SaleController@reportCount')->name('sales.reportCount');
        Route::get('/excel', 'SaleController@reportExcel')->name('sales.reportExcel');
      });
      Route::group(['prefix' => '/debt-payments'], function () {
        Route::get('/count', 'SupplierDebtPaymentController@reportCount')->name('debt-payments.reportCount');
        Route::get('/transaction', 'SupplierDebtPaymentController@reportTransaction')->name('debt-payments.reportTransaction');
        Route::get('/transaction/excel', 'SupplierDebtPaymentController@reportTransactionExcel')->name('debt-payments.reportTransactionExcel');
      });
      Route::group(['prefix' => '/receivable-payments'], function () {
        Route::get('/count', 'CustomerDebtPaymentController@reportCount')->name('receivable-payments.reportCount');
        Route::get('/transaction', 'CustomerDebtPaymentController@reportTransaction')->name('receivable-payments.reportTransaction');
        Route::get('/transaction/excel', 'CustomerDebtPaymentController@reportTransactionExcel')->name('debt-payments.reportTransactionExcel');
      });
      Route::group(['prefix' => '/stock-cards'], function () {
        Route::get('/', 'StockController@stockcard')->name('stocks.stockcard');
        Route::get('/count', 'StockController@reportCount')->name('stocks.reportCount');
        Route::get('/excel', 'StockController@stockcardExcel')->name('stocks.stockcardExcel');
      });
      Route::group(['prefix' => '/statistics'], function () {
        Route::get('/count', 'DashboardController@reportCount')->name('dashboard.reportCount');
        Route::get('/transaction', 'DashboardController@transactionDetail')->name('dashboard.transactionDetail');
        Route::get('/excel', 'DashboardController@transactionExcel')->name('dashboard.transactionExcel');
      });
    });
  });
});

Route::get('/{vue_capture?}', function () {
  return view('layouts.app');
})->where('vue_capture', '[\/\w\.-]*');
