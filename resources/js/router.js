window.Vue = require('vue');
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const Login = () => import(
  /* webpackChunkName: "js/login" */ './components/auth/Login.vue')
const Dashboard = () => import(
  /* webpackChunkName: "js/dashboard" */ './components/backoffice/Dashboard.vue')
const UserType = () => import(
  /* webpackChunkName: "js/user-type" */ './components/backoffice/access/UserType.vue')
const User = () => import(
  /* webpackChunkName: "js/user" */ './components/backoffice/access/User.vue')
const Outlet = () => import(
  /* webpackChunkName: "js/outlet" */ './components/backoffice/master/Outlet.vue')
const Category = () => import(
  /* webpackChunkName: "js/category" */ './components/backoffice/master/Category.vue')
const Brand = () => import(
  /* webpackChunkName: "js/category" */ './components/backoffice/master/Brand.vue')
const Item = () => import(
  /* webpackChunkName: "js/item" */ './components/backoffice/master/Item.vue')
const Supplier = () => import(
  /* webpackChunkName: "js/supplier" */ './components/backoffice/master/Supplier.vue')
const CustomerType = () => import(
  /* webpackChunkName: "js/customer-type" */ './components/backoffice/master/CustomerType.vue')
const Customer = () => import(
  /* webpackChunkName: "js/customer" */ './components/backoffice/master/Customer.vue')
const Technician = () => import(
  /* webpackChunkName: "js/technician" */ './components/backoffice/master/Technician.vue')
const Stock = () => import(
  /* webpackChunkName: "js/stock" */ './components/backoffice/inventory/Stock.vue')
const Purchase = () => import(
  /* webpackChunkName: "js/purchase" */ './components/backoffice/inventory/Purchase.vue')
const PurchaseForm = () => import(
  /* webpackChunkName: "js/purchase-form" */ './components/backoffice/inventory/PurchaseForm.vue')
const Mutation = () => import(
  /* webpackChunkName: "js/mutation" */ './components/backoffice/inventory/Mutation.vue')
const MutationForm = () => import(
  /* webpackChunkName: "js/mutation-form" */ './components/backoffice/inventory/MutationForm.vue')
const Adjustment = () => import(
  /* webpackChunkName: "js/adjustment" */ './components/backoffice/inventory/Adjustment.vue')
const AdjustmentForm = () => import(
  /* webpackChunkName: "js/adjustment-form" */ './components/backoffice/inventory/AdjustmentForm.vue')
const Sale = () => import(
  /* webpackChunkName: "js/sale" */ './components/backoffice/transaction/Sale.vue')
const SaleForm = () => import(
  /* webpackChunkName: "js/sale-form" */ './components/backoffice/transaction/SaleForm.vue')
const Service = () => import(
  /* webpackChunkName: "js/service" */ './components/backoffice/transaction/Service.vue')
const ServiceForm = () => import(
  /* webpackChunkName: "js/service-form" */ './components/backoffice/transaction/ServiceForm.vue')
const DebtPayment = () => import(
  /* webpackChunkName: "js/debt-payments" */ './components/backoffice/finance/DebtPayment.vue')
const DebtPaymentForm = () => import(
  /* webpackChunkName: "js/debt-payments-form" */ './components/backoffice/finance/DebtPaymentForm.vue')
const ReceivablePayment = () => import(
  /* webpackChunkName: "js/receivable-payments" */ './components/backoffice/finance/ReceivablePayment.vue')
const ReceivablePaymentForm = () => import(
  /* webpackChunkName: "js/receivable-payments-form" */ './components/backoffice/finance/ReceivablePaymentForm.vue')
const Operational = () => import(
  /* webpackChunkName: "js/operationals" */ './components/backoffice/finance/Operational.vue')
const OperationalForm = () => import(
  /* webpackChunkName: "js/operationals-form" */ './components/backoffice/finance/OperationalForm.vue')
const OtherExpense = () => import(
  /* webpackChunkName: "js/other-expenses" */ './components/backoffice/finance/OtherExpense.vue')
const OtherExpenseForm = () => import(
  /* webpackChunkName: "js/other-expenses-form" */ './components/backoffice/finance/OtherExpenseForm.vue')
const ReportPurchases = () => import(
  /* webpackChunkName: "js/report-purchases" */ './components/backoffice/report/Purchases.vue')
const ReportServices = () => import(
  /* webpackChunkName: "js/report-services" */ './components/backoffice/report/Services.vue')
const ReportSales = () => import(
  /* webpackChunkName: "js/report-sales" */ './components/backoffice/report/Sales.vue')
const ReportDebtPayments = () => import(
  /* webpackChunkName: "js/report-debt-payments" */ './components/backoffice/report/DebtPayments.vue')
const ReportReceivablePayments = () => import(
  /* webpackChunkName: "js/report-receivable-payments" */ './components/backoffice/report/ReceivablePayments.vue')
const ReportStockCards = () => import(
  /* webpackChunkName: "js/report-stock-cards" */ './components/backoffice/report/StockCards.vue')
const ReportStatictics = () => import(
  /* webpackChunkName: "js/report-statistics" */ './components/backoffice/report/Statistics.vue')

let routes = [
  {
    path: '/',
    name: 'login',
    component: Login,
    meta: {
      title: 'Login | ',
    }
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard,
    meta: {
      title: 'Dashboard | ',
    }
  },
  {
    path: '/access/user-types',
    name: 'user-types',
    component: UserType,
    meta: {
      title: 'Tipe Pengguna | ',
    }
  },
  {
    path: '/access/users',
    name: 'users',
    component: User,
    meta: {
      title: 'Pengguna | ',
    }
  },
  {
    path: '/master/outlets',
    name: 'outlets',
    component: Outlet,
    meta: {
      title: 'Toko / Gudang | ',
    }
  },
  {
    path: '/master/categories',
    name: 'categories',
    component: Category,
    meta: {
      title: 'Kategori | ',
    }
  },
  {
    path: '/master/brands',
    name: 'brands',
    component: Brand,
    meta: {
      title: 'Merek | ',
    }
  },
  {
    path: '/master/items',
    name: 'items',
    component: Item,
    meta: {
      title: 'Barang | ',
    }
  },
  {
    path: '/master/suppliers',
    name: 'suppliers',
    component: Supplier,
    meta: {
      title: 'Pemasok | ',
    }
  },
  {
    path: '/master/customer-types',
    name: 'customer-types',
    component: CustomerType,
    meta: {
      title: 'Tipe Pelanggan | ',
    }
  },
  {
    path: '/master/customers',
    name: 'customers',
    component: Customer,
    meta: {
      title: 'Pelanggan | ',
    }
  },
  {
    path: '/master/technicians',
    name: 'technicians',
    component: Technician,
    meta: {
      title: 'Teknisi | ',
    }
  },
  {
    path: '/inventory/stocks',
    name: 'stocks',
    component: Stock,
    meta: {
      title: 'Stok Barang | ',
    }
  },
  {
    path: '/inventory/purchases',
    name: 'purchases',
    component: Purchase,
    meta: {
      title: 'Pembelian | ',
    }
  },
  {
    path: '/inventory/purchases-form/:id?',
    name: 'purchases-form',
    component: PurchaseForm,
    meta: {
      title: 'Pembelian Formulir | ',
    }
  },
  {
    path: '/inventory/mutations',
    name: 'mutations',
    component: Mutation,
    meta: {
      title: 'Mutasi | ',
    }
  },
  {
    path: '/inventory/mutations-form/:id?',
    name: 'mutations-form',
    component: MutationForm,
    meta: {
      title: 'Mutasi Formulir | ',
    }
  },
  {
    path: '/inventory/adjustments',
    name: 'adjustments',
    component: Adjustment,
    meta: {
      title: 'Penyesuaian | ',
    }
  },
  {
    path: '/inventory/adjustments-form/:id?',
    name: 'adjustments-form',
    component: AdjustmentForm,
    meta: {
      title: 'Penyesuaian Formulir | ',
    }
  },
  {
    path: '/transaction/sales',
    name: 'sales',
    component: Sale,
    meta: {
      title: 'Penjualan | ',
    }
  },
  {
    path: '/transaction/sales-form/:id?',
    name: 'sales-form',
    component: SaleForm,
    meta: {
      title: 'Penjualan Formulir | ',
    }
  },
  {
    path: '/transaction/services',
    name: 'services',
    component: Service,
    meta: {
      title: 'Servis | ',
    }
  },
  {
    path: '/transaction/services-form/:id?',
    name: 'services-form',
    component: ServiceForm,
    meta: {
      title: 'Servis Formulir | ',
    }
  },
  {
    path: '/finance/debt-payments',
    name: 'debt-payments',
    component: DebtPayment,
    meta: {
      title: 'Pembayaran Hutang | ',
    }
  },
  {
    path: '/finance/debt-payments-form/:id?',
    name: 'debt-payments-form',
    component: DebtPaymentForm,
    meta: {
      title: 'Pembayaran Hutang | ',
    }
  },
  {
    path: '/finance/receivable-payments',
    name: 'receivable-payments',
    component: ReceivablePayment,
    meta: {
      title: 'Pembayaran Piutang | ',
    }
  },
  {
    path: '/finance/receivable-payments-form/:id?',
    name: 'receivable-payments-form',
    component: ReceivablePaymentForm,
    meta: {
      title: 'Pembayaran Piutang | ',
    }
  },
  {
    path: '/finance/operationals',
    name: 'operationals',
    component: Operational,
    meta: {
      title: 'Operasional | ',
    }
  },
  {
    path: '/finance/operationals-form/:id?',
    name: 'operationals-form',
    component: OperationalForm,
    meta: {
      title: 'Operasional | ',
    }
  },
  {
    path: '/finance/other-expenses',
    name: 'other-expenses',
    component: OtherExpense,
    meta: {
      title: 'Pengeluaran Lain | ',
    }
  },
  {
    path: '/finance/other-expenses-form/:id?',
    name: 'other-expenses-form',
    component: OtherExpenseForm,
    meta: {
      title: 'Pengeluaran Lain | ',
    }
  },
  {
    path: '/report/purchases',
    name: 'report-purchases',
    component: ReportPurchases,
    meta: {
      title: 'Laporan Pembelian | ',
    }
  },
  {
    path: '/report/services',
    name: 'report-services',
    component: ReportServices,
    meta: {
      title: 'Laporan Servis | ',
    }
  },
  {
    path: '/report/sales',
    name: 'report-sales',
    component: ReportSales,
    meta: {
      title: 'Laporan Penjualan | ',
    }
  },
  {
    path: '/report/debt-payments',
    name: 'report-debt-payments',
    component: ReportDebtPayments,
    meta: {
      title: 'Laporan Hutang | ',
    }
  },
  {
    path: '/report/receivable-payments',
    name: 'report-receivable-payments',
    component: ReportReceivablePayments,
    meta: {
      title: 'Laporan Piutang | ',
    }
  },
  {
    path: '/report/stock-cards',
    name: 'report-stock-cards',
    component: ReportStockCards,
    meta: {
      title: 'Laporan Kartu Stok | ',
    }
  },
  {
    path: '/report/statistics',
    name: 'report-statistics',
    component: ReportStatictics,
    meta: {
      title: 'Laporan Statistik | ',
    }
  },
]

const router = new VueRouter({
  mode: 'history',
  routes, // short for `routes: routes`
  scrollBehavior: function(to) {
      if (to.hash) {
          return {selector: to.hash}
      } else {
          return { x: 0, y: 0 }
      }
  },
})

export default router
