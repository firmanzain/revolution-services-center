window.Vue = require('vue');
import numeral from 'numeral'
import moment from 'moment'

const globalMethods = Vue.mixin({
    methods: {
        refreshTokens() {
          return new Promise((resolve, reject) => {
              axios.get('/refreshtoken')
                  .then( response => {
                      window.axios.defaults.headers.common['X-CSRF-TOKEN'] = response.data.csrfToken;
                      resolve(response);
                  })
                  .catch( error => {
                      this.showErrorMessage(error.message);
                      reject(error);
                  });
          });
        },
        async checkAuth() {

          try {
              let response = await axios.get(`${this.globalVar.publicPath}auth/check`, {})

              if (response.data.data.user) {
                if (this.$route.path == "/" || this.$route.path == "/login") {
                  // this.$router.replace('/dashboard')
                  window.location = '/dashboard'
                }
                this.globalVar.session = response.data.data.user;
                this.globalVar.menu = response.data.data.menu;
              } else {
                if (this.$route.path != '/' && this.$route.path != "/login") {
                  this.$router.replace('/')
                  // window.location = '/'
                }
              }
          } catch (error) {
          }
        },
        doLogout() {
            axios.post(`${this.globalVar.publicPath}logout`,{})
            .then(function (response){
             Toast.fire({
               type: 'success',
               title: 'Session destroy'
             })
             window.location = '/'
            })
            .catch( function (error){
              Swal.fire(
                'Alert!',
                'Something went wrong.',
                'warning'
              )
              this.refreshTokens()
            });
        },
        isNumber(event) {
          event = (event) ? event : window.event;
          var charCode = (event.which) ? event.which : event.keyCode;
          if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
            event.preventDefault();
          } else {
            return true;
          }
        },
        formatNumber(value) {
            let formatted = numeral(value).format('0,0')
            return formatted
        },
        parseNumber(value) {
            value = value.toString()
            let parse = parseInt(value.replace(/\,/g, ""))
            return parse
        },
        formatDateDefault(date) {
            return moment(date).format('YYYY-MM-DD');
        },
        formattedAutocompleteItems(result) {
            return ' (' + result.code + ') ' + result.name
        },
        statusLabel(status) {
            if (status == 0) {
                return '<span class="badge badge-danger">Close</span>';
            } else {
                return '<span class="badge badge-success">Open</span>';
            }
        },
        paymentLabel(payment) {
            if (payment == 1) {
                return '<span class="badge badge-success">Tunai</span>';
            } else if (payment == 2) {
                return '<span class="badge badge-danger">Hutang</span>';
            } else {
                return '';
            }
        },
        openSaleForm() {
            window.open("/transaction/sales-form", "_blank")
        },
        actionShortKey(event) {
            switch (event.srcKey) {
                case 'inputItems':
                    if (document.getElementById('autocompleteItems')) {
                        $("#autocompleteItems input").focus()
                    }
                    break;
                case 'inputCustomer':
                    if (document.getElementById('customer_id')) {
                        $("#customer_id").select2('open')
                    }
                    break;
                case 'showShortkey':
                    if (document.getElementById('shotkeyinfo')) {
                        if ($("#shotkeyinfo").hasClass('d-none')) {
                            $("#shotkeyinfo").removeClass('d-none')
                        } else {
                            $("#shotkeyinfo").addClass('d-none')
                        }
                    }
                    break;
            }
        },
    },
    filters: {
        formatNumber: function (value) {
            let formatted = numeral(value).format('0,0')
            return formatted
        },
        formatDate: function (date) {
            return moment(date).format("DD/MM/YYYY");
        },
        typeSaleLabel: function (status) {
            if (status == 1) {
                return 'Barang';
            } else {
                return 'Jasa';
            }
        },
    },
})

export default globalMethods
