require('./bootstrap');
window.Vue = require('vue');

import VueProgressBar from 'vue-progressbar'
Vue.use(VueProgressBar, {
  color: 'rgb(143, 255, 199)',
  failedColor: 'red',
  height: '2px'
})

import Swal from 'sweetalert2'
window.Swal = Swal;
const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 400
});
window.Toast = Toast;

window.Fire = new Vue();

Vue.use(require('vue-shortkey'))

import router from './router'
require('./method')
import select2 from 'select2'

Vue.component('loader', require('./components/Loader.vue').default);

Vue.component('loader', require('./components/Loader.vue').default);
Vue.component(
  'datatable',
  () => import(/* webpackChunkName: "js/datatable" */ './components/backoffice/reusable/Datatable/Datatable.vue')
)
Vue.component(
  'pagination',
  () => import(/* webpackChunkName: "js/pagination" */ './components/backoffice/reusable/Datatable/Pagination.vue')
)
Vue.component(
  'select2',
  () => import(/* webpackChunkName: "js/select2" */ './components/backoffice/reusable/Select2/SelectComponent.vue')
)

const app = new Vue({
    el: '#app',
    router
});
