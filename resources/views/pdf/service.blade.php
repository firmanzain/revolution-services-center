<!DOCTYPE html>
<html lang="ar">
<!-- <html lang="ar"> for arabic only -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>
        {{ $number }} | {{ config('app.name', 'Laravel') }}
    </title>
    <style>
        @page {
        }
        @media print {
          html, body {
            margin: 0;
          }
        }
        html {
            direction: rtl;
        }
        html,body {
            margin:5px;
            margin-left:20px;
            margin-right:10px;
            padding:0;
            font-size: 14px;
            font-family: Tahoma, Geneva, sans-serif;
        }
        #printContainer {
            margin: auto;
            text-align: justify;
        }

        .text-left {
            text-align: left;
        }
        .text-center {
            text-align: center;
        }
        .text-right {
            text-align: right;
        }
        .dashed{
            border: 1px dashed #000000;
            border-style: none none dashed;
            color: #fff;
            background-color: #fff;
        }
        .bold {
            border: 1px solid #000000;
            color: #fff;
            background-color: #fff;
        }
        .header {
        }
        .header-title {
            text-transform: uppercase;
            font-weight: bold;
            font-size: 16px;
        }
        .text-bold {
            font-weight: bold;
        }
        .text-uppercase {
            text-transform: uppercase;
        }
        .border1 {
            border: 1px solid #000000;
        }
        .pd5 {
            padding: 5px;
        }
        .mt5 {
            margin-top: 5px;
        }
    </style>
</head>
<body>

    <div class="header text-left">
        <!-- <p class="header-title">{{ config('app.name', 'Laravel') }}</p> -->
        <p class="header-title">NOTA SERVIS</p>
        <table style="width:100%;">
            <tr>
                <td style="width:75%;">
                    {{ $outlet['name'] }} <br>
                    {{ $outlet['address'] }} <br>
                    Telp. {{ $outlet['phone'] }}
                </td>
                <td class="text-right text-bold text-uppercase" style="width:25%; font-size: 16px;">
                    <br>
                    NOTA SERVIS <br>
                    {{ $number }}
                </td>
            </tr>
        </table>
        <hr class="bold">
    </div>

    <div class="body">
        <table style="width:100%;">
            @if ($customer['id'] != 1)
                <tr>
                    <td style="width: 25%;">
                        <b>Kode Member</b>
                    </td>
                    <td style="width: 30%;">
                        <b>:</b> {{ $customer['number'] }}
                    </td>
                    <td style="width: 25%;">
                        <b>Tanggal</b>
                    </td>
                    <td style="width: 20%;">
                        <b>:</b> {{ Carbon\Carbon::parse($date)->format('d/m/Y') }}
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%;">
                        <b>Nama</b>
                    </td>
                    <td style="width: 30%;">
                        <b>:</b> {{ $customer_name }}
                    </td>
                    <td style="width: 25%;">
                        <b>Estimasi Selesai</b>
                    </td>
                    <td style="width: 20%;">
                        <b>:</b> {{ Carbon\Carbon::parse($date_estimation)->format('d/m/Y') }}
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%;">
                        <b>No. Telp</b>
                    </td>
                    <td style="width: 75%;">
                        <b>:</b> {{ $customer_phone }}
                    </td>
                    @if ($status != 1)
                    <td style="width: 25%;">
                        <b>Teknisi</b>
                    </td>
                    <td style="width: 20%;">
                        <b>:</b> {{ $technician['name'] }}
                    </td>
                    @endif
                </tr>
            @else
                <tr>
                    <td style="width: 25%;">
                        <b>Nama</b>
                    </td>
                    <td style="width: 25%;">
                        <b>:</b> {{ $customer_name }}
                    </td>
                    @if ($customer_phone)
                        <td style="width: 25%;">
                            <b>No. Telp</b>
                        </td>
                        <td style="width: 25%;">
                            <b>:</b> {{ $customer_phone }}
                        </td>
                    @else
                        <td style="width: 25%;">
                        </td>
                        <td style="width: 25%;">
                        </td>
                    @endif
                </tr>
                <tr>
                    <td style="width: 25%;">
                        <b>Nama</b>
                    </td>
                    <td style="width: 30%;">
                        <b>:</b> {{ $customer_name }}
                    </td>
                    <td style="width: 25%;">
                        <b>Tanggal</b>
                    </td>
                    <td style="width: 20%;">
                        <b>:</b> {{ Carbon\Carbon::parse($date)->format('d/m/Y') }}
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%;">
                    </td>
                    <td style="width: 30%;">
                    </td>
                    <td style="width: 25%;">
                        <b>Estimasi Selesai</b>
                    </td>
                    <td style="width: 20%;">
                        <b>:</b> {{ Carbon\Carbon::parse($date_estimation)->format('d/m/Y') }}
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%;">
                        <b>No. Telp</b>
                    </td>
                    <td style="width: 75%;">
                        <b>:</b> {{ $customer_phone }}
                    </td>
                    @if ($status != 1)
                    <td style="width: 25%;">
                        <b>Teknisi</b>
                    </td>
                    <td style="width: 20%;">
                        <b>:</b> {{ $technician['name'] }}
                    </td>
                    @endif
                </tr>
            @endif
            <tr>
                <td style="width: 25%;">
                    <b>Barang</b>
                </td>
                <td style="width: 75%;">
                    <b>:</b> {{ $item_name }}
                </td>
            </tr>
        </table>
        <table class="border1 mt5" style="width:100%;">
            <tr>
                <td style="height:80px; vertical-align:text-top;">
                    <b>Keluhan : </b><br>
                    {{ $complaint }}
                </td>
            </tr>
        </table>
        <table class="border1 mt5" style="width:100%;">
            <tr>
                <td style="height:80px; vertical-align:text-top;">
                    <b>Kelengkapan : </b><br>
                    {{ $equipment }}
                </td>
            </tr>
        </table>
    </div>

    <div class="footer text-center">
        <hr class="bold">
        <table class="" style="width:100%;">
            <tr>
                <td style="width:50%; height:100px; vertical-align:text-top;">
                    <b>Keterangan : </b><br>
                    {{ $information }}
                </td>
                <td class="text-center" style="width:25%; height:100px; vertical-align:text-top;">
                    <b>Diserahkan,</b>
                </td>
                <td class="text-center" style="width:25%; height:100px; vertical-align:text-top;">
                    <b>Penerima,</b>
                </td>
            </tr>
        </table>
    </div>

</body>
</html>
