<!DOCTYPE html>
<html lang="ar">
<!-- <html lang="ar"> for arabic only -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>
        {{ $number }} | {{ config('app.name', 'Laravel') }}
    </title>
    <style>
        @page {
        }
        @media print {
          html, body {
            margin: 0;
          }
        }
        html {
            direction: rtl;
        }
        html,body {
            margin:5px;
            margin-left:20px;
            margin-right:10px;
            padding:0;
            font-size: 14px;
            font-family: Tahoma, Geneva, sans-serif;
            /* line-height: 0.7; */
            /* letter-spacing: 1.2px; */
        }
        #printContainer {
            margin: auto;
            text-align: justify;
        }

        .text-left {
            text-align: left;
        }
        .text-center {
            text-align: center;
        }
        .text-right {
            text-align: right;
        }
        .dashed{
            border: 1px dashed #000000;
            border-style: none none dashed;
            color: #fff;
            background-color: #fff;
        }
        .bold {
            border: 1px solid #000000;
            color: #fff;
            background-color: #fff;
        }
        .header {
        }
        .header-title {
            text-transform: uppercase;
            font-weight: bold;
            font-size: 16px;
        }
        .text-bold {
            font-weight: bold;
        }
        .text-uppercase {
            text-transform: uppercase;
        }
        .border1 {
            border: 1px solid #000000;
        }
        .pd5 {
            padding: 5px;
        }
        .mt5 {
            margin-top: 5px;
        }
    </style>
</head>
<body>

    <div class="header text-left">
        <!-- <p class="header-title">{{ config('app.name', 'Laravel') }}</p> -->
        <p class="header-title">NOTA PENJUALAN</p>
        <table style="width:100%;">
            <tr>
                <td style="width:75%;">
                    {{ $outlet['name'] }} <br>
                    {{ $outlet['address'] }} <br>
                    Telp. {{ $outlet['phone'] }}
                </td>
                <td class="text-right text-bold text-uppercase" style="width:25%; font-size: 16px;">
                    <br>
                    NOTA PENJUALAN <br>
                    {{ $number }}
                </td>
            </tr>
        </table>
        <hr class="bold">
    </div>

    <div class="body">
        <table style="width:100%;">
            @if ($customer['id'] != 1)
                <tr>
                    <td style="width: 25%;">
                        <b>Kode Member</b>
                    </td>
                    <td style="width: 25%;">
                        <b>:</b> {{ $customer['number'] }}
                    </td>
                    <td style="width: 25%;">
                        <b>Nama</b>
                    </td>
                    <td style="width: 25%;">
                        <b>:</b> {{ $customer_name }}
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%;">
                        <b>Tipe</b>
                    </td>
                    <td style="width: 25%;">
                        <b>:</b> {{ $customer['customertype']['name'] }}
                    </td>
                    <td style="width: 25%;">
                        <b>No. Telp</b>
                    </td>
                    <td style="width: 25%;">
                        <b>:</b> {{ $customer_phone }}
                    </td>
                </tr>
            @else
                <tr>
                    <td style="width: 25%;">
                        <b>Nama</b>
                    </td>
                    <td style="width: 25%;">
                        <b>:</b> {{ $customer_name }}
                    </td>
                    @if ($customer_phone)
                        <td style="width: 25%;">
                            <b>No. Telp</b>
                        </td>
                        <td style="width: 25%;">
                            <b>:</b> {{ $customer_phone }}
                        </td>
                    @else
                        <td style="width: 25%;">
                        </td>
                        <td style="width: 25%;">
                        </td>
                    @endif
                </tr>
            @endif
            <tr>
                <td style="width: 25%;">
                    <b>Tanggal</b>
                </td>
                <td style="width: 25%;">
                    <b>:</b> {{ Carbon\Carbon::parse($date)->format('d/m/Y') }} {{ Carbon\Carbon::parse($created_at)->format('H:i:s') }}
                </td>
                <td style="width: 25%;">
                    <b>Kasir</b>
                </td>
                <td style="width: 25%;">
                    <b>:</b> {{ $user['name'] }}
                </td>
            </tr>
            @if ($type == 2)
                <tr>
                    <td style="width: 25%;">
                        <b>Barang</b>
                    </td>
                    <td style="width: 25%;">
                        <b>:</b> {{ $service_item_name }}
                    </td>
                    <td style="width: 25%;">
                    </td>
                    <td style="width: 25%;">
                    </td>
                </tr>
            @endif
        </table>
        @if ($type == 2)
            <table class="border1 mt5" style="width:100%;">
                <tr>
                    <td style="height:80px; vertical-align:text-top;">
                        <b>Keluhan : </b><br>
                        {{ $service_complaint }}
                    </td>
                </tr>
            </table>
            <table class="border1 mt5" style="width:100%;">
                <tr>
                    <td style="height:80px; vertical-align:text-top;">
                        <b>Kelengkapan : </b><br>
                        {{ $service_equipment }}
                    </td>
                </tr>
            </table>
        @endif
        <hr class="bold">
        <table class="text-bold" style="width:100%;">
            <tr>
                <td class="text-right" style="width: 5%;">
                    No.
                </td>
                <td style="width: 30%; padding-left: 10px;">
                    Barang
                </td>
                <td class="text-right" style="width: 10%;">
                    Kuantiti
                </td>
                <td class="text-right" style="width: 20%;">
                    Harga
                </td>
                <td class="text-right" style="width: 20%;">
                    Potongan
                </td>
                <td class="text-right" style="width: 30%;">
                    Total
                </td>
            </tr>
        </table>
        <hr class="dashed">
        <table style="width:100%;">
            @for ($i = 0; $i < sizeof($detail); $i++)
                <tr>
                    <td class="text-right" style="width: 5%;">
                        {{ $i+1 }}
                    </td>
                    <td style="width: 30%; padding-left: 10px;">
                        {{ $detail[$i]['item']['name'] }}
                    </td>
                    <td class="text-right" style="width: 10%;">
                        {{ number_format($detail[$i]['quantity'], 0, ',', '.') }}
                    </td>
                    <td class="text-right" style="width: 20%;">
                        {{ number_format($detail[$i]['selling_price'], 0, ',', '.') }}
                    </td>
                    <td class="text-right" style="width: 20%;">
                        {{ number_format($detail[$i]['discount'], 0, ',', '.') }}
                    </td>
                    <td class="text-right" style="width: 30%;">
                        {{ number_format(($detail[$i]['selling_price'] * $detail[$i]['quantity']) - ($detail[$i]['quantity'] * $detail[$i]['discount']), 0, ',', '.') }}
                    </td>
                </tr>
            @endfor
        </table>
        <hr class="dashed">
        <table style="width:100%;">
            <tr>
                <td class="text-right" style="width: 85%;">
                    <b>SubTotal</b>
                </td>
                <td class="text-right" style="width: 30%;">
                    {{ number_format($subtotal, 0, ',', '.') }}
                </td>
            </tr>
            @if ($type == 2)
                <tr>
                    <td class="text-right" style="width: 85%;">
                        <b>Biaya Service</b>
                    </td>
                    <td class="text-right" style="width: 30%;">
                        {{ number_format($fee, 0, ',', '.') }}
                    </td>
                </tr>
            @endif
            @php
                $subtotalNew = $subtotal - $fee;
            @endphp
            @php
                $discountNominal = ($subtotal * $discount / 100);
            @endphp
            @if ($discount != 0)
                <tr>
                    <td class="text-right" style="width: 85%;">
                        <b>Diskon</b>
                    </td>
                    <td class="text-right" style="width: 30%;">
                        {{ number_format($discountNominal, 0, ',', '.') }}
                    </td>
                </tr>
            @endif
            @php
                $ppnNominal = (($subtotal - ($subtotal * $discount / 100)) * $ppn / 100);
            @endphp
            @if ($ppn != 0)
                <tr>
                    <td class="text-right" style="width: 85%;">
                        <b>Ppn ({{ $ppn }}%)</b>
                    </td>
                    <td class="text-right" style="width: 30%;">
                        {{ number_format($ppnNominal, 0, ',', '.') }}
                    </td>
                </tr>
            @else
                <tr>
                    <td class="text-right" style="width: 85%;">
                        <b>Ppn</b>
                    </td>
                    <td class="text-right" style="width: 30%;">
                        -
                    </td>
                </tr>
            @endif
            <tr>
                <td class="text-right" style="width: 85%;">
                    <b>Total</b>
                </td>
                <td class="text-right" style="width: 30%;">
                    {{ number_format($total, 0, ',', '.') }}
                </td>
            </tr>
            <tr>
                <td class="text-right" style="width: 85%;">
                    <b>Pembayaran</b>
                </td>
                <td class="text-right" style="width: 30%;">
                    @if ($payment == 1)
                        Tunai
                    @else
                        Hutang
                    @endif
                </td>
            </tr>
            <tr>
                <td class="text-right" style="width: 85%;">
                    <b>Bayar</b>
                </td>
                <td class="text-right" style="width: 30%;">
                    {{ number_format($pay, 0, ',', '.') }}
                </td>
            </tr>
            <tr>
                <td class="text-right" style="width: 85%;">
                    <b>Kembalian</b>
                </td>
                <td class="text-right" style="width: 30%;">
                    {{ number_format($change, 0, ',', '.') }}
                </td>
            </tr>
            @if ($payment == 2 && $down_payment != 0)
                <tr>
                    <td class="text-right" style="width: 85%;">
                        <b>DP</b>
                    </td>
                    <td class="text-right" style="width: 30%;">
                        {{ number_format($down_payment, 0, ',', '.') }}
                    </td>
                </tr>
            @endif
        </table>
    </div>

    <div class="footer text-center">
        <hr class="bold">
        <table class="" style="width:100%;">
            <tr>
                <td style="width:50%; height:100px; vertical-align:text-top;">
                    <b>Keterangan : </b><br>
                    {{ $information }}
                </td>
                <td class="text-center" style="width:25%; height:100px; vertical-align:text-top;">
                    <b>Penerima,</b>
                </td>
                <td class="text-center" style="width:25%; height:100px; vertical-align:text-top;">
                    <b>Hormat Kami,</b>
                </td>
            </tr>
        </table>
        <p>
            <!-- Terima kasih atas kunjungan anda<br>
            Struk ini merupakan alat bukti<br>
            pembayaran yang sah -->
            {!! $outlet['receipt_note'] !!}
        </p>
    </div>

</body>
</html>
