<!DOCTYPE html>
<html lang="ar">
<!-- <html lang="ar"> for arabic only -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>
        {{ $number }} | {{ config('app.name', 'Laravel') }}
    </title>
    <style>
        @page {
        }
        @media print {
          html, body {
            margin: 0;
          }
        }
        html {
            direction: rtl;
        }
        html,body {
            margin:5px;
            margin-left:20px;
            margin-right:10px;
            padding:0;
            font-size: 14px;
            font-family: Tahoma, Geneva, sans-serif;
        }
        #printContainer {
            margin: auto;
            text-align: justify;
        }

        .text-left {
            text-align: left;
        }
        .text-center {
            text-align: center;
        }
        .text-right {
            text-align: right;
        }
        .dashed{
            border: 1px dashed #000000;
            border-style: none none dashed;
            color: #fff;
            background-color: #fff;
        }
        .bold {
            border: 1px solid #000000;
            color: #fff;
            background-color: #fff;
        }
        .header {
        }
        .header-title {
            text-transform: uppercase;
            font-weight: bold;
            font-size: 16px;
        }
        .text-bold {
            font-weight: bold;
        }
        .text-uppercase {
            text-transform: uppercase;
        }
        .border1 {
            border: 1px solid #000000;
        }
        .pd5 {
            padding: 5px;
        }
        .mt5 {
            margin-top: 5px;
        }
    </style>
</head>
<body>

    <div class="header text-left">
        <p class="header-title">{{ config('app.name', 'Laravel') }}</p>
        <table style="width:100%;">
            <tr>
                <td style="width:75%;">
                    {{ $outlet['name'] }} <br>
                    {{ $outlet['address'] }} <br>
                    Telp. {{ $outlet['phone'] }}
                </td>
                <td class="text-right text-bold text-uppercase" style="width:25%; font-size: 16px;">
                    <br>
                    NOTA PEMBELIAN <br>
                    {{ $number }}
                </td>
            </tr>
        </table>
        <hr class="bold">
    </div>

    <div class="body">
        <table style="width:100%;">
            <tr>
                <td style="width: 25%;">
                    <b>Nama</b>
                </td>
                <td style="width: 25%;">
                    <b>:</b> {{ $supplier['name'] }}
                </td>
                <td style="width: 25%;">
                    <b>No. Telp</b>
                </td>
                <td style="width: 25%;">
                    <b>:</b> {{ $supplier['phone'] }}
                </td>
            </tr>
            <tr>
                <td style="width: 25%;">
                    <b>Tanggal</b>
                </td>
                <td style="width: 25%;">
                    <b>:</b> {{ Carbon\Carbon::parse($date)->format('d/m/Y') }} {{ Carbon\Carbon::parse($created_at)->format('H:i:s') }}
                </td>
                <td style="width: 25%;">
                    <b>Kasir</b>
                </td>
                <td style="width: 25%;">
                    <b>:</b> {{ $user['name'] }}
                </td>
            </tr>
        </table>
        <hr class="bold">
        <table class="text-bold" style="width:100%;">
            <tr>
                <td class="text-right" style="width: 5%;">
                    No.
                </td>
                <td style="width: 30%; padding-left: 10px;">
                    Barang
                </td>
                <td class="text-right" style="width: 10%;">
                    Kuantiti
                </td>
                <td class="text-right" style="width: 20%;">
                    Harga
                </td>
                <td class="text-right" style="width: 20%;">
                    Potongan
                </td>
                <td class="text-right" style="width: 30%;">
                    Total
                </td>
            </tr>
        </table>
        <hr class="dashed">
        <table style="width:100%;">
            @for ($i = 0; $i < sizeof($detail); $i++)
                <tr>
                    <td class="text-right" style="width: 5%;">
                        {{ $i+1 }}
                    </td>
                    <td style="width: 30%; padding-left: 10px;">
                        {{ $detail[$i]['item']['name'] }}
                    </td>
                    <td class="text-right" style="width: 10%;">
                        {{ number_format($detail[$i]['quantity'], 0, ',', '.') }}
                    </td>
                    <td class="text-right" style="width: 20%;">
                        {{ number_format($detail[$i]['price'], 0, ',', '.') }}
                    </td>
                    <td class="text-right" style="width: 20%;">
                        {{ number_format($detail[$i]['discount'], 0, ',', '.') }}
                    </td>
                    <td class="text-right" style="width: 30%;">
                        {{ number_format(($detail[$i]['price'] * $detail[$i]['quantity']) - ($detail[$i]['quantity'] * $detail[$i]['discount']), 0, ',', '.') }}
                    </td>
                </tr>
            @endfor
        </table>
        <hr class="dashed">
        <table style="width:100%;">
            <tr>
                <td class="text-right" style="width: 85%;">
                    <b>SubTotal</b>
                </td>
                <td class="text-right" style="width: 30%;">
                    {{ number_format($subtotal, 0, ',', '.') }}
                </td>
            </tr>
            @php
                $discountNominal = ($subtotal * $discount / 100);
            @endphp
            @if ($discount != 0)
                <tr>
                    <td class="text-right" style="width: 85%;">
                        <b>Diskon</b>
                    </td>
                    <td class="text-right" style="width: 30%;">
                        {{ number_format($discountNominal, 0, ',', '.') }}
                    </td>
                </tr>
            @endif
            @php
                $ppnNominal = (($subtotal - ($subtotal * $discount / 100)) * $ppn / 100);
            @endphp
            @if ($ppn != 0)
                <tr>
                    <td class="text-right" style="width: 85%;">
                        <b>Ppn ({{ $ppn }}%)</b>
                    </td>
                    <td class="text-right" style="width: 30%;">
                        {{ number_format($ppnNominal, 0, ',', '.') }}
                    </td>
                </tr>
            @else
                <tr>
                    <td class="text-right" style="width: 85%;">
                        <b>Ppn</b>
                    </td>
                    <td class="text-right" style="width: 30%;">
                        -
                    </td>
                </tr>
            @endif
            <tr>
                <td class="text-right" style="width: 85%;">
                    <b>Total</b>
                </td>
                <td class="text-right" style="width: 30%;">
                    {{ number_format($total, 0, ',', '.') }}
                </td>
            </tr>
            <tr>
                <td class="text-right" style="width: 85%;">
                    <b>Pembayaran</b>
                </td>
                <td class="text-right" style="width: 30%;">
                    @if ($payment == 1)
                        Tunai
                    @else
                        Hutang
                    @endif
                </td>
            </tr>
        </table>
    </div>

    <div class="footer text-center">
        <hr class="bold">
        <table class="" style="width:100%;">
            <tr>
                <td style="width:50%; height:100px; vertical-align:text-top;">
                    <b>Keterangan : </b><br>
                    {{ $information }}
                </td>
            </tr>
        </table>
    </div>

</body>
</html>
