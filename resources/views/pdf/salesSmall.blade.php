<!DOCTYPE html>
<html lang="ar">
<!-- <html lang="ar"> for arabic only -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>
        {{ $number }} | {{ config('app.name', 'Laravel') }}
    </title>
    <style>
        @page {
            size: 70mm 60mm portrait;
        }
        @media print {
          html, body {
            margin: 0;
          }
          @page {
              size: 70mm 60mm portrait;
          }
        }
        html {
            /* direction: rtl; */
        }
        html,body {
            margin:5px;
            margin-left:10px;
            margin-right:10px;
            padding:0;
            font-size: 11px;
            /* font-family: Tahoma, Geneva, sans-serif; */
            font-family: Arial;
            line-height: 0.8;
            letter-spacing: 1.2px;
            /* font-family: Consolas, monaco, monospace; */
        }
        #printContainer {
            margin: auto;
            text-align: justify;
        }

        .text-left {
            text-align: left;
        }
        .text-center {
            text-align: center;
        }
        .text-right {
            text-align: right;
        }
        .dashed{
            border: 1px dashed #000000;
            border-style: none none dashed;
            color: #fff;
            background-color: #fff;
        }
        .bold {
            border: 0.2px solid #000000;
            color: #fff;
            background-color: #fff;
        }
        .header {
        }
        .header-title {
            text-transform: uppercase;
            font-weight: bold;
        }
        .text-bold {
            font-weight: bold;
        }
        .text-uppercase {
            text-transform: uppercase;
        }
        .border1 {
            border: 1px solid #000000;
        }
        .pd5 {
            padding: 5px;
        }
        .mt5 {
            margin-top: 5px;
        }
        .mt10 {
            margin-top: 10px;
        }
        .mt15 {
            margin-top: 15px;
        }
    </style>
</head>
<body onload="window.print();">

    <div class="header text-left">
        <table class="text-left" style="width:100%;">
            <tr>
                <td>
                    <!-- <b>{{ config('app.name', 'Laravel') }}</b> <br> -->
                    <b>{{ $outlet['name'] }}</b> <br><br>
                    {{ $outlet['address'] }} <br><br>
                    Telp. {{ $outlet['phone'] }}
                </td>
            </tr>
        </table>
    </div>

    <div class="body">
        <table class="mt15" style="width:100%;">
            @if ($customer['id'] != 1)
                <tr>
                    <td style="width: 40%;">
                        Kd Member
                    </td>
                    <td style="width: 80%;">
                        : {{ $customer['number'] }}
                    </td>
                </tr>
                <tr>
                    <td style="width: 40%;">
                        Tipe
                    </td>
                    <td style="width: 80%;">
                        : {{ $customer['customertype']['name'] }}
                    </td>
                </tr>
            @endif
            <tr>
                <td style="width: 40%;">
                    Nama
                </td>
                <td style="width: 80%;">
                    : {{ Str::words($customer_name, $words = 20, $end = '-') }}
                </td>
            </tr>
            @if ($customer_phone)
                <tr>
                    <td style="width: 40%;">
                        No. Telp
                    </td>
                    <td style="width: 80%;">
                        : {{ $customer_phone }}
                    </td>
                </tr>
            @endif
        </table>
        <hr class="bold">
        <table style="width:100%;">
            <tr>
                <td style="width: 40%;">
                    Tgl
                </td>
                <td style="width: 80%;">
                    : {{ Carbon\Carbon::parse($date)->format('d/m/Y') }}
                </td>
            </tr>
            <tr>
                <td style="width: 40%;">

                </td>
                <td style="width: 80%;">
                    : {{ Carbon\Carbon::parse($created_at)->format('H:i:s') }}
                </td>
            </tr>
            <tr>
                <td style="width: 40%;">
                    Kasir
                </td>
                <td style="width: 80%;">
                    : {{ $user['name'] }}
                </td>
            </tr>
            <tr>
                <td style="width: 40%;">
                    No.
                </td>
                <td style="width: 80%;">
                    : {{ $number }}
                </td>
            </tr>
        </table>
        <hr class="bold">
        @if ($type == 2)
            <table style="width:100%;">
                <tr>
                    <td style="width: 40%;">
                        No. Servis
                    </td>
                    <td style="width: 80%;">
                        : {{ $service['number'] }}
                    </td>
                </tr>
                <tr>
                    <td style="width: 40%;">
                        Barang
                    </td>
                    <td style="width: 80%;">
                        : {{ $service['item_name'] }}
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="width: 100%;">
                        Keluhan
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="width: 100%;">
                        {{ $service['complaint'] }}
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="width: 100%;">
                        Kelengkapan
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="width: 100%;">
                        {{ $service['equipment'] }}
                    </td>
                </tr>
            </table>
            <hr class="bold">
        @endif
        <!-- <table style="width:100%;">
            <tr>
                <td style="width: 40%;">
                    Brg.
                </td>
                <td class="text-right" style="width: 10%;">
                    Qty
                </td>
                <td class="text-right" style="width: 15%;">
                    Hrg.
                </td>
                <td class="text-right" style="width: 15%;">
                    Pot.
                </td>
                <td class="text-right" style="width: 40%;">
                    Total
                </td>
            </tr>
        </table>
        <hr class="dashed"> -->
        <table style="width:100%;">
            @for ($i = 0; $i < sizeof($detail); $i++)
                <tr>
                    <td colspan="3">
                        {{ $detail[$i]['item']['name'] }}
                    </td>
                </tr>
                <tr>
                    <td class="" style="width: 20%;">
                        {{ number_format($detail[$i]['quantity'], 0, ',', '.') }}
                    </td>
                    <td class="text-right" style="width: 40%;">
                        x {{ number_format(( ($detail[$i]['selling_price'] - $detail[$i]['discount'])), 0, ',', '.') }}
                    </td>
                    <td class="text-right" style="width: 40%;">
                        {{ number_format(($detail[$i]['selling_price'] * $detail[$i]['quantity']) - ($detail[$i]['quantity'] * $detail[$i]['discount']), 0, ',', '.') }}
                    </td>
                </tr>
                <!-- <tr>
                    <td style="width: 40%;">
                        {{ Str::words($detail[$i]['item']['name'], $words = 10, $end = '-') }}
                    </td>
                    <td class="text-right" style="width: 10%;">
                        {{ number_format($detail[$i]['quantity'], 0, ',', '.') }}
                    </td>
                    <td class="text-right" style="width: 15%;">
                        {{ number_format(($detail[$i]['selling_price'] - $detail[$i]['discount']), 0, ',', '.') }}
                    </td>
                    <td class="text-right" style="width: 15%;">
                        {{ number_format($detail[$i]['discount'], 0, ',', '.') }}
                    </td>
                    <td class="text-right" style="width: 40%;">
                        {{ number_format(($detail[$i]['selling_price'] * $detail[$i]['quantity']) - ($detail[$i]['quantity'] * $detail[$i]['discount']), 0, ',', '.') }}
                    </td>
                </tr> -->
            @endfor
        </table>
        <hr class="dashed">
        <table style="width:100%;">
            <tr>
                <td class="text-right" style="width: 60%;">
                    SubTotal
                </td>
                <td class="text-right" style="width: 40%;">
                    {{ number_format($subtotal, 0, ',', '.') }}
                </td>
            </tr>
            @if ($type == 2)
                <tr>
                    <td class="text-right" style="width: 60%;">
                        Biaya Service
                    </td>
                    <td class="text-right" style="width: 40%;">
                        {{ number_format($fee, 0, ',', '.') }}
                    </td>
                </tr>
            @endif
            @php
                $subtotalNew = $subtotal - $fee;
            @endphp
            @php
                $discountNominal = ($subtotal * $discount / 100);
            @endphp
            @if ($discount != 0)
                <tr>
                    <td class="text-right" style="width: 60%;">
                        Diskon
                    </td>
                    <td class="text-right" style="width: 40%;">
                        {{ number_format($discountNominal, 0, ',', '.') }}
                    </td>
                </tr>
            @endif
            @php
                $ppnNominal = (($subtotal - ($subtotal * $discount / 100)) * $ppn / 100);
            @endphp
            @if ($ppn != 0)
                <tr>
                    <td class="text-right" style="width: 60%;">
                        Ppn ({{ $ppn }}%)
                    </td>
                    <td class="text-right" style="width: 40%;">
                        {{ number_format($ppnNominal, 0, ',', '.') }}
                    </td>
                </tr>
            @else
                <tr>
                    <td class="text-right" style="width: 60%;">
                        Ppn
                    </td>
                    <td class="text-right" style="width: 40%;">
                        -
                    </td>
                </tr>
            @endif
            <tr>
                <td class="text-right" style="width: 60%;">
                    Total
                </td>
                <td class="text-right" style="width: 40%;">
                    {{ number_format($total, 0, ',', '.') }}
                </td>
            </tr>
            <tr>
                <td class="text-right" style="width: 60%;">
                    Pembayaran
                </td>
                <td class="text-right" style="width: 40%;">
                    @if ($payment == 1)
                        Tunai
                    @else
                        Hutang
                    @endif
                </td>
            </tr>
            <tr>
                <td class="text-right" style="width: 60%;">
                    Bayar
                </td>
                <td class="text-right" style="width: 40%;">
                    {{ number_format($pay, 0, ',', '.') }}
                </td>
            </tr>
            <tr>
                <td class="text-right" style="width: 60%;">
                    Kembalian
                </td>
                <td class="text-right" style="width: 40%;">
                    {{ number_format($change, 0, ',', '.') }}
                </td>
            </tr>
            @if ($payment == 2 && $down_payment != 0)
                <tr>
                    <td class="text-right" style="width: 60%;">
                        DP
                    </td>
                    <td class="text-right" style="width: 40%;">
                        {{ number_format($down_payment, 0, ',', '.') }}
                    </td>
                </tr>
            @endif
        </table>
        <hr class="bold">
    </div>

    <div class="footer text-center">
        <p>
            <!-- Terima kasih atas kunjungan anda<br>
            Struk ini merupakan alat bukti<br>
            pembayaran yang sah -->
            {!! $outlet['receipt_note'] !!}
        </p>
    </div>

</body>
</html>
