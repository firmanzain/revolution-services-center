<!DOCTYPE html>
<html lang="ar">
<!-- <html lang="ar"> for arabic only -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>
        {{ $number }} | {{ config('app.name', 'Laravel') }}
    </title>
    <style>
        @page {
        }
        @media print {
            html, body {
                margin: 0;
            }
        }
        html {
          direction: rtl;
        }
        html,body {
          margin:5px;
          margin-left:10px;
          margin-right:10px;
          padding:0;
          font-size: 11px;
          font-family: Tahoma, Geneva, sans-serif;
          line-height: 0.7;
          letter-spacing: 1.2px;
          /* font-family: Consolas, monaco, monospace; */
        }
        #printContainer {
          margin: auto;
          text-align: justify;
        }

        .text-left {
          text-align: left;
        }
        .text-center {
          text-align: center;
        }
        .text-right {
          text-align: right;
        }
        .dashed{
          border: 1px dashed #000000;
          border-style: none none dashed;
          color: #fff;
          background-color: #fff;
        }
        .bold {
          border: 0.2px solid #000000;
          color: #fff;
          background-color: #fff;
        }
        .header {
        }
        .header-title {
          text-transform: uppercase;
          font-weight: bold;
        }
        .text-bold {
          font-weight: bold;
        }
        .text-uppercase {
          text-transform: uppercase;
        }
        .border1 {
          border: 1px solid #000000;
        }
        .pd5 {
          padding: 5px;
        }
        .mt5 {
          margin-top: 5px;
        }
        .mt10 {
          margin-top: 10px;
        }
        .mt15 {
          margin-top: 15px;
        }
    </style>
</head>
<body>

    <div class="header text-center">
        <table class="text-center" style="width:100%;">
            <tr>
                <td>
                    <b>{{ config('app.name', 'Laravel') }}</b> <br>
                    {{ $outlet['name'] }} <br>
                    {{ $outlet['address'] }} <br>
                    Telp. {{ $outlet['phone'] }}
                </td>
            </tr>
        </table>
    </div>

    <div class="body">
        <table class="mt15" style="width:100%;">
            <tr>
                <td style="width: 40%;">
                    Nama
                </td>
                <td style="width: 80%;">
                    : {{ Str::words($supplier['name'], $words = 20, $end = '-') }}
                </td>
            </tr>
            <tr>
                <td style="width: 40%;">
                    No. Telp
                </td>
                <td style="width: 80%;">
                    : {{ $supplier['phone'] }}
                </td>
            </tr>
        </table>
        <hr class="bold">
        <table style="width:100%;">
            <tr>
                <td style="width: 40%;">
                    Tgl
                </td>
                <td style="width: 80%;">
                    : {{ Carbon\Carbon::parse($date)->format('d/m/Y') }} {{ Carbon\Carbon::parse($created_at)->format('H:i:s') }}
                </td>
            </tr>
            <tr>
                <td style="width: 40%;">
                    Kasir
                </td>
                <td style="width: 80%;">
                    : {{ $user['name'] }}
                </td>
            </tr>
            <tr>
                <td style="width: 40%;">
                    No.
                </td>
                <td style="width: 80%;">
                    : {{ $number }}
                </td>
            </tr>
        </table>
        <hr class="bold">
        <table style="width:100%;">
            <tr>
                <td style="width: 30%;">
                    Brg.
                </td>
                <td class="text-right" style="width: 10%;">
                    Qty
                </td>
                <td class="text-right" style="width: 15%;">
                    Hrg.
                </td>
                <td class="text-right" style="width: 15%;">
                    Pot.
                </td>
                <td class="text-right" style="width: 30%;">
                    Total
                </td>
            </tr>
        </table>
        <hr class="dashed">
        <table style="width:100%;">
            @for ($i = 0; $i < sizeof($detail); $i++)
                <tr>
                    <td colspan="4">
                        {{ $detail[$i]['item']['name'] }}
                    </td>
                </tr>
                <tr>
                    <td class="text-right" style="width: 20%;">
                        {{ number_format($detail[$i]['quantity'], 0, ',', '.') }} Pcs
                    </td>
                    <td class="text-right" style="width: 25%;">
                        {{ number_format($detail[$i]['price'], 0, ',', '.') }}
                    </td>
                    <td class="text-right" style="width: 25%;">
                        {{ number_format($detail[$i]['discount'], 0, ',', '.') }}
                    </td>
                    <td class="text-right" style="width: 30%;">
                        {{ number_format(($detail[$i]['price'] * $detail[$i]['quantity']) - ($detail[$i]['quantity'] * $detail[$i]['discount']), 0, ',', '.') }}
                    </td>
                </tr>
                <!-- <tr>
                    <td style="width: 30%;">
                        {{ Str::words($detail[$i]['item']['name'], $words = 10, $end = '-') }}
                    </td>
                    <td class="text-right" style="width: 10%;">
                        {{ number_format($detail[$i]['quantity'], 0, ',', '.') }}
                    </td>
                    <td class="text-right" style="width: 15%;">
                        {{ number_format($detail[$i]['price'], 0, ',', '.') }}
                    </td>
                    <td class="text-right" style="width: 15%;">
                        {{ number_format($detail[$i]['discount'], 0, ',', '.') }}
                    </td>
                    <td class="text-right" style="width: 30%;">
                        {{ number_format(($detail[$i]['price'] * $detail[$i]['quantity']) - ($detail[$i]['quantity'] * $detail[$i]['discount']), 0, ',', '.') }}
                    </td>
                </tr> -->
            @endfor
        </table>
        <hr class="dashed">
        <table style="width:100%;">
            <tr>
                <td class="text-right" style="width: 70%;">
                    SubTotal
                </td>
                <td class="text-right" style="width: 30%;">
                    {{ number_format($subtotal, 0, ',', '.') }}
                </td>
            </tr>
            @php
                $discountNominal = ($subtotal * $discount / 100);
            @endphp
            @if ($discount != 0)
                <tr>
                    <td class="text-right" style="width: 70%;">
                        Diskon
                    </td>
                    <td class="text-right" style="width: 30%;">
                        {{ number_format($discountNominal, 0, ',', '.') }}
                    </td>
                </tr>
            @endif
            @php
                $ppnNominal = (($subtotal - ($subtotal * $discount / 100)) * $ppn / 100);
            @endphp
            @if ($ppn != 0)
                <tr>
                    <td class="text-right" style="width: 70%;">
                        Ppn ({{ $ppn }}%)
                    </td>
                    <td class="text-right" style="width: 30%;">
                        {{ number_format($ppnNominal, 0, ',', '.') }}
                    </td>
                </tr>
            @else
                <tr>
                    <td class="text-right" style="width: 70%;">
                        Ppn
                    </td>
                    <td class="text-right" style="width: 30%;">
                        -
                    </td>
                </tr>
            @endif
            <tr>
                <td class="text-right" style="width: 70%;">
                    Total
                </td>
                <td class="text-right" style="width: 30%;">
                    {{ number_format($total, 0, ',', '.') }}
                </td>
            </tr>
            <tr>
                <td class="text-right" style="width: 70%;">
                    Pembayaran
                </td>
                <td class="text-right" style="width: 30%;">
                    @if ($payment == 1)
                        Tunai
                    @else
                        Hutang
                    @endif
                </td>
            </tr>
        </table>
        <hr class="bold">
    </div>

</body>
</html>
