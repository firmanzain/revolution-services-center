/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 100410
 Source Host           : localhost:3306
 Source Schema         : kubbie-revolution

 Target Server Type    : MySQL
 Target Server Version : 100410
 File Encoding         : 65001

 Date: 24/11/2019 14:44:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for adjustment_details
-- ----------------------------
DROP TABLE IF EXISTS `adjustment_details`;
CREATE TABLE `adjustment_details`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `adjustment_id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) UNSIGNED NOT NULL,
  `quantity_old` smallint(6) NOT NULL DEFAULT 0,
  `quantity` smallint(6) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `adjustment_details_adjustment_id_foreign`(`adjustment_id`) USING BTREE,
  INDEX `adjustment_details_item_id_foreign`(`item_id`) USING BTREE,
  CONSTRAINT `adjustment_details_adjustment_id_foreign` FOREIGN KEY (`adjustment_id`) REFERENCES `adjustments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `adjustment_details_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of adjustment_details
-- ----------------------------
INSERT INTO `adjustment_details` VALUES (1, 1, 1, 109, 220, '2019-11-23 20:41:06', '2019-11-23 20:41:06', NULL);
INSERT INTO `adjustment_details` VALUES (2, 2, 1, 220, 230, '2019-11-23 20:42:20', '2019-11-23 20:42:20', NULL);
INSERT INTO `adjustment_details` VALUES (3, 2, 2, 218, 190, '2019-11-23 20:42:20', '2019-11-23 20:42:20', NULL);

-- ----------------------------
-- Table structure for adjustments
-- ----------------------------
DROP TABLE IF EXISTS `adjustments`;
CREATE TABLE `adjustments`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `outlet_id` bigint(20) UNSIGNED NOT NULL,
  `information` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 1,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `adjustments_outlet_id_foreign`(`outlet_id`) USING BTREE,
  INDEX `adjustments_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `adjustments_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `outlets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `adjustments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of adjustments
-- ----------------------------
INSERT INTO `adjustments` VALUES (1, '2019-11-23', 'AD20191100001', 2, NULL, 1, 0, '2019-11-23 20:41:06', '2019-11-23 20:41:06', NULL);
INSERT INTO `adjustments` VALUES (2, '2019-11-23', 'AD20191100002', 2, NULL, 1, 0, '2019-11-23 20:42:20', '2019-11-23 20:42:20', NULL);

-- ----------------------------
-- Table structure for brands
-- ----------------------------
DROP TABLE IF EXISTS `brands`;
CREATE TABLE `brands`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of brands
-- ----------------------------
INSERT INTO `brands` VALUES (1, 'Tidak ada merek', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 'Tidak ada kategori', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for customer_debt_payments
-- ----------------------------
DROP TABLE IF EXISTS `customer_debt_payments`;
CREATE TABLE `customer_debt_payments`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `debt_nominal` decimal(10, 0) NOT NULL DEFAULT 0,
  `debt_nominal_payment` decimal(10, 0) NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `customer_debt_payments_customer_id_foreign`(`customer_id`) USING BTREE,
  INDEX `customer_debt_payments_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `customer_debt_payments_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `customer_debt_payments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for customer_debts
-- ----------------------------
DROP TABLE IF EXISTS `customer_debts`;
CREATE TABLE `customer_debts`  (
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `nominal` decimal(10, 0) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `customer_debts_customer_id_foreign`(`customer_id`) USING BTREE,
  CONSTRAINT `customer_debts_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for customer_types
-- ----------------------------
DROP TABLE IF EXISTS `customer_types`;
CREATE TABLE `customer_types`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` decimal(8, 2) NOT NULL DEFAULT 0,
  `debt_limit` decimal(10, 0) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer_types
-- ----------------------------
INSERT INTO `customer_types` VALUES (1, 'Umum', 0.00, 0, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for customers
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `customer_type_id` bigint(20) UNSIGNED NOT NULL,
  `number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `customers_customer_type_id_foreign`(`customer_type_id`) USING BTREE,
  CONSTRAINT `customers_customer_type_id_foreign` FOREIGN KEY (`customer_type_id`) REFERENCES `customer_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customers
-- ----------------------------
INSERT INTO `customers` VALUES (1, 1, 'UMUM', 'Umum', NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for incoming_item_details
-- ----------------------------
DROP TABLE IF EXISTS `incoming_item_details`;
CREATE TABLE `incoming_item_details`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `incoming_item_id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` smallint(6) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `incoming_item_details_incoming_item_id_foreign`(`incoming_item_id`) USING BTREE,
  INDEX `incoming_item_details_item_id_foreign`(`item_id`) USING BTREE,
  CONSTRAINT `incoming_item_details_incoming_item_id_foreign` FOREIGN KEY (`incoming_item_id`) REFERENCES `incoming_items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `incoming_item_details_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for incoming_items
-- ----------------------------
DROP TABLE IF EXISTS `incoming_items`;
CREATE TABLE `incoming_items`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `supplier_id` bigint(20) UNSIGNED NOT NULL,
  `purchase_id` bigint(20) UNSIGNED NOT NULL,
  `outlet_id` bigint(20) UNSIGNED NOT NULL,
  `information` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 1,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `incoming_items_supplier_id_foreign`(`supplier_id`) USING BTREE,
  INDEX `incoming_items_purchase_id_foreign`(`purchase_id`) USING BTREE,
  INDEX `incoming_items_outlet_id_foreign`(`outlet_id`) USING BTREE,
  INDEX `incoming_items_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `incoming_items_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `outlets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `incoming_items_purchase_id_foreign` FOREIGN KEY (`purchase_id`) REFERENCES `purchases` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `incoming_items_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `incoming_items_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for item_customer_type_prices
-- ----------------------------
DROP TABLE IF EXISTS `item_customer_type_prices`;
CREATE TABLE `item_customer_type_prices`  (
  `item_id` bigint(20) UNSIGNED NOT NULL,
  `customer_type_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(10, 0) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  UNIQUE INDEX `item_customer_type_prices_item_id_customer_type_id_unique`(`item_id`, `customer_type_id`) USING BTREE,
  INDEX `item_customer_type_prices_customer_type_id_foreign`(`customer_type_id`) USING BTREE,
  CONSTRAINT `item_customer_type_prices_customer_type_id_foreign` FOREIGN KEY (`customer_type_id`) REFERENCES `customer_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `item_customer_type_prices_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for item_purchase_prices
-- ----------------------------
DROP TABLE IF EXISTS `item_purchase_prices`;
CREATE TABLE `item_purchase_prices`  (
  `item_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(10, 0) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  UNIQUE INDEX `item_purchase_prices_item_id_unique`(`item_id`) USING BTREE,
  CONSTRAINT `item_purchase_prices_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of item_purchase_prices
-- ----------------------------
INSERT INTO `item_purchase_prices` VALUES (1, 20000, '2019-10-07 16:12:52', '2019-10-07 16:12:52');
INSERT INTO `item_purchase_prices` VALUES (2, 25000, '2019-10-07 16:13:06', '2019-10-07 16:13:06');

-- ----------------------------
-- Table structure for item_selling_prices
-- ----------------------------
DROP TABLE IF EXISTS `item_selling_prices`;
CREATE TABLE `item_selling_prices`  (
  `item_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(10, 0) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  UNIQUE INDEX `item_selling_prices_item_id_unique`(`item_id`) USING BTREE,
  CONSTRAINT `item_selling_prices_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of item_selling_prices
-- ----------------------------
INSERT INTO `item_selling_prices` VALUES (1, 30000, '2019-10-07 16:12:52', '2019-10-07 16:12:52');
INSERT INTO `item_selling_prices` VALUES (2, 40000, '2019-10-07 16:13:06', '2019-10-07 16:13:06');

-- ----------------------------
-- Table structure for item_supplier_prices
-- ----------------------------
DROP TABLE IF EXISTS `item_supplier_prices`;
CREATE TABLE `item_supplier_prices`  (
  `item_id` bigint(20) UNSIGNED NOT NULL,
  `supplier_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(10, 0) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  UNIQUE INDEX `item_supplier_prices_item_id_supplier_id_unique`(`item_id`, `supplier_id`) USING BTREE,
  INDEX `item_supplier_prices_supplier_id_foreign`(`supplier_id`) USING BTREE,
  CONSTRAINT `item_supplier_prices_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `item_supplier_prices_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of item_supplier_prices
-- ----------------------------
INSERT INTO `item_supplier_prices` VALUES (1, 1, 18000, '2019-10-07 16:14:00', '2019-10-07 16:14:00');

-- ----------------------------
-- Table structure for items
-- ----------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE `items`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `brand_id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.png',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `items_category_id_foreign`(`category_id`) USING BTREE,
  INDEX `items_brand_id_foreign`(`brand_id`) USING BTREE,
  CONSTRAINT `items_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `items_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of items
-- ----------------------------
INSERT INTO `items` VALUES (1, 1, 1, 'BRG0001', 'Barang 1', 'default.png', NULL, '2019-10-07 16:12:52', '2019-10-07 16:12:52', NULL);
INSERT INTO `items` VALUES (2, 1, 1, 'BRG0002', 'Barang 2', 'default.png', NULL, '2019-10-07 16:13:06', '2019-10-07 16:13:06', NULL);

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `index` smallint(6) NOT NULL DEFAULT 0,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES (1, 'Akses', 1, 0, 'javascript:;', '<i class=\"fas fa-lock\"></i>', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (2, 'Tipe Pengguna', 1, 1, 'access/user-types', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (3, 'Pengguna', 2, 1, 'access/users', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (4, 'Master', 4, 0, 'javascript:;', '<i class=\"fas fa-folder\"></i>', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (5, 'Toko/Gudang', 1, 4, 'master/outlets', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (6, 'Kategori', 2, 4, 'master/categories', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (7, 'Merek', 3, 4, 'master/brands', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (8, 'Barang', 4, 4, 'master/items', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (9, 'Pemasok', 5, 4, 'master/suppliers', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (10, 'Tipe Pelanggan', 6, 4, 'master/customer-types', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (11, 'Pelanggan', 7, 4, 'master/customers', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (12, 'Teknisi', 8, 4, 'master/technicians', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (13, 'Inventaris', 3, 0, 'javascript:;', '<i class=\"fas fa-boxes\"></i>', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (14, 'Stok', 1, 13, 'inventory/stocks', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (15, 'Pembelian', 2, 13, 'inventory/purchases', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (16, 'Barang Masuk', 3, 13, 'inventory/incoming-items', '', NULL, NULL, '2019-11-23 05:03:37');
INSERT INTO `menus` VALUES (17, 'Mutasi', 4, 13, 'inventory/mutations', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (18, 'Penyesuaian', 5, 13, 'inventory/adjustments', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (19, 'Transaksi', 4, 0, 'javascript:;', '<i class=\"fas fa-book\"></i>', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (20, 'Servis', 1, 19, 'transaction/services', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (21, 'Penjualan', 2, 19, 'transaction/sales', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (22, 'Keuangan', 5, 0, 'javascript:;', '<i class=\"fas fa-file-invoice-dollar\"></i>', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (23, 'Pembayaran Hutang', 1, 22, 'finance/debt-payments', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (24, 'Pembayaran Piutang', 2, 22, 'finance/receivable-payments', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (25, 'Operasional', 3, 22, 'finance/operationals', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (26, 'Pengeluaran Lain', 4, 22, 'finance/other-expenses', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (27, 'Laporan', 6, 0, 'javascript:;', '<i class=\"fas fa-chart-line\"></i>', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (28, 'Statistik', 1, 27, 'report/statistics', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (29, 'Kartu Stok', 2, 27, 'report/stock-cards', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (30, 'Pembelian', 3, 27, 'report/purchases', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (31, 'Servis', 4, 27, 'report/services', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (32, 'Penjualan', 5, 27, 'report/sales', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (33, 'Hutang', 6, 27, 'report/debt-payments', '', NULL, NULL, NULL);
INSERT INTO `menus` VALUES (34, 'Piutang', 7, 27, 'report/receivable-payments', '', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 331 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (293, '2019_08_20_071854_create_user_types_table', 1);
INSERT INTO `migrations` VALUES (294, '2019_08_20_072515_create_menus_table', 1);
INSERT INTO `migrations` VALUES (295, '2019_08_20_072524_create_user_privileges_table', 1);
INSERT INTO `migrations` VALUES (296, '2019_08_20_073333_create_users_table', 1);
INSERT INTO `migrations` VALUES (297, '2019_08_20_073719_create_outlets_table', 1);
INSERT INTO `migrations` VALUES (298, '2019_08_20_074021_create_user_outlets_table', 1);
INSERT INTO `migrations` VALUES (299, '2019_08_20_075258_create_categories_table', 1);
INSERT INTO `migrations` VALUES (300, '2019_08_20_075523_create_brands_table', 1);
INSERT INTO `migrations` VALUES (301, '2019_08_20_080034_create_items_table', 1);
INSERT INTO `migrations` VALUES (302, '2019_08_20_080527_create_suppliers_table', 1);
INSERT INTO `migrations` VALUES (303, '2019_08_20_081842_create_item_purchase_prices_table', 1);
INSERT INTO `migrations` VALUES (304, '2019_08_20_082139_create_item_supplier_prices_table', 1);
INSERT INTO `migrations` VALUES (305, '2019_08_20_082312_create_technicians_table', 1);
INSERT INTO `migrations` VALUES (306, '2019_08_20_082512_create_customer_types_table', 1);
INSERT INTO `migrations` VALUES (307, '2019_08_20_082726_create_customers_table', 1);
INSERT INTO `migrations` VALUES (308, '2019_08_20_083201_create_item_selling_prices_table', 1);
INSERT INTO `migrations` VALUES (309, '2019_08_20_083243_create_item_customer_type_prices_table', 1);
INSERT INTO `migrations` VALUES (310, '2019_08_20_085957_create_stocks_table', 1);
INSERT INTO `migrations` VALUES (311, '2019_08_20_090343_create_stock_cards_table', 1);
INSERT INTO `migrations` VALUES (312, '2019_08_20_091105_create_purchases_table', 1);
INSERT INTO `migrations` VALUES (313, '2019_08_20_091120_create_purchase_details_table', 1);
INSERT INTO `migrations` VALUES (314, '2019_08_21_070310_create_incoming_items_table', 1);
INSERT INTO `migrations` VALUES (315, '2019_08_21_070358_create_incoming_item_details_table', 1);
INSERT INTO `migrations` VALUES (316, '2019_08_21_070915_create_mutations_table', 1);
INSERT INTO `migrations` VALUES (317, '2019_08_21_071002_create_mutation_details_table', 1);
INSERT INTO `migrations` VALUES (318, '2019_08_21_071210_create_adjustments_table', 1);
INSERT INTO `migrations` VALUES (319, '2019_08_21_071301_create_adjustment_details_table', 1);
INSERT INTO `migrations` VALUES (320, '2019_09_06_061630_create_services_table', 1);
INSERT INTO `migrations` VALUES (321, '2019_09_06_061636_create_sales_table', 1);
INSERT INTO `migrations` VALUES (322, '2019_09_06_061700_create_sale_details_table', 1);
INSERT INTO `migrations` VALUES (323, '2019_09_07_132932_create_customer_debts_table', 1);
INSERT INTO `migrations` VALUES (324, '2019_09_08_063319_create_customer_debt_payments_table', 1);
INSERT INTO `migrations` VALUES (325, '2019_09_30_072349_create_supplier_debt_table', 1);
INSERT INTO `migrations` VALUES (326, '2019_09_30_072629_create_supplier_debt_payment_table', 1);
INSERT INTO `migrations` VALUES (329, '2019_10_31_063705_create_operationals_table', 2);
INSERT INTO `migrations` VALUES (330, '2019_10_31_064109_create_other_expenses_table', 2);

-- ----------------------------
-- Table structure for mutation_details
-- ----------------------------
DROP TABLE IF EXISTS `mutation_details`;
CREATE TABLE `mutation_details`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `mutation_id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) UNSIGNED NOT NULL,
  `quantity_old` smallint(6) NOT NULL DEFAULT 0,
  `quantity` smallint(6) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `mutation_details_mutation_id_foreign`(`mutation_id`) USING BTREE,
  INDEX `mutation_details_item_id_foreign`(`item_id`) USING BTREE,
  CONSTRAINT `mutation_details_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mutation_details_mutation_id_foreign` FOREIGN KEY (`mutation_id`) REFERENCES `mutations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for mutations
-- ----------------------------
DROP TABLE IF EXISTS `mutations`;
CREATE TABLE `mutations`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `outlet_origin_id` bigint(20) UNSIGNED NOT NULL,
  `outlet_destination_id` bigint(20) UNSIGNED NOT NULL,
  `information` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 1,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `mutations_outlet_origin_id_foreign`(`outlet_origin_id`) USING BTREE,
  INDEX `mutations_outlet_destination_id_foreign`(`outlet_destination_id`) USING BTREE,
  INDEX `mutations_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `mutations_outlet_destination_id_foreign` FOREIGN KEY (`outlet_destination_id`) REFERENCES `outlets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mutations_outlet_origin_id_foreign` FOREIGN KEY (`outlet_origin_id`) REFERENCES `outlets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mutations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for operationals
-- ----------------------------
DROP TABLE IF EXISTS `operationals`;
CREATE TABLE `operationals`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `outlet_id` bigint(20) UNSIGNED NOT NULL,
  `nominal` decimal(10, 0) NOT NULL DEFAULT 0,
  `information` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `operationals_outlet_id_foreign`(`outlet_id`) USING BTREE,
  INDEX `operationals_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `operationals_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `outlets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `operationals_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of operationals
-- ----------------------------
INSERT INTO `operationals` VALUES (1, '2019-10-31', 'OP20191000001', 2, 2000000, 'Pembayaran listrik', 1, '2019-10-31 07:34:06', '2019-10-31 07:36:38', NULL);

-- ----------------------------
-- Table structure for other_expenses
-- ----------------------------
DROP TABLE IF EXISTS `other_expenses`;
CREATE TABLE `other_expenses`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `outlet_id` bigint(20) UNSIGNED NOT NULL,
  `nominal` decimal(10, 0) NOT NULL DEFAULT 0,
  `information` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `other_expenses_outlet_id_foreign`(`outlet_id`) USING BTREE,
  INDEX `other_expenses_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `other_expenses_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `outlets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `other_expenses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of other_expenses
-- ----------------------------
INSERT INTO `other_expenses` VALUES (1, '2019-10-31', 'OP20191000001', 2, 500000, 'Pembelian Rak', 1, '2019-10-31 07:54:19', '2019-10-31 07:55:24', NULL);

-- ----------------------------
-- Table structure for outlets
-- ----------------------------
DROP TABLE IF EXISTS `outlets`;
CREATE TABLE `outlets`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of outlets
-- ----------------------------
INSERT INTO `outlets` VALUES (1, 'Semua Outlet', '0812', '-', NULL, NULL, NULL);
INSERT INTO `outlets` VALUES (2, 'Revolution Komputer', '031', '-', '2019-10-07 16:12:34', '2019-10-07 16:12:34', NULL);

-- ----------------------------
-- Table structure for purchase_details
-- ----------------------------
DROP TABLE IF EXISTS `purchase_details`;
CREATE TABLE `purchase_details`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `purchase_id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(10, 0) NOT NULL DEFAULT 0,
  `quantity` smallint(6) NOT NULL DEFAULT 0,
  `discount` decimal(10, 0) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `purchase_details_purchase_id_foreign`(`purchase_id`) USING BTREE,
  INDEX `purchase_details_item_id_foreign`(`item_id`) USING BTREE,
  CONSTRAINT `purchase_details_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `purchase_details_purchase_id_foreign` FOREIGN KEY (`purchase_id`) REFERENCES `purchases` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of purchase_details
-- ----------------------------
INSERT INTO `purchase_details` VALUES (1, 1, 1, 18000, 100, 0, '2019-10-07 16:16:15', '2019-10-07 16:16:15', NULL);
INSERT INTO `purchase_details` VALUES (2, 1, 2, 25000, 200, 0, '2019-10-07 16:16:15', '2019-10-07 16:16:15', NULL);
INSERT INTO `purchase_details` VALUES (3, 2, 1, 20000, 10, 0, '2019-11-23 08:05:32', '2019-11-23 08:05:32', NULL);
INSERT INTO `purchase_details` VALUES (4, 2, 2, 25000, 20, 0, '2019-11-23 08:05:33', '2019-11-23 08:05:33', NULL);
INSERT INTO `purchase_details` VALUES (5, 3, 1, 18000, 20, 0, '2019-11-24 05:40:43', '2019-11-24 05:40:43', NULL);

-- ----------------------------
-- Table structure for purchases
-- ----------------------------
DROP TABLE IF EXISTS `purchases`;
CREATE TABLE `purchases`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `outlet_id` bigint(20) UNSIGNED NOT NULL,
  `supplier_id` bigint(20) UNSIGNED NOT NULL,
  `subtotal` decimal(10, 0) NOT NULL DEFAULT 0,
  `discount` decimal(8, 2) NOT NULL DEFAULT 0,
  `ppn` smallint(6) NOT NULL DEFAULT 0,
  `total` decimal(10, 0) NOT NULL DEFAULT 0,
  `payment` smallint(6) NOT NULL DEFAULT 1 COMMENT '1:Cash, 2:Debt',
  `information` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 1,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `purchases_outlet_id_foreign`(`outlet_id`) USING BTREE,
  INDEX `purchases_supplier_id_foreign`(`supplier_id`) USING BTREE,
  INDEX `purchases_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `purchases_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `outlets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `purchases_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `purchases_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of purchases
-- ----------------------------
INSERT INTO `purchases` VALUES (1, '2019-10-07', 'PO20191000001', 2, 1, 6800000, 5.00, 10, 7106000, 1, NULL, 1, 1, '2019-10-07 16:16:15', '2019-10-07 16:16:15', NULL);
INSERT INTO `purchases` VALUES (2, '2019-11-23', 'PO20191100001', 2, 2, 700000, 10.00, 0, 630000, 2, NULL, 1, 1, '2019-11-23 08:05:31', '2019-11-23 08:05:31', NULL);
INSERT INTO `purchases` VALUES (3, '2019-11-24', 'PO20191100002', 2, 1, 360000, 0.00, 0, 360000, 1, NULL, 1, 0, '2019-11-24 05:40:43', '2019-11-24 05:40:43', NULL);

-- ----------------------------
-- Table structure for sale_details
-- ----------------------------
DROP TABLE IF EXISTS `sale_details`;
CREATE TABLE `sale_details`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sale_id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) UNSIGNED NOT NULL,
  `purchase_price` decimal(10, 0) NOT NULL DEFAULT 0,
  `selling_price` decimal(10, 0) NOT NULL DEFAULT 0,
  `quantity` smallint(6) NOT NULL DEFAULT 0,
  `discount` decimal(10, 0) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sale_details_sale_id_foreign`(`sale_id`) USING BTREE,
  INDEX `sale_details_item_id_foreign`(`item_id`) USING BTREE,
  CONSTRAINT `sale_details_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sale_details_sale_id_foreign` FOREIGN KEY (`sale_id`) REFERENCES `sales` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sale_details
-- ----------------------------
INSERT INTO `sale_details` VALUES (1, 1, 1, 20000, 30000, 1, 2000, '2019-10-07 16:18:42', '2019-10-07 16:18:42', NULL);
INSERT INTO `sale_details` VALUES (2, 1, 2, 25000, 40000, 2, 0, '2019-10-07 16:18:42', '2019-10-07 16:18:42', NULL);

-- ----------------------------
-- Table structure for sales
-- ----------------------------
DROP TABLE IF EXISTS `sales`;
CREATE TABLE `sales`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `outlet_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `type` smallint(6) NOT NULL DEFAULT 1 COMMENT '1:Barang, 2:Service',
  `service_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `technician_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `service_item_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `service_complaint` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `service_equipment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `subtotal` decimal(10, 0) NOT NULL DEFAULT 0,
  `fee` decimal(10, 0) NOT NULL DEFAULT 0,
  `down_payment` decimal(10, 0) NOT NULL DEFAULT 0,
  `discount` decimal(8, 0) NOT NULL DEFAULT 0,
  `ppn` smallint(6) NOT NULL DEFAULT 0,
  `total` decimal(10, 0) NOT NULL DEFAULT 0,
  `payment` smallint(6) NOT NULL DEFAULT 1 COMMENT '1:Cash, 2:Debt',
  `information` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 1,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sales_outlet_id_foreign`(`outlet_id`) USING BTREE,
  INDEX `sales_customer_id_foreign`(`customer_id`) USING BTREE,
  INDEX `sales_technician_id_foreign`(`technician_id`) USING BTREE,
  INDEX `sales_service_id_foreign`(`service_id`) USING BTREE,
  INDEX `sales_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `sales_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sales_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `outlets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sales_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sales_technician_id_foreign` FOREIGN KEY (`technician_id`) REFERENCES `technicians` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sales_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sales
-- ----------------------------
INSERT INTO `sales` VALUES (1, '2019-10-07', 'SO20191000001', 2, 1, 1, NULL, NULL, NULL, NULL, NULL, 108000, 0, 0, 0, 10, 118800, 1, NULL, 1, 1, '2019-10-07 16:18:42', '2019-10-07 16:18:42', NULL);

-- ----------------------------
-- Table structure for services
-- ----------------------------
DROP TABLE IF EXISTS `services`;
CREATE TABLE `services`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `date_estimation` date NOT NULL,
  `number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `outlet_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `item_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `complaint` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `equipment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `information` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 1,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `services_outlet_id_foreign`(`outlet_id`) USING BTREE,
  INDEX `services_customer_id_foreign`(`customer_id`) USING BTREE,
  INDEX `services_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `services_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `services_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `outlets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `services_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of services
-- ----------------------------
INSERT INTO `services` VALUES (1, '2019-10-07', '2019-10-07', 'SC20191000001', 2, 1, 'Laptop Asus ROG', 'ngandat', 'charger, baterai', NULL, 1, 1, '2019-10-07 16:18:04', '2019-10-07 16:18:04', NULL);

-- ----------------------------
-- Table structure for stock_cards
-- ----------------------------
DROP TABLE IF EXISTS `stock_cards`;
CREATE TABLE `stock_cards`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `outlet_id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) UNSIGNED NOT NULL,
  `beginning` smallint(6) NOT NULL DEFAULT 0,
  `in` smallint(6) NOT NULL DEFAULT 0,
  `out` smallint(6) NOT NULL DEFAULT 0,
  `ending` smallint(6) NOT NULL DEFAULT 0,
  `information` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `stock_cards_outlet_id_foreign`(`outlet_id`) USING BTREE,
  INDEX `stock_cards_item_id_foreign`(`item_id`) USING BTREE,
  CONSTRAINT `stock_cards_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `stock_cards_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `outlets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of stock_cards
-- ----------------------------
INSERT INTO `stock_cards` VALUES (1, '2019-10-07', 2, 1, 0, 100, 0, 100, 'Tunai', '2019-10-07 16:16:15', '2019-10-07 16:16:15');
INSERT INTO `stock_cards` VALUES (2, '2019-10-07', 2, 2, 0, 200, 0, 200, 'Tunai', '2019-10-07 16:16:15', '2019-10-07 16:16:15');
INSERT INTO `stock_cards` VALUES (3, '2019-10-07', 2, 1, 100, 0, 1, 99, 'Tunai', '2019-10-07 16:18:42', '2019-10-07 16:18:42');
INSERT INTO `stock_cards` VALUES (4, '2019-10-07', 2, 2, 200, 0, 2, 198, 'Tunai', '2019-10-07 16:18:43', '2019-10-07 16:18:43');
INSERT INTO `stock_cards` VALUES (5, '2019-11-23', 2, 1, 99, 10, 0, 109, 'Tunai', '2019-11-23 08:05:32', '2019-11-23 08:05:32');
INSERT INTO `stock_cards` VALUES (6, '2019-11-23', 2, 2, 198, 20, 0, 218, 'Tunai', '2019-11-23 08:05:34', '2019-11-23 08:05:34');
INSERT INTO `stock_cards` VALUES (7, '2019-11-23', 2, 1, 109, 220, 109, 220, 'Penyesuaian AD20191100001', '2019-11-23 20:41:06', '2019-11-23 20:41:06');
INSERT INTO `stock_cards` VALUES (8, '2019-11-23', 2, 1, 220, 230, 220, 230, 'Penyesuaian AD20191100002', '2019-11-23 20:42:20', '2019-11-23 20:42:20');
INSERT INTO `stock_cards` VALUES (9, '2019-11-23', 2, 2, 218, 190, 218, 190, 'Penyesuaian AD20191100002', '2019-11-23 20:42:21', '2019-11-23 20:42:21');
INSERT INTO `stock_cards` VALUES (10, '2019-11-24', 2, 1, 230, 20, 0, 250, 'Pembelian Tunai PO20191100002', '2019-11-24 05:40:43', '2019-11-24 05:40:43');

-- ----------------------------
-- Table structure for stocks
-- ----------------------------
DROP TABLE IF EXISTS `stocks`;
CREATE TABLE `stocks`  (
  `outlet_id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` smallint(6) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `stocks_outlet_id_foreign`(`outlet_id`) USING BTREE,
  INDEX `stocks_item_id_foreign`(`item_id`) USING BTREE,
  CONSTRAINT `stocks_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `stocks_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `outlets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of stocks
-- ----------------------------
INSERT INTO `stocks` VALUES (2, 2, 190, '2019-11-23 20:42:20', '2019-11-23 20:42:20');
INSERT INTO `stocks` VALUES (2, 1, 250, '2019-11-24 05:40:43', '2019-11-24 05:40:43');

-- ----------------------------
-- Table structure for supplier_debt_payments
-- ----------------------------
DROP TABLE IF EXISTS `supplier_debt_payments`;
CREATE TABLE `supplier_debt_payments`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `supplier_id` bigint(20) UNSIGNED NOT NULL,
  `debt_nominal` decimal(10, 0) NOT NULL DEFAULT 0,
  `debt_nominal_payment` decimal(10, 0) NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `supplier_debt_payments_supplier_id_foreign`(`supplier_id`) USING BTREE,
  INDEX `supplier_debt_payments_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `supplier_debt_payments_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `supplier_debt_payments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of supplier_debt_payments
-- ----------------------------
INSERT INTO `supplier_debt_payments` VALUES (1, '2019-11-23', 'AR20191100001', 2, 630000, 130000, 1, '2019-11-23 08:05:59', '2019-11-23 08:05:59', NULL);

-- ----------------------------
-- Table structure for supplier_debts
-- ----------------------------
DROP TABLE IF EXISTS `supplier_debts`;
CREATE TABLE `supplier_debts`  (
  `supplier_id` bigint(20) UNSIGNED NOT NULL,
  `nominal` decimal(10, 0) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `supplier_debts_supplier_id_foreign`(`supplier_id`) USING BTREE,
  CONSTRAINT `supplier_debts_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of supplier_debts
-- ----------------------------
INSERT INTO `supplier_debts` VALUES (2, 500000, '2019-11-23 08:06:00', '2019-11-23 08:06:00');

-- ----------------------------
-- Table structure for suppliers
-- ----------------------------
DROP TABLE IF EXISTS `suppliers`;
CREATE TABLE `suppliers`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of suppliers
-- ----------------------------
INSERT INTO `suppliers` VALUES (1, 'Supplier 1', '088', '-', '2019-10-07 16:13:18', '2019-10-07 16:13:18', NULL);
INSERT INTO `suppliers` VALUES (2, 'Supplier 2', '089', '-', '2019-10-07 16:15:38', '2019-10-07 16:15:38', NULL);

-- ----------------------------
-- Table structure for technicians
-- ----------------------------
DROP TABLE IF EXISTS `technicians`;
CREATE TABLE `technicians`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of technicians
-- ----------------------------
INSERT INTO `technicians` VALUES (1, 'Budi', '088', '-', '2019-11-23 04:15:32', '2019-11-23 04:15:32', NULL);

-- ----------------------------
-- Table structure for user_outlets
-- ----------------------------
DROP TABLE IF EXISTS `user_outlets`;
CREATE TABLE `user_outlets`  (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `outlet_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  UNIQUE INDEX `user_outlets_user_id_outlet_id_unique`(`user_id`, `outlet_id`) USING BTREE,
  INDEX `user_outlets_outlet_id_foreign`(`outlet_id`) USING BTREE,
  CONSTRAINT `user_outlets_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `outlets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_outlets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_outlets
-- ----------------------------
INSERT INTO `user_outlets` VALUES (1, 1, NULL, NULL);
INSERT INTO `user_outlets` VALUES (1, 2, '2019-10-07 16:12:34', '2019-10-07 16:12:34');

-- ----------------------------
-- Table structure for user_privileges
-- ----------------------------
DROP TABLE IF EXISTS `user_privileges`;
CREATE TABLE `user_privileges`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_type_id` bigint(20) UNSIGNED NOT NULL,
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `create` smallint(6) NOT NULL DEFAULT 0,
  `read` smallint(6) NOT NULL DEFAULT 0,
  `update` smallint(6) NOT NULL DEFAULT 0,
  `delete` smallint(6) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_privileges_user_type_id_foreign`(`user_type_id`) USING BTREE,
  INDEX `user_privileges_menu_id_foreign`(`menu_id`) USING BTREE,
  CONSTRAINT `user_privileges_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_privileges_user_type_id_foreign` FOREIGN KEY (`user_type_id`) REFERENCES `user_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_privileges
-- ----------------------------
INSERT INTO `user_privileges` VALUES (1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (2, 1, 2, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (3, 1, 3, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (4, 1, 4, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (5, 1, 5, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (6, 1, 6, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (7, 1, 7, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (8, 1, 8, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (9, 1, 9, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (10, 1, 10, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (11, 1, 11, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (12, 1, 12, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (13, 1, 13, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (14, 1, 14, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (15, 1, 15, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (16, 1, 16, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (17, 1, 17, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (18, 1, 18, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (19, 1, 19, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (20, 1, 20, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (21, 1, 21, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (22, 1, 22, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (23, 1, 23, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (24, 1, 24, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (25, 1, 25, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (26, 1, 26, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (27, 1, 27, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (28, 1, 28, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (29, 1, 29, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (30, 1, 30, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (31, 1, 31, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (32, 1, 32, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (33, 1, 33, 1, 1, 1, 1, NULL, NULL, NULL);
INSERT INTO `user_privileges` VALUES (34, 1, 34, 1, 1, 1, 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for user_types
-- ----------------------------
DROP TABLE IF EXISTS `user_types`;
CREATE TABLE `user_types`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_types
-- ----------------------------
INSERT INTO `user_types` VALUES (1, 'Superadmin', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_type_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.png',
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_phone_unique`(`phone`) USING BTREE,
  UNIQUE INDEX `users_username_unique`(`username`) USING BTREE,
  INDEX `users_user_type_id_foreign`(`user_type_id`) USING BTREE,
  CONSTRAINT `users_user_type_id_foreign` FOREIGN KEY (`user_type_id`) REFERENCES `user_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 1, 'Superadmin', '0812', 'admin', '$2y$10$RFQR4qWylgMrWQFdCgaEuOBBlWusCfYxSRsbmDDZfTh9BDYZwMFAe', 'default.png', 'EKUrxwRG8YRwG5mcMTXRRPJwHKeVXhLUl0ZsEWMagYwrOqT23L5aceJqKJje', NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
