<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdjustmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adjustments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->string('number');
            $table->unsignedBigInteger('outlet_id');
            $table->text('information')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->smallInteger('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('outlet_id')->references('id')->on('outlets')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adjustments');
    }
}
