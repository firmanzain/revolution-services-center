<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->string('number');
            $table->unsignedBigInteger('outlet_id');
            $table->unsignedBigInteger('customer_id');
            $table->smallInteger('type')->default(1)->comment('1:Barang, 2:Service');
            $table->unsignedBigInteger('service_id')->nullable();
            $table->unsignedBigInteger('technician_id')->nullable();
            $table->string('service_item_name')->nullable();
            $table->string('service_complaint')->nullable();
            $table->string('service_equipment')->nullable();
            $table->decimal('subtotal', 10, 0)->default(0);
            $table->decimal('fee', 10, 0)->default(0);
            $table->decimal('down_payment', 10, 0)->default(0);
            $table->decimal('discount', 8, 0)->default(0);
            $table->smallInteger('ppn')->default(0);
            $table->decimal('total', 10, 0)->default(0);
            $table->smallInteger('payment')->default(1)->comment('1:Cash, 2:Debt');
            $table->text('information')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->smallInteger('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('outlet_id')->references('id')->on('outlets')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('customer_id')->references('id')->on('customers')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('technician_id')->references('id')->on('technicians')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('service_id')->references('id')->on('services')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
