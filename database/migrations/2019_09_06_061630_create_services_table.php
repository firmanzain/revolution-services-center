<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->date('date_estimation');
            $table->string('number');
            $table->unsignedBigInteger('outlet_id');
            $table->unsignedBigInteger('customer_id');
            $table->string('item_name')->nullable();
            $table->string('complaint')->nullable();
            $table->text('equipment')->nullable();
            $table->text('information')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->smallInteger('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('outlet_id')->references('id')->on('outlets')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('customer_id')->references('id')->on('customers')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
