<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->string('number');
            $table->unsignedBigInteger('outlet_id');
            $table->unsignedBigInteger('supplier_id');
            $table->decimal('subtotal', 10, 0)->default(0);
            $table->decimal('discount', 8, 2)->default(0);
            $table->smallInteger('ppn')->default(0);
            $table->decimal('total', 10, 0)->default(0);
            $table->smallInteger('payment')->default(1)->comment('1:Cash, 2:Debt');
            $table->text('information')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->smallInteger('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('outlet_id')->references('id')->on('outlets')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('supplier_id')->references('id')->on('suppliers')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
