<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierDebtTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_debts', function (Blueprint $table) {
            $table->unsignedBigInteger('supplier_id');
            $table->decimal('nominal', 10, 0)->default(0);
            $table->timestamps();
            $table->foreign('supplier_id')->references('id')->on('suppliers')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_debts');
    }
}
