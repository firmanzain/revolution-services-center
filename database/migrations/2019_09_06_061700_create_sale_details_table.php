<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sale_id');
            $table->unsignedBigInteger('item_id');
            $table->decimal('purchase_price', 10, 0)->default(0);
            $table->decimal('selling_price', 10, 0)->default(0);
            $table->smallInteger('quantity')->default(0);
            $table->decimal('discount', 10, 0)->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('sale_id')->references('id')->on('sales')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('item_id')->references('id')->on('items')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_details');
    }
}
