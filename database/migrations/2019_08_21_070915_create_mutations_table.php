<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMutationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mutations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->string('number');
            $table->unsignedBigInteger('outlet_origin_id');
            $table->unsignedBigInteger('outlet_destination_id');
            $table->text('information')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->smallInteger('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('outlet_origin_id')->references('id')->on('outlets')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('outlet_destination_id')->references('id')->on('outlets')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mutations');
    }
}
