<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMutationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mutation_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('mutation_id');
            $table->unsignedBigInteger('item_id');
            $table->smallInteger('quantity_old')->default(0);
            $table->smallInteger('quantity')->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('mutation_id')->references('id')->on('mutations')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('item_id')->references('id')->on('items')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mutation_details');
    }
}
