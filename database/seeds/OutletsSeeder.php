<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OutletsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('outlets')->insert([
            'name' => 'Semua Outlet',
            'phone' => '0812',
            'address' => '-',
        ]);
    }
}
