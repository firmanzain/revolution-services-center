<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserPrivilegesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      for ($i = 1; $i <= 34; $i++) {
        DB::table('user_privileges')->insert([
          'user_type_id' => 1,
          'menu_id' => $i,
          'create' => 1,
          'read' => 1,
          'update' => 1,
          'delete' => 1,
        ]);
      }
    }
}
