<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserOutletsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          DB::table('user_outlets')->insert([
              'user_id' => 1,
              'outlet_id' => 1,
          ]);
    }
}
