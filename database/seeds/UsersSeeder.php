<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
           'user_type_id' => 1,
           'name' => 'Superadmin',
           'phone' => '0812',
           'username' => 'admin',
           'password' => bcrypt('superadmin'),
         ]);
    }
}
