<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // AKSES
        DB::table('menus')->insert([
            'name' => 'Akses',
            'index' => 1,
            'link' => 'javascript:;',
            'icon' => '<i class="fas fa-lock"></i>',
        ]);
        DB::table('menus')->insert([
            'name' => 'Tipe Pengguna',
            'index' => 1,
            'parent' => 1,
            'link' => 'access/user-types',
            'icon' => '',
        ]);
        DB::table('menus')->insert([
            'name' => 'Pengguna',
            'index' => 2,
            'parent' => 1,
            'link' => 'access/users',
            'icon' => '',
        ]);
        // MASTER
        DB::table('menus')->insert([
            'name' => 'Master',
            'index' => 4,
            'link' => 'javascript:;',
            'icon' => '<i class="fas fa-folder"></i>',
        ]);
        DB::table('menus')->insert([
            'name' => 'Toko/Gudang',
            'index' => 1,
            'parent' => 4,
            'link' => 'master/outlets',
            'icon' => '',
        ]);
        DB::table('menus')->insert([
            'name' => 'Kategori',
            'index' => 2,
            'parent' => 4,
            'link' => 'master/categories',
            'icon' => '',
        ]);
        DB::table('menus')->insert([
            'name' => 'Merek',
            'index' => 3,
            'parent' => 4,
            'link' => 'master/brands',
            'icon' => '',
        ]);
        DB::table('menus')->insert([
            'name' => 'Barang',
            'index' => 4,
            'parent' => 4,
            'link' => 'master/items',
            'icon' => '',
        ]);
        DB::table('menus')->insert([
            'name' => 'Pemasok',
            'index' => 5,
            'parent' => 4,
            'link' => 'master/suppliers',
            'icon' => '',
        ]);
        DB::table('menus')->insert([
            'name' => 'Tipe Pelanggan',
            'index' => 6,
            'parent' => 4,
            'link' => 'master/customer-types',
            'icon' => '',
        ]);
        DB::table('menus')->insert([
            'name' => 'Pelanggan',
            'index' => 7,
            'parent' => 4,
            'link' => 'master/customers',
            'icon' => '',
        ]);
        DB::table('menus')->insert([
            'name' => 'Teknisi',
            'index' => 8,
            'parent' => 4,
            'link' => 'master/technicians',
            'icon' => '',
        ]);
        // INVENTARIS
        DB::table('menus')->insert([
            'name' => 'Inventaris',
            'index' => 3,
            'link' => 'javascript:;',
            'icon' => '<i class="fas fa-boxes"></i>',
        ]);
        DB::table('menus')->insert([
            'name' => 'Stok',
            'index' => 1,
            'parent' => 13,
            'link' => 'inventory/stocks',
            'icon' => '',
        ]);
        DB::table('menus')->insert([
            'name' => 'Pembelian',
            'index' => 2,
            'parent' => 13,
            'link' => 'inventory/purchases',
            'icon' => '',
        ]);
        DB::table('menus')->insert([
            'name' => 'Barang Masuk',
            'index' => 3,
            'parent' => 13,
            'link' => 'inventory/incoming-items',
            'icon' => '',
            'deleted_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('menus')->insert([
            'name' => 'Mutasi',
            'index' => 4,
            'parent' => 13,
            'link' => 'inventory/mutations',
            'icon' => '',
        ]);
        DB::table('menus')->insert([
            'name' => 'Penyesuaian',
            'index' => 5,
            'parent' => 13,
            'link' => 'inventory/adjustments',
            'icon' => '',
        ]);
        // TRANSAKSI
        DB::table('menus')->insert([
            'name' => 'Transaksi',
            'index' => 4,
            'link' => 'javascript:;',
            'icon' => '<i class="fas fa-book"></i>',
        ]);
        DB::table('menus')->insert([
            'name' => 'Servis',
            'index' => 1,
            'parent' => 19,
            'link' => 'transaction/services',
            'icon' => '',
        ]);
        DB::table('menus')->insert([
            'name' => 'Penjualan',
            'index' => 2,
            'parent' => 19,
            'link' => 'transaction/sales',
            'icon' => '',
        ]);
        // KEUANGAN
        DB::table('menus')->insert([
            'name' => 'Keuangan',
            'index' => 5,
            'link' => 'javascript:;',
            'icon' => '<i class="fas fa-file-invoice-dollar"></i>',
        ]);
        DB::table('menus')->insert([
            'name' => 'Pembayaran Hutang',
            'index' => 1,
            'parent' => 22,
            'link' => 'finance/debt-payments',
            'icon' => '',
        ]);
        DB::table('menus')->insert([
            'name' => 'Pembayaran Piutang',
            'index' => 2,
            'parent' => 22,
            'link' => 'finance/receivable-payments',
            'icon' => '',
        ]);
        DB::table('menus')->insert([
            'name' => 'Operasional',
            'index' => 3,
            'parent' => 22,
            'link' => 'finance/operationals',
            'icon' => '',
        ]);
        DB::table('menus')->insert([
            'name' => 'Pengeluaran Lain',
            'index' => 4,
            'parent' => 22,
            'link' => 'finance/other-expenses',
            'icon' => '',
        ]);
        // LAPORAN
        DB::table('menus')->insert([
            'name' => 'Laporan',
            'index' => 6,
            'link' => 'javascript:;',
            'icon' => '<i class="fas fa-chart-line"></i>',
        ]);
        DB::table('menus')->insert([
            'name' => 'Statistik',
            'index' => 1,
            'parent' => 27,
            'link' => 'report/statistics',
            'icon' => '',
        ]);
        DB::table('menus')->insert([
            'name' => 'Kartu Stok',
            'index' => 2,
            'parent' => 27,
            'link' => 'report/stock-cards',
            'icon' => '',
        ]);
        DB::table('menus')->insert([
            'name' => 'Pembelian',
            'index' => 3,
            'parent' => 27,
            'link' => 'report/purchases',
            'icon' => '',
        ]);
        DB::table('menus')->insert([
            'name' => 'Servis',
            'index' => 4,
            'parent' => 27,
            'link' => 'report/services',
            'icon' => '',
        ]);
        DB::table('menus')->insert([
            'name' => 'Penjualan',
            'index' => 5,
            'parent' => 27,
            'link' => 'report/sales',
            'icon' => '',
        ]);
        DB::table('menus')->insert([
            'name' => 'Hutang',
            'index' => 6,
            'parent' => 27,
            'link' => 'report/debt-payments',
            'icon' => '',
        ]);
        DB::table('menus')->insert([
            'name' => 'Piutang',
            'index' => 7,
            'parent' => 27,
            'link' => 'report/receivable-payments',
            'icon' => '',
        ]);
    }
}
