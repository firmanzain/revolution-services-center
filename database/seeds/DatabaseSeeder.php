<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTypesSeeder::class);
        $this->call(MenusSeeder::class);
        $this->call(UserPrivilegesSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(OutletsSeeder::class);
        $this->call(UserOutletsSeeder::class);
        $this->call(CategoriesSeeder::class);
        $this->call(BrandsSeeder::class);
        $this->call(CustomerTypesSeeder::class);
        $this->call(CustomersSeeder::class);
    }
}
