(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/report-debt-payments"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/report/DebtPayments.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/report/DebtPayments.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vuejs_datepicker__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuejs-datepicker */ "./node_modules/vuejs-datepicker/dist/vuejs-datepicker.esm.js");
/* harmony import */ var highcharts_vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! highcharts-vue */ "./node_modules/highcharts-vue/dist/highcharts-vue.min.js");
/* harmony import */ var highcharts_vue__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(highcharts_vue__WEBPACK_IMPORTED_MODULE_3__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



var date = new Date();
var chart1 = {
  chart: {
    type: 'column'
  },
  title: {
    text: 'Sisa Hutang ke Pemasok'
  },
  subtitle: {
    text: 'Periode: ' + moment__WEBPACK_IMPORTED_MODULE_1___default()(new Date(new Date(date.getFullYear(), date.getMonth(), 1))).format('DD MMMM YYYY') + ' s/d ' + moment__WEBPACK_IMPORTED_MODULE_1___default()(new Date(new Date(date.getFullYear(), date.getMonth() + 1, 0))).format('DD MMMM YYYY')
  },
  xAxis: {
    categories: ['Sisa Hutang'],
    crosshair: true
  },
  yAxis: {
    min: 0,
    title: {
      text: 'Nominal'
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>Rp. {point.y:.1f}</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: []
};
/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {
    var _this = this;

    if (this.globalVar.session) {
      if (this.globalVar.session.outlet) {
        if (this.globalVar.session.outlet.length > 0) {
          for (var i = 0; i < this.globalVar.session.outlet.length; i++) {
            if (this.globalVar.session.outlet[i].outlet.id != 1) {
              this.outlet = this.globalVar.session.outlet[i].outlet.id;
              this.selecteditemsOutlet = [{
                id: this.globalVar.session.outlet[i].outlet.id,
                name: this.globalVar.session.outlet[i].outlet.name
              }];
              break;
            }
          }
        }
      }
    }

    this.loadData();
    Fire.$on('AfterModified', function () {
      _this.loadData();
    });
    Fire.$on('LoadDataTable', function () {
      _this.loadDataTable();
    });
  },
  mounted: function mounted() {},
  components: {
    Datepicker: vuejs_datepicker__WEBPACK_IMPORTED_MODULE_2__["default"],
    highcharts: highcharts_vue__WEBPACK_IMPORTED_MODULE_3__["Chart"]
  },
  data: function data() {
    var sortOrders = {};
    var columns = [{
      width: '5%',
      label: 'No.',
      sortable: false
    }, {
      label: 'Tanggal',
      name: 'date',
      sortable: true
    }, {
      label: 'Nomor Refrensi',
      name: 'number',
      sortable: true
    }, {
      label: 'Pemasok',
      name: 'supplier',
      sortable: true
    }, {
      label: 'Nominal',
      name: 'nominal',
      sortable: true
    }, {
      label: 'Tipe',
      name: 'type',
      sortable: true
    }, {
      label: 'Keterangan',
      name: 'information',
      sortable: true
    }];
    columns.forEach(function (column) {
      sortOrders[column.name] = -1;
    });
    return {
      globalVar: this.$attrs.globalVar,
      renderingCount: 0,
      state: {
        firstDate: new Date(new Date(date.getFullYear(), date.getMonth(), 1)),
        lastDate: new Date(new Date(date.getFullYear(), date.getMonth() + 1, 0))
      },
      outlet: 0,
      count: {
        debt_nominal: 0,
        debt_paid: 0,
        debt_remaining: 0
      },
      selecteditemsOutlet: [],
      chart1Options: chart1,
      updateArgs1: [this.chart1Options, true, true],
      dataTables: [],
      columns: columns,
      sortKey: 'date',
      sortOrders: sortOrders,
      perPage: ['10', '25', '50', '100'],
      tableData: {
        draw: 0,
        length: 10,
        search: '',
        column: 2,
        dir: 'asc',
        outlet: 0,
        firstDate: new Date(new Date(date.getFullYear(), date.getMonth(), 1)),
        lastDate: new Date(new Date(date.getFullYear(), date.getMonth() + 1, 0))
      },
      pagination: {
        lastPage: '',
        currentPage: '',
        total: '',
        lastPageUrl: '',
        nextPageUrl: '',
        prevPageUrl: '',
        from: '',
        to: ''
      }
    };
  },
  methods: {
    configPagination: function configPagination(data) {
      this.pagination.lastPage = data.last_page;
      this.pagination.currentPage = data.current_page;
      this.pagination.total = data.total;
      this.pagination.lastPageUrl = data.last_page_url;
      this.pagination.nextPageUrl = data.next_page_url;
      this.pagination.prevPageUrl = data.prev_page_url;
      this.pagination.from = data.from;
      this.pagination.to = data.to;
    },
    sortBy: function sortBy(key) {
      this.sortKey = key; // this.sortOrders[key] = this.sortOrders[key] * -1;

      if (this.tableData.column == this.getIndex(this.columns, 'name', key)) {
        if (this.tableData.dir == 'asc') {
          this.tableData.dir = 'desc';
        } else {
          this.tableData.dir = 'asc';
        }
      } else {
        this.tableData.column = this.getIndex(this.columns, 'name', key);
        this.tableData.dir = this.sortOrders[key] === 1 ? 'asc' : 'desc';
      }

      if (this.tableData.dir == 'asc') {
        this.sortOrders[key] = 1;
      } else {
        this.sortOrders[key] = -1;
      }

      this.loadDataTable();
    },
    getIndex: function getIndex(array, key, value) {
      return array.findIndex(function (i) {
        return i[key] == value;
      });
    },
    loadDataTable: function loadDataTable() {
      var _this2 = this;

      var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '/method/report/debt-payments/transaction';
      this.tableData.draw++;
      this.tableData.outlet = this.outlet;

      if (this.tableData.outlet != 0) {
        if (this.tableData.outlet != this.selecteditemsOutlet.id) {
          this.tableData.outlet = this.selecteditemsOutlet.id;
        }
      }

      if ($("#outlet_id").select2('data')) {
        if ($("#outlet_id").select2('data')[0]) {
          this.tableData.outlet = $("#outlet_id").select2('data')[0].id;
        }
      }

      this.tableData.firstDate = moment__WEBPACK_IMPORTED_MODULE_1___default()(this.state.firstDate).format('YYYY-MM-DD');
      this.tableData.lastDate = moment__WEBPACK_IMPORTED_MODULE_1___default()(this.state.lastDate).format('YYYY-MM-DD');
      axios.get(url, {
        params: this.tableData
      }).then(function (response) {
        var data = response.data;

        if (_this2.tableData.draw == data.draw) {
          _this2.dataTables = data.data.data;

          _this2.configPagination(data.data);
        }
      })["catch"](function (errors) {});
    },
    loadData: function () {
      var _loadData = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var self, firstDate, lastDate, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                self = this;
                firstDate = moment__WEBPACK_IMPORTED_MODULE_1___default()(self.state.firstDate);
                lastDate = moment__WEBPACK_IMPORTED_MODULE_1___default()(self.state.lastDate);
                self.count.debt_nominal = 0;
                self.count.debt_paid = 0;
                self.count.debt_remaining = 0;
                _context.prev = 6;
                _context.next = 9;
                return axios.get("".concat(self.globalVar.publicPath, "method/report/debt-payments/count"), {
                  params: {
                    outlet: self.outlet,
                    firstDate: firstDate.format('YYYY-MM-DD'),
                    lastDate: lastDate.format('YYYY-MM-DD')
                  }
                });

              case 9:
                response = _context.sent;

                if (response.data) {
                  self.count.debt_nominal = response.data.debt_nominal;
                  self.count.debt_paid = response.data.debt_paid;
                  self.count.debt_remaining = response.data.debt_remaining;
                  chart1.series = response.data.chart1;
                }

                chart1.subtitle.text = 'Periode: ' + firstDate.format('DD MMMM YYYY') + ' s/d ' + lastDate.format('DD MMMM YYYY');
                _context.next = 16;
                break;

              case 14:
                _context.prev = 14;
                _context.t0 = _context["catch"](6);

              case 16:
                Fire.$emit('LoadDataTable');

              case 17:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[6, 14]]);
      }));

      function loadData() {
        return _loadData.apply(this, arguments);
      }

      return loadData;
    }(),
    checkSelected: function checkSelected() {
      this.outlet = $("#outlet_id").select2('data')[0].id;
      Fire.$emit('AfterModified');
    },
    exportExcel: function exportExcel() {
      var _this3 = this;

      this.$Progress.start();
      var outletId = this.tableData.outlet;
      axios.get('/method/report/debt-payments/transaction/excel', {
        params: {
          outlet: outletId,
          firstDate: moment__WEBPACK_IMPORTED_MODULE_1___default()(this.state.firstDate).format('YYYY-MM-DD'),
          lastDate: moment__WEBPACK_IMPORTED_MODULE_1___default()(this.state.lastDate).format('YYYY-MM-DD')
        },
        responseType: 'blob'
      }).then(function (response) {
        var fileURL = window.URL.createObjectURL(new Blob([response.data]));
        var fileLink = document.createElement('a');
        fileLink.href = fileURL;
        fileLink.setAttribute('download', 'Laporan_Hutang_' + moment__WEBPACK_IMPORTED_MODULE_1___default()(_this3.state.firstDate).format('YYYY-MM-DD') + '_s/d_' + moment__WEBPACK_IMPORTED_MODULE_1___default()(_this3.state.lastDate).format('YYYY-MM-DD') + '.xlsx');
        document.body.appendChild(fileLink);
        fileLink.click();

        _this3.$Progress.finish();
      })["catch"](function (errors) {
        _this3.$Progress.fail();
      });
    }
  },
  watch: {
    'state.firstDate': function stateFirstDate(to, from) {
      var firstDate = moment__WEBPACK_IMPORTED_MODULE_1___default()(this.state.firstDate);
      var lastDate = moment__WEBPACK_IMPORTED_MODULE_1___default()(this.state.lastDate);

      if (moment__WEBPACK_IMPORTED_MODULE_1___default()(firstDate).isAfter(lastDate)) {
        this.state.firstDate = this.formatDateDefault(lastDate);
      }

      Fire.$emit('AfterModified');
    },
    'state.lastDate': function stateLastDate(to, from) {
      var firstDate = moment__WEBPACK_IMPORTED_MODULE_1___default()(this.state.firstDate);
      var lastDate = moment__WEBPACK_IMPORTED_MODULE_1___default()(this.state.lastDate);

      if (moment__WEBPACK_IMPORTED_MODULE_1___default()(lastDate).isBefore(firstDate)) {
        this.state.lastDate = this.formatDateDefault(firstDate);
      }

      Fire.$emit('AfterModified');
    }
  },
  activated: function activated() {
    var self = this;

    if (self.globalVar.session) {
      if (self.globalVar.session.outlet) {
        if (self.globalVar.session.outlet.length > 0) {
          for (var i = 0; i < self.globalVar.session.outlet.length; i++) {
            if (self.globalVar.session.outlet[i].outlet.id != 1) {
              self.outlet = self.globalVar.session.outlet[i].outlet.id;
              self.selecteditemsOutlet = [{
                id: self.globalVar.session.outlet[i].outlet.id,
                name: self.globalVar.session.outlet[i].outlet.name
              }];
              break;
            }
          }
        }
      }
    }

    self.renderingCount++;
    setTimeout(function () {
      self.loadData();
    }, 400);
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/report/DebtPayments.vue?vue&type=template&id=39d3bc4c&":
/*!*********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/report/DebtPayments.vue?vue&type=template&id=39d3bc4c& ***!
  \*********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "content-header" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row mb-2" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "col-sm-6" }, [
            _c("ol", { staticClass: "breadcrumb float-sm-right" }, [
              _c("li", { staticClass: "breadcrumb-item" }, [
                _c("a", { attrs: { href: "javascript:;" } }, [
                  _vm._v(_vm._s(_vm.globalVar.appName))
                ])
              ]),
              _vm._v(" "),
              _vm._m(1),
              _vm._v(" "),
              _c("li", { staticClass: "breadcrumb-item active" }, [
                _vm._v("Hutang")
              ])
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("section", { staticClass: "content" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row mb-4" }, [
          _c("div", { staticClass: "col-12 col-sm-6 col-md-6" }, [
            _vm.globalVar.session
              ? _c("div", {}, [
                  _vm.globalVar.session.outlet
                    ? _c("div", {}, [
                        _vm.globalVar.session.outlet.length > 1
                          ? _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "div",
                                {
                                  key: _vm.renderingCount + "-outlet",
                                  staticClass: "col-sm-4"
                                },
                                [
                                  _c("select2", {
                                    ref: "outlet_id",
                                    attrs: {
                                      url: "/method/master/outlets/select",
                                      name: "outlet_id",
                                      selecteditems: _vm.selecteditemsOutlet,
                                      identifier: "outlet_id",
                                      placeholder: "Toko/Gudang",
                                      required: ""
                                    },
                                    on: { changed: _vm.checkSelected }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "col-sm-8" }, [
                                _c("div", { staticClass: "input-group" }, [
                                  _c(
                                    "div",
                                    { staticClass: "input-group-prepend" },
                                    [
                                      _c("datepicker", {
                                        ref: "firstDate",
                                        attrs: {
                                          "input-class": "form-control",
                                          name: "firstDate",
                                          id: "firstDate",
                                          placeholder: "Tanggal",
                                          autocomplete: "off",
                                          format: _vm.formatDateDefault
                                        },
                                        model: {
                                          value: _vm.state.firstDate,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.state,
                                              "firstDate",
                                              $$v
                                            )
                                          },
                                          expression: "state.firstDate"
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c(
                                        "span",
                                        { staticClass: "input-group-text" },
                                        [_vm._v("s/d")]
                                      ),
                                      _vm._v(" "),
                                      _c("datepicker", {
                                        ref: "lastDate",
                                        attrs: {
                                          "input-class": "form-control",
                                          name: "lastDate",
                                          id: "lastDate",
                                          placeholder: "Tanggal",
                                          autocomplete: "off",
                                          format: _vm.formatDateDefault
                                        },
                                        model: {
                                          value: _vm.state.lastDate,
                                          callback: function($$v) {
                                            _vm.$set(_vm.state, "lastDate", $$v)
                                          },
                                          expression: "state.lastDate"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                ])
                              ])
                            ])
                          : _c("div", { staticClass: "form-group row" }, [
                              _c("div", { staticClass: "col-sm-8" }, [
                                _c("div", { staticClass: "input-group" }, [
                                  _c(
                                    "div",
                                    { staticClass: "input-group-prepend" },
                                    [
                                      _c("datepicker", {
                                        ref: "firstDate",
                                        attrs: {
                                          "input-class": "form-control",
                                          name: "firstDate",
                                          id: "firstDate",
                                          placeholder: "Tanggal",
                                          autocomplete: "off",
                                          format: _vm.formatDateDefault
                                        },
                                        model: {
                                          value: _vm.state.firstDate,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.state,
                                              "firstDate",
                                              $$v
                                            )
                                          },
                                          expression: "state.firstDate"
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c(
                                        "span",
                                        { staticClass: "input-group-text" },
                                        [_vm._v("s/d")]
                                      ),
                                      _vm._v(" "),
                                      _c("datepicker", {
                                        ref: "lastDate",
                                        attrs: {
                                          "input-class": "form-control",
                                          name: "lastDate",
                                          id: "lastDate",
                                          placeholder: "Tanggal",
                                          autocomplete: "off",
                                          format: _vm.formatDateDefault
                                        },
                                        model: {
                                          value: _vm.state.lastDate,
                                          callback: function($$v) {
                                            _vm.$set(_vm.state, "lastDate", $$v)
                                          },
                                          expression: "state.lastDate"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                ])
                              ])
                            ])
                      ])
                    : _vm._e()
                ])
              : _c("div", {}, [
                  _c("div", {}, [
                    _c("div", { staticClass: "form-group row" }, [
                      _c("div", { staticClass: "col-sm-8" }, [
                        _c("div", { staticClass: "input-group" }, [
                          _c(
                            "div",
                            { staticClass: "input-group-prepend" },
                            [
                              _c("datepicker", {
                                ref: "firstDate",
                                attrs: {
                                  "input-class": "form-control",
                                  name: "firstDate",
                                  id: "firstDate",
                                  placeholder: "Tanggal",
                                  autocomplete: "off",
                                  format: _vm.formatDateDefault
                                },
                                model: {
                                  value: _vm.state.firstDate,
                                  callback: function($$v) {
                                    _vm.$set(_vm.state, "firstDate", $$v)
                                  },
                                  expression: "state.firstDate"
                                }
                              }),
                              _vm._v(" "),
                              _c("span", { staticClass: "input-group-text" }, [
                                _vm._v("s/d")
                              ]),
                              _vm._v(" "),
                              _c("datepicker", {
                                ref: "lastDate",
                                attrs: {
                                  "input-class": "form-control",
                                  name: "lastDate",
                                  id: "lastDate",
                                  placeholder: "Tanggal",
                                  autocomplete: "off",
                                  format: _vm.formatDateDefault
                                },
                                model: {
                                  value: _vm.state.lastDate,
                                  callback: function($$v) {
                                    _vm.$set(_vm.state, "lastDate", $$v)
                                  },
                                  expression: "state.lastDate"
                                }
                              })
                            ],
                            1
                          )
                        ])
                      ])
                    ])
                  ])
                ])
          ])
        ]),
        _vm._v(" "),
        _vm._m(2),
        _vm._v(" "),
        _c("div", { staticClass: "row mb-4" }, [
          _c("div", { staticClass: "col-12 col-sm-4 col-md-4" }, [
            _c("div", { staticClass: "info-box" }, [
              _c("div", { staticClass: "info-box-content" }, [
                _c("span", { staticClass: "info-box-text text-uppercase" }, [
                  _vm._v("Nominal Hutang")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "info-box-number" }, [
                  _vm._v(
                    "\n                  Rp. " +
                      _vm._s(_vm._f("formatNumber")(_vm.count.debt_nominal)) +
                      "\n              "
                  )
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-12 col-sm-4 col-md-4" }, [
            _c("div", { staticClass: "info-box mb-3" }, [
              _c("div", { staticClass: "info-box-content" }, [
                _c("span", { staticClass: "info-box-text text-uppercase" }, [
                  _vm._v("Hutang Dibayarkan")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "info-box-number" }, [
                  _vm._v(
                    "\n                  Rp. " +
                      _vm._s(_vm._f("formatNumber")(_vm.count.debt_paid)) +
                      "\n              "
                  )
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-12 col-sm-4 col-md-4" }, [
            _c("div", { staticClass: "info-box mb-3" }, [
              _c("div", { staticClass: "info-box-content" }, [
                _c("span", { staticClass: "info-box-text text-uppercase" }, [
                  _vm._v("Sisa Hutang")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "info-box-number" }, [
                  _vm._v(
                    "\n                  Rp. " +
                      _vm._s(_vm._f("formatNumber")(_vm.count.debt_remaining)) +
                      "\n              "
                  )
                ])
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row mb-4" }, [
          _c(
            "div",
            { staticClass: "col-md-12" },
            [
              _c("highcharts", {
                attrs: {
                  options: _vm.chart1Options,
                  updateArgs: _vm.updateArgs1
                }
              })
            ],
            1
          )
        ]),
        _vm._v(" "),
        _vm._m(3),
        _vm._v(" "),
        _c("div", { staticClass: "row mb-4" }, [
          _c("div", { staticClass: "col-12" }, [
            _c("div", { staticClass: "card" }, [
              _c("div", { staticClass: "card-header" }, [
                _c("h3", { staticClass: "card-title" }),
                _vm._v(" "),
                _c("div", { staticClass: "card-tools" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-success",
                      attrs: { type: "button" },
                      on: { click: _vm.exportExcel }
                    },
                    [
                      _c("i", { staticClass: "fas fa-file-excel" }),
                      _vm._v("   Export Excel\n                      ")
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("br")
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "card-body" }, [
                _c(
                  "div",
                  { staticClass: "dataTables_wrapper scroll" },
                  [
                    _c(
                      "div",
                      {
                        staticClass: "dataTables_length",
                        attrs: { id: "tableData_length" }
                      },
                      [
                        _c("label", [
                          _vm._v("Show\n                                  "),
                          _c(
                            "select",
                            {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.tableData.length,
                                  expression: "tableData.length"
                                }
                              ],
                              on: {
                                change: [
                                  function($event) {
                                    var $$selectedVal = Array.prototype.filter
                                      .call($event.target.options, function(o) {
                                        return o.selected
                                      })
                                      .map(function(o) {
                                        var val =
                                          "_value" in o ? o._value : o.value
                                        return val
                                      })
                                    _vm.$set(
                                      _vm.tableData,
                                      "length",
                                      $event.target.multiple
                                        ? $$selectedVal
                                        : $$selectedVal[0]
                                    )
                                  },
                                  function($event) {
                                    return _vm.loadDataTable()
                                  }
                                ]
                              }
                            },
                            _vm._l(_vm.perPage, function(records, index) {
                              return _c(
                                "option",
                                { key: index, domProps: { value: records } },
                                [_vm._v(_vm._s(records))]
                              )
                            }),
                            0
                          ),
                          _vm._v("\n                                  entries")
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "dataTables_filter",
                        attrs: { id: "tableData_filter" }
                      },
                      [
                        _c("label", [
                          _vm._v(
                            "Search:\n                                      "
                          ),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.tableData.search,
                                expression: "tableData.search"
                              }
                            ],
                            attrs: {
                              type: "text",
                              placeholder: "",
                              "aria-controls": "tableData"
                            },
                            domProps: { value: _vm.tableData.search },
                            on: {
                              input: [
                                function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.tableData,
                                    "search",
                                    $event.target.value
                                  )
                                },
                                function($event) {
                                  return _vm.loadDataTable()
                                }
                              ]
                            }
                          })
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "datatable",
                      {
                        attrs: {
                          columns: _vm.columns,
                          sortKey: _vm.sortKey,
                          sortOrders: _vm.sortOrders
                        },
                        on: { sort: _vm.sortBy }
                      },
                      [
                        _vm.dataTables.length != 0
                          ? _c(
                              "tbody",
                              _vm._l(_vm.dataTables, function(
                                dataTable,
                                index
                              ) {
                                return _c("tr", { key: index }, [
                                  _c("td", [_vm._v(_vm._s(index + 1))]),
                                  _vm._v(" "),
                                  _c("td", [
                                    _vm._v(
                                      _vm._s(
                                        _vm._f("formatDate")(dataTable.date)
                                      )
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c("td", [_vm._v(_vm._s(dataTable.number))]),
                                  _vm._v(" "),
                                  _c("td", [
                                    _vm._v(_vm._s(dataTable.supplier))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(
                                      "\n                                              " +
                                        _vm._s(
                                          _vm._f("formatNumber")(
                                            dataTable.nominal
                                          )
                                        ) +
                                        "\n                                          "
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c("td", [_vm._v(_vm._s(dataTable.type))]),
                                  _vm._v(" "),
                                  _c("td", [
                                    _vm._v(_vm._s(dataTable.information))
                                  ])
                                ])
                              }),
                              0
                            )
                          : _c("tbody", [
                              _c("tr", [
                                _c(
                                  "td",
                                  {
                                    staticClass: "text-center",
                                    attrs: { colspan: "7" }
                                  },
                                  [
                                    _vm._v(
                                      "\n                                              Tidak ada data yang cocok ditemukan\n                                          "
                                    )
                                  ]
                                )
                              ])
                            ])
                      ]
                    ),
                    _vm._v(" "),
                    _c("pagination", {
                      attrs: { pagination: _vm.pagination },
                      on: {
                        prev: function($event) {
                          return _vm.loadDataTable(_vm.pagination.prevPageUrl)
                        },
                        next: function($event) {
                          return _vm.loadDataTable(_vm.pagination.nextPageUrl)
                        }
                      }
                    })
                  ],
                  1
                )
              ])
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-sm-6" }, [
      _c("h1", { staticClass: "m-0 text-dark" }, [_vm._v("Hutang")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "breadcrumb-item" }, [
      _c("a", { attrs: { href: "javascript:;" } }, [_vm._v("Laporan")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row mb-4" }, [
      _c("div", { staticClass: "col-sm-12" }, [
        _c("h5", { staticClass: "m-0 text-dark text-uppercase" }, [
          _vm._v("Ringkasan")
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row mb-4" }, [
      _c("div", { staticClass: "col-sm-12" }, [
        _c("h5", { staticClass: "m-0 text-dark text-uppercase" }, [
          _vm._v("Detail Transaksi")
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backoffice/report/DebtPayments.vue":
/*!********************************************************************!*\
  !*** ./resources/js/components/backoffice/report/DebtPayments.vue ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _DebtPayments_vue_vue_type_template_id_39d3bc4c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./DebtPayments.vue?vue&type=template&id=39d3bc4c& */ "./resources/js/components/backoffice/report/DebtPayments.vue?vue&type=template&id=39d3bc4c&");
/* harmony import */ var _DebtPayments_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DebtPayments.vue?vue&type=script&lang=js& */ "./resources/js/components/backoffice/report/DebtPayments.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _DebtPayments_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _DebtPayments_vue_vue_type_template_id_39d3bc4c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _DebtPayments_vue_vue_type_template_id_39d3bc4c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backoffice/report/DebtPayments.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backoffice/report/DebtPayments.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/components/backoffice/report/DebtPayments.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DebtPayments_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./DebtPayments.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/report/DebtPayments.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DebtPayments_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backoffice/report/DebtPayments.vue?vue&type=template&id=39d3bc4c&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/components/backoffice/report/DebtPayments.vue?vue&type=template&id=39d3bc4c& ***!
  \***************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DebtPayments_vue_vue_type_template_id_39d3bc4c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./DebtPayments.vue?vue&type=template&id=39d3bc4c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/report/DebtPayments.vue?vue&type=template&id=39d3bc4c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DebtPayments_vue_vue_type_template_id_39d3bc4c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DebtPayments_vue_vue_type_template_id_39d3bc4c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);