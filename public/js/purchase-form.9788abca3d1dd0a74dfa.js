(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/purchase-form"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/inventory/PurchaseForm.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/inventory/PurchaseForm.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuejs_datepicker__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuejs-datepicker */ "./node_modules/vuejs-datepicker/dist/vuejs-datepicker.esm.js");
/* harmony import */ var vuejs_auto_complete__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuejs-auto-complete */ "./node_modules/vuejs-auto-complete/dist/build.js");
/* harmony import */ var vuejs_auto_complete__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vuejs_auto_complete__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


var date = new Date();
/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {
    var _this = this;

    if (this.$route.params.id) {
      this.loadData();
    }

    Fire.$on('AfterLoaded', function () {
      _this.loadData();
    });
  },
  mounted: function mounted() {},
  data: function data() {
    return {
      globalVar: this.$attrs.globalVar,
      renderingCount: 0,
      renderingCountSupplier: 0,
      renderingCountTechnician: 0,
      form: new Form({
        id: '',
        date: date,
        number: '',
        outlet_id: '',
        supplier_id: '',
        supplier_debt: 0,
        subtotal: 0,
        discount: 0,
        ppn: 0,
        total: 0,
        payment: 1,
        information: '',
        details: []
      }),
      selecteditemsOutlet: [],
      selecteditemsSupplier: [],
      itemAutocomplete: {
        placeholder: 'Cari barang'
      },
      itemDetails: []
    };
  },
  components: {
    Datepicker: vuejs_datepicker__WEBPACK_IMPORTED_MODULE_0__["default"],
    Autocomplete: vuejs_auto_complete__WEBPACK_IMPORTED_MODULE_1___default.a
  },
  methods: {
    submitData: function submitData() {
      var self = this;
      self.$Progress.start();
      var outletId = 0;

      if (self.globalVar.session.outlet.length > 1) {
        if ($("#outlet_id").select2('data')[0]) {
          outletId = $("#outlet_id").select2('data')[0].id;
        }
      } else {
        outletId = self.globalVar.session.outlet[0].outlet.id;
      }

      self.form.outlet_id = outletId;
      var supplierId = 0;

      if ($("#supplier_id").select2('data')[0]) {
        supplierId = $("#supplier_id").select2('data')[0].id;
      }

      self.form.supplier_id = supplierId;
      self.form.details = self.itemDetails;

      if (self.form.id) {
        self.form.put('/method/inventory/purchases/' + self.form.id).then(function () {
          Toast.fire({
            type: 'success',
            title: 'Data updated successfully'
          });
          self.$Progress.finish();
          setTimeout(function () {
            self.$router.replace('/inventory/purchases');
          }, 2100);
        })["catch"](function () {
          Swal.fire('Alert!', 'Something went wrong.', 'warning');
          self.$Progress.fail();
        });
      } else {
        self.form.post('/method/inventory/purchases').then(function () {
          Toast.fire({
            type: 'success',
            title: 'Data saved successfully'
          });
          self.$Progress.finish();
          setTimeout(function () {
            self.$router.replace('/inventory/purchases');
          }, 2100);
        })["catch"](function () {
          Swal.fire('Alert!', 'Something went wrong.', 'warning');
          self.$Progress.fail();
        });
      }
    },
    getItemDetails: function getItemDetails(data) {
      var self = this;
      var outletId = 0;

      if (self.globalVar.session.outlet.length > 1) {
        if ($("#outlet_id").select2('data')[0]) {
          outletId = $("#outlet_id").select2('data')[0].id;
        }
      } else {
        outletId = self.globalVar.session.outlet[0].outlet.id;
      }

      var supplierId = 0;

      if ($("#supplier_id").select2('data')[0]) {
        supplierId = $("#supplier_id").select2('data')[0].id;
      }

      axios.get('/method/inventory/stocks/' + outletId + '/' + data.value).then(function (response) {
        var quantityStock = 0;

        if (response.data.quantity) {
          quantityStock = response.data.quantity;
        }

        var flag = 0;

        for (var i = 0; i < self.itemDetails.length; i++) {
          if (self.itemDetails[i].item_id == data.selectedObject.id) {
            flag = 1;
            break;
          }
        }

        axios.get('/method/master/suppliers/price/' + supplierId + '/' + data.selectedObject.id).then(function (response) {
          var price = data.selectedObject.purchase_price.price;

          if (response.data.price) {
            price = response.data.price;
          }

          if (flag == 0) {
            self.itemDetails.push({
              item_id: data.selectedObject.id,
              code: data.selectedObject.code,
              name: data.selectedObject.name,
              price: self.formatNumber(price),
              quantity_stock: self.formatNumber(quantityStock),
              quantity: 0,
              discount_nominal: 0,
              price_total: 0
            });
          }
        });
      })["catch"](function (error) {});
      this.$refs.autocompleteItems.clear();
    },
    deleteDataDetail: function deleteDataDetail(index) {
      var _this2 = this;

      Swal.fire({
        title: 'Are you sure?',
        text: 'Record will be deleted.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
      }).then(function (result) {
        if (result.value) {
          _this2.$delete(_this2.itemDetails, index);

          _this2.sumTotal();
        }
      });
    },
    formatNumberDetails: function formatNumberDetails(value, index) {
      var formatted = this.formatNumber(value);
      this.itemDetails[index].quantity = formatted;
      this.sumItemPrice();
    },
    formatNumberDiscountDetails: function formatNumberDiscountDetails(value, index) {
      var formatted = this.formatNumber(value);
      this.itemDetails[index].discount_nominal = formatted;
      this.sumItemPrice();
    },
    sumItemPrice: function sumItemPrice() {
      for (var i = 0; i < this.itemDetails.length; i++) {
        var price = this.parseNumber(this.itemDetails[i].price);
        var qty = this.parseNumber(this.itemDetails[i].quantity);
        var discount = this.parseNumber(this.itemDetails[i].discount_nominal);
        var formatted = this.formatNumber(price * qty - qty * discount);
        this.itemDetails[i].price_total = formatted;
      }

      this.sumTotal();
    },
    resetDetail: function resetDetail() {
      this.itemDetails = [];
    },
    sumTotal: function sumTotal() {
      var subTotal = 0;

      for (var i = 0; i < this.itemDetails.length; i++) {
        subTotal += this.parseNumber(this.itemDetails[i].price_total);
      }

      this.form.subtotal = this.formatNumber(subTotal);
      var discount = 0;

      if (this.form.discount != 0) {
        discount = subTotal * this.parseNumber(this.form.discount) / 100;
      }

      subTotal -= discount;
      var ppn = 0;

      if (this.form.ppn != 0) {
        ppn = subTotal * this.parseNumber(this.form.ppn) / 100;
      }

      subTotal += ppn;
      this.form.total = this.formatNumber(subTotal);
    },
    getSupplier: function getSupplier() {
      var _this3 = this;

      var self = this;
      self.itemDetails = [];
      var supplierId = 0;

      if ($("#supplier_id").select2('data')[0]) {
        supplierId = $("#supplier_id").select2('data')[0].id;
      }

      axios.get('/method/master/suppliers/' + supplierId).then(function (response) {
        self.form.supplier_debt = 0;

        if (response.data.debt) {
          self.form.supplier_debt = _this3.formatNumber(response.data.debt.nominal);
        }
      })["catch"](function (error) {});
    },
    loadData: function loadData() {
      var self = this;
      axios.get('/method/inventory/purchases/' + self.$route.params.id).then(function (response) {
        if (response.data) {
          self.form.id = response.data.id;
          self.form.date = response.data.date;
          self.form.number = response.data.number;
          self.form.outlet_id = response.data.outlet.id;
          self.form.supplier_id = response.data.supplier.id;
          self.form.supplier_debt = 0;

          if (response.data.supplier.debt) {
            self.form.supplier_debt = self.formatNumber(response.data.supplier.debt.nominal);
          }

          self.form.subtotal = self.formatNumber(response.data.subtotal);
          self.form.discount = self.formatNumber(response.data.discount);
          self.form.ppn = response.data.ppn;
          self.form.total = self.formatNumber(response.data.total);
          self.form.payment = response.data.payment;
          self.form.information = response.data.information;
          self.selecteditemsOutlet = [{
            id: response.data.outlet.id,
            name: response.data.outlet.name
          }];
          self.selecteditemsSupplier = [{
            id: response.data.supplier.id,
            name: response.data.supplier.name
          }];
          self.itemDetails = [];

          for (var i = 0; i < response.data.detail.length; i++) {
            self.itemDetails.push({
              item_id: response.data.detail[i].item.id,
              code: response.data.detail[i].item.code,
              name: response.data.detail[i].item.name,
              price: self.formatNumber(response.data.detail[i].price),
              quantity_stock: 0,
              quantity: self.formatNumber(response.data.detail[i].quantity),
              price_total: self.formatNumber(response.data.detail[i].price * response.data.detail[i].quantity)
            });
          }
        }
      })["catch"](function (error) {});
    }
  },
  watch: {
    'form.ppn': function formPpn(to, from) {
      this.sumTotal();
    },
    'form.discount': function formDiscount(to, from) {
      var formatted = this.formatNumber(to);
      this.form.discount = formatted;
      this.sumTotal();
    }
  },
  activated: function activated() {
    var self = this;
    $("#outlet_id").val('').trigger('change');
    self.form.clear();
    self.form.reset();
    self.form.date = date;
    self.selecteditemsOutlet = [];
    self.selecteditemsSupplier = [];
    self.itemDetails = [];
    self.renderingCount++;
    self.renderingCountSupplier++;

    if (self.$route.params.id) {
      setTimeout(function () {
        self.loadData();
      }, 400);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/inventory/PurchaseForm.vue?vue&type=template&id=74d1a9c9&":
/*!************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/inventory/PurchaseForm.vue?vue&type=template&id=74d1a9c9& ***!
  \************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "content-header" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row mb-2" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "col-sm-6" }, [
            _c("ol", { staticClass: "breadcrumb float-sm-right" }, [
              _c("li", { staticClass: "breadcrumb-item" }, [
                _c("a", { attrs: { href: "javascript:;" } }, [
                  _vm._v(_vm._s(_vm.globalVar.appName))
                ])
              ]),
              _vm._v(" "),
              _vm._m(1),
              _vm._v(" "),
              _c("li", { staticClass: "breadcrumb-item active" }, [
                _vm._v("Pembelian")
              ])
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("section", { staticClass: "content" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12" }, [
            _c("div", { staticClass: "card" }, [
              _vm._m(2),
              _vm._v(" "),
              _c("div", { staticClass: "card-body" }, [
                _c(
                  "form",
                  {
                    attrs: { id: "formAdd" },
                    on: {
                      submit: function($event) {
                        $event.preventDefault()
                        return _vm.submitData($event)
                      }
                    }
                  },
                  [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.form.id,
                          expression: "form.id"
                        }
                      ],
                      staticClass: "d-none",
                      class: { "is-invalid": _vm.form.errors.has("id") },
                      attrs: { type: "text", name: "id", id: "id" },
                      domProps: { value: _vm.form.id },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.form, "id", $event.target.value)
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("has-error", { attrs: { form: _vm.form, field: "id" } }),
                    _vm._v(" "),
                    _vm.globalVar.session
                      ? _c("div", { staticClass: "form-group row" }, [
                          _vm.globalVar.session.outlet.length > 1
                            ? _c(
                                "label",
                                {
                                  staticClass: "col-sm-2 col-form-label",
                                  attrs: { for: "outlet_id" }
                                },
                                [
                                  _vm._v(
                                    "\n                                Toko/Gudang\n                            "
                                  )
                                ]
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.globalVar.session.outlet.length > 1
                            ? _c(
                                "div",
                                {
                                  key: _vm.renderingCount + "-outlet",
                                  staticClass: "col-sm-4"
                                },
                                [
                                  _c("select2", {
                                    ref: "outlet_id",
                                    attrs: {
                                      url: "/method/master/outlets/select",
                                      name: "outlet_id",
                                      selecteditems: _vm.selecteditemsOutlet,
                                      identifier: "outlet_id",
                                      placeholder: "Toko/Gudang",
                                      required: "",
                                      disabled: _vm.$route.params.id
                                        ? true
                                        : false
                                    },
                                    on: { changed: _vm.resetDetail }
                                  }),
                                  _vm._v(" "),
                                  _c("has-error", {
                                    attrs: {
                                      form: _vm.form,
                                      field: "outlet_id"
                                    }
                                  })
                                ],
                                1
                              )
                            : _vm._e()
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "date" }
                        },
                        [
                          _vm._v(
                            "\n                                Tanggal\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("datepicker", {
                            ref: "date",
                            class: {
                              "is-invalid": _vm.form.errors.has("date")
                            },
                            attrs: {
                              "input-class": "form-control",
                              name: "date",
                              id: "date",
                              placeholder: "Tanggal",
                              autocomplete: "off",
                              format: _vm.formatDateDefault,
                              required: "",
                              disabled: _vm.$route.params.id ? true : false
                            },
                            model: {
                              value: _vm.form.date,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "date", $$v)
                              },
                              expression: "form.date"
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "date" }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "number" }
                        },
                        [
                          _vm._v(
                            "\n                                Nomor Transaksi\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.number,
                                expression: "form.number"
                              }
                            ],
                            ref: "number",
                            staticClass: "form-control",
                            class: {
                              "is-invalid": _vm.form.errors.has("number")
                            },
                            attrs: {
                              type: "text",
                              name: "number",
                              id: "number",
                              placeholder: "Nomor Transaksi",
                              autocomplete: "off",
                              readonly: ""
                            },
                            domProps: { value: _vm.form.number },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "number",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "number" }
                          }),
                          _vm._v(" "),
                          _c("small", { staticClass: "form-text text-muted" }, [
                            _vm._v(
                              "\n                                    *) Otomatis dari sistem.\n                                "
                            )
                          ])
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "supplier_id" }
                        },
                        [
                          _vm._v(
                            "\n                                Pemasok\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          key: _vm.renderingCountSupplier + "-supplier",
                          staticClass: "col-sm-4"
                        },
                        [
                          _c("select2", {
                            ref: "supplier_id",
                            attrs: {
                              url: "/method/master/suppliers/select",
                              name: "supplier_id",
                              selecteditems: _vm.selecteditemsSupplier,
                              identifier: "supplier_id",
                              placeholder: "Pemasok",
                              required: "",
                              disabled: _vm.$route.params.id ? true : false
                            },
                            on: { changed: _vm.getSupplier }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "supplier_id" }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "supplier_debt" }
                        },
                        [
                          _vm._v(
                            "\n                                Hutang ke supplier\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.supplier_debt,
                                expression: "form.supplier_debt"
                              }
                            ],
                            ref: "supplier_debt",
                            staticClass: "form-control text-right",
                            class: {
                              "is-invalid": _vm.form.errors.has("supplier_debt")
                            },
                            attrs: {
                              type: "text",
                              name: "supplier_debt",
                              id: "supplier_debt",
                              placeholder: "Hutang",
                              autocomplete: "off",
                              readonly: ""
                            },
                            domProps: { value: _vm.form.supplier_debt },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "supplier_debt",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "supplier_debt" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    !_vm.$route.params.id
                      ? _c("div", { staticClass: "form-group row" }, [
                          _c(
                            "label",
                            { staticClass: "col-sm-2 col-form-label" },
                            [
                              _vm._v(
                                "\n                                Cari Barang\n                            "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "col-sm-4" },
                            [
                              _c("autocomplete", {
                                ref: "autocompleteItems",
                                attrs: {
                                  inputClass: "form-control",
                                  placeholder:
                                    "" + _vm.itemAutocomplete.placeholder,
                                  source:
                                    "/method/master/items/autocomplete?search=",
                                  "results-display":
                                    _vm.formattedAutocompleteItems
                                },
                                on: { selected: _vm.getItemDetails }
                              })
                            ],
                            1
                          )
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c("div", { staticClass: "col-sm-12" }, [
                        _c(
                          "table",
                          {
                            staticClass:
                              "table table-bordered table-striped table-hover dataTable no-footer",
                            attrs: { id: "tableData" }
                          },
                          [
                            _c("thead", [
                              _c("tr", [
                                _c("th", { staticStyle: { width: "5%" } }, [
                                  _vm._v("No.")
                                ]),
                                _vm._v(" "),
                                _c("th", [_vm._v("Kode")]),
                                _vm._v(" "),
                                _c("th", [_vm._v("Nama Barang")]),
                                _vm._v(" "),
                                _c("th", { staticStyle: { width: "15%" } }, [
                                  _vm._v("Harga")
                                ]),
                                _vm._v(" "),
                                !_vm.$route.params.id
                                  ? _c(
                                      "th",
                                      { staticStyle: { width: "10%" } },
                                      [_vm._v("Stok")]
                                    )
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("th", { staticStyle: { width: "10%" } }, [
                                  _vm._v("Kuantiti")
                                ]),
                                _vm._v(" "),
                                _c("th", { staticStyle: { width: "15%" } }, [
                                  _vm._v("Potongan")
                                ]),
                                _vm._v(" "),
                                _c("th", { staticStyle: { width: "15%" } }, [
                                  _vm._v("Total")
                                ]),
                                _vm._v(" "),
                                !_vm.$route.params.id
                                  ? _c("th", { staticStyle: { width: "5%" } })
                                  : _vm._e()
                              ])
                            ]),
                            _vm._v(" "),
                            _vm.itemDetails.length > 0
                              ? _c(
                                  "tbody",
                                  _vm._l(_vm.itemDetails, function(
                                    itemDetail,
                                    index
                                  ) {
                                    return _c("tr", { key: index }, [
                                      _c("td", [
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: itemDetail.item_id,
                                              expression: "itemDetail.item_id"
                                            }
                                          ],
                                          staticClass: "d-none",
                                          attrs: {
                                            type: "text",
                                            name: "item_id[]",
                                            id: "item_id" + index
                                          },
                                          domProps: {
                                            value: itemDetail.item_id
                                          },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                itemDetail,
                                                "item_id",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        }),
                                        _vm._v(
                                          "\n                                                  " +
                                            _vm._s(index + 1) +
                                            "\n                                              "
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("td", [
                                        _vm._v(_vm._s(itemDetail.code))
                                      ]),
                                      _vm._v(" "),
                                      _c("td", [
                                        _vm._v(_vm._s(itemDetail.name))
                                      ]),
                                      _vm._v(" "),
                                      _c("td", [
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: itemDetail.price,
                                              expression: "itemDetail.price"
                                            }
                                          ],
                                          staticClass:
                                            "form-control text-right",
                                          attrs: {
                                            type: "text",
                                            name: "price[]",
                                            id: "price" + index,
                                            placeholder: "Stok",
                                            autocomplete: "off",
                                            maxlength: "10",
                                            required: "",
                                            readonly: ""
                                          },
                                          domProps: { value: itemDetail.price },
                                          on: {
                                            keypress: function($event) {
                                              return _vm.isNumber($event)
                                            },
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                itemDetail,
                                                "price",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      !_vm.$route.params.id
                                        ? _c("td", [
                                            _c("input", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value:
                                                    itemDetail.quantity_stock,
                                                  expression:
                                                    "itemDetail.quantity_stock"
                                                }
                                              ],
                                              staticClass:
                                                "form-control text-right",
                                              attrs: {
                                                type: "text",
                                                name: "quantity_stock[]",
                                                id: "quantity_stock" + index,
                                                placeholder: "Stok",
                                                autocomplete: "off",
                                                maxlength: "10",
                                                required: "",
                                                readonly: ""
                                              },
                                              domProps: {
                                                value: itemDetail.quantity_stock
                                              },
                                              on: {
                                                keypress: function($event) {
                                                  return _vm.isNumber($event)
                                                },
                                                input: function($event) {
                                                  if ($event.target.composing) {
                                                    return
                                                  }
                                                  _vm.$set(
                                                    itemDetail,
                                                    "quantity_stock",
                                                    $event.target.value
                                                  )
                                                }
                                              }
                                            })
                                          ])
                                        : _vm._e(),
                                      _vm._v(" "),
                                      _c("td", [
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: itemDetail.quantity,
                                              expression: "itemDetail.quantity"
                                            }
                                          ],
                                          staticClass:
                                            "form-control text-right",
                                          attrs: {
                                            type: "text",
                                            name: "quantity[]",
                                            id: "quantity" + index,
                                            placeholder: "Kuantiti Lama",
                                            autocomplete: "off",
                                            maxlength: "10",
                                            required: "",
                                            readonly: _vm.$route.params.id
                                              ? true
                                              : false
                                          },
                                          domProps: {
                                            value: itemDetail.quantity
                                          },
                                          on: {
                                            keypress: function($event) {
                                              return _vm.isNumber($event)
                                            },
                                            input: [
                                              function($event) {
                                                if ($event.target.composing) {
                                                  return
                                                }
                                                _vm.$set(
                                                  itemDetail,
                                                  "quantity",
                                                  $event.target.value
                                                )
                                              },
                                              function($event) {
                                                return _vm.formatNumberDetails(
                                                  itemDetail.quantity,
                                                  index
                                                )
                                              }
                                            ]
                                          }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      _c("td", [
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value:
                                                itemDetail.discount_nominal,
                                              expression:
                                                "itemDetail.discount_nominal"
                                            }
                                          ],
                                          staticClass:
                                            "form-control text-right",
                                          attrs: {
                                            type: "text",
                                            name: "discount_nominal[]",
                                            id: "discount_nominal" + index,
                                            placeholder: "Potongan",
                                            autocomplete: "off",
                                            maxlength: "10",
                                            required: "",
                                            readonly: _vm.$route.params.id
                                              ? true
                                              : false
                                          },
                                          domProps: {
                                            value: itemDetail.discount_nominal
                                          },
                                          on: {
                                            keypress: function($event) {
                                              return _vm.isNumber($event)
                                            },
                                            input: [
                                              function($event) {
                                                if ($event.target.composing) {
                                                  return
                                                }
                                                _vm.$set(
                                                  itemDetail,
                                                  "discount_nominal",
                                                  $event.target.value
                                                )
                                              },
                                              function($event) {
                                                return _vm.formatNumberDiscountDetails(
                                                  itemDetail.discount_nominal,
                                                  index
                                                )
                                              }
                                            ]
                                          }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      _c("td", [
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: itemDetail.price_total,
                                              expression:
                                                "itemDetail.price_total"
                                            }
                                          ],
                                          staticClass:
                                            "form-control text-right",
                                          attrs: {
                                            type: "text",
                                            name: "price_total[]",
                                            id: "price_total" + index,
                                            placeholder: "Stok",
                                            autocomplete: "off",
                                            maxlength: "10",
                                            required: "",
                                            readonly: ""
                                          },
                                          domProps: {
                                            value: itemDetail.price_total
                                          },
                                          on: {
                                            keypress: function($event) {
                                              return _vm.isNumber($event)
                                            },
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                itemDetail,
                                                "price_total",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      !_vm.$route.params.id
                                        ? _c("td", [
                                            _c(
                                              "button",
                                              {
                                                staticClass:
                                                  "btn btn-danger btn-sm",
                                                attrs: {
                                                  type: "button",
                                                  title: "Hapus Data"
                                                },
                                                on: {
                                                  click: function($event) {
                                                    return _vm.deleteDataDetail(
                                                      index
                                                    )
                                                  }
                                                }
                                              },
                                              [
                                                _c("i", {
                                                  staticClass: "fas fa-ban"
                                                })
                                              ]
                                            )
                                          ])
                                        : _vm._e()
                                    ])
                                  }),
                                  0
                                )
                              : _c("tbody", [_vm._m(3)])
                          ]
                        )
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label offset-md-6",
                          attrs: { for: "subtotal" }
                        },
                        [
                          _vm._v(
                            "\n                                Sub Total\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.subtotal,
                                expression: "form.subtotal"
                              }
                            ],
                            ref: "subtotal",
                            staticClass: "form-control text-right",
                            class: {
                              "is-invalid": _vm.form.errors.has("subtotal")
                            },
                            attrs: {
                              type: "text",
                              name: "subtotal",
                              id: "subtotal",
                              placeholder: "Sub Total",
                              autocomplete: "off",
                              readonly: "",
                              required: ""
                            },
                            domProps: { value: _vm.form.subtotal },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "subtotal",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "subtotal" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label offset-md-6",
                          attrs: { for: "discount" }
                        },
                        [
                          _vm._v(
                            "\n                                Diskon\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("div", { staticClass: "input-group" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.discount,
                                  expression: "form.discount"
                                }
                              ],
                              ref: "discount",
                              staticClass: "form-control text-right",
                              class: {
                                "is-invalid": _vm.form.errors.has("discount")
                              },
                              attrs: {
                                type: "text",
                                name: "discount",
                                id: "discount",
                                placeholder: "Sub Total",
                                autocomplete: "off",
                                required: "",
                                readonly: _vm.$route.params.id ? true : false
                              },
                              domProps: { value: _vm.form.discount },
                              on: {
                                keypress: function($event) {
                                  return _vm.isNumber($event)
                                },
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.form,
                                    "discount",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _vm._m(4)
                          ]),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "discount" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label offset-md-6",
                          attrs: { for: "ppn" }
                        },
                        [
                          _vm._v(
                            "\n                                PPN\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c(
                            "div",
                            { staticClass: "form-check form-check-inline" },
                            [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.form.ppn,
                                    expression: "form.ppn"
                                  }
                                ],
                                staticClass: "form-check-input",
                                class: {
                                  "is-invalid": _vm.form.errors.has("ppn")
                                },
                                attrs: {
                                  type: "radio",
                                  name: "ppn",
                                  id: "ppn1",
                                  value: "0",
                                  checked: "",
                                  disabled: _vm.$route.params.id ? true : false
                                },
                                domProps: {
                                  checked: _vm._q(_vm.form.ppn, "0")
                                },
                                on: {
                                  change: function($event) {
                                    return _vm.$set(_vm.form, "ppn", "0")
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "label",
                                {
                                  staticClass: "form-check-label",
                                  attrs: { for: "ppn1" }
                                },
                                [
                                  _vm._v(
                                    "\n                                      Tidak ada\n                                  "
                                  )
                                ]
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "form-check form-check-inline" },
                            [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.form.ppn,
                                    expression: "form.ppn"
                                  }
                                ],
                                staticClass: "form-check-input",
                                class: {
                                  "is-invalid": _vm.form.errors.has("ppn")
                                },
                                attrs: {
                                  type: "radio",
                                  name: "ppn",
                                  id: "ppn2",
                                  value: "10",
                                  disabled: _vm.$route.params.id ? true : false
                                },
                                domProps: {
                                  checked: _vm._q(_vm.form.ppn, "10")
                                },
                                on: {
                                  change: function($event) {
                                    return _vm.$set(_vm.form, "ppn", "10")
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "label",
                                {
                                  staticClass: "form-check-label",
                                  attrs: { for: "ppn2" }
                                },
                                [
                                  _vm._v(
                                    "\n                                      10%\n                                  "
                                  )
                                ]
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "ppn" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label offset-md-6",
                          attrs: { for: "total" }
                        },
                        [
                          _vm._v(
                            "\n                                Total\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.total,
                                expression: "form.total"
                              }
                            ],
                            ref: "total",
                            staticClass: "form-control text-right",
                            class: {
                              "is-invalid": _vm.form.errors.has("total")
                            },
                            attrs: {
                              type: "text",
                              name: "total",
                              id: "total",
                              placeholder: "Total",
                              autocomplete: "off",
                              readonly: "",
                              required: ""
                            },
                            domProps: { value: _vm.form.total },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(_vm.form, "total", $event.target.value)
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "total" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label offset-md-6",
                          attrs: { for: "payment" }
                        },
                        [
                          _vm._v(
                            "\n                                Pembayaran\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c(
                            "div",
                            { staticClass: "form-check form-check-inline" },
                            [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.form.payment,
                                    expression: "form.payment"
                                  }
                                ],
                                staticClass: "form-check-input",
                                class: {
                                  "is-invalid": _vm.form.errors.has("payment")
                                },
                                attrs: {
                                  type: "radio",
                                  name: "payment",
                                  id: "payment1",
                                  value: "1",
                                  checked: "",
                                  disabled: _vm.$route.params.id ? true : false
                                },
                                domProps: {
                                  checked: _vm._q(_vm.form.payment, "1")
                                },
                                on: {
                                  change: function($event) {
                                    return _vm.$set(_vm.form, "payment", "1")
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "label",
                                {
                                  staticClass: "form-check-label",
                                  attrs: { for: "payment1" }
                                },
                                [
                                  _vm._v(
                                    "\n                                      Tunai\n                                  "
                                  )
                                ]
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "form-check form-check-inline" },
                            [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.form.payment,
                                    expression: "form.payment"
                                  }
                                ],
                                staticClass: "form-check-input",
                                class: {
                                  "is-invalid": _vm.form.errors.has("payment")
                                },
                                attrs: {
                                  type: "radio",
                                  name: "payment",
                                  id: "payment2",
                                  value: "2",
                                  disabled: _vm.$route.params.id ? true : false
                                },
                                domProps: {
                                  checked: _vm._q(_vm.form.payment, "2")
                                },
                                on: {
                                  change: function($event) {
                                    return _vm.$set(_vm.form, "payment", "2")
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "label",
                                {
                                  staticClass: "form-check-label",
                                  attrs: { for: "payment2" }
                                },
                                [
                                  _vm._v(
                                    "\n                                      Hutang\n                                  "
                                  )
                                ]
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "payment" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "information" }
                        },
                        [
                          _vm._v(
                            "\n                                Keterangan\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-10" },
                        [
                          _c("textarea", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.information,
                                expression: "form.information"
                              }
                            ],
                            ref: "information",
                            staticClass: "form-control",
                            class: {
                              "is-invalid": _vm.form.errors.has("information")
                            },
                            attrs: {
                              name: "information",
                              rows: "3",
                              id: "information",
                              placeholder: "Keterangan",
                              autocomplete: "off",
                              readonly: _vm.$route.params.id ? true : false
                            },
                            domProps: { value: _vm.form.information },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "information",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "information" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "div",
                        { staticClass: "col-sm-12 text-right" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "btn btn-secondary",
                              attrs: { to: "/inventory/purchases" }
                            },
                            [
                              _vm._v(
                                "\n                                Batal\n                              "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          !_vm.$route.params.id
                            ? _c(
                                "button",
                                {
                                  staticClass: "btn btn-success",
                                  attrs: { type: "submit" }
                                },
                                [
                                  _vm._v(
                                    "\n                                  Simpan\n                              "
                                  )
                                ]
                              )
                            : _vm._e()
                        ],
                        1
                      )
                    ])
                  ],
                  1
                )
              ])
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-sm-6" }, [
      _c("h1", { staticClass: "m-0 text-dark" }, [_vm._v("Pembelian")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "breadcrumb-item" }, [
      _c("a", { attrs: { href: "javascript:;" } }, [_vm._v("Inventaris")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header" }, [
      _c("h3", { staticClass: "card-title" }),
      _vm._v(" "),
      _c("div", { staticClass: "card-tools" }),
      _vm._v(" "),
      _c("br")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [
      _c("td", { staticClass: "text-center", attrs: { colspan: "9" } }, [
        _vm._v(
          "\n                                                Tidak ada data yang cocok ditemukan\n                                              "
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-append" }, [
      _c(
        "span",
        { staticClass: "input-group-text", attrs: { id: "basic-addon" } },
        [_vm._v("%")]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backoffice/inventory/PurchaseForm.vue":
/*!***********************************************************************!*\
  !*** ./resources/js/components/backoffice/inventory/PurchaseForm.vue ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PurchaseForm_vue_vue_type_template_id_74d1a9c9___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PurchaseForm.vue?vue&type=template&id=74d1a9c9& */ "./resources/js/components/backoffice/inventory/PurchaseForm.vue?vue&type=template&id=74d1a9c9&");
/* harmony import */ var _PurchaseForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PurchaseForm.vue?vue&type=script&lang=js& */ "./resources/js/components/backoffice/inventory/PurchaseForm.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PurchaseForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PurchaseForm_vue_vue_type_template_id_74d1a9c9___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PurchaseForm_vue_vue_type_template_id_74d1a9c9___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backoffice/inventory/PurchaseForm.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backoffice/inventory/PurchaseForm.vue?vue&type=script&lang=js&":
/*!************************************************************************************************!*\
  !*** ./resources/js/components/backoffice/inventory/PurchaseForm.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PurchaseForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./PurchaseForm.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/inventory/PurchaseForm.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PurchaseForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backoffice/inventory/PurchaseForm.vue?vue&type=template&id=74d1a9c9&":
/*!******************************************************************************************************!*\
  !*** ./resources/js/components/backoffice/inventory/PurchaseForm.vue?vue&type=template&id=74d1a9c9& ***!
  \******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PurchaseForm_vue_vue_type_template_id_74d1a9c9___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./PurchaseForm.vue?vue&type=template&id=74d1a9c9& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/inventory/PurchaseForm.vue?vue&type=template&id=74d1a9c9&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PurchaseForm_vue_vue_type_template_id_74d1a9c9___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PurchaseForm_vue_vue_type_template_id_74d1a9c9___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);