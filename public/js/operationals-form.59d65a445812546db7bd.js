(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/operationals-form"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/finance/OperationalForm.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/finance/OperationalForm.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuejs_datepicker__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuejs-datepicker */ "./node_modules/vuejs-datepicker/dist/vuejs-datepicker.esm.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var date = new Date();
/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {
    var _this = this;

    if (this.$route.params.id) {
      this.loadData();
    }

    Fire.$on('AfterLoaded', function () {
      _this.loadData();
    });
  },
  mounted: function mounted() {},
  data: function data() {
    return {
      globalVar: this.$attrs.globalVar,
      renderingCount: 0,
      form: new Form({
        id: '',
        date: date,
        number: '',
        outlet_id: '',
        nominal: '',
        information: ''
      }),
      selecteditemsOutlet: []
    };
  },
  components: {
    Datepicker: vuejs_datepicker__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  methods: {
    submitData: function submitData() {
      var self = this;
      self.$Progress.start();
      var outletId = 0;

      if (self.globalVar.session.outlet.length > 1) {
        if ($("#outlet_id").select2('data')[0]) {
          outletId = $("#outlet_id").select2('data')[0].id;
        }
      } else {
        outletId = self.globalVar.session.outlet[0].outlet.id;
      }

      self.form.outlet_id = outletId;

      if (self.form.id) {
        self.form.put('/method/finance/operationals/' + self.form.id).then(function () {
          Toast.fire({
            type: 'success',
            title: 'Data updated successfully'
          });
          self.$Progress.finish();
          setTimeout(function () {
            self.$router.replace('/finance/operationals');
          }, 2100);
        })["catch"](function () {
          Swal.fire('Alert!', 'Something went wrong.', 'warning');
          self.$Progress.fail();
        });
      } else {
        self.form.post('/method/finance/operationals').then(function () {
          Toast.fire({
            type: 'success',
            title: 'Data saved successfully'
          });
          self.$Progress.finish();
          setTimeout(function () {
            self.$router.replace('/finance/operationals');
          }, 2100);
        })["catch"](function () {
          Swal.fire('Alert!', 'Something went wrong.', 'warning');
          self.$Progress.fail();
        });
      }
    },
    loadData: function loadData() {
      var _this2 = this;

      var self = this;
      axios.get('/method/finance/operationals/' + this.$route.params.id).then(function (response) {
        if (response.data) {
          _this2.form.id = response.data.id;
          _this2.form.date = response.data.date;
          _this2.form.number = response.data.number;
          _this2.form.outlet_id = response.data.outlet.id;
          _this2.form.nominal = response.data.nominal;
          _this2.form.information = response.data.information;
          _this2.selecteditemsOutlet = [{
            id: response.data.outlet.id,
            name: response.data.outlet.name
          }];
        }
      })["catch"](function (error) {});
    }
  },
  watch: {
    'form.nominal': function formNominal(to, from) {
      this.form.nominal = this.formatNumber(this.form.nominal);
    }
  },
  activated: function activated() {
    var self = this;
    $("#outlet_id").val('').trigger('change');
    self.form.clear();
    self.form.reset();
    self.form.date = date;
    self.selecteditemsOutlet = [];

    if (self.$route.params.id) {
      setTimeout(function () {
        self.loadData();
      }, 400);
    }

    self.renderingCount++;
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/finance/OperationalForm.vue?vue&type=template&id=2865e680&":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/finance/OperationalForm.vue?vue&type=template&id=2865e680& ***!
  \*************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "content-header" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row mb-2" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "col-sm-6" }, [
            _c("ol", { staticClass: "breadcrumb float-sm-right" }, [
              _c("li", { staticClass: "breadcrumb-item" }, [
                _c("a", { attrs: { href: "javascript:;" } }, [
                  _vm._v(_vm._s(_vm.globalVar.appName))
                ])
              ]),
              _vm._v(" "),
              _vm._m(1),
              _vm._v(" "),
              _c("li", { staticClass: "breadcrumb-item active" }, [
                _vm._v("Operasional")
              ])
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("section", { staticClass: "content" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12" }, [
            _c("div", { staticClass: "card" }, [
              _vm._m(2),
              _vm._v(" "),
              _c("div", { staticClass: "card-body" }, [
                _c(
                  "form",
                  {
                    attrs: { id: "formAdd" },
                    on: {
                      submit: function($event) {
                        $event.preventDefault()
                        return _vm.submitData($event)
                      }
                    }
                  },
                  [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.form.id,
                          expression: "form.id"
                        }
                      ],
                      staticClass: "d-none",
                      class: { "is-invalid": _vm.form.errors.has("id") },
                      attrs: { type: "text", name: "id", id: "id" },
                      domProps: { value: _vm.form.id },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.form, "id", $event.target.value)
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("has-error", { attrs: { form: _vm.form, field: "id" } }),
                    _vm._v(" "),
                    _vm.globalVar.session
                      ? _c("div", { staticClass: "form-group row" }, [
                          _vm.globalVar.session.outlet.length > 1
                            ? _c(
                                "label",
                                {
                                  staticClass: "col-sm-2 col-form-label",
                                  attrs: { for: "outlet_id" }
                                },
                                [
                                  _vm._v(
                                    "\n                                Toko/Gudang\n                            "
                                  )
                                ]
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.globalVar.session.outlet.length > 1
                            ? _c(
                                "div",
                                {
                                  key: _vm.renderingCount + "-outlet",
                                  staticClass: "col-sm-4"
                                },
                                [
                                  _c("select2", {
                                    ref: "outlet_id",
                                    attrs: {
                                      url: "/method/master/outlets/select",
                                      name: "outlet_id",
                                      selecteditems: _vm.selecteditemsOutlet,
                                      identifier: "outlet_id",
                                      placeholder: "Toko/Gudang",
                                      required: "",
                                      disabled: _vm.$route.params.id
                                        ? true
                                        : false
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("has-error", {
                                    attrs: {
                                      form: _vm.form,
                                      field: "outlet_id"
                                    }
                                  })
                                ],
                                1
                              )
                            : _vm._e()
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "date" }
                        },
                        [
                          _vm._v(
                            "\n                                Tanggal\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("datepicker", {
                            ref: "date",
                            class: {
                              "is-invalid": _vm.form.errors.has("date")
                            },
                            attrs: {
                              "input-class": "form-control",
                              name: "date",
                              id: "date",
                              placeholder: "Tanggal",
                              autocomplete: "off",
                              format: _vm.formatDateDefault,
                              required: "",
                              disabled: _vm.$route.params.id ? true : false
                            },
                            model: {
                              value: _vm.form.date,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "date", $$v)
                              },
                              expression: "form.date"
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "date" }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "number" }
                        },
                        [
                          _vm._v(
                            "\n                                Nomor Transaksi\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.number,
                                expression: "form.number"
                              }
                            ],
                            ref: "number",
                            staticClass: "form-control",
                            class: {
                              "is-invalid": _vm.form.errors.has("number")
                            },
                            attrs: {
                              type: "text",
                              name: "number",
                              id: "number",
                              placeholder: "Nomor Transaksi",
                              autocomplete: "off",
                              readonly: ""
                            },
                            domProps: { value: _vm.form.number },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "number",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "number" }
                          }),
                          _vm._v(" "),
                          _c("small", { staticClass: "form-text text-muted" }, [
                            _vm._v(
                              "\n                                    *) Otomatis dari sistem.\n                                "
                            )
                          ])
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "nominal" }
                        },
                        [
                          _vm._v(
                            "\n                                Nominal\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.nominal,
                                expression: "form.nominal"
                              }
                            ],
                            ref: "nominal",
                            staticClass: "form-control text-right",
                            class: {
                              "is-invalid": _vm.form.errors.has("nominal")
                            },
                            attrs: {
                              type: "text",
                              name: "nominal",
                              id: "nominal",
                              placeholder: "Nominal",
                              autocomplete: "off",
                              required: "",
                              readonly: _vm.$route.params.id ? true : false
                            },
                            domProps: { value: _vm.form.nominal },
                            on: {
                              keypress: function($event) {
                                return _vm.isNumber($event)
                              },
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "nominal",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "nominal" }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "information" }
                        },
                        [
                          _vm._v(
                            "\n                                Keterangan\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("textarea", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.information,
                                expression: "form.information"
                              }
                            ],
                            ref: "information",
                            staticClass: "form-control",
                            class: {
                              "is-invalid": _vm.form.errors.has("information")
                            },
                            attrs: {
                              name: "information",
                              rows: "3",
                              id: "information",
                              placeholder: "Keterangan",
                              autocomplete: "off",
                              readonly: _vm.$route.params.id ? true : false
                            },
                            domProps: { value: _vm.form.information },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "information",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "information" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "div",
                        { staticClass: "col-sm-12 text-right" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "btn btn-secondary",
                              attrs: { to: "/finance/operationals" }
                            },
                            [
                              _vm._v(
                                "\n                                Batal\n                              "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          !_vm.$route.params.id
                            ? _c(
                                "button",
                                {
                                  staticClass: "btn btn-success",
                                  attrs: { type: "submit" }
                                },
                                [
                                  _vm._v(
                                    "\n                                  Simpan\n                              "
                                  )
                                ]
                              )
                            : _vm._e()
                        ],
                        1
                      )
                    ])
                  ],
                  1
                )
              ])
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-sm-6" }, [
      _c("h1", { staticClass: "m-0 text-dark" }, [_vm._v("Operasional")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "breadcrumb-item" }, [
      _c("a", { attrs: { href: "javascript:;" } }, [_vm._v("Keuangan")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header" }, [
      _c("h3", { staticClass: "card-title" }),
      _vm._v(" "),
      _c("div", { staticClass: "card-tools" }),
      _vm._v(" "),
      _c("br")
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backoffice/finance/OperationalForm.vue":
/*!************************************************************************!*\
  !*** ./resources/js/components/backoffice/finance/OperationalForm.vue ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _OperationalForm_vue_vue_type_template_id_2865e680___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./OperationalForm.vue?vue&type=template&id=2865e680& */ "./resources/js/components/backoffice/finance/OperationalForm.vue?vue&type=template&id=2865e680&");
/* harmony import */ var _OperationalForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./OperationalForm.vue?vue&type=script&lang=js& */ "./resources/js/components/backoffice/finance/OperationalForm.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _OperationalForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _OperationalForm_vue_vue_type_template_id_2865e680___WEBPACK_IMPORTED_MODULE_0__["render"],
  _OperationalForm_vue_vue_type_template_id_2865e680___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backoffice/finance/OperationalForm.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backoffice/finance/OperationalForm.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/components/backoffice/finance/OperationalForm.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OperationalForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./OperationalForm.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/finance/OperationalForm.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OperationalForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backoffice/finance/OperationalForm.vue?vue&type=template&id=2865e680&":
/*!*******************************************************************************************************!*\
  !*** ./resources/js/components/backoffice/finance/OperationalForm.vue?vue&type=template&id=2865e680& ***!
  \*******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OperationalForm_vue_vue_type_template_id_2865e680___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./OperationalForm.vue?vue&type=template&id=2865e680& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/finance/OperationalForm.vue?vue&type=template&id=2865e680&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OperationalForm_vue_vue_type_template_id_2865e680___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OperationalForm_vue_vue_type_template_id_2865e680___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);