(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/outlet"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/master/Outlet.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/master/Outlet.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {
    var _this = this;

    this.loadData();
    this.loadDataUsers();
    Fire.$on('AfterModified', function () {
      _this.loadData();
    });
    Fire.$on('AfterModifiedUsers', function () {
      _this.loadDataUsers();
    });
  },
  mounted: function mounted() {},
  data: function data() {
    var sortOrders = {};
    var columns = [{
      width: '5%',
      label: 'No.',
      sortable: false
    }, {
      label: 'Nama',
      name: 'name',
      sortable: true
    }, {
      label: 'No. Telepon',
      name: 'phone',
      sortable: true
    }, {
      label: 'Alamat',
      name: 'address',
      sortable: true
    }, {
      width: '15%',
      label: 'Action',
      sortable: false
    }];
    columns.forEach(function (column) {
      sortOrders[column.name] = -1;
    });
    var sortOrdersUsers = {};
    var columnsUsers = [{
      width: '5%',
      label: 'No.',
      sortable: false
    }, {
      label: 'Nama Akun Pengguna',
      name: 'user_id',
      sortable: false
    }, {
      width: '15%',
      label: 'Action',
      sortable: false
    }];
    columnsUsers.forEach(function (column) {
      sortOrdersUsers[column.name] = -1;
    });
    return {
      globalVar: this.$attrs.globalVar,
      dataTables: [],
      columns: columns,
      sortKey: 'name',
      sortOrders: sortOrders,
      perPage: ['10', '25', '50', '100'],
      tableData: {
        draw: 0,
        length: 10,
        search: '',
        column: 1,
        dir: 'asc'
      },
      pagination: {
        lastPage: '',
        currentPage: '',
        total: '',
        lastPageUrl: '',
        nextPageUrl: '',
        prevPageUrl: '',
        from: '',
        to: ''
      },
      form: new Form({
        id: '',
        name: '',
        phone: '',
        address: '',
        receipt_note: ''
      }),
      dataTablesUsers: [],
      columnsUsers: columnsUsers,
      sortKeyUsers: 'user_id',
      sortOrdersUsers: sortOrdersUsers,
      perPageUsers: ['10', '25', '50', '100'],
      tableDataUsers: {
        draw: 0,
        length: 10,
        search: '',
        column: 1,
        dir: 'asc',
        outlet: 1
      },
      paginationUsers: {
        lastPage: '',
        currentPage: '',
        total: '',
        lastPageUrl: '',
        nextPageUrl: '',
        prevPageUrl: '',
        from: '',
        to: ''
      },
      formUsers: new Form({
        user_id: '',
        outlet_id: ''
      }),
      selecteditemsUser: [],
      selecteditemsUserExist: {
        user_id: ''
      },
      selectUsersKey: 0
    };
  },
  methods: {
    configPagination: function configPagination(data) {
      this.pagination.lastPage = data.last_page;
      this.pagination.currentPage = data.current_page;
      this.pagination.total = data.total;
      this.pagination.lastPageUrl = data.last_page_url;
      this.pagination.nextPageUrl = data.next_page_url;
      this.pagination.prevPageUrl = data.prev_page_url;
      this.pagination.from = data.from;
      this.pagination.to = data.to;
    },
    sortBy: function sortBy(key) {
      this.sortKey = key;
      this.sortOrders[key] = this.sortOrders[key] * -1;
      this.tableData.column = this.getIndex(this.columns, 'name', key);
      this.tableData.dir = this.sortOrders[key] === 1 ? 'asc' : 'desc';
      this.loadData();
    },
    getIndex: function getIndex(array, key, value) {
      return array.findIndex(function (i) {
        return i[key] == value;
      });
    },
    loadData: function loadData() {
      var _this2 = this;

      var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '/method/master/outlets';
      this.tableData.draw++;
      axios.get(url, {
        params: this.tableData
      }).then(function (response) {
        var data = response.data;

        if (_this2.tableData.draw == data.draw) {
          _this2.dataTables = data.data.data;

          _this2.configPagination(data.data);
        }
      })["catch"](function (errors) {});
    },
    openModal: function openModal() {
      var _this3 = this;

      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      this.form.clear();
      this.form.reset();
      $("#modalForm").modal("show");

      if (data) {
        this.form.fill(data);
      }

      setTimeout(function () {
        _this3.$refs.name.focus();
      }, 400);
    },
    submitData: function submitData() {
      var _this4 = this;

      this.$Progress.start();

      if (this.form.id) {
        this.form.put('/method/master/outlets/' + this.form.id).then(function () {
          Toast.fire({
            type: 'success',
            title: 'Data updated successfully'
          });

          _this4.$Progress.finish();

          Fire.$emit('AfterModified');
          $("#modalForm").modal("hide");
        })["catch"](function () {
          Swal.fire('Alert!', 'Something went wrong.', 'warning');

          _this4.$Progress.fail();
        });
      } else {
        this.form.post('/method/master/outlets').then(function () {
          Toast.fire({
            type: 'success',
            title: 'Data saved successfully'
          });

          _this4.$Progress.finish();

          Fire.$emit('AfterModified');
          $("#modalForm").modal("hide");
        })["catch"](function () {
          Swal.fire('Alert!', 'Something went wrong.', 'warning');

          _this4.$Progress.fail();
        });
      }
    },
    deleteData: function deleteData(data) {
      Swal.fire({
        title: 'Are you sure?',
        text: 'Record will be deleted.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
      }).then(function (result) {
        if (result.value) {
          axios["delete"]('/method/master/outlets/' + data.id, {}).then(function (response) {
            Toast.fire({
              type: 'success',
              title: 'Data saved successfully'
            });
            Fire.$emit('AfterModified');
          })["catch"](function (error) {
            Swal.fire('Alert!', 'Something went wrong.', 'warning');
          });
        }
      });
    },
    configPaginationUsers: function configPaginationUsers(data) {
      this.paginationUsers.lastPage = data.last_page;
      this.paginationUsers.currentPage = data.current_page;
      this.paginationUsers.total = data.total;
      this.paginationUsers.lastPageUrl = data.last_page_url;
      this.paginationUsers.nextPageUrl = data.next_page_url;
      this.paginationUsers.prevPageUrl = data.prev_page_url;
      this.paginationUsers.from = data.from;
      this.paginationUsers.to = data.to;
    },
    sortByUsers: function sortByUsers(key) {
      this.sortKeyUsers = key;
      this.sortOrdersUsers[key] = this.sortOrdersUsers[key] * -1;
      this.tableDataUsers.column = this.getIndexUsers(this.columnsUsers, 'user_id', key);
      this.tableDataUsers.dir = this.sortOrdersUsers[key] === 1 ? 'asc' : 'desc';
      this.loadDataUsers();
    },
    getIndexUsers: function getIndexUsers(array, key, value) {
      return array.findIndex(function (i) {
        return i[key] == value;
      });
    },
    loadDataUsers: function loadDataUsers() {
      var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '/method/master/user-outlets';
      var self = this;
      self.tableDataUsers.draw++;
      axios.get(url, {
        params: self.tableDataUsers
      }).then(function (response) {
        var data = response.data;

        if (self.tableDataUsers.draw == data.draw) {
          self.dataTablesUsers = data.data.data;
          self.configPaginationUsers(data.data);
          self.selecteditemsUserExist.user_id = '';

          if (data.data.data.length > 0) {
            self.selecteditemsUserExist.user_id = '';
          }

          for (var i = 0; i < data.data.data.length; i++) {
            self.selecteditemsUserExist.user_id += data.data.data[i].user_id;

            if (i != data.data.data.length - 1) {
              self.selecteditemsUserExist.user_id += ',';
            }
          }
        }
      })["catch"](function (errors) {});
    },
    openModalUser: function openModalUser() {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      this.formUsers.clear();
      this.formUsers.reset();
      this.selecteditemsUser = [];
      $("#modalFormUser").modal("show");

      if (data) {
        this.tableDataUsers.outlet = data.id;
        this.formUsers.outlet_id = data.id;
        Fire.$emit('AfterModifiedUsers');
        this.selectUsersKey++;
      }
    },
    submitDataUsers: function submitDataUsers() {
      var _this5 = this;

      this.$Progress.start();
      var elm = $("#user_id").select2('data');
      this.formUsers.user_id = elm[0].id;
      this.formUsers.post('/method/master/user-outlets').then(function () {
        Toast.fire({
          type: 'success',
          title: 'Data saved successfully'
        });

        _this5.$Progress.finish();

        Fire.$emit('AfterModifiedUsers');
        _this5.selectUsersKey++;
      })["catch"](function () {
        Swal.fire('Alert!', 'Something went wrong.', 'warning');

        _this5.$Progress.fail();
      });
    },
    deleteDataUsers: function deleteDataUsers(data) {
      var self = this;
      Swal.fire({
        title: 'Are you sure?',
        text: 'Record will be deleted.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
      }).then(function (result) {
        if (result.value) {
          axios["delete"]('/method/master/user-outlets/' + data.user_id + '/' + data.outlet_id, {}).then(function (response) {
            Toast.fire({
              type: 'success',
              title: 'Data saved successfully'
            });
            Fire.$emit('AfterModifiedUsers');
            self.selectUsersKey++;
          })["catch"](function (error) {
            Swal.fire('Alert!', 'Something went wrong.', 'warning');
          });
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/master/Outlet.vue?vue&type=template&id=1cc25219&":
/*!***************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/master/Outlet.vue?vue&type=template&id=1cc25219& ***!
  \***************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "content-header" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row mb-2" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "col-sm-6" }, [
            _c("ol", { staticClass: "breadcrumb float-sm-right" }, [
              _c("li", { staticClass: "breadcrumb-item" }, [
                _c("a", { attrs: { href: "javascript:;" } }, [
                  _vm._v(_vm._s(_vm.globalVar.appName))
                ])
              ]),
              _vm._v(" "),
              _vm._m(1),
              _vm._v(" "),
              _c("li", { staticClass: "breadcrumb-item active" }, [
                _vm._v("Toko / Gudang")
              ])
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("section", { staticClass: "content" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12" }, [
            _c("div", { staticClass: "card" }, [
              _c("div", { staticClass: "card-header" }, [
                _c("h3", { staticClass: "card-title" }),
                _vm._v(" "),
                _c("div", { staticClass: "card-tools" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-primary",
                      attrs: { type: "button" },
                      on: { click: _vm.openModal }
                    },
                    [
                      _c("i", { staticClass: "fas fa-plus-circle" }),
                      _vm._v("   Tambah Data\n                            ")
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("br")
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "card-body dataTables_wrapper scroll" },
                [
                  _c(
                    "div",
                    {
                      staticClass: "dataTables_length",
                      attrs: { id: "tableData_length" }
                    },
                    [
                      _c("label", [
                        _vm._v("Show\n                                "),
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.tableData.length,
                                expression: "tableData.length"
                              }
                            ],
                            on: {
                              change: [
                                function($event) {
                                  var $$selectedVal = Array.prototype.filter
                                    .call($event.target.options, function(o) {
                                      return o.selected
                                    })
                                    .map(function(o) {
                                      var val =
                                        "_value" in o ? o._value : o.value
                                      return val
                                    })
                                  _vm.$set(
                                    _vm.tableData,
                                    "length",
                                    $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  )
                                },
                                function($event) {
                                  return _vm.loadData()
                                }
                              ]
                            }
                          },
                          _vm._l(_vm.perPage, function(records, index) {
                            return _c(
                              "option",
                              { key: index, domProps: { value: records } },
                              [_vm._v(_vm._s(records))]
                            )
                          }),
                          0
                        ),
                        _vm._v("\n                                entries")
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "dataTables_filter",
                      attrs: { id: "tableData_filter" }
                    },
                    [
                      _c("label", [
                        _vm._v("Search:\n                                "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.tableData.search,
                              expression: "tableData.search"
                            }
                          ],
                          attrs: {
                            type: "text",
                            placeholder: "",
                            "aria-controls": "tableData"
                          },
                          domProps: { value: _vm.tableData.search },
                          on: {
                            input: [
                              function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.tableData,
                                  "search",
                                  $event.target.value
                                )
                              },
                              function($event) {
                                return _vm.loadData()
                              }
                            ]
                          }
                        })
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "datatable",
                    {
                      attrs: {
                        columns: _vm.columns,
                        sortKey: _vm.sortKey,
                        sortOrders: _vm.sortOrders
                      },
                      on: { sort: _vm.sortBy }
                    },
                    [
                      _vm.dataTables.length != 0
                        ? _c(
                            "tbody",
                            _vm._l(_vm.dataTables, function(dataTable, index) {
                              return _c("tr", { key: index }, [
                                _c("td", [_vm._v(_vm._s(index + 1))]),
                                _vm._v(" "),
                                _c("td", [_vm._v(_vm._s(dataTable.name))]),
                                _vm._v(" "),
                                _c("td", [_vm._v(_vm._s(dataTable.phone))]),
                                _vm._v(" "),
                                _c("td", [_vm._v(_vm._s(dataTable.address))]),
                                _vm._v(" "),
                                _c("td", { staticClass: "text-center" }, [
                                  _c(
                                    "button",
                                    {
                                      staticClass: "btn btn-primary btn-sm",
                                      attrs: {
                                        type: "button",
                                        title: "Ubah Data"
                                      },
                                      on: {
                                        click: function($event) {
                                          return _vm.openModal(dataTable)
                                        }
                                      }
                                    },
                                    [_c("i", { staticClass: "fas fa-edit" })]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "button",
                                    {
                                      staticClass: "btn btn-success btn-sm",
                                      attrs: {
                                        type: "button",
                                        title: "Tambah Akun Pengguna"
                                      },
                                      on: {
                                        click: function($event) {
                                          return _vm.openModalUser(dataTable)
                                        }
                                      }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "fas fa-user-plus"
                                      })
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "button",
                                    {
                                      staticClass: "btn btn-danger btn-sm",
                                      attrs: {
                                        type: "button",
                                        title: "Hapus Data"
                                      },
                                      on: {
                                        click: function($event) {
                                          return _vm.deleteData(dataTable)
                                        }
                                      }
                                    },
                                    [_c("i", { staticClass: "fas fa-ban" })]
                                  )
                                ])
                              ])
                            }),
                            0
                          )
                        : _c("tbody", [
                            _c("tr", [
                              _c(
                                "td",
                                {
                                  staticClass: "text-center",
                                  attrs: { colspan: "5" }
                                },
                                [
                                  _vm._v(
                                    "\n                                      Tidak ada data yang cocok ditemukan\n                                    "
                                  )
                                ]
                              )
                            ])
                          ])
                    ]
                  ),
                  _vm._v(" "),
                  _c("pagination", {
                    attrs: { pagination: _vm.pagination },
                    on: {
                      prev: function($event) {
                        return _vm.loadData(_vm.pagination.prevPageUrl)
                      },
                      next: function($event) {
                        return _vm.loadData(_vm.pagination.nextPageUrl)
                      }
                    }
                  })
                ],
                1
              )
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "modal fade bd-example-modal-lg",
        attrs: {
          id: "modalForm",
          role: "dialog",
          "aria-labelledby": "modalFormLabel",
          "aria-hidden": "true"
        }
      },
      [
        _c(
          "div",
          { staticClass: "modal-dialog modal-lg", attrs: { role: "document" } },
          [
            _c("div", { staticClass: "modal-content" }, [
              _vm._m(2),
              _vm._v(" "),
              _c(
                "form",
                {
                  attrs: { id: "formAdd" },
                  on: {
                    submit: function($event) {
                      $event.preventDefault()
                      return _vm.submitData($event)
                    }
                  }
                },
                [
                  _c(
                    "div",
                    { staticClass: "modal-body" },
                    [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.form.id,
                            expression: "form.id"
                          }
                        ],
                        staticClass: "d-none",
                        class: { "is-invalid": _vm.form.errors.has("id") },
                        attrs: { type: "text", name: "id", id: "id" },
                        domProps: { value: _vm.form.id },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.form, "id", $event.target.value)
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("has-error", {
                        attrs: { form: _vm.form, field: "id" }
                      }),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group row" }, [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-4 col-form-label",
                            attrs: { for: "name" }
                          },
                          [
                            _vm._v(
                              "\n                    Nama\n                "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-8" },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.name,
                                  expression: "form.name"
                                }
                              ],
                              ref: "name",
                              staticClass: "form-control",
                              class: {
                                "is-invalid": _vm.form.errors.has("name")
                              },
                              attrs: {
                                type: "text",
                                name: "name",
                                id: "name",
                                placeholder: "Nama",
                                autocomplete: "off",
                                required: ""
                              },
                              domProps: { value: _vm.form.name },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.form,
                                    "name",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: { form: _vm.form, field: "name" }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group row" }, [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-4 col-form-label",
                            attrs: { for: "phone" }
                          },
                          [
                            _vm._v(
                              "\n                    No. Telepon\n                "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-8" },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.phone,
                                  expression: "form.phone"
                                }
                              ],
                              ref: "phone",
                              staticClass: "form-control",
                              class: {
                                "is-invalid": _vm.form.errors.has("phone")
                              },
                              attrs: {
                                type: "text",
                                name: "phone",
                                id: "phone",
                                placeholder: "No. Telepon",
                                autocomplete: "off",
                                maxlength: "15",
                                required: ""
                              },
                              domProps: { value: _vm.form.phone },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.form,
                                    "phone",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: { form: _vm.form, field: "phone" }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group row" }, [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-4 col-form-label",
                            attrs: { for: "address" }
                          },
                          [
                            _vm._v(
                              "\n                    Alamat\n                "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-8" },
                          [
                            _c("textarea", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.address,
                                  expression: "form.address"
                                }
                              ],
                              ref: "address",
                              staticClass: "form-control",
                              class: {
                                "is-invalid": _vm.form.errors.has("address")
                              },
                              attrs: {
                                name: "address",
                                rows: "3",
                                id: "address",
                                placeholder: "Alamat",
                                autocomplete: "off",
                                required: ""
                              },
                              domProps: { value: _vm.form.address },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.form,
                                    "address",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: { form: _vm.form, field: "address" }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group row" }, [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-4 col-form-label",
                            attrs: { for: "receipt_note" }
                          },
                          [
                            _vm._v(
                              "\n                    Teks Nota\n                "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-8" },
                          [
                            _c("textarea", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.receipt_note,
                                  expression: "form.receipt_note"
                                }
                              ],
                              ref: "receipt_note",
                              staticClass: "form-control",
                              class: {
                                "is-invalid": _vm.form.errors.has(
                                  "receipt_note"
                                )
                              },
                              attrs: {
                                name: "receipt_note",
                                rows: "3",
                                id: "receipt_note",
                                placeholder: "Teks Nota",
                                autocomplete: "off",
                                required: ""
                              },
                              domProps: { value: _vm.form.receipt_note },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.form,
                                    "receipt_note",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: { form: _vm.form, field: "receipt_note" }
                            })
                          ],
                          1
                        )
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _vm._m(3)
                ]
              )
            ])
          ]
        )
      ]
    ),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "modal fade bd-example-modal-lg",
        attrs: {
          id: "modalFormUser",
          role: "dialog",
          "aria-labelledby": "modalFormUserLabel",
          "aria-hidden": "true"
        }
      },
      [
        _c(
          "div",
          { staticClass: "modal-dialog modal-lg", attrs: { role: "document" } },
          [
            _c("div", { staticClass: "modal-content" }, [
              _vm._m(4),
              _vm._v(" "),
              _c("div", { staticClass: "modal-body" }, [
                _c(
                  "form",
                  {
                    attrs: { id: "formAddUsers" },
                    on: {
                      submit: function($event) {
                        $event.preventDefault()
                        return _vm.submitDataUsers($event)
                      }
                    }
                  },
                  [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.formUsers.outlet_id,
                          expression: "formUsers.outlet_id"
                        }
                      ],
                      staticClass: "d-none",
                      class: {
                        "is-invalid": _vm.formUsers.errors.has("outlet_id")
                      },
                      attrs: { type: "text" },
                      domProps: { value: _vm.formUsers.outlet_id },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.formUsers,
                            "outlet_id",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("has-error", {
                      attrs: { form: _vm.formUsers, field: "outlet_id" }
                    }),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-4 col-form-label",
                          attrs: { for: "user_id" }
                        },
                        [
                          _vm._v(
                            "\n                    Pengguna\n                "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { key: _vm.selectUsersKey, staticClass: "col-sm-6" },
                        [
                          _c("select2", {
                            attrs: {
                              url: "/method/access/users/select",
                              name: "user_id",
                              id: "user_id",
                              selecteditems: _vm.selecteditemsUser,
                              identifier: "user_id",
                              placeholder: "Pengguna",
                              params: _vm.selecteditemsUserExist,
                              required: ""
                            },
                            model: {
                              value: _vm.formUsers.user_id,
                              callback: function($$v) {
                                _vm.$set(_vm.formUsers, "user_id", $$v)
                              },
                              expression: "formUsers.user_id"
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.formUsers, field: "user_id" }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _vm._m(5)
                    ])
                  ],
                  1
                ),
                _vm._v(" "),
                _c("br"),
                _c("br"),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "dataTables_wrapper" },
                  [
                    _c(
                      "div",
                      {
                        staticClass: "dataTables_length",
                        attrs: { id: "tableData_length" }
                      },
                      [
                        _c("label", [
                          _vm._v("Show\n                        "),
                          _c(
                            "select",
                            {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.tableDataUsers.length,
                                  expression: "tableDataUsers.length"
                                }
                              ],
                              on: {
                                change: [
                                  function($event) {
                                    var $$selectedVal = Array.prototype.filter
                                      .call($event.target.options, function(o) {
                                        return o.selected
                                      })
                                      .map(function(o) {
                                        var val =
                                          "_value" in o ? o._value : o.value
                                        return val
                                      })
                                    _vm.$set(
                                      _vm.tableDataUsers,
                                      "length",
                                      $event.target.multiple
                                        ? $$selectedVal
                                        : $$selectedVal[0]
                                    )
                                  },
                                  function($event) {
                                    return _vm.loadDataUsers()
                                  }
                                ]
                              }
                            },
                            _vm._l(_vm.perPage, function(records, index) {
                              return _c(
                                "option",
                                { key: index, domProps: { value: records } },
                                [_vm._v(_vm._s(records))]
                              )
                            }),
                            0
                          ),
                          _vm._v("\n                        entries")
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "datatable",
                      {
                        attrs: {
                          columns: _vm.columnsUsers,
                          sortKey: _vm.sortKeyUsers,
                          sortOrders: _vm.sortOrdersUsers
                        },
                        on: { sort: _vm.sortByUsers }
                      },
                      [
                        _vm.dataTablesUsers.length != 0
                          ? _c(
                              "tbody",
                              _vm._l(_vm.dataTablesUsers, function(
                                dataTable,
                                index
                              ) {
                                return _c("tr", { key: index }, [
                                  _c("td", [_vm._v(_vm._s(index + 1))]),
                                  _vm._v(" "),
                                  _c("td", [
                                    _vm._v(_vm._s(dataTable.user[0].name))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-center" }, [
                                    _c(
                                      "button",
                                      {
                                        staticClass: "btn btn-danger btn-sm",
                                        attrs: {
                                          type: "button",
                                          title: "Hapus Data"
                                        },
                                        on: {
                                          click: function($event) {
                                            return _vm.deleteDataUsers(
                                              dataTable
                                            )
                                          }
                                        }
                                      },
                                      [_c("i", { staticClass: "fas fa-ban" })]
                                    )
                                  ])
                                ])
                              }),
                              0
                            )
                          : _c("tbody", [
                              _c("tr", [
                                _c(
                                  "td",
                                  {
                                    staticClass: "text-center",
                                    attrs: { colspan: "3" }
                                  },
                                  [
                                    _vm._v(
                                      "\n                              Tidak ada data yang cocok ditemukan\n                            "
                                    )
                                  ]
                                )
                              ])
                            ])
                      ]
                    ),
                    _vm._v(" "),
                    _c("pagination", {
                      attrs: { pagination: _vm.paginationUsers },
                      on: {
                        prev: function($event) {
                          return _vm.loadDataUsers(
                            _vm.paginationUsers.prevPageUrl
                          )
                        },
                        next: function($event) {
                          return _vm.loadDataUsers(
                            _vm.paginationUsers.nextPageUrl
                          )
                        }
                      }
                    })
                  ],
                  1
                )
              ]),
              _vm._v(" "),
              _vm._m(6)
            ])
          ]
        )
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-sm-6" }, [
      _c("h1", { staticClass: "m-0 text-dark" }, [_vm._v("Toko / Gudang")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "breadcrumb-item" }, [
      _c("a", { attrs: { href: "javascript:;" } }, [_vm._v("Master")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "h5",
        { staticClass: "modal-title", attrs: { id: "modalFormLabel" } },
        [_vm._v("\n              Modal Form\n          ")]
      ),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-footer text-right" }, [
      _c(
        "button",
        {
          staticClass: "btn btn-secondary",
          attrs: { type: "button", "data-dismiss": "modal" }
        },
        [_vm._v("\n                  Batal\n              ")]
      ),
      _vm._v(" "),
      _c(
        "button",
        { staticClass: "btn btn-success", attrs: { type: "submit" } },
        [_vm._v("\n                  Simpan\n              ")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "h5",
        { staticClass: "modal-title", attrs: { id: "modalFormUserLabel" } },
        [_vm._v("\n              Modal Form User\n          ")]
      ),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-sm-2 text-right" }, [
      _c(
        "button",
        { staticClass: "btn btn-success", attrs: { type: "submit" } },
        [_vm._v("\n                        Tambahkan\n                    ")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-footer text-right" }, [
      _c(
        "button",
        {
          staticClass: "btn btn-secondary",
          attrs: { type: "button", "data-dismiss": "modal" }
        },
        [_vm._v("\n              Batal\n          ")]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backoffice/master/Outlet.vue":
/*!**************************************************************!*\
  !*** ./resources/js/components/backoffice/master/Outlet.vue ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Outlet_vue_vue_type_template_id_1cc25219___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Outlet.vue?vue&type=template&id=1cc25219& */ "./resources/js/components/backoffice/master/Outlet.vue?vue&type=template&id=1cc25219&");
/* harmony import */ var _Outlet_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Outlet.vue?vue&type=script&lang=js& */ "./resources/js/components/backoffice/master/Outlet.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Outlet_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Outlet_vue_vue_type_template_id_1cc25219___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Outlet_vue_vue_type_template_id_1cc25219___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backoffice/master/Outlet.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backoffice/master/Outlet.vue?vue&type=script&lang=js&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/backoffice/master/Outlet.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Outlet_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Outlet.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/master/Outlet.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Outlet_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backoffice/master/Outlet.vue?vue&type=template&id=1cc25219&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/components/backoffice/master/Outlet.vue?vue&type=template&id=1cc25219& ***!
  \*********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Outlet_vue_vue_type_template_id_1cc25219___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Outlet.vue?vue&type=template&id=1cc25219& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/master/Outlet.vue?vue&type=template&id=1cc25219&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Outlet_vue_vue_type_template_id_1cc25219___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Outlet_vue_vue_type_template_id_1cc25219___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);