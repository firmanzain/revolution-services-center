(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/service"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/transaction/Service.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/transaction/Service.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {
    var _this = this;

    if (this.globalVar.session) {
      if (this.globalVar.session.outlet) {
        if (this.globalVar.session.outlet.length > 0) {
          for (var i = 0; i < this.globalVar.session.outlet.length; i++) {
            if (this.globalVar.session.outlet[i].outlet.id != 1) {
              this.tableData.outlet = this.globalVar.session.outlet[i].outlet.id;
              this.selecteditemsOutlet = [{
                id: this.globalVar.session.outlet[i].outlet.id,
                name: this.globalVar.session.outlet[i].outlet.name
              }];
              break;
            }
          }
        }
      }
    }

    this.loadData();
    Fire.$on('AfterModified', function () {
      _this.loadData();
    });
  },
  mounted: function mounted() {},
  data: function data() {
    var sortOrders = {};
    var columns = [{
      width: '5%',
      label: 'No.',
      sortable: false
    }, {
      label: 'Tanggal',
      name: 'date',
      sortable: true
    }, {
      label: 'Estimasi Selesai',
      name: 'date_estimation',
      sortable: true
    }, {
      label: 'Nomor Transaksi',
      name: 'number',
      sortable: true
    }, {
      label: 'Pelanggan',
      name: 'customer_name',
      sortable: true
    }, {
      label: 'Barang',
      name: 'item_name',
      sortable: true
    }, {
      label: 'Status',
      name: 'status',
      sortable: true
    }, {
      width: '15%',
      label: 'Action',
      sortable: false
    }];
    columns.forEach(function (column) {
      sortOrders[column.name] = -1;
    });
    return {
      globalVar: this.$attrs.globalVar,
      renderingCount: 0,
      dataTables: [],
      columns: columns,
      sortKey: 'date',
      sortOrders: sortOrders,
      perPage: ['10', '25', '50', '100'],
      tableData: {
        draw: 0,
        length: 10,
        search: '',
        column: 2,
        dir: 'desc',
        outlet: 0
      },
      pagination: {
        lastPage: '',
        currentPage: '',
        total: '',
        lastPageUrl: '',
        nextPageUrl: '',
        prevPageUrl: '',
        from: '',
        to: ''
      },
      selecteditemsOutlet: []
    };
  },
  methods: {
    configPagination: function configPagination(data) {
      this.pagination.lastPage = data.last_page;
      this.pagination.currentPage = data.current_page;
      this.pagination.total = data.total;
      this.pagination.lastPageUrl = data.last_page_url;
      this.pagination.nextPageUrl = data.next_page_url;
      this.pagination.prevPageUrl = data.prev_page_url;
      this.pagination.from = data.from;
      this.pagination.to = data.to;
    },
    sortBy: function sortBy(key) {
      this.sortKey = key; // this.sortOrders[key] = this.sortOrders[key] * -1;

      if (this.tableData.column == this.getIndex(this.columns, 'name', key)) {
        if (this.tableData.dir == 'asc') {
          this.tableData.dir = 'desc';
        } else {
          this.tableData.dir = 'asc';
        }
      } else {
        this.tableData.column = this.getIndex(this.columns, 'name', key);
        this.tableData.dir = this.sortOrders[key] === 1 ? 'asc' : 'desc';
      }

      if (this.tableData.dir == 'asc') {
        this.sortOrders[key] = 1;
      } else {
        this.sortOrders[key] = -1;
      }

      this.loadData();
    },
    getIndex: function getIndex(array, key, value) {
      return array.findIndex(function (i) {
        return i[key] == value;
      });
    },
    loadData: function loadData() {
      var _this2 = this;

      var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '/method/transaction/services';
      this.tableData.draw++;

      if (this.tableData.outlet != 0) {
        if (this.tableData.outlet != this.selecteditemsOutlet.id) {
          this.tableData.outlet = this.selecteditemsOutlet.id;
        }
      }

      if ($("#outlet_id").select2('data')) {
        if ($("#outlet_id").select2('data')[0]) {
          this.tableData.outlet = $("#outlet_id").select2('data')[0].id;
        }
      }

      axios.get(url, {
        params: this.tableData
      }).then(function (response) {
        var data = response.data;

        if (_this2.tableData.draw == data.draw) {
          _this2.dataTables = data.data.data;

          _this2.configPagination(data.data);
        }
      })["catch"](function (errors) {});
    },
    deleteData: function deleteData(data) {
      Swal.fire({
        title: 'Are you sure?',
        text: 'Record will be deleted.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
      }).then(function (result) {
        if (result.value) {
          axios["delete"]('/method/transaction/services/' + data.id, {}).then(function (response) {
            Toast.fire({
              type: 'success',
              title: 'Data saved successfully'
            });
            Fire.$emit('AfterModified');
          })["catch"](function (error) {
            Swal.fire('Alert!', 'Something went wrong.', 'warning');
          });
        }
      });
    },
    checkSelected: function checkSelected() {
      this.tableData.outlet = $("#outlet_id").select2('data')[0].id;
      Fire.$emit('AfterModified');
    },
    statusLabelService: function statusLabelService(status) {
      if (status == 0) {
        return '<span class="badge badge-success">Sudah Diambil</span>';
      } else if (status == 1) {
        return '<span class="badge badge-danger">Belum Selesai</span>';
      } else if (status == 2) {
        return '<span class="badge badge-warning">Selesai Belum Diambil</span>';
      }
    }
  },
  watch: {},
  activated: function activated() {
    var self = this;

    if (self.globalVar.session) {
      if (self.globalVar.session.outlet) {
        if (self.globalVar.session.outlet.length > 0) {
          for (var i = 0; i < self.globalVar.session.outlet.length; i++) {
            if (self.globalVar.session.outlet[i].outlet.id != 1) {
              self.tableData.outlet = self.globalVar.session.outlet[i].outlet.id;
              self.selecteditemsOutlet = [{
                id: self.globalVar.session.outlet[i].outlet.id,
                name: self.globalVar.session.outlet[i].outlet.name
              }];
              break;
            }
          }
        }
      }
    }

    self.renderingCount++;
    setTimeout(function () {
      self.loadData();
    }, 400);
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/transaction/Service.vue?vue&type=template&id=41da03c3&":
/*!*********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/transaction/Service.vue?vue&type=template&id=41da03c3& ***!
  \*********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "content-header" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row mb-2" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "col-sm-6" }, [
            _c("ol", { staticClass: "breadcrumb float-sm-right" }, [
              _c("li", { staticClass: "breadcrumb-item" }, [
                _c("a", { attrs: { href: "javascript:;" } }, [
                  _vm._v(_vm._s(_vm.globalVar.appName))
                ])
              ]),
              _vm._v(" "),
              _vm._m(1),
              _vm._v(" "),
              _c("li", { staticClass: "breadcrumb-item active" }, [
                _vm._v("Servis")
              ])
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("section", { staticClass: "content" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12" }, [
            _c("div", { staticClass: "card" }, [
              _c("div", { staticClass: "card-header" }, [
                _c("h3", { staticClass: "card-title" }),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "card-tools" },
                  [
                    _c(
                      "router-link",
                      {
                        staticClass: "btn btn-primary",
                        attrs: { to: "/transaction/services-form" }
                      },
                      [
                        _c("i", { staticClass: "fas fa-plus-circle" }),
                        _vm._v("   Tambah Data\n                            ")
                      ]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c("br")
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "card-body" }, [
                _vm.globalVar.session
                  ? _c("form", { attrs: { id: "formFilter" } }, [
                      _vm.globalVar.session.outlet.length > 1
                        ? _c("div", { staticClass: "form-group row" }, [
                            _c("div", { staticClass: "col-sm-4" }),
                            _vm._v(" "),
                            _c(
                              "label",
                              {
                                staticClass:
                                  "col-sm-4 col-form-label text-right",
                                attrs: { for: "outlet_id" }
                              },
                              [
                                _vm._v(
                                  "\n                                Toko/Gudang\n                            "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                key: _vm.renderingCount + "-outlet",
                                staticClass: "col-sm-4"
                              },
                              [
                                _c("select2", {
                                  ref: "outlet_id",
                                  attrs: {
                                    url: "/method/master/outlets/select",
                                    name: "outlet_id",
                                    selecteditems: _vm.selecteditemsOutlet,
                                    identifier: "outlet_id",
                                    placeholder: "Toko/Gudang",
                                    required: ""
                                  },
                                  on: { changed: _vm.checkSelected }
                                })
                              ],
                              1
                            )
                          ])
                        : _vm._e()
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "dataTables_wrapper scroll" },
                  [
                    _c(
                      "div",
                      {
                        staticClass: "dataTables_length",
                        attrs: { id: "tableData_length" }
                      },
                      [
                        _c("label", [
                          _vm._v("Show\n                                    "),
                          _c(
                            "select",
                            {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.tableData.length,
                                  expression: "tableData.length"
                                }
                              ],
                              on: {
                                change: [
                                  function($event) {
                                    var $$selectedVal = Array.prototype.filter
                                      .call($event.target.options, function(o) {
                                        return o.selected
                                      })
                                      .map(function(o) {
                                        var val =
                                          "_value" in o ? o._value : o.value
                                        return val
                                      })
                                    _vm.$set(
                                      _vm.tableData,
                                      "length",
                                      $event.target.multiple
                                        ? $$selectedVal
                                        : $$selectedVal[0]
                                    )
                                  },
                                  function($event) {
                                    return _vm.loadData()
                                  }
                                ]
                              }
                            },
                            _vm._l(_vm.perPage, function(records, index) {
                              return _c(
                                "option",
                                { key: index, domProps: { value: records } },
                                [_vm._v(_vm._s(records))]
                              )
                            }),
                            0
                          ),
                          _vm._v(
                            "\n                                    entries"
                          )
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "dataTables_filter",
                        attrs: { id: "tableData_filter" }
                      },
                      [
                        _c("label", [
                          _vm._v(
                            "Search:\n                                    "
                          ),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.tableData.search,
                                expression: "tableData.search"
                              }
                            ],
                            attrs: {
                              type: "text",
                              placeholder: "",
                              "aria-controls": "tableData"
                            },
                            domProps: { value: _vm.tableData.search },
                            on: {
                              input: [
                                function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.tableData,
                                    "search",
                                    $event.target.value
                                  )
                                },
                                function($event) {
                                  return _vm.loadData()
                                }
                              ]
                            }
                          })
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "datatable",
                      {
                        attrs: {
                          columns: _vm.columns,
                          sortKey: _vm.sortKey,
                          sortOrders: _vm.sortOrders
                        },
                        on: { sort: _vm.sortBy }
                      },
                      [
                        _vm.dataTables.length != 0
                          ? _c(
                              "tbody",
                              _vm._l(_vm.dataTables, function(
                                dataTable,
                                index
                              ) {
                                return _c("tr", { key: index }, [
                                  _c("td", [_vm._v(_vm._s(index + 1))]),
                                  _vm._v(" "),
                                  _c("td", [
                                    _vm._v(
                                      _vm._s(
                                        _vm._f("formatDate")(dataTable.date)
                                      )
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c("td", [
                                    _vm._v(
                                      _vm._s(
                                        _vm._f("formatDate")(
                                          dataTable.date_estimation
                                        )
                                      )
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c("td", [_vm._v(_vm._s(dataTable.number))]),
                                  _vm._v(" "),
                                  _c("td", [
                                    _vm._v(_vm._s(dataTable.customer_name))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", [
                                    _vm._v(_vm._s(dataTable.item_name))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", {
                                    domProps: {
                                      innerHTML: _vm._s(
                                        _vm.statusLabelService(dataTable.status)
                                      )
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "td",
                                    { staticClass: "text-center" },
                                    [
                                      dataTable.status != 1
                                        ? _c(
                                            "router-link",
                                            {
                                              staticClass:
                                                "btn btn-primary btn-sm",
                                              attrs: {
                                                to:
                                                  "/transaction/services-form/" +
                                                  dataTable.id,
                                                title: "Lihat Data"
                                              }
                                            },
                                            [
                                              _c("i", {
                                                staticClass: "fas fa-eye"
                                              })
                                            ]
                                          )
                                        : _c(
                                            "router-link",
                                            {
                                              staticClass:
                                                "btn btn-primary btn-sm",
                                              attrs: {
                                                to:
                                                  "/transaction/services-form/" +
                                                  dataTable.id,
                                                title: "Ubah Data"
                                              }
                                            },
                                            [
                                              _c("i", {
                                                staticClass: "fas fa-edit"
                                              })
                                            ]
                                          ),
                                      _vm._v(" "),
                                      _c(
                                        "button",
                                        {
                                          staticClass: "btn btn-danger btn-sm",
                                          attrs: {
                                            type: "button",
                                            title: "Hapus Data"
                                          },
                                          on: {
                                            click: function($event) {
                                              return _vm.deleteData(dataTable)
                                            }
                                          }
                                        },
                                        [_c("i", { staticClass: "fas fa-ban" })]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "a",
                                        {
                                          staticClass: "btn btn-danger btn-sm",
                                          attrs: {
                                            href:
                                              _vm.globalVar.publicPath +
                                              "method/transaction/services/print/" +
                                              dataTable.id,
                                            title: "Cetak",
                                            target: "_blank"
                                          }
                                        },
                                        [
                                          _c("i", {
                                            staticClass: "fas fa-print"
                                          })
                                        ]
                                      )
                                    ],
                                    1
                                  )
                                ])
                              }),
                              0
                            )
                          : _c("tbody", [
                              _c("tr", [
                                _c(
                                  "td",
                                  {
                                    staticClass: "text-center",
                                    attrs: { colspan: "8" }
                                  },
                                  [
                                    _vm._v(
                                      "\n                                          Tidak ada data yang cocok ditemukan\n                                        "
                                    )
                                  ]
                                )
                              ])
                            ])
                      ]
                    ),
                    _vm._v(" "),
                    _c("pagination", {
                      attrs: { pagination: _vm.pagination },
                      on: {
                        prev: function($event) {
                          return _vm.loadData(_vm.pagination.prevPageUrl)
                        },
                        next: function($event) {
                          return _vm.loadData(_vm.pagination.nextPageUrl)
                        }
                      }
                    })
                  ],
                  1
                )
              ])
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-sm-6" }, [
      _c("h1", { staticClass: "m-0 text-dark" }, [_vm._v("Servis")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "breadcrumb-item" }, [
      _c("a", { attrs: { href: "javascript:;" } }, [_vm._v("Transaksi")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backoffice/transaction/Service.vue":
/*!********************************************************************!*\
  !*** ./resources/js/components/backoffice/transaction/Service.vue ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Service_vue_vue_type_template_id_41da03c3___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Service.vue?vue&type=template&id=41da03c3& */ "./resources/js/components/backoffice/transaction/Service.vue?vue&type=template&id=41da03c3&");
/* harmony import */ var _Service_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Service.vue?vue&type=script&lang=js& */ "./resources/js/components/backoffice/transaction/Service.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Service_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Service_vue_vue_type_template_id_41da03c3___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Service_vue_vue_type_template_id_41da03c3___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backoffice/transaction/Service.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backoffice/transaction/Service.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/components/backoffice/transaction/Service.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Service_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Service.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/transaction/Service.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Service_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backoffice/transaction/Service.vue?vue&type=template&id=41da03c3&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/components/backoffice/transaction/Service.vue?vue&type=template&id=41da03c3& ***!
  \***************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Service_vue_vue_type_template_id_41da03c3___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Service.vue?vue&type=template&id=41da03c3& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/transaction/Service.vue?vue&type=template&id=41da03c3&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Service_vue_vue_type_template_id_41da03c3___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Service_vue_vue_type_template_id_41da03c3___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);