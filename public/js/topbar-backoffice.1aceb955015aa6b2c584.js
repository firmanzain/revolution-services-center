(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/topbar-backoffice"],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/reusable/Navbar.vue?vue&type=template&id=1dcf9e9a&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/reusable/Navbar.vue?vue&type=template&id=1dcf9e9a& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "nav",
      {
        staticClass:
          "main-header navbar navbar-expand navbar-white navbar-light border-bottom"
      },
      [
        _c("ul", { staticClass: "navbar-nav" }, [
          _c("li", { staticClass: "nav-item" }, [
            _vm._m(0),
            _vm._v(" "),
            _c("button", {
              directives: [
                {
                  name: "shortkey",
                  rawName: "v-shortkey",
                  value: ["alt", "n"],
                  expression: "['alt', 'n']"
                }
              ],
              staticClass: "d-none",
              on: {
                shortkey: function($event) {
                  return _vm.openSaleForm()
                }
              }
            }),
            _vm._v(" "),
            _c("button", {
              directives: [
                {
                  name: "shortkey",
                  rawName: "v-shortkey",
                  value: {
                    inputItems: ["alt", "f"],
                    inputCustomer: ["alt", "c"],
                    showShortkey: ["alt", "i"]
                  },
                  expression:
                    "{inputItems: ['alt', 'f'], inputCustomer: ['alt', 'c'], showShortkey: ['alt', 'i']}"
                }
              ],
              staticClass: "d-none",
              on: { shortkey: _vm.actionShortKey }
            })
          ])
        ]),
        _vm._v(" "),
        _vm._m(1)
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      {
        staticClass: "nav-link",
        attrs: { "data-widget": "pushmenu", href: "javascript:;" }
      },
      [_c("i", { staticClass: "fas fa-bars" })]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", { staticClass: "navbar-nav ml-auto" }, [
      _c("li", { staticClass: "nav-item dropdown d-none" }, [
        _c(
          "a",
          {
            staticClass: "nav-link",
            attrs: { "data-toggle": "dropdown", href: "#" }
          },
          [
            _c("i", { staticClass: "far fa-bell" }),
            _vm._v(" "),
            _c("span", { staticClass: "badge badge-warning navbar-badge" }, [
              _vm._v("15")
            ])
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "dropdown-menu dropdown-menu-lg dropdown-menu-right" },
          [
            _c("span", { staticClass: "dropdown-item dropdown-header" }, [
              _vm._v("15 Notifications")
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "dropdown-divider" }),
            _vm._v(" "),
            _c("a", { staticClass: "dropdown-item", attrs: { href: "#" } }, [
              _c("i", { staticClass: "fas fa-envelope mr-2" }),
              _vm._v(" 4 new messages\n            "),
              _c("span", { staticClass: "float-right text-muted text-sm" }, [
                _vm._v("3 mins")
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "dropdown-divider" }),
            _vm._v(" "),
            _c("a", { staticClass: "dropdown-item", attrs: { href: "#" } }, [
              _c("i", { staticClass: "fas fa-users mr-2" }),
              _vm._v(" 8 friend requests\n            "),
              _c("span", { staticClass: "float-right text-muted text-sm" }, [
                _vm._v("12 hours")
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "dropdown-divider" }),
            _vm._v(" "),
            _c("a", { staticClass: "dropdown-item", attrs: { href: "#" } }, [
              _c("i", { staticClass: "fas fa-file mr-2" }),
              _vm._v(" 3 new reports\n            "),
              _c("span", { staticClass: "float-right text-muted text-sm" }, [
                _vm._v("2 days")
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "dropdown-divider" }),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "dropdown-item dropdown-footer",
                attrs: { href: "#" }
              },
              [_vm._v("See All Notifications")]
            )
          ]
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backoffice/reusable/Navbar.vue":
/*!****************************************************************!*\
  !*** ./resources/js/components/backoffice/reusable/Navbar.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Navbar_vue_vue_type_template_id_1dcf9e9a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Navbar.vue?vue&type=template&id=1dcf9e9a& */ "./resources/js/components/backoffice/reusable/Navbar.vue?vue&type=template&id=1dcf9e9a&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _Navbar_vue_vue_type_template_id_1dcf9e9a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Navbar_vue_vue_type_template_id_1dcf9e9a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backoffice/reusable/Navbar.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backoffice/reusable/Navbar.vue?vue&type=template&id=1dcf9e9a&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/components/backoffice/reusable/Navbar.vue?vue&type=template&id=1dcf9e9a& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_template_id_1dcf9e9a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Navbar.vue?vue&type=template&id=1dcf9e9a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/reusable/Navbar.vue?vue&type=template&id=1dcf9e9a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_template_id_1dcf9e9a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_template_id_1dcf9e9a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);