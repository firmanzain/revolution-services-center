(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/sale-form"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/transaction/SaleForm.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/transaction/SaleForm.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuejs_datepicker__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuejs-datepicker */ "./node_modules/vuejs-datepicker/dist/vuejs-datepicker.esm.js");
/* harmony import */ var vuejs_auto_complete__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuejs-auto-complete */ "./node_modules/vuejs-auto-complete/dist/build.js");
/* harmony import */ var vuejs_auto_complete__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vuejs_auto_complete__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


var date = new Date();
/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {
    var _this = this;

    if (this.$route.params.id) {
      this.loadData();
    }

    Fire.$on('AfterLoaded', function () {
      _this.loadData();
    });
  },
  mounted: function mounted() {},
  data: function data() {
    return {
      globalVar: this.$attrs.globalVar,
      renderingCount: 0,
      renderingCountCustomer: 0,
      renderingCountTechnician: 0,
      form: new Form({
        id: '',
        date: date,
        number: '',
        outlet_id: '',
        customer_id: '',
        customer_debt: 0,
        customer_type: '',
        customer_limit: 0,
        customer_name: '',
        customer_phone: '',
        type: 1,
        technician_id: '',
        service_id: '',
        service_item_number: '',
        service_item_name: '',
        service_complaint: '',
        service_equipment: '',
        subtotal: 0,
        fee: 0,
        discount: 0,
        ppn: 0,
        total: 0,
        payment: 1,
        pay: 0,
        change: 0,
        down_payment: 0,
        information: '',
        details: []
      }),
      selecteditemsOutlet: [],
      selecteditemsCustomer: [],
      selecteditemsTechnician: [],
      serviceAutocomplete: {
        placeholder: 'Masukkan nomor servis'
      },
      itemAutocomplete: {
        placeholder: 'Cari barang'
      },
      itemDetails: [],
      linkPrint: {
        small: 'javascript:;',
        large: 'javascript:;'
      }
    };
  },
  components: {
    Datepicker: vuejs_datepicker__WEBPACK_IMPORTED_MODULE_0__["default"],
    Autocomplete: vuejs_auto_complete__WEBPACK_IMPORTED_MODULE_1___default.a
  },
  methods: {
    submitData: function submitData() {
      var self = this;
      self.$Progress.start();
      var outletId = 0;

      if (self.globalVar.session.outlet.length > 1) {
        if ($("#outlet_id").select2('data')[0]) {
          outletId = $("#outlet_id").select2('data')[0].id;
        }
      } else {
        outletId = self.globalVar.session.outlet[0].outlet.id;
      }

      self.form.outlet_id = outletId;
      var customerId = 0;

      if ($("#customer_id").select2('data')[0]) {
        customerId = $("#customer_id").select2('data')[0].id;
      }

      self.form.customer_id = customerId;
      var technicianId = '';

      if (self.form.type == 2) {
        if ($("#technician_id").select2('data')[0]) {
          technicianId = $("#technician_id").select2('data')[0].id;
        }
      }

      self.form.technician_id = technicianId;
      self.form.details = self.itemDetails;

      if (self.form.payment == 2) {
        if (this.parseNumber(self.form.customer_debt) + this.parseNumber(self.form.total) - this.parseNumber(self.form.down_payment) >= this.parseNumber(self.form.customer_limit)) {
          Swal.fire('Alert!', 'Melebihi limit hutang.', 'warning');
          self.$Progress.fail();
        } else {
          if (self.form.id) {
            self.form.put('/method/transaction/sales/' + self.form.id).then(function (response) {
              Toast.fire({
                type: 'success',
                title: 'Data updated successfully'
              });
              self.$Progress.finish();
              self.changeLinkPrint(response.data); // setTimeout(function(){
              //     self.$router.replace('/transaction/sales')
              // }, 2100);
            })["catch"](function () {
              Swal.fire('Alert!', 'Something went wrong.', 'warning');
              self.$Progress.fail();
            });
          } else {
            self.form.post('/method/transaction/sales').then(function (response) {
              Toast.fire({
                type: 'success',
                title: 'Data saved successfully'
              });
              self.$Progress.finish();
              self.changeLinkPrint(response.data); // setTimeout(function(){
              //     self.$router.replace('/transaction/sales')
              // }, 2100);
            })["catch"](function () {
              Swal.fire('Alert!', 'Something went wrong.', 'warning');
              self.$Progress.fail();
            });
          }
        }
      } else {
        if (self.form.id) {
          self.form.put('/method/transaction/sales/' + self.form.id).then(function (response) {
            Toast.fire({
              type: 'success',
              title: 'Data updated successfully'
            });
            self.$Progress.finish();
            self.changeLinkPrint(response.data); // setTimeout(function(){
            //     self.$router.replace('/transaction/sales')
            // }, 2100);
          })["catch"](function () {
            Swal.fire('Alert!', 'Something went wrong.', 'warning');
            self.$Progress.fail();
          });
        } else {
          self.form.post('/method/transaction/sales').then(function (response) {
            Toast.fire({
              type: 'success',
              title: 'Data saved successfully'
            });
            self.$Progress.finish();
            self.changeLinkPrint(response.data); // setTimeout(function(){
            //     self.$router.replace('/transaction/sales')
            // }, 2100);
          })["catch"](function () {
            Swal.fire('Alert!', 'Something went wrong.', 'warning');
            self.$Progress.fail();
          });
        }
      }
    },
    getItemDetails: function getItemDetails(data) {
      var self = this;
      var outletId = 0;

      if (self.globalVar.session.outlet.length > 1) {
        if ($("#outlet_id").select2('data')[0]) {
          outletId = $("#outlet_id").select2('data')[0].id;
        }
      } else {
        outletId = self.globalVar.session.outlet[0].outlet.id;
      }

      axios.get('/method/inventory/stocks/' + outletId + '/' + data.value).then(function (response) {
        var quantityStock = 0;

        if (response.data.quantity) {
          quantityStock = response.data.quantity;
        }

        var flag = 0;

        for (var i = 0; i < self.itemDetails.length; i++) {
          if (self.itemDetails[i].item_id == data.selectedObject.id) {
            flag = 1;
            break;
          }
        }

        if (flag == 0) {
          self.itemDetails.push({
            item_id: data.selectedObject.id,
            code: data.selectedObject.code,
            name: data.selectedObject.name,
            purchase_price: self.formatNumber(data.selectedObject.purchase_price.price),
            selling_price: self.formatNumber(data.selectedObject.selling_price.price),
            quantity_stock: self.formatNumber(quantityStock),
            quantity: 0,
            discount_nominal: 0,
            price_total: 0
          });
        }
      })["catch"](function (error) {});
      this.$refs.autocompleteItems.clear();
    },
    deleteDataDetail: function deleteDataDetail(index) {
      var _this2 = this;

      Swal.fire({
        title: 'Are you sure?',
        text: 'Record will be deleted.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
      }).then(function (result) {
        if (result.value) {
          _this2.$delete(_this2.itemDetails, index);

          _this2.sumTotal();
        }
      });
    },
    formatNumberDetails: function formatNumberDetails(value, index) {
      if (index != null) {
        var qtyStock = this.parseNumber(this.itemDetails[index].quantity_stock);
        var qty = this.parseNumber(value);

        if (qty > qtyStock) {
          Swal.fire('Alert!', 'Kuantiti lebih besar dari stok.', 'warning');
          value = 0;
        }
      }

      var formatted = this.formatNumber(value);
      this.itemDetails[index].quantity = formatted;
      this.sumItemPrice();
    },
    formatNumberDiscountDetails: function formatNumberDiscountDetails(value, index) {
      var formatted = this.formatNumber(value);
      this.itemDetails[index].discount_nominal = formatted;
      this.sumItemPrice();
    },
    sumItemPrice: function sumItemPrice() {
      for (var i = 0; i < this.itemDetails.length; i++) {
        var price = this.parseNumber(this.itemDetails[i].selling_price);
        var qty = this.parseNumber(this.itemDetails[i].quantity);
        var discount = this.parseNumber(this.itemDetails[i].discount_nominal);
        var formatted = this.formatNumber(price * qty - qty * discount);
        this.itemDetails[i].price_total = formatted;
      }

      this.sumTotal();
    },
    resetDetail: function resetDetail() {
      this.itemDetails = [];
    },
    sumTotal: function sumTotal() {
      var subTotal = 0;

      for (var i = 0; i < this.itemDetails.length; i++) {
        subTotal += this.parseNumber(this.itemDetails[i].price_total);
      }

      this.form.subtotal = this.formatNumber(subTotal);
      var fee = 0;

      if (this.form.fee != 0) {
        fee = this.parseNumber(this.form.fee);
      }

      subTotal += fee;
      var discount = 0;

      if (this.form.discount != 0) {
        discount = subTotal * this.parseNumber(this.form.discount) / 100;
      }

      subTotal -= discount;
      var ppn = 0;

      if (this.form.ppn != 0) {
        ppn = subTotal * this.parseNumber(this.form.ppn) / 100;
      }

      subTotal += ppn;
      this.form.total = this.formatNumber(subTotal);
    },
    sumChange: function sumChange() {
      var total = this.parseNumber(this.form.total);
      var pay = this.parseNumber(this.form.pay);
      this.form.change = this.formatNumber(pay - total);
    },
    checkType: function checkType() {
      var type = 0;

      if ($("#type").select2('data')[0]) {
        type = $("#type").select2('data')[0].id;
      }

      this.form.type = type;
    },
    getCustomer: function getCustomer() {
      var _this3 = this;

      var self = this;
      var customerId = 0;

      if ($("#customer_id").select2('data')[0]) {
        customerId = $("#customer_id").select2('data')[0].id;
      }

      axios.get('/method/master/customers/' + customerId).then(function (response) {
        if (self.form.type != 2) {
          self.form.customer_name = response.data.name;
          self.form.customer_phone = response.data.phone;
        }

        self.form.customer_type = response.data.customertype.name;
        self.form.customer_limit = _this3.formatNumber(response.data.customertype.debt_limit);
        self.form.discount = _this3.formatNumber(response.data.customertype.discount);
        self.form.customer_debt = 0;

        if (response.data.debt) {
          self.form.customer_debt = _this3.formatNumber(response.data.debt.nominal);
        }
      })["catch"](function (error) {});
    },
    formattedAutocompleteServices: function formattedAutocompleteServices(result) {
      return result.number;
    },
    getServices: function getServices(data) {
      var self = this;
      self.form.customer_name = data.selectedObject.customer_name;
      self.form.customer_phone = data.selectedObject.customer_phone;
      self.form.service_id = data.selectedObject.id;
      self.form.service_number = data.selectedObject.number;
      self.form.service_item_name = data.selectedObject.item_name;
      self.form.service_complaint = data.selectedObject.complaint;
      self.form.service_equipment = data.selectedObject.equipment;
      self.selecteditemsCustomer = [{
        id: data.selectedObject.customer.id,
        name: data.selectedObject.customer.name
      }];
      self.selecteditemsTechnician = [{
        id: data.selectedObject.technician.id,
        name: data.selectedObject.technician.name
      }];
      setTimeout(function () {
        self.getCustomer();
      }, 400);
    },
    resetServices: function resetServices() {
      var self = this;
      self.form.service_id = '';
      self.form.service_number = '';
      self.form.service_item_name = '';
      self.form.service_complaint = '';
      self.form.service_equipment = '';
      self.selecteditemsCustomer = [];
      self.form.customer_type = '';
      self.form.customer_limit = 0;
      self.form.discount = 0;
      self.form.customer_debt = 0;
      self.form.customer_name = '';
      self.form.customer_phone = '';
    },
    loadData: function loadData() {
      var self = this;
      axios.get('/method/transaction/sales/' + self.$route.params.id).then(function (response) {
        if (response.data) {
          self.form.id = response.data.id;
          self.form.date = response.data.date;
          self.form.number = response.data.number;
          self.form.outlet_id = response.data.outlet.id;
          self.form.customer_id = response.data.customer.id;
          self.form.customer_name = response.data.customer_name;
          self.form.customer_phone = response.data.customer_phone;
          self.form.customer_debt = 0;

          if (response.data.customer.debt) {
            self.form.customer_debt = self.formatNumber(response.data.customer.debt.nominal);
          }

          self.form.customer_type = response.data.customer.customertype.name;
          self.form.customer_limit = self.formatNumber(response.data.customer.customertype.debt_limit);
          self.form.type = response.data.type;
          self.form.technician_id = '';

          if (response.data.technician) {
            self.form.technician_id = response.data.technician.id;
            self.selecteditemsTechnician = [{
              id: response.data.technician.id,
              name: response.data.technician.name
            }];
          }

          self.form.service_item_name = response.data.service_item_name;
          self.form.service_complaint = response.data.service_complaint;
          self.form.service_equipment = response.data.service_equipment;
          self.form.subtotal = self.formatNumber(response.data.subtotal);
          self.form.fee = self.formatNumber(response.data.fee);
          self.form.discount = self.formatNumber(response.data.discount);
          self.form.ppn = response.data.ppn;
          self.form.total = self.formatNumber(response.data.total);
          self.form.payment = response.data.payment;
          self.form.down_payment = self.formatNumber(response.data.down_payment);
          self.form.pay = self.formatNumber(response.data.pay);
          self.form.change = self.formatNumber(response.data.change);
          self.form.information = response.data.information;
          self.selecteditemsOutlet = [{
            id: response.data.outlet.id,
            name: response.data.outlet.name
          }];
          self.selecteditemsCustomer = [{
            id: response.data.customer.id,
            name: response.data.customer.name
          }];
          self.itemDetails = [];

          for (var i = 0; i < response.data.detail.length; i++) {
            self.itemDetails.push({
              item_id: response.data.detail[i].item.id,
              code: response.data.detail[i].item.code,
              name: response.data.detail[i].item.name,
              purchase_price: self.formatNumber(response.data.detail[i].purchase_price),
              selling_price: self.formatNumber(response.data.detail[i].selling_price),
              quantity_stock: 0,
              quantity: self.formatNumber(response.data.detail[i].quantity),
              price_total: self.formatNumber(response.data.detail[i].selling_price * response.data.detail[i].quantity)
            });
          }
        }
      })["catch"](function (error) {});
    },
    changeLinkPrint: function changeLinkPrint(data) {
      this.linkPrint.small = 'small/' + data.id;
      this.linkPrint.large = 'large/' + data.id;
      $("#modalPrint").modal({
        backdrop: "static"
      });
    }
  },
  watch: {
    'form.type': function formType(to, from) {
      if (to == 1) {
        this.form.fee = 0;
      }

      this.$refs.service_item_number.clear();

      if (!this.$route.params.id) {
        this.resetServices();
      }

      this.sumTotal();
    },
    'form.ppn': function formPpn(to, from) {
      this.sumTotal();
    },
    'form.fee': function formFee(to, from) {
      var formatted = this.formatNumber(to);
      this.form.fee = formatted;
      this.sumTotal();
    },
    'form.down_payment': function formDown_payment(to, from) {
      var formatted = this.formatNumber(to);
      this.form.down_payment = formatted;
    },
    'form.discount': function formDiscount(to, from) {
      var formatted = this.formatNumber(to);
      this.form.discount = formatted;
      this.sumTotal();
    },
    'form.pay': function formPay(to, from) {
      var formatted = this.formatNumber(to);
      this.form.pay = formatted;
      this.sumChange();
    },
    'form.change': function formChange(to, from) {
      var formatted = this.formatNumber(to);
      this.form.change = formatted;
    }
  },
  activated: function activated() {
    var self = this;
    $("#outlet_id").val('').trigger('change');
    self.form.clear();
    self.form.reset();
    self.form.date = date;
    self.selecteditemsOutlet = [];
    self.selecteditemsCustomer = [];
    self.selecteditemsTechnician = [];
    self.itemDetails = [];
    self.renderingCount++;
    self.renderingCountCustomer++;
    self.renderingCountTechnician++;
    self.$refs.service_item_number.clear();

    if (self.$route.params.id) {
      setTimeout(function () {
        self.loadData();
      }, 400);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/transaction/SaleForm.vue?vue&type=template&id=74175c8d&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/transaction/SaleForm.vue?vue&type=template&id=74175c8d& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "content-header" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row mb-2" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "col-sm-6" }, [
            _c("ol", { staticClass: "breadcrumb float-sm-right" }, [
              _c("li", { staticClass: "breadcrumb-item" }, [
                _c("a", { attrs: { href: "javascript:;" } }, [
                  _vm._v(_vm._s(_vm.globalVar.appName))
                ])
              ]),
              _vm._v(" "),
              _vm._m(1),
              _vm._v(" "),
              _c("li", { staticClass: "breadcrumb-item active" }, [
                _vm._v("Penjualan")
              ])
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("section", { staticClass: "content" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12" }, [
            _vm._m(2),
            _vm._v(" "),
            _c("div", { staticClass: "card" }, [
              _vm._m(3),
              _vm._v(" "),
              _c("div", { staticClass: "card-body" }, [
                _c(
                  "form",
                  {
                    attrs: { id: "formAdd" },
                    on: {
                      submit: function($event) {
                        $event.preventDefault()
                        return _vm.submitData($event)
                      }
                    }
                  },
                  [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.form.id,
                          expression: "form.id"
                        }
                      ],
                      staticClass: "d-none",
                      class: { "is-invalid": _vm.form.errors.has("id") },
                      attrs: { type: "text", name: "id", id: "id" },
                      domProps: { value: _vm.form.id },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.form, "id", $event.target.value)
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("has-error", { attrs: { form: _vm.form, field: "id" } }),
                    _vm._v(" "),
                    _vm.globalVar.session
                      ? _c("div", { staticClass: "form-group row" }, [
                          _vm.globalVar.session.outlet.length > 1
                            ? _c(
                                "label",
                                {
                                  staticClass: "col-sm-2 col-form-label",
                                  attrs: { for: "outlet_id" }
                                },
                                [
                                  _vm._v(
                                    "\n                                Toko/Gudang\n                            "
                                  )
                                ]
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.globalVar.session.outlet.length > 1
                            ? _c(
                                "div",
                                {
                                  key: _vm.renderingCount + "-outlet",
                                  staticClass: "col-sm-4"
                                },
                                [
                                  _c("select2", {
                                    ref: "outlet_id",
                                    attrs: {
                                      url: "/method/master/outlets/select",
                                      name: "outlet_id",
                                      selecteditems: _vm.selecteditemsOutlet,
                                      identifier: "outlet_id",
                                      placeholder: "Toko/Gudang",
                                      required: "",
                                      disabled: _vm.$route.params.id
                                        ? true
                                        : false
                                    },
                                    on: { changed: _vm.resetDetail }
                                  }),
                                  _vm._v(" "),
                                  _c("has-error", {
                                    attrs: {
                                      form: _vm.form,
                                      field: "outlet_id"
                                    }
                                  })
                                ],
                                1
                              )
                            : _vm._e()
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "date" }
                        },
                        [
                          _vm._v(
                            "\n                                Tanggal\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("datepicker", {
                            ref: "date",
                            class: {
                              "is-invalid": _vm.form.errors.has("date")
                            },
                            attrs: {
                              "input-class": "form-control",
                              name: "date",
                              id: "date",
                              placeholder: "Tanggal",
                              autocomplete: "off",
                              format: _vm.formatDateDefault,
                              required: "",
                              disabled: _vm.$route.params.id ? true : false
                            },
                            model: {
                              value: _vm.form.date,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "date", $$v)
                              },
                              expression: "form.date"
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "date" }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "number" }
                        },
                        [
                          _vm._v(
                            "\n                                Nomor Transaksi\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.number,
                                expression: "form.number"
                              }
                            ],
                            ref: "number",
                            staticClass: "form-control",
                            class: {
                              "is-invalid": _vm.form.errors.has("number")
                            },
                            attrs: {
                              type: "text",
                              name: "number",
                              id: "number",
                              placeholder: "Nomor Transaksi",
                              autocomplete: "off",
                              readonly: ""
                            },
                            domProps: { value: _vm.form.number },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "number",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "number" }
                          }),
                          _vm._v(" "),
                          _c("small", { staticClass: "form-text text-muted" }, [
                            _vm._v(
                              "\n                                    *) Otomatis dari sistem.\n                                "
                            )
                          ])
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "type" }
                        },
                        [
                          _vm._v(
                            "\n                                Jenis Penjualan\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c(
                            "div",
                            { staticClass: "form-check form-check-inline" },
                            [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.form.type,
                                    expression: "form.type"
                                  }
                                ],
                                staticClass: "form-check-input",
                                class: {
                                  "is-invalid": _vm.form.errors.has("type")
                                },
                                attrs: {
                                  type: "radio",
                                  name: "type",
                                  id: "type1",
                                  value: "1",
                                  checked: "",
                                  disabled: _vm.$route.params.id ? true : false
                                },
                                domProps: {
                                  checked: _vm._q(_vm.form.type, "1")
                                },
                                on: {
                                  change: function($event) {
                                    return _vm.$set(_vm.form, "type", "1")
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "label",
                                {
                                  staticClass: "form-check-label",
                                  attrs: { for: "type1" }
                                },
                                [
                                  _vm._v(
                                    "\n                                      Barang\n                                  "
                                  )
                                ]
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "form-check form-check-inline" },
                            [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.form.type,
                                    expression: "form.type"
                                  }
                                ],
                                staticClass: "form-check-input",
                                class: {
                                  "is-invalid": _vm.form.errors.has("type")
                                },
                                attrs: {
                                  type: "radio",
                                  name: "type",
                                  id: "type2",
                                  value: "2",
                                  disabled: _vm.$route.params.id ? true : false
                                },
                                domProps: {
                                  checked: _vm._q(_vm.form.type, "2")
                                },
                                on: {
                                  change: function($event) {
                                    return _vm.$set(_vm.form, "type", "2")
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "label",
                                {
                                  staticClass: "form-check-label",
                                  attrs: { for: "type2" }
                                },
                                [
                                  _vm._v(
                                    "\n                                      Service\n                                  "
                                  )
                                ]
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "type" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "form-group row",
                        class:
                          _vm.form.type == 1 || _vm.$route.params.id
                            ? "d-none"
                            : ""
                      },
                      [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-2 col-form-label",
                            attrs: { for: "service_item_number" }
                          },
                          [
                            _vm._v(
                              "\n                                Nomor Servis\n                            "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-4" },
                          [
                            _c("autocomplete", {
                              ref: "service_item_number",
                              attrs: {
                                inputClass: "form-control",
                                placeholder:
                                  "" + _vm.serviceAutocomplete.placeholder,
                                source:
                                  "/method/transaction/services/autocomplete?search=",
                                "results-display":
                                  _vm.formattedAutocompleteServices
                              },
                              on: {
                                selected: _vm.getServices,
                                clear: _vm.resetServices
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: {
                                form: _vm.form,
                                field: "service_item_number"
                              }
                            })
                          ],
                          1
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "form-group row",
                        class: _vm.form.type == 1 ? "d-none" : ""
                      },
                      [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-2 col-form-label",
                            attrs: { for: "service_item_name" }
                          },
                          [
                            _vm._v(
                              "\n                                Nama Barang\n                            "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-4" },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.service_item_name,
                                  expression: "form.service_item_name"
                                }
                              ],
                              ref: "service_item_name",
                              staticClass: "form-control",
                              class: {
                                "is-invalid": _vm.form.errors.has(
                                  "service_item_name"
                                )
                              },
                              attrs: {
                                type: "text",
                                name: "service_item_name",
                                id: "service_item_name",
                                placeholder: "Nama Barang, Merek, Tipe",
                                autocomplete: "off",
                                required: _vm.form.type == 1 ? false : true,
                                readonly: ""
                              },
                              domProps: { value: _vm.form.service_item_name },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.form,
                                    "service_item_name",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: {
                                form: _vm.form,
                                field: "service_item_name"
                              }
                            })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-2 col-form-label",
                            attrs: { for: "technician_id" }
                          },
                          [
                            _vm._v(
                              "\n                                Teknisi\n                            "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            key: _vm.renderingCountTechnician + "-technician",
                            staticClass: "col-sm-4"
                          },
                          [
                            _c("select2", {
                              ref: "technician_id",
                              attrs: {
                                url: "/method/master/technicians/select",
                                name: "technician_id",
                                selecteditems: _vm.selecteditemsTechnician,
                                identifier: "technician_id",
                                placeholder: "Teknisi",
                                required: _vm.form.type == 1 ? false : true,
                                disabled: _vm.$route.params.id ? true : false
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: { form: _vm.form, field: "technician_id" }
                            })
                          ],
                          1
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "form-group row",
                        class: _vm.form.type == 1 ? "d-none" : ""
                      },
                      [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-2 col-form-label",
                            attrs: { for: "service_complaint" }
                          },
                          [
                            _vm._v(
                              "\n                                Keluhan\n                            "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-10" },
                          [
                            _c("has-error", {
                              attrs: {
                                form: _vm.form,
                                field: "service_complaint"
                              }
                            }),
                            _vm._v(" "),
                            _c("textarea", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.service_complaint,
                                  expression: "form.service_complaint"
                                }
                              ],
                              ref: "service_complaint",
                              staticClass: "form-control",
                              class: {
                                "is-invalid": _vm.form.errors.has(
                                  "service_complaint"
                                )
                              },
                              attrs: {
                                name: "service_complaint",
                                rows: "3",
                                id: "service_complaint",
                                placeholder: "Keluhan",
                                autocomplete: "off",
                                required: _vm.form.type == 1 ? false : true,
                                readonly: ""
                              },
                              domProps: { value: _vm.form.service_complaint },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.form,
                                    "service_complaint",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: {
                                form: _vm.form,
                                field: "service_complaint"
                              }
                            })
                          ],
                          1
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "form-group row",
                        class: _vm.form.type == 1 ? "d-none" : ""
                      },
                      [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-2 col-form-label",
                            attrs: { for: "service_equipment" }
                          },
                          [
                            _vm._v(
                              "\n                                Kelengkapan\n                            "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-10" },
                          [
                            _c("has-error", {
                              attrs: {
                                form: _vm.form,
                                field: "service_equipment"
                              }
                            }),
                            _vm._v(" "),
                            _c("textarea", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.service_equipment,
                                  expression: "form.service_equipment"
                                }
                              ],
                              ref: "service_equipment",
                              staticClass: "form-control",
                              class: {
                                "is-invalid": _vm.form.errors.has(
                                  "service_equipment"
                                )
                              },
                              attrs: {
                                name: "service_equipment",
                                rows: "3",
                                id: "service_equipment",
                                placeholder: "Kelengkapan",
                                autocomplete: "off",
                                required: _vm.form.type == 1 ? false : true,
                                readonly: ""
                              },
                              domProps: { value: _vm.form.service_equipment },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.form,
                                    "service_equipment",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: {
                                form: _vm.form,
                                field: "service_equipment"
                              }
                            })
                          ],
                          1
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "customer_id" }
                        },
                        [
                          _vm._v(
                            "\n                                Pelanggan\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          key: _vm.renderingCountCustomer + "-customer",
                          staticClass: "col-sm-4"
                        },
                        [
                          _c("select2", {
                            ref: "customer_id",
                            attrs: {
                              url: "/method/master/customers/select",
                              name: "customer_id",
                              selecteditems: _vm.selecteditemsCustomer,
                              identifier: "customer_id",
                              placeholder: "Pelanggan",
                              required: "",
                              disabled: _vm.$route.params.id ? true : false
                            },
                            on: { changed: _vm.getCustomer }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "customer_id" }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "customer_type" }
                        },
                        [
                          _vm._v(
                            "\n                                Tipe Pelanggan\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.customer_type,
                                expression: "form.customer_type"
                              }
                            ],
                            ref: "customer_type",
                            staticClass: "form-control",
                            class: {
                              "is-invalid": _vm.form.errors.has("customer_type")
                            },
                            attrs: {
                              type: "text",
                              name: "customer_type",
                              id: "customer_type",
                              placeholder: "Tipe Pelanggan",
                              autocomplete: "off",
                              readonly: ""
                            },
                            domProps: { value: _vm.form.customer_type },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "customer_type",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "customer_type" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "customer_name" }
                        },
                        [
                          _vm._v(
                            "\n                              Nama Pelanggan\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.customer_name,
                                expression: "form.customer_name"
                              }
                            ],
                            ref: "customer_name",
                            staticClass: "form-control",
                            class: {
                              "is-invalid": _vm.form.errors.has("customer_name")
                            },
                            attrs: {
                              type: "text",
                              name: "customer_name",
                              id: "customer_name",
                              placeholder: "Nama Pelanggan",
                              autocomplete: "off",
                              readonly: _vm.$route.params.id ? true : false
                            },
                            domProps: { value: _vm.form.customer_name },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "customer_name",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "customer_name" }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "customer_phone" }
                        },
                        [
                          _vm._v(
                            "\n                              Nomor Telepon\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.customer_phone,
                                expression: "form.customer_phone"
                              }
                            ],
                            ref: "customer_phone",
                            staticClass: "form-control",
                            class: {
                              "is-invalid": _vm.form.errors.has(
                                "customer_phone"
                              )
                            },
                            attrs: {
                              type: "text",
                              name: "customer_phone",
                              id: "customer_phone",
                              placeholder: "Nomor Telepon",
                              autocomplete: "off",
                              readonly: _vm.$route.params.id ? true : false
                            },
                            domProps: { value: _vm.form.customer_phone },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "customer_phone",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "customer_phone" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "customer_debt" }
                        },
                        [
                          _vm._v(
                            "\n                                Piutang\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.customer_debt,
                                expression: "form.customer_debt"
                              }
                            ],
                            ref: "customer_debt",
                            staticClass: "form-control text-right",
                            class: {
                              "is-invalid": _vm.form.errors.has("customer_debt")
                            },
                            attrs: {
                              type: "text",
                              name: "customer_debt",
                              id: "customer_debt",
                              placeholder: "Piutang",
                              autocomplete: "off",
                              readonly: ""
                            },
                            domProps: { value: _vm.form.customer_debt },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "customer_debt",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "customer_debt" }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "customer_limit" }
                        },
                        [
                          _vm._v(
                            "\n                                Batas Piutang\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.customer_limit,
                                expression: "form.customer_limit"
                              }
                            ],
                            ref: "customer_limit",
                            staticClass: "form-control text-right",
                            class: {
                              "is-invalid": _vm.form.errors.has(
                                "customer_limit"
                              )
                            },
                            attrs: {
                              type: "text",
                              name: "customer_limit",
                              id: "customer_limit",
                              placeholder: "Batas Piutang",
                              autocomplete: "off",
                              readonly: ""
                            },
                            domProps: { value: _vm.form.customer_limit },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "customer_limit",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "customer_limit" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    !_vm.$route.params.id
                      ? _c("div", { staticClass: "form-group row" }, [
                          _c(
                            "label",
                            { staticClass: "col-sm-2 col-form-label" },
                            [
                              _vm._v(
                                "\n                                Cari Barang\n                            "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "col-sm-4" },
                            [
                              _c("autocomplete", {
                                ref: "autocompleteItems",
                                attrs: {
                                  id: "autocompleteItems",
                                  inputClass: "form-control",
                                  placeholder:
                                    "" + _vm.itemAutocomplete.placeholder,
                                  source:
                                    "/method/master/items/autocomplete?search=",
                                  "results-display":
                                    _vm.formattedAutocompleteItems
                                },
                                on: { selected: _vm.getItemDetails }
                              })
                            ],
                            1
                          )
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c("div", { staticClass: "col-sm-12" }, [
                        _c(
                          "table",
                          {
                            staticClass:
                              "table table-bordered table-striped table-hover dataTable no-footer",
                            attrs: { id: "tableData" }
                          },
                          [
                            _c("thead", [
                              _c("tr", [
                                _c("th", { staticStyle: { width: "5%" } }, [
                                  _vm._v("No.")
                                ]),
                                _vm._v(" "),
                                _c("th", [_vm._v("Kode")]),
                                _vm._v(" "),
                                _c("th", [_vm._v("Nama Barang")]),
                                _vm._v(" "),
                                _c("th", { staticStyle: { width: "15%" } }, [
                                  _vm._v("Harga")
                                ]),
                                _vm._v(" "),
                                !_vm.$route.params.id
                                  ? _c(
                                      "th",
                                      { staticStyle: { width: "10%" } },
                                      [_vm._v("Stok")]
                                    )
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("th", { staticStyle: { width: "10%" } }, [
                                  _vm._v("Kuantiti")
                                ]),
                                _vm._v(" "),
                                _c("th", { staticStyle: { width: "15%" } }, [
                                  _vm._v("Potongan")
                                ]),
                                _vm._v(" "),
                                _c("th", { staticStyle: { width: "15%" } }, [
                                  _vm._v("Total")
                                ]),
                                _vm._v(" "),
                                !_vm.$route.params.id
                                  ? _c("th", { staticStyle: { width: "5%" } })
                                  : _vm._e()
                              ])
                            ]),
                            _vm._v(" "),
                            _vm.itemDetails.length > 0
                              ? _c(
                                  "tbody",
                                  _vm._l(_vm.itemDetails, function(
                                    itemDetail,
                                    index
                                  ) {
                                    return _c("tr", { key: index }, [
                                      _c("td", [
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: itemDetail.item_id,
                                              expression: "itemDetail.item_id"
                                            }
                                          ],
                                          staticClass: "d-none",
                                          attrs: {
                                            type: "text",
                                            name: "item_id[]",
                                            id: "item_id" + index
                                          },
                                          domProps: {
                                            value: itemDetail.item_id
                                          },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                itemDetail,
                                                "item_id",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        }),
                                        _vm._v(
                                          "\n                                                  " +
                                            _vm._s(index + 1) +
                                            "\n                                              "
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("td", [
                                        _vm._v(_vm._s(itemDetail.code))
                                      ]),
                                      _vm._v(" "),
                                      _c("td", [
                                        _vm._v(_vm._s(itemDetail.name))
                                      ]),
                                      _vm._v(" "),
                                      _c("td", [
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: itemDetail.selling_price,
                                              expression:
                                                "itemDetail.selling_price"
                                            }
                                          ],
                                          staticClass:
                                            "form-control text-right",
                                          attrs: {
                                            type: "text",
                                            name: "selling_price[]",
                                            id: "selling_price" + index,
                                            placeholder: "Stok",
                                            autocomplete: "off",
                                            maxlength: "10",
                                            required: "",
                                            readonly: ""
                                          },
                                          domProps: {
                                            value: itemDetail.selling_price
                                          },
                                          on: {
                                            keypress: function($event) {
                                              return _vm.isNumber($event)
                                            },
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                itemDetail,
                                                "selling_price",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      !_vm.$route.params.id
                                        ? _c("td", [
                                            _c("input", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value:
                                                    itemDetail.quantity_stock,
                                                  expression:
                                                    "itemDetail.quantity_stock"
                                                }
                                              ],
                                              staticClass:
                                                "form-control text-right",
                                              attrs: {
                                                type: "text",
                                                name: "quantity_stock[]",
                                                id: "quantity_stock" + index,
                                                placeholder: "Stok",
                                                autocomplete: "off",
                                                maxlength: "10",
                                                required: "",
                                                readonly: ""
                                              },
                                              domProps: {
                                                value: itemDetail.quantity_stock
                                              },
                                              on: {
                                                keypress: function($event) {
                                                  return _vm.isNumber($event)
                                                },
                                                input: function($event) {
                                                  if ($event.target.composing) {
                                                    return
                                                  }
                                                  _vm.$set(
                                                    itemDetail,
                                                    "quantity_stock",
                                                    $event.target.value
                                                  )
                                                }
                                              }
                                            })
                                          ])
                                        : _vm._e(),
                                      _vm._v(" "),
                                      _c("td", [
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: itemDetail.quantity,
                                              expression: "itemDetail.quantity"
                                            }
                                          ],
                                          staticClass:
                                            "form-control text-right",
                                          attrs: {
                                            type: "text",
                                            name: "quantity[]",
                                            id: "quantity" + index,
                                            placeholder: "Kuantiti Lama",
                                            autocomplete: "off",
                                            maxlength: "10",
                                            required: "",
                                            readonly: _vm.$route.params.id
                                              ? true
                                              : false
                                          },
                                          domProps: {
                                            value: itemDetail.quantity
                                          },
                                          on: {
                                            keypress: function($event) {
                                              return _vm.isNumber($event)
                                            },
                                            input: [
                                              function($event) {
                                                if ($event.target.composing) {
                                                  return
                                                }
                                                _vm.$set(
                                                  itemDetail,
                                                  "quantity",
                                                  $event.target.value
                                                )
                                              },
                                              function($event) {
                                                return _vm.formatNumberDetails(
                                                  itemDetail.quantity,
                                                  index
                                                )
                                              }
                                            ]
                                          }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      _c("td", [
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value:
                                                itemDetail.discount_nominal,
                                              expression:
                                                "itemDetail.discount_nominal"
                                            }
                                          ],
                                          staticClass:
                                            "form-control text-right",
                                          attrs: {
                                            type: "text",
                                            name: "discount_nominal[]",
                                            id: "discount_nominal" + index,
                                            placeholder: "Potongan",
                                            autocomplete: "off",
                                            maxlength: "10",
                                            required: "",
                                            readonly: _vm.$route.params.id
                                              ? true
                                              : false
                                          },
                                          domProps: {
                                            value: itemDetail.discount_nominal
                                          },
                                          on: {
                                            keypress: function($event) {
                                              return _vm.isNumber($event)
                                            },
                                            input: [
                                              function($event) {
                                                if ($event.target.composing) {
                                                  return
                                                }
                                                _vm.$set(
                                                  itemDetail,
                                                  "discount_nominal",
                                                  $event.target.value
                                                )
                                              },
                                              function($event) {
                                                return _vm.formatNumberDiscountDetails(
                                                  itemDetail.discount_nominal,
                                                  index
                                                )
                                              }
                                            ]
                                          }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      _c("td", [
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: itemDetail.price_total,
                                              expression:
                                                "itemDetail.price_total"
                                            }
                                          ],
                                          staticClass:
                                            "form-control text-right",
                                          attrs: {
                                            type: "text",
                                            name: "price_total[]",
                                            id: "price_total" + index,
                                            placeholder: "Stok",
                                            autocomplete: "off",
                                            maxlength: "10",
                                            required: "",
                                            readonly: ""
                                          },
                                          domProps: {
                                            value: itemDetail.price_total
                                          },
                                          on: {
                                            keypress: function($event) {
                                              return _vm.isNumber($event)
                                            },
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                itemDetail,
                                                "price_total",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      !_vm.$route.params.id
                                        ? _c("td", [
                                            _c(
                                              "button",
                                              {
                                                staticClass:
                                                  "btn btn-danger btn-sm",
                                                attrs: {
                                                  type: "button",
                                                  title: "Hapus Data"
                                                },
                                                on: {
                                                  click: function($event) {
                                                    return _vm.deleteDataDetail(
                                                      index
                                                    )
                                                  }
                                                }
                                              },
                                              [
                                                _c("i", {
                                                  staticClass: "fas fa-ban"
                                                })
                                              ]
                                            )
                                          ])
                                        : _vm._e()
                                    ])
                                  }),
                                  0
                                )
                              : _c("tbody", [_vm._m(4)])
                          ]
                        )
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label offset-md-6",
                          attrs: { for: "subtotal" }
                        },
                        [
                          _vm._v(
                            "\n                                Sub Total\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.subtotal,
                                expression: "form.subtotal"
                              }
                            ],
                            ref: "subtotal",
                            staticClass: "form-control text-right",
                            class: {
                              "is-invalid": _vm.form.errors.has("subtotal")
                            },
                            attrs: {
                              type: "text",
                              name: "subtotal",
                              id: "subtotal",
                              placeholder: "Sub Total",
                              autocomplete: "off",
                              readonly: "",
                              required: ""
                            },
                            domProps: { value: _vm.form.subtotal },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "subtotal",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "subtotal" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "form-group row",
                        class: _vm.form.type == 1 ? "d-none" : ""
                      },
                      [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-2 col-form-label offset-md-6",
                            attrs: { for: "fee" }
                          },
                          [
                            _vm._v(
                              "\n                                Biaya Service\n                            "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-4" },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.fee,
                                  expression: "form.fee"
                                }
                              ],
                              ref: "fee",
                              staticClass: "form-control text-right",
                              class: {
                                "is-invalid": _vm.form.errors.has("fee")
                              },
                              attrs: {
                                type: "text",
                                name: "fee",
                                id: "fee",
                                placeholder: "Sub Total",
                                autocomplete: "off",
                                required: _vm.form.type == 1 ? false : true,
                                readonly: _vm.$route.params.id ? true : false
                              },
                              domProps: { value: _vm.form.fee },
                              on: {
                                keypress: function($event) {
                                  return _vm.isNumber($event)
                                },
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(_vm.form, "fee", $event.target.value)
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: { form: _vm.form, field: "fee" }
                            })
                          ],
                          1
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label offset-md-6",
                          attrs: { for: "discount" }
                        },
                        [
                          _vm._v(
                            "\n                                Diskon\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("div", { staticClass: "input-group" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.discount,
                                  expression: "form.discount"
                                },
                                {
                                  name: "shortkey",
                                  rawName: "v-shortkey.focus",
                                  value: ["alt", "d"],
                                  expression: "['alt', 'd']",
                                  modifiers: { focus: true }
                                }
                              ],
                              ref: "discount",
                              staticClass: "form-control text-right",
                              class: {
                                "is-invalid": _vm.form.errors.has("discount")
                              },
                              attrs: {
                                type: "text",
                                name: "discount",
                                id: "discount",
                                placeholder: "Sub Total",
                                autocomplete: "off",
                                required: "",
                                readonly: _vm.$route.params.id ? true : false
                              },
                              domProps: { value: _vm.form.discount },
                              on: {
                                keypress: function($event) {
                                  return _vm.isNumber($event)
                                },
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.form,
                                    "discount",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _vm._m(5)
                          ]),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "discount" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label offset-md-6",
                          attrs: { for: "ppn" }
                        },
                        [
                          _vm._v(
                            "\n                                PPN\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c(
                            "div",
                            { staticClass: "form-check form-check-inline" },
                            [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.form.ppn,
                                    expression: "form.ppn"
                                  }
                                ],
                                staticClass: "form-check-input",
                                class: {
                                  "is-invalid": _vm.form.errors.has("ppn")
                                },
                                attrs: {
                                  type: "radio",
                                  name: "ppn",
                                  id: "ppn1",
                                  value: "0",
                                  checked: "",
                                  disabled: _vm.$route.params.id ? true : false
                                },
                                domProps: {
                                  checked: _vm._q(_vm.form.ppn, "0")
                                },
                                on: {
                                  change: function($event) {
                                    return _vm.$set(_vm.form, "ppn", "0")
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "label",
                                {
                                  staticClass: "form-check-label",
                                  attrs: { for: "ppn1" }
                                },
                                [
                                  _vm._v(
                                    "\n                                      Tidak ada\n                                  "
                                  )
                                ]
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "form-check form-check-inline" },
                            [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.form.ppn,
                                    expression: "form.ppn"
                                  }
                                ],
                                staticClass: "form-check-input",
                                class: {
                                  "is-invalid": _vm.form.errors.has("ppn")
                                },
                                attrs: {
                                  type: "radio",
                                  name: "ppn",
                                  id: "ppn2",
                                  value: "10",
                                  disabled: _vm.$route.params.id ? true : false
                                },
                                domProps: {
                                  checked: _vm._q(_vm.form.ppn, "10")
                                },
                                on: {
                                  change: function($event) {
                                    return _vm.$set(_vm.form, "ppn", "10")
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "label",
                                {
                                  staticClass: "form-check-label",
                                  attrs: { for: "ppn2" }
                                },
                                [
                                  _vm._v(
                                    "\n                                      10%\n                                  "
                                  )
                                ]
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "ppn" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label offset-md-6",
                          attrs: { for: "total" }
                        },
                        [
                          _vm._v(
                            "\n                                Total\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.total,
                                expression: "form.total"
                              }
                            ],
                            ref: "total",
                            staticClass: "form-control text-right",
                            class: {
                              "is-invalid": _vm.form.errors.has("total")
                            },
                            attrs: {
                              type: "text",
                              name: "total",
                              id: "total",
                              placeholder: "Total",
                              autocomplete: "off",
                              readonly: "",
                              required: ""
                            },
                            domProps: { value: _vm.form.total },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(_vm.form, "total", $event.target.value)
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "total" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label offset-md-6",
                          attrs: { for: "payment" }
                        },
                        [
                          _vm._v(
                            "\n                                Pembayaran\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c(
                            "div",
                            { staticClass: "form-check form-check-inline" },
                            [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.form.payment,
                                    expression: "form.payment"
                                  }
                                ],
                                staticClass: "form-check-input",
                                class: {
                                  "is-invalid": _vm.form.errors.has("payment")
                                },
                                attrs: {
                                  type: "radio",
                                  name: "payment",
                                  id: "payment1",
                                  value: "1",
                                  checked: "",
                                  disabled: _vm.$route.params.id ? true : false
                                },
                                domProps: {
                                  checked: _vm._q(_vm.form.payment, "1")
                                },
                                on: {
                                  change: function($event) {
                                    return _vm.$set(_vm.form, "payment", "1")
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "label",
                                {
                                  staticClass: "form-check-label",
                                  attrs: { for: "payment1" }
                                },
                                [
                                  _vm._v(
                                    "\n                                      Tunai\n                                  "
                                  )
                                ]
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "form-check form-check-inline" },
                            [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.form.payment,
                                    expression: "form.payment"
                                  }
                                ],
                                staticClass: "form-check-input",
                                class: {
                                  "is-invalid": _vm.form.errors.has("payment")
                                },
                                attrs: {
                                  type: "radio",
                                  name: "payment",
                                  id: "payment2",
                                  value: "2",
                                  disabled: _vm.$route.params.id ? true : false
                                },
                                domProps: {
                                  checked: _vm._q(_vm.form.payment, "2")
                                },
                                on: {
                                  change: function($event) {
                                    return _vm.$set(_vm.form, "payment", "2")
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "label",
                                {
                                  staticClass: "form-check-label",
                                  attrs: { for: "payment2" }
                                },
                                [
                                  _vm._v(
                                    "\n                                      Hutang\n                                  "
                                  )
                                ]
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "payment" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label offset-md-6",
                          attrs: { for: "pay" }
                        },
                        [
                          _vm._v(
                            "\n                                Bayar\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.pay,
                                expression: "form.pay"
                              },
                              {
                                name: "shortkey",
                                rawName: "v-shortkey.focus",
                                value: ["alt", "p"],
                                expression: "['alt', 'p']",
                                modifiers: { focus: true }
                              }
                            ],
                            ref: "pay",
                            staticClass: "form-control text-right",
                            class: { "is-invalid": _vm.form.errors.has("pay") },
                            attrs: {
                              type: "text",
                              name: "pay",
                              id: "pay",
                              placeholder: "Bayar",
                              autocomplete: "off",
                              required: "",
                              readonly: _vm.$route.params.id ? true : false
                            },
                            domProps: { value: _vm.form.pay },
                            on: {
                              keypress: function($event) {
                                return _vm.isNumber($event)
                              },
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(_vm.form, "pay", $event.target.value)
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "pay" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label offset-md-6",
                          attrs: { for: "change" }
                        },
                        [
                          _vm._v(
                            "\n                                Kembalian\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.change,
                                expression: "form.change"
                              }
                            ],
                            ref: "change",
                            staticClass: "form-control text-right",
                            class: {
                              "is-invalid": _vm.form.errors.has("change")
                            },
                            attrs: {
                              type: "text",
                              name: "change",
                              id: "change",
                              placeholder: "Kembalian",
                              autocomplete: "off",
                              required: "",
                              readonly: ""
                            },
                            domProps: { value: _vm.form.change },
                            on: {
                              keypress: function($event) {
                                return _vm.isNumber($event)
                              },
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "change",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "change" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "form-group row",
                        class: _vm.form.payment == 1 ? "d-none" : ""
                      },
                      [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-2 col-form-label offset-md-6",
                            attrs: { for: "down_payment" }
                          },
                          [
                            _vm._v(
                              "\n                                Uang Muka\n                            "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-4" },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.down_payment,
                                  expression: "form.down_payment"
                                }
                              ],
                              ref: "down_payment",
                              staticClass: "form-control text-right",
                              class: {
                                "is-invalid": _vm.form.errors.has(
                                  "down_payment"
                                )
                              },
                              attrs: {
                                type: "text",
                                name: "down_payment",
                                id: "down_payment",
                                placeholder: "Uang Muka",
                                autocomplete: "off",
                                required: _vm.form.payment == 1 ? false : true,
                                readonly: _vm.$route.params.id ? true : false
                              },
                              domProps: { value: _vm.form.down_payment },
                              on: {
                                keypress: function($event) {
                                  return _vm.isNumber($event)
                                },
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.form,
                                    "down_payment",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: { form: _vm.form, field: "down_payment" }
                            })
                          ],
                          1
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "information" }
                        },
                        [
                          _vm._v(
                            "\n                                Keterangan\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-10" },
                        [
                          _c("textarea", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.information,
                                expression: "form.information"
                              }
                            ],
                            ref: "information",
                            staticClass: "form-control",
                            class: {
                              "is-invalid": _vm.form.errors.has("information")
                            },
                            attrs: {
                              name: "information",
                              rows: "3",
                              id: "information",
                              placeholder: "Keterangan",
                              autocomplete: "off",
                              readonly: _vm.$route.params.id ? true : false
                            },
                            domProps: { value: _vm.form.information },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "information",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "information" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "div",
                        { staticClass: "col-sm-12 text-right" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "btn btn-secondary",
                              attrs: { to: "/transaction/sales" }
                            },
                            [
                              _vm._v(
                                "\n                                Batal\n                              "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          !_vm.$route.params.id
                            ? _c(
                                "button",
                                {
                                  directives: [
                                    {
                                      name: "shortkey",
                                      rawName: "v-shortkey.focus",
                                      value: ["alt", "s"],
                                      expression: "['alt', 's']",
                                      modifiers: { focus: true }
                                    }
                                  ],
                                  staticClass: "btn btn-success",
                                  attrs: { type: "submit" }
                                },
                                [
                                  _vm._v(
                                    "\n                                  Simpan\n                              "
                                  )
                                ]
                              )
                            : _vm._e()
                        ],
                        1
                      )
                    ])
                  ],
                  1
                )
              ])
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "modal fade",
        attrs: {
          id: "modalPrint",
          role: "dialog",
          "aria-labelledby": "modalPrintLabel",
          "aria-hidden": "true"
        }
      },
      [
        _c(
          "div",
          {
            staticClass: "modal-dialog modal-dialog-centered",
            attrs: { role: "document" }
          },
          [
            _c("div", { staticClass: "modal-content" }, [
              _vm._m(6),
              _vm._v(" "),
              _c("div", { staticClass: "modal-body" }, [
                _c("div", { staticClass: "form-group row" }, [
                  _c("div", { staticClass: "col-sm-12 text-center" }, [
                    _c(
                      "a",
                      {
                        staticClass: "btn btn-primary",
                        attrs: {
                          href: _vm.globalVar.publicPath + "transaction/sales",
                          title: "Kembali"
                        }
                      },
                      [
                        _c("i", { staticClass: "fas fa-arrow-circle-left" }),
                        _vm._v(" Kembali\n                            ")
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "a",
                      {
                        staticClass: "btn btn-danger",
                        attrs: {
                          href:
                            _vm.globalVar.publicPath +
                            "method/transaction/sales/print/" +
                            _vm.linkPrint.small,
                          title: "Cetak Nota Kecil",
                          target: "_blank"
                        }
                      },
                      [
                        _c("i", { staticClass: "fas fa-print" }),
                        _vm._v(" Nota Kecil\n                            ")
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "a",
                      {
                        staticClass: "btn btn-danger",
                        attrs: {
                          href:
                            _vm.globalVar.publicPath +
                            "method/transaction/sales/print/" +
                            _vm.linkPrint.large,
                          title: "Cetak Nota Besar",
                          target: "_blank"
                        }
                      },
                      [
                        _c("i", { staticClass: "fas fa-print" }),
                        _vm._v(" Nota Besar\n                            ")
                      ]
                    )
                  ])
                ])
              ])
            ])
          ]
        )
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-sm-6" }, [
      _c("h1", { staticClass: "m-0 text-dark" }, [_vm._v("Penjualan")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "breadcrumb-item" }, [
      _c("a", { attrs: { href: "javascript:;" } }, [_vm._v("Transaksi")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card", attrs: { id: "shotkeyinfo" } }, [
      _c("div", { staticClass: "card-header" }, [
        _c("h3", { staticClass: "card-title" }, [
          _c("i", { staticClass: "fas fa-keyboard" }),
          _vm._v(
            "\n                            Short Key\n                        "
          )
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "card-body" }, [
        _c("ol", [
          _c("li", [
            _vm._v("Tampilkan/Hilangkan Informasi Short Key (alt + i)")
          ]),
          _vm._v(" "),
          _c("li", [_vm._v("Form Penjualan Baru (alt + n)")]),
          _vm._v(" "),
          _c("li", [_vm._v("Focus Input Pelanggan (alt + c)")]),
          _vm._v(" "),
          _c("li", [_vm._v("Focus Input Pencarian Barang (alt + f)")]),
          _vm._v(" "),
          _c("li", [_vm._v("Focus Input Diskon Penjualan (alt + d)")]),
          _vm._v(" "),
          _c("li", [_vm._v("Focus Input Pembayaran Penjualan (alt + p)")]),
          _vm._v(" "),
          _c("li", [_vm._v("Simpan Penjualan (alt + s)")])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header" }, [
      _c("h3", { staticClass: "card-title" }),
      _vm._v(" "),
      _c("div", { staticClass: "card-tools" }),
      _vm._v(" "),
      _c("br")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [
      _c("td", { staticClass: "text-center", attrs: { colspan: "9" } }, [
        _vm._v(
          "\n                                                Tidak ada data yang cocok ditemukan\n                                              "
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-append" }, [
      _c(
        "span",
        { staticClass: "input-group-text", attrs: { id: "basic-addon" } },
        [_vm._v("%")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "h5",
        { staticClass: "modal-title", attrs: { id: "modalFormLabel" } },
        [_vm._v("\n                        Cetak Nota\n                    ")]
      ),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backoffice/transaction/SaleForm.vue":
/*!*********************************************************************!*\
  !*** ./resources/js/components/backoffice/transaction/SaleForm.vue ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SaleForm_vue_vue_type_template_id_74175c8d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SaleForm.vue?vue&type=template&id=74175c8d& */ "./resources/js/components/backoffice/transaction/SaleForm.vue?vue&type=template&id=74175c8d&");
/* harmony import */ var _SaleForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SaleForm.vue?vue&type=script&lang=js& */ "./resources/js/components/backoffice/transaction/SaleForm.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _SaleForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SaleForm_vue_vue_type_template_id_74175c8d___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SaleForm_vue_vue_type_template_id_74175c8d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backoffice/transaction/SaleForm.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backoffice/transaction/SaleForm.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/backoffice/transaction/SaleForm.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SaleForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./SaleForm.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/transaction/SaleForm.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SaleForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backoffice/transaction/SaleForm.vue?vue&type=template&id=74175c8d&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/components/backoffice/transaction/SaleForm.vue?vue&type=template&id=74175c8d& ***!
  \****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SaleForm_vue_vue_type_template_id_74175c8d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./SaleForm.vue?vue&type=template&id=74175c8d& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/transaction/SaleForm.vue?vue&type=template&id=74175c8d&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SaleForm_vue_vue_type_template_id_74175c8d___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SaleForm_vue_vue_type_template_id_74175c8d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);