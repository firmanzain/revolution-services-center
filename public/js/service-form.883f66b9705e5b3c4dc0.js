(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/service-form"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/transaction/ServiceForm.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/transaction/ServiceForm.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuejs_datepicker__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuejs-datepicker */ "./node_modules/vuejs-datepicker/dist/vuejs-datepicker.esm.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var date = new Date();
/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {
    var _this = this;

    if (this.$route.params.id) {
      this.loadData();
    }

    Fire.$on('AfterLoaded', function () {
      _this.loadData();
    });
  },
  mounted: function mounted() {},
  data: function data() {
    return {
      globalVar: this.$attrs.globalVar,
      renderingCount: 0,
      renderingCountCustomer: 0,
      renderingCountTechnician: 0,
      form: new Form({
        id: '',
        date: date,
        date_estimation: date,
        number: '',
        outlet_id: '',
        customer_id: '',
        customer_name: '',
        customer_phone: '',
        item_name: '',
        complaint: '',
        equipment: '',
        information: '',
        technician_id: '',
        status: -1,
        details: []
      }),
      selecteditemsOutlet: [],
      selecteditemsCustomer: [],
      selecteditemsTechnician: [],
      linkPrint: {
        small: 'javascript:;',
        large: 'javascript:;'
      }
    };
  },
  components: {
    Datepicker: vuejs_datepicker__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  methods: {
    submitData: function submitData() {
      var self = this;
      self.$Progress.start();
      var outletId = 0;

      if (self.globalVar.session.outlet.length > 1) {
        if ($("#outlet_id").select2('data')[0]) {
          outletId = $("#outlet_id").select2('data')[0].id;
        }
      } else {
        outletId = self.globalVar.session.outlet[0].outlet.id;
      }

      self.form.outlet_id = outletId;
      var customerId = 0;

      if ($("#customer_id").select2('data')[0]) {
        customerId = $("#customer_id").select2('data')[0].id;
      }

      self.form.customer_id = customerId;

      if (self.form.id) {
        var technicianId = 0;

        if ($("#technician_id").select2('data')[0]) {
          technicianId = $("#technician_id").select2('data')[0].id;
        }

        self.form.technician_id = technicianId;
        self.form.put('/method/transaction/services/' + self.form.id).then(function (response) {
          Toast.fire({
            type: 'success',
            title: 'Data updated successfully'
          });
          self.$Progress.finish();
          self.changeLinkPrint(response.data); // setTimeout(function(){
          //     self.$router.replace('/transaction/services')
          // }, 2100);
        })["catch"](function () {
          Swal.fire('Alert!', 'Something went wrong.', 'warning');
          self.$Progress.fail();
        });
      } else {
        self.form.post('/method/transaction/services').then(function (response) {
          Toast.fire({
            type: 'success',
            title: 'Data saved successfully'
          });
          self.$Progress.finish();
          self.changeLinkPrint(response.data); // setTimeout(function(){
          //     self.$router.replace('/transaction/services')
          // }, 2100);
        })["catch"](function () {
          Swal.fire('Alert!', 'Something went wrong.', 'warning');
          self.$Progress.fail();
        });
      }
    },
    loadData: function loadData() {
      var self = this;
      axios.get('/method/transaction/services/' + self.$route.params.id).then(function (response) {
        if (response.data) {
          self.form.id = response.data.id;
          self.form.date = response.data.date;
          self.form.date_estimation = response.data.date_estimation;
          self.form.number = response.data.number;
          self.form.outlet_id = response.data.outlet.id;
          self.form.customer_id = response.data.customer.id;
          self.form.customer_name = response.data.customer_name;
          self.form.customer_phone = response.data.customer_phone;
          self.form.item_name = response.data.item_name;
          self.form.complaint = response.data.complaint;
          self.form.equipment = response.data.equipment;
          self.form.information = response.data.information;
          self.form.status = response.data.status;
          self.selecteditemsOutlet = [{
            id: response.data.outlet.id,
            name: response.data.outlet.name
          }];
          self.selecteditemsCustomer = [{
            id: response.data.customer.id,
            name: response.data.customer.name
          }];
          self.selecteditemsTechnician = [{
            id: response.data.technician.id,
            name: response.data.technician.name
          }];
        }
      })["catch"](function (error) {});
    },
    getCustomer: function getCustomer() {
      var self = this;
      var customerId = 0;

      if ($("#customer_id").select2('data')[0]) {
        customerId = $("#customer_id").select2('data')[0].id;
      }

      axios.get('/method/master/customers/' + customerId).then(function (response) {
        self.form.customer_name = response.data.name;
        self.form.customer_phone = response.data.phone;
      })["catch"](function (error) {});
    },
    changeLinkPrint: function changeLinkPrint(data) {
      this.linkPrint.small = 'small/' + data.id;
      this.linkPrint.large = 'large/' + data.id;
      $("#modalPrint").modal({
        backdrop: "static"
      });
    }
  },
  watch: {},
  activated: function activated() {
    var self = this;
    $("#outlet_id").val('').trigger('change');
    self.form.clear();
    self.form.reset();
    self.form.date = date;
    self.form.date_estimation = date;
    self.selecteditemsOutlet = [];
    self.selecteditemsCustomer = [];
    self.selecteditemsTechnician = [];
    self.renderingCount++;
    self.renderingCountCustomer++;
    self.renderingCountTechnician++;

    if (self.$route.params.id) {
      setTimeout(function () {
        self.loadData();
      }, 400);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/transaction/ServiceForm.vue?vue&type=template&id=4015e527&":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/transaction/ServiceForm.vue?vue&type=template&id=4015e527& ***!
  \*************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "content-header" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row mb-2" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "col-sm-6" }, [
            _c("ol", { staticClass: "breadcrumb float-sm-right" }, [
              _c("li", { staticClass: "breadcrumb-item" }, [
                _c("a", { attrs: { href: "javascript:;" } }, [
                  _vm._v(_vm._s(_vm.globalVar.appName))
                ])
              ]),
              _vm._v(" "),
              _vm._m(1),
              _vm._v(" "),
              _c("li", { staticClass: "breadcrumb-item active" }, [
                _vm._v("Servis")
              ])
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("section", { staticClass: "content" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12" }, [
            _c("div", { staticClass: "card" }, [
              _vm._m(2),
              _vm._v(" "),
              _c("div", { staticClass: "card-body" }, [
                _c(
                  "form",
                  {
                    attrs: { id: "formAdd" },
                    on: {
                      submit: function($event) {
                        $event.preventDefault()
                        return _vm.submitData($event)
                      }
                    }
                  },
                  [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.form.id,
                          expression: "form.id"
                        }
                      ],
                      staticClass: "d-none",
                      class: { "is-invalid": _vm.form.errors.has("id") },
                      attrs: { type: "text", name: "id", id: "id" },
                      domProps: { value: _vm.form.id },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.form, "id", $event.target.value)
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("has-error", { attrs: { form: _vm.form, field: "id" } }),
                    _vm._v(" "),
                    _vm.globalVar.session
                      ? _c("div", { staticClass: "form-group row" }, [
                          _vm.globalVar.session.outlet.length > 1
                            ? _c(
                                "label",
                                {
                                  staticClass: "col-sm-2 col-form-label",
                                  attrs: { for: "outlet_id" }
                                },
                                [
                                  _vm._v(
                                    "\n                                Toko/Gudang\n                            "
                                  )
                                ]
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.globalVar.session.outlet.length > 1
                            ? _c(
                                "div",
                                {
                                  key: _vm.renderingCount + "-outlet",
                                  staticClass: "col-sm-4"
                                },
                                [
                                  _c("select2", {
                                    ref: "outlet_id",
                                    attrs: {
                                      url: "/method/master/outlets/select",
                                      name: "outlet_id",
                                      selecteditems: _vm.selecteditemsOutlet,
                                      identifier: "outlet_id",
                                      placeholder: "Toko/Gudang",
                                      required: "",
                                      disabled: _vm.$route.params.id
                                        ? true
                                        : false
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("has-error", {
                                    attrs: {
                                      form: _vm.form,
                                      field: "outlet_id"
                                    }
                                  })
                                ],
                                1
                              )
                            : _vm._e()
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "date" }
                        },
                        [
                          _vm._v(
                            "\n                                Tanggal\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("datepicker", {
                            ref: "date",
                            class: {
                              "is-invalid": _vm.form.errors.has("date")
                            },
                            attrs: {
                              "input-class": "form-control",
                              name: "date",
                              id: "date",
                              placeholder: "Tanggal",
                              autocomplete: "off",
                              format: _vm.formatDateDefault,
                              required: "",
                              disabled: _vm.$route.params.id ? true : false
                            },
                            model: {
                              value: _vm.form.date,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "date", $$v)
                              },
                              expression: "form.date"
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "date" }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "number" }
                        },
                        [
                          _vm._v(
                            "\n                                Nomor Transaksi\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.number,
                                expression: "form.number"
                              }
                            ],
                            ref: "number",
                            staticClass: "form-control",
                            class: {
                              "is-invalid": _vm.form.errors.has("number")
                            },
                            attrs: {
                              type: "text",
                              name: "number",
                              id: "number",
                              placeholder: "Nomor Transaksi",
                              autocomplete: "off",
                              readonly: ""
                            },
                            domProps: { value: _vm.form.number },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "number",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "number" }
                          }),
                          _vm._v(" "),
                          _c("small", { staticClass: "form-text text-muted" }, [
                            _vm._v(
                              "\n                                    *) Otomatis dari sistem.\n                                "
                            )
                          ])
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "date_estimation" }
                        },
                        [
                          _vm._v(
                            "\n                                Estimasi Selesai\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("datepicker", {
                            ref: "date_estimation",
                            class: {
                              "is-invalid": _vm.form.errors.has(
                                "date_estimation"
                              )
                            },
                            attrs: {
                              "input-class": "form-control",
                              name: "date_estimation",
                              id: "date_estimation",
                              placeholder: "Tanggal",
                              autocomplete: "off",
                              format: _vm.formatDateDefault,
                              required: "",
                              disabled: _vm.$route.params.id ? true : false
                            },
                            model: {
                              value: _vm.form.date_estimation,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "date_estimation", $$v)
                              },
                              expression: "form.date_estimation"
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "date_estimation" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "customer_id" }
                        },
                        [
                          _vm._v(
                            "\n                                Pelanggan\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          key: _vm.renderingCountCustomer + "-customer",
                          staticClass: "col-sm-4"
                        },
                        [
                          _c("select2", {
                            ref: "customer_id",
                            attrs: {
                              url: "/method/master/customers/select",
                              name: "customer_id",
                              selecteditems: _vm.selecteditemsCustomer,
                              identifier: "customer_id",
                              placeholder: "Pelanggan",
                              required: "",
                              disabled: _vm.$route.params.id ? true : false
                            },
                            on: { changed: _vm.getCustomer }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "customer_id" }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _vm.$route.params.id
                        ? _c(
                            "label",
                            {
                              staticClass: "col-sm-2 col-form-label",
                              attrs: { for: "technician_id" }
                            },
                            [
                              _vm._v(
                                "\n                                Teknisi\n                            "
                              )
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.$route.params.id
                        ? _c(
                            "div",
                            {
                              key: _vm.renderingCountTechnician + "-technician",
                              staticClass: "col-sm-4"
                            },
                            [
                              _c("select2", {
                                ref: "technician_id",
                                attrs: {
                                  url: "/method/master/technicians/select",
                                  name: "technician_id",
                                  selecteditems: _vm.selecteditemsTechnician,
                                  identifier: "technician_id",
                                  placeholder: "Teknisi",
                                  required: _vm.form.type == 1 ? false : true,
                                  disabled:
                                    _vm.$route.params.id && _vm.form.status != 1
                                      ? true
                                      : false
                                }
                              }),
                              _vm._v(" "),
                              _c("has-error", {
                                attrs: {
                                  form: _vm.form,
                                  field: "technician_id"
                                }
                              })
                            ],
                            1
                          )
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "customer_name" }
                        },
                        [
                          _vm._v(
                            "\n                                Nama Pelanggan\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.customer_name,
                                expression: "form.customer_name"
                              }
                            ],
                            ref: "customer_name",
                            staticClass: "form-control",
                            class: {
                              "is-invalid": _vm.form.errors.has("customer_name")
                            },
                            attrs: {
                              type: "text",
                              name: "customer_name",
                              id: "customer_name",
                              placeholder: "Nama Pelanggan",
                              autocomplete: "off",
                              required: "",
                              readonly: _vm.$route.params.id ? true : false
                            },
                            domProps: { value: _vm.form.customer_name },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "customer_name",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "customer_name" }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "customer_phone" }
                        },
                        [
                          _vm._v(
                            "\n                                Nomor Telepon\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.customer_phone,
                                expression: "form.customer_phone"
                              }
                            ],
                            ref: "customer_phone",
                            staticClass: "form-control",
                            class: {
                              "is-invalid": _vm.form.errors.has(
                                "customer_phone"
                              )
                            },
                            attrs: {
                              type: "text",
                              name: "customer_phone",
                              id: "customer_phone",
                              placeholder: "Nomor Telepon",
                              autocomplete: "off",
                              required: "",
                              readonly: _vm.$route.params.id ? true : false
                            },
                            domProps: { value: _vm.form.customer_phone },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "customer_phone",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "customer_phone" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "item_name" }
                        },
                        [
                          _vm._v(
                            "\n                                Nama Barang\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-10" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.item_name,
                                expression: "form.item_name"
                              }
                            ],
                            ref: "item_name",
                            staticClass: "form-control",
                            class: {
                              "is-invalid": _vm.form.errors.has("item_name")
                            },
                            attrs: {
                              type: "text",
                              name: "item_name",
                              id: "item_name",
                              placeholder: "Nama Barang, Merek, Tipe",
                              autocomplete: "off",
                              required: "",
                              readonly: _vm.$route.params.id ? true : false
                            },
                            domProps: { value: _vm.form.item_name },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "item_name",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "item_name" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "complaint" }
                        },
                        [
                          _vm._v(
                            "\n                                Keluhan\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-10" },
                        [
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "complaint" }
                          }),
                          _vm._v(" "),
                          _c("textarea", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.complaint,
                                expression: "form.complaint"
                              }
                            ],
                            ref: "complaint",
                            staticClass: "form-control",
                            class: {
                              "is-invalid": _vm.form.errors.has("complaint")
                            },
                            attrs: {
                              name: "complaint",
                              rows: "3",
                              id: "complaint",
                              placeholder: "Keluhan",
                              autocomplete: "off",
                              required: "",
                              readonly: _vm.$route.params.id ? true : false
                            },
                            domProps: { value: _vm.form.complaint },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "complaint",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "complaint" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "form-group row",
                        class: _vm.form.type == 1 ? "d-none" : ""
                      },
                      [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-2 col-form-label",
                            attrs: { for: "equipment" }
                          },
                          [
                            _vm._v(
                              "\n                                Kelengkapan\n                            "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-10" },
                          [
                            _c("has-error", {
                              attrs: { form: _vm.form, field: "equipment" }
                            }),
                            _vm._v(" "),
                            _c("textarea", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.equipment,
                                  expression: "form.equipment"
                                }
                              ],
                              ref: "equipment",
                              staticClass: "form-control",
                              class: {
                                "is-invalid": _vm.form.errors.has("equipment")
                              },
                              attrs: {
                                name: "equipment",
                                rows: "3",
                                id: "equipment",
                                placeholder: "Kelengkapan",
                                autocomplete: "off",
                                required: "",
                                readonly: _vm.$route.params.id ? true : false
                              },
                              domProps: { value: _vm.form.equipment },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.form,
                                    "equipment",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: { form: _vm.form, field: "equipment" }
                            })
                          ],
                          1
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-sm-2 col-form-label",
                          attrs: { for: "information" }
                        },
                        [
                          _vm._v(
                            "\n                                Keterangan\n                            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-10" },
                        [
                          _c("textarea", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.information,
                                expression: "form.information"
                              }
                            ],
                            ref: "information",
                            staticClass: "form-control",
                            class: {
                              "is-invalid": _vm.form.errors.has("information")
                            },
                            attrs: {
                              name: "information",
                              rows: "3",
                              id: "information",
                              placeholder: "Keterangan",
                              autocomplete: "off"
                            },
                            domProps: { value: _vm.form.information },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "information",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "information" }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "div",
                        { staticClass: "col-sm-12 text-right" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "btn btn-secondary",
                              attrs: { to: "/transaction/services" }
                            },
                            [
                              _vm._v(
                                "\n                                Batal\n                              "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          !_vm.$route.params.id || _vm.form.status == 1
                            ? _c(
                                "button",
                                {
                                  staticClass: "btn btn-success",
                                  attrs: { type: "submit" }
                                },
                                [
                                  _vm._v(
                                    "\n                                  Simpan\n                              "
                                  )
                                ]
                              )
                            : _vm._e()
                        ],
                        1
                      )
                    ])
                  ],
                  1
                )
              ])
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "modal fade",
        attrs: {
          id: "modalPrint",
          role: "dialog",
          "aria-labelledby": "modalPrintLabel",
          "aria-hidden": "true"
        }
      },
      [
        _c(
          "div",
          {
            staticClass: "modal-dialog modal-dialog-centered",
            attrs: { role: "document" }
          },
          [
            _c("div", { staticClass: "modal-content" }, [
              _vm._m(3),
              _vm._v(" "),
              _c("div", { staticClass: "modal-body" }, [
                _c("div", { staticClass: "form-group row" }, [
                  _c("div", { staticClass: "col-sm-12 text-center" }, [
                    _c(
                      "a",
                      {
                        staticClass: "btn btn-primary",
                        attrs: {
                          href:
                            _vm.globalVar.publicPath + "transaction/services",
                          title: "Kembali"
                        }
                      },
                      [
                        _c("i", { staticClass: "fas fa-arrow-circle-left" }),
                        _vm._v(" Kembali\n                           ")
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "a",
                      {
                        staticClass: "btn btn-danger",
                        attrs: {
                          href:
                            _vm.globalVar.publicPath +
                            "method/transaction/services/print/" +
                            _vm.linkPrint.large,
                          title: "Cetak Nota Besar",
                          target: "_blank"
                        }
                      },
                      [
                        _c("i", { staticClass: "fas fa-print" }),
                        _vm._v(" Nota Besar\n                           ")
                      ]
                    )
                  ])
                ])
              ])
            ])
          ]
        )
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-sm-6" }, [
      _c("h1", { staticClass: "m-0 text-dark" }, [_vm._v("Servis")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "breadcrumb-item" }, [
      _c("a", { attrs: { href: "javascript:;" } }, [_vm._v("Transaksi")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header" }, [
      _c("h3", { staticClass: "card-title" }),
      _vm._v(" "),
      _c("div", { staticClass: "card-tools" }),
      _vm._v(" "),
      _c("br")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "h5",
        { staticClass: "modal-title", attrs: { id: "modalFormLabel" } },
        [_vm._v("\n                       Cetak Nota\n                   ")]
      ),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backoffice/transaction/ServiceForm.vue":
/*!************************************************************************!*\
  !*** ./resources/js/components/backoffice/transaction/ServiceForm.vue ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ServiceForm_vue_vue_type_template_id_4015e527___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ServiceForm.vue?vue&type=template&id=4015e527& */ "./resources/js/components/backoffice/transaction/ServiceForm.vue?vue&type=template&id=4015e527&");
/* harmony import */ var _ServiceForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ServiceForm.vue?vue&type=script&lang=js& */ "./resources/js/components/backoffice/transaction/ServiceForm.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ServiceForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ServiceForm_vue_vue_type_template_id_4015e527___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ServiceForm_vue_vue_type_template_id_4015e527___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backoffice/transaction/ServiceForm.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backoffice/transaction/ServiceForm.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/components/backoffice/transaction/ServiceForm.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ServiceForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ServiceForm.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/transaction/ServiceForm.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ServiceForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backoffice/transaction/ServiceForm.vue?vue&type=template&id=4015e527&":
/*!*******************************************************************************************************!*\
  !*** ./resources/js/components/backoffice/transaction/ServiceForm.vue?vue&type=template&id=4015e527& ***!
  \*******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ServiceForm_vue_vue_type_template_id_4015e527___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ServiceForm.vue?vue&type=template&id=4015e527& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/transaction/ServiceForm.vue?vue&type=template&id=4015e527&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ServiceForm_vue_vue_type_template_id_4015e527___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ServiceForm_vue_vue_type_template_id_4015e527___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);