(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/item"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/master/Item.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/master/Item.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {
    var _this = this;

    this.loadData();
    Fire.$on('AfterModified', function () {
      _this.loadData();
    });
  },
  mounted: function mounted() {},
  data: function data() {
    var sortOrders = {};
    var columns = [{
      width: '5%',
      label: 'No.',
      sortable: false
    }, {
      label: 'Kode',
      name: 'code',
      sortable: true
    }, {
      label: 'Nama',
      name: 'name',
      sortable: true
    }, {
      label: 'Kategori',
      name: 'category_id',
      sortable: false
    }, {
      label: 'Merek',
      name: 'brand_id',
      sortable: false
    }, {
      width: '15%',
      label: 'Action',
      sortable: false
    }];
    columns.forEach(function (column) {
      sortOrders[column.name] = -1;
    });
    return {
      globalVar: this.$attrs.globalVar,
      dataTables: [],
      columns: columns,
      sortKey: 'name',
      sortOrders: sortOrders,
      perPage: ['10', '25', '50', '100'],
      tableData: {
        draw: 0,
        length: 10,
        search: '',
        column: 2,
        dir: 'asc'
      },
      pagination: {
        lastPage: '',
        currentPage: '',
        total: '',
        lastPageUrl: '',
        nextPageUrl: '',
        prevPageUrl: '',
        from: '',
        to: ''
      },
      form: new Form({
        id: '',
        code: '',
        name: '',
        description: '',
        category_id: '',
        brand_id: '',
        purchasePrice: '',
        sellingPrice: ''
      }),
      selecteditemsCategory: [],
      selecteditemsBrand: []
    };
  },
  methods: {
    configPagination: function configPagination(data) {
      this.pagination.lastPage = data.last_page;
      this.pagination.currentPage = data.current_page;
      this.pagination.total = data.total;
      this.pagination.lastPageUrl = data.last_page_url;
      this.pagination.nextPageUrl = data.next_page_url;
      this.pagination.prevPageUrl = data.prev_page_url;
      this.pagination.from = data.from;
      this.pagination.to = data.to;
    },
    sortBy: function sortBy(key) {
      this.sortKey = key;
      this.sortOrders[key] = this.sortOrders[key] * -1;
      this.tableData.column = this.getIndex(this.columns, 'name', key);
      this.tableData.dir = this.sortOrders[key] === 1 ? 'asc' : 'desc';
      this.loadData();
    },
    getIndex: function getIndex(array, key, value) {
      return array.findIndex(function (i) {
        return i[key] == value;
      });
    },
    loadData: function loadData() {
      var _this2 = this;

      var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '/method/master/items';
      this.tableData.draw++;
      axios.get(url, {
        params: this.tableData
      }).then(function (response) {
        var data = response.data;

        if (_this2.tableData.draw == data.draw) {
          _this2.dataTables = data.data.data;

          _this2.configPagination(data.data);
        }
      })["catch"](function (errors) {});
    },
    openModal: function openModal() {
      var _this3 = this;

      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      this.form.clear();
      this.form.reset();
      this.selecteditemsCategory = [];
      this.selecteditemsBrand = [];
      $("#modalForm").modal("show");

      if (data) {
        this.form.fill(data);

        if (data.category) {
          this.selecteditemsCategory = [{
            id: data.category.id,
            name: data.category.name
          }];
        }

        if (data.brand) {
          this.selecteditemsBrand = [{
            id: data.brand.id,
            name: data.brand.name
          }];
        }

        if (data.purchaseprice) {
          this.form.purchasePrice = data.purchaseprice.price;
        }

        if (data.sellingprice) {
          this.form.sellingPrice = data.sellingprice.price;
        }
      }

      setTimeout(function () {
        _this3.$refs.code.focus();
      }, 400);
    },
    submitData: function submitData() {
      var _this4 = this;

      this.$Progress.start();
      this.form.category_id = $("#category_id").select2('data')[0].id;
      this.form.brand_id = $("#brand_id").select2('data')[0].id;

      if (this.form.id) {
        this.form.put('/method/master/items/' + this.form.id).then(function () {
          Toast.fire({
            type: 'success',
            title: 'Data updated successfully'
          });

          _this4.$Progress.finish();

          Fire.$emit('AfterModified');
          $("#modalForm").modal("hide");
        })["catch"](function () {
          Swal.fire('Alert!', 'Something went wrong.', 'warning');

          _this4.$Progress.fail();
        });
      } else {
        this.form.post('/method/master/items').then(function () {
          Toast.fire({
            type: 'success',
            title: 'Data saved successfully'
          });

          _this4.$Progress.finish();

          Fire.$emit('AfterModified');
          $("#modalForm").modal("hide");
        })["catch"](function () {
          Swal.fire('Alert!', 'Something went wrong.', 'warning');

          _this4.$Progress.fail();
        });
      }
    },
    deleteData: function deleteData(data) {
      Swal.fire({
        title: 'Are you sure?',
        text: 'Record will be deleted.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
      }).then(function (result) {
        if (result.value) {
          axios["delete"]('/method/master/items/' + data.id, {}).then(function (response) {
            Toast.fire({
              type: 'success',
              title: 'Data saved successfully'
            });
            Fire.$emit('AfterModified');
          })["catch"](function (error) {
            Swal.fire('Alert!', 'Something went wrong.', 'warning');
          });
        }
      });
    }
  },
  watch: {
    'form.purchasePrice': function formPurchasePrice(to, from) {
      var formatted = this.formatNumber(to);
      this.form.purchasePrice = formatted;
    },
    'form.sellingPrice': function formSellingPrice(to, from) {
      var formatted = this.formatNumber(to);
      this.form.sellingPrice = formatted;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/master/Item.vue?vue&type=template&id=788ebf1f&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/master/Item.vue?vue&type=template&id=788ebf1f& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "content-header" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row mb-2" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "col-sm-6" }, [
            _c("ol", { staticClass: "breadcrumb float-sm-right" }, [
              _c("li", { staticClass: "breadcrumb-item" }, [
                _c("a", { attrs: { href: "javascript:;" } }, [
                  _vm._v(_vm._s(_vm.globalVar.appName))
                ])
              ]),
              _vm._v(" "),
              _vm._m(1),
              _vm._v(" "),
              _c("li", { staticClass: "breadcrumb-item active" }, [
                _vm._v("Barang")
              ])
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("section", { staticClass: "content" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12" }, [
            _c("div", { staticClass: "card" }, [
              _c("div", { staticClass: "card-header" }, [
                _c("h3", { staticClass: "card-title" }),
                _vm._v(" "),
                _c("div", { staticClass: "card-tools" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-primary",
                      attrs: { type: "button" },
                      on: { click: _vm.openModal }
                    },
                    [
                      _c("i", { staticClass: "fas fa-plus-circle" }),
                      _vm._v("   Tambah Data\n                            ")
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("br")
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "card-body dataTables_wrapper scroll" },
                [
                  _c(
                    "div",
                    {
                      staticClass: "dataTables_length",
                      attrs: { id: "tableData_length" }
                    },
                    [
                      _c("label", [
                        _vm._v("Show\n                                "),
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.tableData.length,
                                expression: "tableData.length"
                              }
                            ],
                            on: {
                              change: [
                                function($event) {
                                  var $$selectedVal = Array.prototype.filter
                                    .call($event.target.options, function(o) {
                                      return o.selected
                                    })
                                    .map(function(o) {
                                      var val =
                                        "_value" in o ? o._value : o.value
                                      return val
                                    })
                                  _vm.$set(
                                    _vm.tableData,
                                    "length",
                                    $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  )
                                },
                                function($event) {
                                  return _vm.loadData()
                                }
                              ]
                            }
                          },
                          _vm._l(_vm.perPage, function(records, index) {
                            return _c(
                              "option",
                              { key: index, domProps: { value: records } },
                              [_vm._v(_vm._s(records))]
                            )
                          }),
                          0
                        ),
                        _vm._v("\n                                entries")
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "dataTables_filter",
                      attrs: { id: "tableData_filter" }
                    },
                    [
                      _c("label", [
                        _vm._v("Search:\n                                "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.tableData.search,
                              expression: "tableData.search"
                            }
                          ],
                          attrs: {
                            type: "text",
                            placeholder: "",
                            "aria-controls": "tableData"
                          },
                          domProps: { value: _vm.tableData.search },
                          on: {
                            input: [
                              function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.tableData,
                                  "search",
                                  $event.target.value
                                )
                              },
                              function($event) {
                                return _vm.loadData()
                              }
                            ]
                          }
                        })
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "datatable",
                    {
                      attrs: {
                        columns: _vm.columns,
                        sortKey: _vm.sortKey,
                        sortOrders: _vm.sortOrders
                      },
                      on: { sort: _vm.sortBy }
                    },
                    [
                      _vm.dataTables.length != 0
                        ? _c(
                            "tbody",
                            _vm._l(_vm.dataTables, function(dataTable, index) {
                              return _c("tr", { key: index }, [
                                _c("td", [_vm._v(_vm._s(index + 1))]),
                                _vm._v(" "),
                                _c("td", [_vm._v(_vm._s(dataTable.code))]),
                                _vm._v(" "),
                                _c("td", [_vm._v(_vm._s(dataTable.name))]),
                                _vm._v(" "),
                                _c("td", [
                                  _vm._v(_vm._s(dataTable.category.name))
                                ]),
                                _vm._v(" "),
                                _c("td", [
                                  _vm._v(_vm._s(dataTable.brand.name))
                                ]),
                                _vm._v(" "),
                                _c("td", { staticClass: "text-center" }, [
                                  _c(
                                    "button",
                                    {
                                      staticClass: "btn btn-primary btn-sm",
                                      attrs: {
                                        type: "button",
                                        title: "Ubah Data"
                                      },
                                      on: {
                                        click: function($event) {
                                          return _vm.openModal(dataTable)
                                        }
                                      }
                                    },
                                    [_c("i", { staticClass: "fas fa-edit" })]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "button",
                                    {
                                      staticClass: "btn btn-danger btn-sm",
                                      attrs: {
                                        type: "button",
                                        title: "Hapus Data"
                                      },
                                      on: {
                                        click: function($event) {
                                          return _vm.deleteData(dataTable)
                                        }
                                      }
                                    },
                                    [_c("i", { staticClass: "fas fa-ban" })]
                                  )
                                ])
                              ])
                            }),
                            0
                          )
                        : _c("tbody", [
                            _c("tr", [
                              _c(
                                "td",
                                {
                                  staticClass: "text-center",
                                  attrs: { colspan: "6" }
                                },
                                [
                                  _vm._v(
                                    "\n                                      Tidak ada data yang cocok ditemukan\n                                    "
                                  )
                                ]
                              )
                            ])
                          ])
                    ]
                  ),
                  _vm._v(" "),
                  _c("pagination", {
                    attrs: { pagination: _vm.pagination },
                    on: {
                      prev: function($event) {
                        return _vm.loadData(_vm.pagination.prevPageUrl)
                      },
                      next: function($event) {
                        return _vm.loadData(_vm.pagination.nextPageUrl)
                      }
                    }
                  })
                ],
                1
              )
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "modal fade bd-example-modal-lg",
        attrs: {
          id: "modalForm",
          role: "dialog",
          "aria-labelledby": "modalFormLabel",
          "aria-hidden": "true"
        }
      },
      [
        _c(
          "div",
          { staticClass: "modal-dialog modal-lg", attrs: { role: "document" } },
          [
            _c("div", { staticClass: "modal-content" }, [
              _vm._m(2),
              _vm._v(" "),
              _c(
                "form",
                {
                  attrs: { id: "formAdd" },
                  on: {
                    submit: function($event) {
                      $event.preventDefault()
                      return _vm.submitData($event)
                    }
                  }
                },
                [
                  _c(
                    "div",
                    { staticClass: "modal-body" },
                    [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.form.id,
                            expression: "form.id"
                          }
                        ],
                        staticClass: "d-none",
                        class: { "is-invalid": _vm.form.errors.has("id") },
                        attrs: { type: "text", name: "id", id: "id" },
                        domProps: { value: _vm.form.id },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.form, "id", $event.target.value)
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("has-error", {
                        attrs: { form: _vm.form, field: "id" }
                      }),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group row" }, [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-4 col-form-label",
                            attrs: { for: "category_id" }
                          },
                          [
                            _vm._v(
                              "\n                  Kategori\n                "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-8" },
                          [
                            _c("select2", {
                              attrs: {
                                url: "/method/master/categories/select",
                                name: "category_id",
                                selecteditems: _vm.selecteditemsCategory,
                                identifier: "category_id",
                                placeholder: "Kategori",
                                required: ""
                              },
                              model: {
                                value: _vm.form.category_id,
                                callback: function($$v) {
                                  _vm.$set(_vm.form, "category_id", $$v)
                                },
                                expression: "form.category_id"
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: { form: _vm.form, field: "category_id" }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group row" }, [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-4 col-form-label",
                            attrs: { for: "brand_id" }
                          },
                          [
                            _vm._v(
                              "\n                  Merek\n                "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-8" },
                          [
                            _c("select2", {
                              attrs: {
                                url: "/method/master/brands/select",
                                name: "brand_id",
                                selecteditems: _vm.selecteditemsBrand,
                                identifier: "brand_id",
                                placeholder: "Merek",
                                required: ""
                              },
                              model: {
                                value: _vm.form.brand_id,
                                callback: function($$v) {
                                  _vm.$set(_vm.form, "brand_id", $$v)
                                },
                                expression: "form.brand_id"
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: { form: _vm.form, field: "brand_id" }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group row" }, [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-4 col-form-label",
                            attrs: { for: "code" }
                          },
                          [
                            _vm._v(
                              "\n                    Kode\n                "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-8" },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.code,
                                  expression: "form.code"
                                }
                              ],
                              ref: "code",
                              staticClass: "form-control",
                              class: {
                                "is-invalid": _vm.form.errors.has("code")
                              },
                              attrs: {
                                type: "text",
                                name: "code",
                                id: "code",
                                placeholder: "Kode",
                                autocomplete: "off",
                                required: ""
                              },
                              domProps: { value: _vm.form.code },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.form,
                                    "code",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: { form: _vm.form, field: "code" }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group row" }, [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-4 col-form-label",
                            attrs: { for: "name" }
                          },
                          [
                            _vm._v(
                              "\n                    Nama\n                "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-8" },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.name,
                                  expression: "form.name"
                                }
                              ],
                              ref: "name",
                              staticClass: "form-control",
                              class: {
                                "is-invalid": _vm.form.errors.has("name")
                              },
                              attrs: {
                                type: "text",
                                name: "name",
                                id: "name",
                                placeholder: "Nama",
                                autocomplete: "off",
                                required: ""
                              },
                              domProps: { value: _vm.form.name },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.form,
                                    "name",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: { form: _vm.form, field: "name" }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group row" }, [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-4 col-form-label",
                            attrs: { for: "purchasePrice" }
                          },
                          [
                            _vm._v(
                              "\n                    Harga Beli\n                "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-8" },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.purchasePrice,
                                  expression: "form.purchasePrice"
                                }
                              ],
                              ref: "purchasePrice",
                              staticClass: "form-control",
                              class: {
                                "is-invalid": _vm.form.errors.has(
                                  "purchasePrice"
                                )
                              },
                              attrs: {
                                type: "text",
                                name: "purchasePrice",
                                id: "purchasePrice",
                                placeholder: "Harga Beli",
                                autocomplete: "off",
                                maxlength: "10",
                                required: ""
                              },
                              domProps: { value: _vm.form.purchasePrice },
                              on: {
                                keypress: function($event) {
                                  return _vm.isNumber($event)
                                },
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.form,
                                    "purchasePrice",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: { form: _vm.form, field: "purchasePrice" }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group row" }, [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-4 col-form-label",
                            attrs: { for: "sellingPrice" }
                          },
                          [
                            _vm._v(
                              "\n                    Harga Jual\n                "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-8" },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.sellingPrice,
                                  expression: "form.sellingPrice"
                                }
                              ],
                              ref: "sellingPrice",
                              staticClass: "form-control",
                              class: {
                                "is-invalid": _vm.form.errors.has(
                                  "sellingPrice"
                                )
                              },
                              attrs: {
                                type: "text",
                                name: "sellingPrice",
                                id: "sellingPrice",
                                placeholder: "Harga Beli",
                                autocomplete: "off",
                                maxlength: "10",
                                required: ""
                              },
                              domProps: { value: _vm.form.sellingPrice },
                              on: {
                                keypress: function($event) {
                                  return _vm.isNumber($event)
                                },
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.form,
                                    "sellingPrice",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: { form: _vm.form, field: "sellingPrice" }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group row" }, [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-4 col-form-label",
                            attrs: { for: "description" }
                          },
                          [
                            _vm._v(
                              "\n                    Deskripsi\n                "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-8" },
                          [
                            _c("textarea", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.description,
                                  expression: "form.description"
                                }
                              ],
                              ref: "description",
                              staticClass: "form-control",
                              class: {
                                "is-invalid": _vm.form.errors.has("description")
                              },
                              attrs: {
                                name: "description",
                                rows: "3",
                                id: "description",
                                placeholder: "Deskripsi",
                                autocomplete: "off"
                              },
                              domProps: { value: _vm.form.description },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.form,
                                    "description",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: { form: _vm.form, field: "description" }
                            })
                          ],
                          1
                        )
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _vm._m(3)
                ]
              )
            ])
          ]
        )
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-sm-6" }, [
      _c("h1", { staticClass: "m-0 text-dark" }, [_vm._v("Barang")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "breadcrumb-item" }, [
      _c("a", { attrs: { href: "javascript:;" } }, [_vm._v("Master")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "h5",
        { staticClass: "modal-title", attrs: { id: "modalFormLabel" } },
        [_vm._v("\n              Modal Form\n          ")]
      ),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-footer text-right" }, [
      _c(
        "button",
        {
          staticClass: "btn btn-secondary",
          attrs: { type: "button", "data-dismiss": "modal" }
        },
        [_vm._v("\n                  Batal\n              ")]
      ),
      _vm._v(" "),
      _c(
        "button",
        { staticClass: "btn btn-success", attrs: { type: "submit" } },
        [_vm._v("\n                  Simpan\n              ")]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backoffice/master/Item.vue":
/*!************************************************************!*\
  !*** ./resources/js/components/backoffice/master/Item.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Item_vue_vue_type_template_id_788ebf1f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Item.vue?vue&type=template&id=788ebf1f& */ "./resources/js/components/backoffice/master/Item.vue?vue&type=template&id=788ebf1f&");
/* harmony import */ var _Item_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Item.vue?vue&type=script&lang=js& */ "./resources/js/components/backoffice/master/Item.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Item_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Item_vue_vue_type_template_id_788ebf1f___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Item_vue_vue_type_template_id_788ebf1f___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backoffice/master/Item.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backoffice/master/Item.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/backoffice/master/Item.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Item_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Item.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/master/Item.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Item_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backoffice/master/Item.vue?vue&type=template&id=788ebf1f&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/backoffice/master/Item.vue?vue&type=template&id=788ebf1f& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Item_vue_vue_type_template_id_788ebf1f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Item.vue?vue&type=template&id=788ebf1f& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/master/Item.vue?vue&type=template&id=788ebf1f&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Item_vue_vue_type_template_id_788ebf1f___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Item_vue_vue_type_template_id_788ebf1f___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);