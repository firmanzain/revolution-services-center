(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/dashboard"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/Dashboard.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/Dashboard.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vuejs_datepicker__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuejs-datepicker */ "./node_modules/vuejs-datepicker/dist/vuejs-datepicker.esm.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


var date = new Date();
/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {
    var _this = this;

    if (this.globalVar.session) {
      if (this.globalVar.session.outlet) {
        if (this.globalVar.session.outlet.length > 0) {
          for (var i = 0; i < this.globalVar.session.outlet.length; i++) {
            if (this.globalVar.session.outlet[i].outlet.id != 1) {
              this.outlet = this.globalVar.session.outlet[i].outlet.id;
              this.selecteditemsOutlet = [{
                id: this.globalVar.session.outlet[i].outlet.id,
                name: this.globalVar.session.outlet[i].outlet.name
              }];
              break;
            }
          }
        }
      }
    }

    this.loadData();
    Fire.$on('AfterModified', function () {
      _this.loadData();
    });
  },
  mounted: function mounted() {},
  components: {
    Datepicker: vuejs_datepicker__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      globalVar: this.$attrs.globalVar,
      renderingCount: 0,
      state: {
        firstDate: new Date(new Date(date.getFullYear(), date.getMonth(), 1)),
        lastDate: new Date(new Date(date.getFullYear(), date.getMonth() + 1, 0))
      },
      outlet: 0,
      count: {
        stock: 0,
        customer: 0,
        sale_item: 0,
        sale_service: 0,
        sale_nominal: 0,
        sale_service_nominal: 0,
        service_progress: 0,
        service_done: 0,
        service_close: 0,
        item: 0,
        supplier: 0,
        purchase: 0,
        purchase_nominal: 0
      },
      selecteditemsOutlet: []
    };
  },
  methods: {
    loadData: function () {
      var _loadData = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var self, firstDate, lastDate, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                self = this;
                firstDate = moment__WEBPACK_IMPORTED_MODULE_1___default()(self.state.firstDate);
                lastDate = moment__WEBPACK_IMPORTED_MODULE_1___default()(self.state.lastDate);
                self.count.stock = 0;
                self.count.customer = 0;
                self.count.sale_item = 0;
                self.count.sale_service = 0;
                self.count.sale_nominal = 0;
                self.count.sale_service_nominal = 0;
                self.count.service_progress = 0;
                self.count.service_done = 0;
                self.count.service_close = 0;
                self.count.item = 0;
                self.count.supplier = 0;
                self.count.purchase = 0;
                self.count.purchase_nominal = 0;
                _context.prev = 16;
                _context.next = 19;
                return axios.get("".concat(self.globalVar.publicPath, "method/dashboard"), {
                  params: {
                    outlet: self.outlet,
                    firstDate: firstDate.format('YYYY-MM-DD'),
                    lastDate: lastDate.format('YYYY-MM-DD')
                  }
                });

              case 19:
                response = _context.sent;

                if (response.data) {
                  self.count.stock = response.data.stock;
                  self.count.customer = response.data.customer;
                  self.count.sale_item = response.data.sale_item;
                  self.count.sale_service = response.data.sale_service;
                  self.count.sale_nominal = response.data.sale_nominal;
                  self.count.sale_service_nominal = response.data.sale_service_nominal;
                  self.count.service_progress = response.data.service_progress;
                  self.count.service_done = response.data.service_done;
                  self.count.service_close = response.data.service_close;
                  self.count.item = response.data.item;
                  self.count.supplier = response.data.supplier;
                  self.count.purchase = response.data.purchase;
                  self.count.purchase_nominal = response.data.purchase_nominal;
                }

                _context.next = 25;
                break;

              case 23:
                _context.prev = 23;
                _context.t0 = _context["catch"](16);

              case 25:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[16, 23]]);
      }));

      function loadData() {
        return _loadData.apply(this, arguments);
      }

      return loadData;
    }(),
    checkSelected: function checkSelected() {
      this.outlet = $("#outlet_id").select2('data')[0].id;
      Fire.$emit('AfterModified');
    }
  },
  watch: {
    'state.firstDate': function stateFirstDate(to, from) {
      var firstDate = moment__WEBPACK_IMPORTED_MODULE_1___default()(this.state.firstDate);
      var lastDate = moment__WEBPACK_IMPORTED_MODULE_1___default()(this.state.lastDate);

      if (moment__WEBPACK_IMPORTED_MODULE_1___default()(firstDate).isAfter(lastDate)) {
        this.state.firstDate = this.formatDateDefault(lastDate);
      }

      Fire.$emit('AfterModified');
    },
    'state.lastDate': function stateLastDate(to, from) {
      var firstDate = moment__WEBPACK_IMPORTED_MODULE_1___default()(this.state.firstDate);
      var lastDate = moment__WEBPACK_IMPORTED_MODULE_1___default()(this.state.lastDate);

      if (moment__WEBPACK_IMPORTED_MODULE_1___default()(lastDate).isBefore(firstDate)) {
        this.state.lastDate = this.formatDateDefault(firstDate);
      }

      Fire.$emit('AfterModified');
    }
  },
  activated: function activated() {
    var self = this;

    if (self.globalVar.session) {
      if (self.globalVar.session.outlet) {
        if (self.globalVar.session.outlet.length > 0) {
          for (var i = 0; i < self.globalVar.session.outlet.length; i++) {
            if (self.globalVar.session.outlet[i].outlet.id != 1) {
              self.outlet = self.globalVar.session.outlet[i].outlet.id;
              self.selecteditemsOutlet = [{
                id: self.globalVar.session.outlet[i].outlet.id,
                name: self.globalVar.session.outlet[i].outlet.name
              }];
              break;
            }
          }
        }
      }
    }

    self.renderingCount++;
    setTimeout(function () {
      self.loadData();
    }, 400);
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/Dashboard.vue?vue&type=template&id=65355ada&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/Dashboard.vue?vue&type=template&id=65355ada& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "content-header" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row mb-2" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "col-sm-6" }, [
            _c("ol", { staticClass: "breadcrumb float-sm-right" }, [
              _c("li", { staticClass: "breadcrumb-item" }, [
                _c("a", { attrs: { href: "javascript:;" } }, [
                  _vm._v(_vm._s(_vm.globalVar.appName))
                ])
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "breadcrumb-item active" }, [
                _vm._v("Dashboard")
              ])
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("section", { staticClass: "content" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row mb-4" }, [
          _c("div", { staticClass: "col-12 col-sm-6 col-md-6" }, [
            _vm.globalVar.session
              ? _c("div", {}, [
                  _vm.globalVar.session.outlet
                    ? _c("div", {}, [
                        _vm.globalVar.session.outlet.length > 1
                          ? _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "div",
                                {
                                  key: _vm.renderingCount + "-outlet",
                                  staticClass: "col-sm-4"
                                },
                                [
                                  _c("select2", {
                                    ref: "outlet_id",
                                    attrs: {
                                      url: "/method/master/outlets/select",
                                      name: "outlet_id",
                                      selecteditems: _vm.selecteditemsOutlet,
                                      identifier: "outlet_id",
                                      placeholder: "Toko/Gudang",
                                      required: ""
                                    },
                                    on: { changed: _vm.checkSelected }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "col-sm-8" }, [
                                _c("div", { staticClass: "input-group" }, [
                                  _c(
                                    "div",
                                    { staticClass: "input-group-prepend" },
                                    [
                                      _c("datepicker", {
                                        ref: "firstDate",
                                        attrs: {
                                          "input-class": "form-control",
                                          name: "firstDate",
                                          id: "firstDate",
                                          placeholder: "Tanggal",
                                          autocomplete: "off",
                                          format: _vm.formatDateDefault
                                        },
                                        model: {
                                          value: _vm.state.firstDate,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.state,
                                              "firstDate",
                                              $$v
                                            )
                                          },
                                          expression: "state.firstDate"
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c(
                                        "span",
                                        { staticClass: "input-group-text" },
                                        [_vm._v("s/d")]
                                      ),
                                      _vm._v(" "),
                                      _c("datepicker", {
                                        ref: "lastDate",
                                        attrs: {
                                          "input-class": "form-control",
                                          name: "lastDate",
                                          id: "lastDate",
                                          placeholder: "Tanggal",
                                          autocomplete: "off",
                                          format: _vm.formatDateDefault
                                        },
                                        model: {
                                          value: _vm.state.lastDate,
                                          callback: function($$v) {
                                            _vm.$set(_vm.state, "lastDate", $$v)
                                          },
                                          expression: "state.lastDate"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                ])
                              ])
                            ])
                          : _c("div", { staticClass: "form-group row" }, [
                              _c("div", { staticClass: "col-sm-8" }, [
                                _c("div", { staticClass: "input-group" }, [
                                  _c(
                                    "div",
                                    { staticClass: "input-group-prepend" },
                                    [
                                      _c("datepicker", {
                                        ref: "firstDate",
                                        attrs: {
                                          "input-class": "form-control",
                                          name: "firstDate",
                                          id: "firstDate",
                                          placeholder: "Tanggal",
                                          autocomplete: "off",
                                          format: _vm.formatDateDefault
                                        },
                                        model: {
                                          value: _vm.state.firstDate,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.state,
                                              "firstDate",
                                              $$v
                                            )
                                          },
                                          expression: "state.firstDate"
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c(
                                        "span",
                                        { staticClass: "input-group-text" },
                                        [_vm._v("s/d")]
                                      ),
                                      _vm._v(" "),
                                      _c("datepicker", {
                                        ref: "lastDate",
                                        attrs: {
                                          "input-class": "form-control",
                                          name: "lastDate",
                                          id: "lastDate",
                                          placeholder: "Tanggal",
                                          autocomplete: "off",
                                          format: _vm.formatDateDefault
                                        },
                                        model: {
                                          value: _vm.state.lastDate,
                                          callback: function($$v) {
                                            _vm.$set(_vm.state, "lastDate", $$v)
                                          },
                                          expression: "state.lastDate"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                ])
                              ])
                            ])
                      ])
                    : _vm._e()
                ])
              : _c("div", {}, [
                  _c("div", {}, [
                    _c("div", { staticClass: "form-group row" }, [
                      _c("div", { staticClass: "col-sm-8" }, [
                        _c("div", { staticClass: "input-group" }, [
                          _c(
                            "div",
                            { staticClass: "input-group-prepend" },
                            [
                              _c("datepicker", {
                                ref: "firstDate",
                                attrs: {
                                  "input-class": "form-control",
                                  name: "firstDate",
                                  id: "firstDate",
                                  placeholder: "Tanggal",
                                  autocomplete: "off",
                                  format: _vm.formatDateDefault
                                },
                                model: {
                                  value: _vm.state.firstDate,
                                  callback: function($$v) {
                                    _vm.$set(_vm.state, "firstDate", $$v)
                                  },
                                  expression: "state.firstDate"
                                }
                              }),
                              _vm._v(" "),
                              _c("span", { staticClass: "input-group-text" }, [
                                _vm._v("s/d")
                              ]),
                              _vm._v(" "),
                              _c("datepicker", {
                                ref: "lastDate",
                                attrs: {
                                  "input-class": "form-control",
                                  name: "lastDate",
                                  id: "lastDate",
                                  placeholder: "Tanggal",
                                  autocomplete: "off",
                                  format: _vm.formatDateDefault
                                },
                                model: {
                                  value: _vm.state.lastDate,
                                  callback: function($$v) {
                                    _vm.$set(_vm.state, "lastDate", $$v)
                                  },
                                  expression: "state.lastDate"
                                }
                              })
                            ],
                            1
                          )
                        ])
                      ])
                    ])
                  ])
                ])
          ])
        ]),
        _vm._v(" "),
        _vm._m(1),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12 col-sm-6 col-md-3" }, [
            _c("div", { staticClass: "info-box" }, [
              _c("div", { staticClass: "info-box-content" }, [
                _c("span", { staticClass: "info-box-text text-uppercase" }, [
                  _vm._v("Barang")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "info-box-number" }, [
                  _vm._v(
                    "\n                  " +
                      _vm._s(_vm._f("formatNumber")(_vm.count.item)) +
                      "\n              "
                  )
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-12 col-sm-6 col-md-3" }, [
            _c("div", { staticClass: "info-box mb-3" }, [
              _c("div", { staticClass: "info-box-content" }, [
                _c("span", { staticClass: "info-box-text text-uppercase" }, [
                  _vm._v("Stok")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "info-box-number" }, [
                  _vm._v(
                    "\n                  " +
                      _vm._s(_vm._f("formatNumber")(_vm.count.stock)) +
                      "\n              "
                  )
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "clearfix hidden-md-up" }),
          _vm._v(" "),
          _c("div", { staticClass: "col-12 col-sm-6 col-md-3" }, [
            _c("div", { staticClass: "info-box mb-3" }, [
              _c("div", { staticClass: "info-box-content" }, [
                _c("span", { staticClass: "info-box-text text-uppercase" }, [
                  _vm._v("Pemasok")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "info-box-number" }, [
                  _vm._v(
                    "\n                  " +
                      _vm._s(_vm._f("formatNumber")(_vm.count.supplier)) +
                      "\n              "
                  )
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-12 col-sm-6 col-md-3" }, [
            _c("div", { staticClass: "info-box mb-3" }, [
              _c("div", { staticClass: "info-box-content" }, [
                _c("span", { staticClass: "info-box-text text-uppercase" }, [
                  _vm._v("Pelanggan")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "info-box-number" }, [
                  _vm._v(
                    "\n                  " +
                      _vm._s(_vm._f("formatNumber")(_vm.count.customer)) +
                      "\n              "
                  )
                ])
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12 col-sm-6 col-md-6" }, [
            _c("div", { staticClass: "info-box" }, [
              _c("div", { staticClass: "info-box-content" }, [
                _c("span", { staticClass: "info-box-text text-uppercase" }, [
                  _vm._v("Pembelian")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "info-box-number" }, [
                  _vm._v(
                    "\n                  " +
                      _vm._s(_vm._f("formatNumber")(_vm.count.purchase)) +
                      "\n              "
                  )
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-12 col-sm-6 col-md-6" }, [
            _c("div", { staticClass: "info-box mb-3" }, [
              _c("div", { staticClass: "info-box-content" }, [
                _c("span", { staticClass: "info-box-text text-uppercase" }, [
                  _vm._v("Nominal Pembelian")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "info-box-number" }, [
                  _vm._v(
                    "\n                  Rp. " +
                      _vm._s(
                        _vm._f("formatNumber")(_vm.count.purchase_nominal)
                      ) +
                      "\n              "
                  )
                ])
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12 col-sm-4 col-md-4" }, [
            _c("div", { staticClass: "info-box mb-3" }, [
              _c("div", { staticClass: "info-box-content" }, [
                _c("span", { staticClass: "info-box-text text-uppercase" }, [
                  _vm._v("Servis Dalam Proses")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "info-box-number" }, [
                  _vm._v(
                    "\n                  " +
                      _vm._s(
                        _vm._f("formatNumber")(_vm.count.service_progress)
                      ) +
                      "\n              "
                  )
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-12 col-sm-4 col-md-4" }, [
            _c("div", { staticClass: "info-box mb-3" }, [
              _c("div", { staticClass: "info-box-content" }, [
                _c("span", { staticClass: "info-box-text text-uppercase" }, [
                  _vm._v("Servis Selesai Belum Diambil")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "info-box-number" }, [
                  _vm._v(
                    "\n                  " +
                      _vm._s(_vm._f("formatNumber")(_vm.count.service_done)) +
                      "\n              "
                  )
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-12 col-sm-4 col-md-4" }, [
            _c("div", { staticClass: "info-box mb-3" }, [
              _c("div", { staticClass: "info-box-content" }, [
                _c("span", { staticClass: "info-box-text text-uppercase" }, [
                  _vm._v("Servis Sudah Diambil")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "info-box-number" }, [
                  _vm._v(
                    "\n                  " +
                      _vm._s(_vm._f("formatNumber")(_vm.count.service_close)) +
                      "\n              "
                  )
                ])
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12 col-sm-6 col-md-3" }, [
            _c("div", { staticClass: "info-box mb-3" }, [
              _c("div", { staticClass: "info-box-content" }, [
                _c("span", { staticClass: "info-box-text text-uppercase" }, [
                  _vm._v("Penjualan Barang")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "info-box-number" }, [
                  _vm._v(
                    "\n                  " +
                      _vm._s(_vm._f("formatNumber")(_vm.count.sale_item)) +
                      "\n              "
                  )
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-12 col-sm-6 col-md-3" }, [
            _c("div", { staticClass: "info-box mb-3" }, [
              _c("div", { staticClass: "info-box-content" }, [
                _c("span", { staticClass: "info-box-text text-uppercase" }, [
                  _vm._v("Penjualan Jasa")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "info-box-number" }, [
                  _vm._v(
                    "\n                  " +
                      _vm._s(_vm._f("formatNumber")(_vm.count.sale_service)) +
                      "\n              "
                  )
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "clearfix hidden-md-up" }),
          _vm._v(" "),
          _c("div", { staticClass: "col-12 col-sm-6 col-md-3" }, [
            _c("div", { staticClass: "info-box mb-3" }, [
              _c("div", { staticClass: "info-box-content" }, [
                _c("span", { staticClass: "info-box-text text-uppercase" }, [
                  _vm._v("Nominal Penjualan")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "info-box-number" }, [
                  _vm._v(
                    "\n                  Rp. " +
                      _vm._s(_vm._f("formatNumber")(_vm.count.sale_nominal)) +
                      "\n              "
                  )
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-12 col-sm-6 col-md-3" }, [
            _c("div", { staticClass: "info-box mb-3" }, [
              _c("div", { staticClass: "info-box-content" }, [
                _c("span", { staticClass: "info-box-text text-uppercase" }, [
                  _vm._v("Biaya Service")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "info-box-number" }, [
                  _vm._v(
                    "\n                  Rp. " +
                      _vm._s(
                        _vm._f("formatNumber")(_vm.count.sale_service_nominal)
                      ) +
                      "\n              "
                  )
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-sm-6" }, [
      _c("h1", { staticClass: "m-0 text-dark" }, [_vm._v("Dashboard")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row mb-4" }, [
      _c("div", { staticClass: "col-sm-12" }, [
        _c("h5", { staticClass: "m-0 text-dark text-uppercase" }, [
          _vm._v("Ringkasan")
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backoffice/Dashboard.vue":
/*!**********************************************************!*\
  !*** ./resources/js/components/backoffice/Dashboard.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Dashboard_vue_vue_type_template_id_65355ada___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=template&id=65355ada& */ "./resources/js/components/backoffice/Dashboard.vue?vue&type=template&id=65355ada&");
/* harmony import */ var _Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=script&lang=js& */ "./resources/js/components/backoffice/Dashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Dashboard_vue_vue_type_template_id_65355ada___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Dashboard_vue_vue_type_template_id_65355ada___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backoffice/Dashboard.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backoffice/Dashboard.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/backoffice/Dashboard.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/Dashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backoffice/Dashboard.vue?vue&type=template&id=65355ada&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/backoffice/Dashboard.vue?vue&type=template&id=65355ada& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_65355ada___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=template&id=65355ada& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/Dashboard.vue?vue&type=template&id=65355ada&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_65355ada___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_65355ada___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);