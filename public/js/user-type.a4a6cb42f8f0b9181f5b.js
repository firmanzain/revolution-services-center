(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/user-type"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/access/UserType.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/access/UserType.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {
    var _this = this;

    this.loadData();
    this.loadDataMenu();
    Fire.$on('AfterModified', function () {
      _this.loadData();
    });
  },
  mounted: function mounted() {},
  filters: {},
  data: function data() {
    var sortOrders = {};
    var columns = [{
      width: '5%',
      label: 'No.',
      sortable: false
    }, {
      label: 'Nama',
      name: 'name',
      sortable: true
    }, {
      width: '15%',
      label: 'Action',
      sortable: false
    }];
    columns.forEach(function (column) {
      sortOrders[column.name] = -1;
    });
    return {
      globalVar: this.$attrs.globalVar,
      dataTables: [],
      columns: columns,
      sortKey: 'name',
      sortOrders: sortOrders,
      perPage: ['10', '25', '50', '100'],
      tableData: {
        draw: 0,
        length: 10,
        search: '',
        column: 1,
        dir: 'asc'
      },
      pagination: {
        lastPage: '',
        currentPage: '',
        total: '',
        lastPageUrl: '',
        nextPageUrl: '',
        prevPageUrl: '',
        from: '',
        to: ''
      },
      form: new Form({
        id: '',
        name: ''
      }),
      privilege: []
    };
  },
  methods: {
    configPagination: function configPagination(data) {
      this.pagination.lastPage = data.last_page;
      this.pagination.currentPage = data.current_page;
      this.pagination.total = data.total;
      this.pagination.lastPageUrl = data.last_page_url;
      this.pagination.nextPageUrl = data.next_page_url;
      this.pagination.prevPageUrl = data.prev_page_url;
      this.pagination.from = data.from;
      this.pagination.to = data.to;
    },
    sortBy: function sortBy(key) {
      this.sortKey = key; // this.sortOrders[key] = this.sortOrders[key] * -1;

      if (this.tableData.column == this.getIndex(this.columns, 'name', key)) {
        if (this.tableData.dir == 'asc') {
          this.tableData.dir = 'desc';
        } else {
          this.tableData.dir = 'asc';
        }
      } else {
        this.tableData.column = this.getIndex(this.columns, 'name', key);
        this.tableData.dir = this.sortOrders[key] === 1 ? 'asc' : 'desc';
      }

      if (this.tableData.dir == 'asc') {
        this.sortOrders[key] = 1;
      } else {
        this.sortOrders[key] = -1;
      }

      this.loadData();
    },
    getIndex: function getIndex(array, key, value) {
      return array.findIndex(function (i) {
        return i[key] == value;
      });
    },
    loadData: function loadData() {
      var _this2 = this;

      var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '/method/access/user-types';
      this.tableData.draw++;
      axios.get(url, {
        params: this.tableData
      }).then(function (response) {
        var data = response.data;

        if (_this2.tableData.draw == data.draw) {
          _this2.dataTables = data.data.data;

          _this2.configPagination(data.data);
        }
      })["catch"](function (errors) {});
    },
    openModal: function openModal() {
      var _this3 = this;

      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      this.form.clear();
      this.form.reset();
      $("#modalForm").modal("show");

      if (data) {
        this.form.fill(data);
      }

      setTimeout(function () {
        _this3.$refs.name.focus();
      }, 400);
    },
    loadDataMenu: function loadDataMenu() {
      var self = this;
      var menuTemp = [];
      axios.get('/method/access/menus', {}).then(function (response) {
        for (var i = 0; i < response.data.data.length; i++) {
          menuTemp = {
            menuId: response.data.data[i].id,
            name: response.data.data[i].name,
            parent: response.data.data[i].parent,
            user_type_id: 0,
            checked: 0
          };
          self.privilege.push(menuTemp);
        }
      })["catch"](function (error) {});
    },
    openModalPrivilege: function openModalPrivilege() {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

      for (var j = 0; j < this.privilege.length; j++) {
        this.privilege[j].user_type_id = data.id;
        this.privilege[j].checked = 0;
      }

      $("#modalFormPrivilege").modal("show");

      if (data) {
        for (var i = 0; i < data.userprivilege.length; i++) {
          for (var j = 0; j < this.privilege.length; j++) {
            this.privilege[j].user_type_id = data.id;

            if (this.privilege[j].menuId == data.userprivilege[i].menu_id) {
              this.privilege[j].checked = data.userprivilege[i].create;
            }
          }
        }
      }
    },
    submitData: function submitData() {
      var _this4 = this;

      this.$Progress.start();

      if (this.form.id) {
        this.form.put('/method/access/user-types/' + this.form.id).then(function () {
          Toast.fire({
            type: 'success',
            title: 'Data updated successfully'
          });

          _this4.$Progress.finish();

          Fire.$emit('AfterModified');
          $("#modalForm").modal("hide");
        })["catch"](function () {
          Swal.fire('Alert!', 'Something went wrong.', 'warning');

          _this4.$Progress.fail();
        });
      } else {
        this.form.post('/method/access/user-types').then(function () {
          Toast.fire({
            type: 'success',
            title: 'Data saved successfully'
          });

          _this4.$Progress.finish();

          Fire.$emit('AfterModified');
          $("#modalForm").modal("hide");
        })["catch"](function () {
          Swal.fire('Alert!', 'Something went wrong.', 'warning');

          _this4.$Progress.fail();
        });
      }
    },
    deleteData: function deleteData(data) {
      Swal.fire({
        title: 'Are you sure?',
        text: 'Record will be deleted.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
      }).then(function (result) {
        if (result.value) {
          axios["delete"]('/method/access/user-types/' + data.id, {}).then(function (response) {
            Toast.fire({
              type: 'success',
              title: 'Data saved successfully'
            });
            Fire.$emit('AfterModified');
          })["catch"](function (error) {
            Swal.fire('Alert!', 'Something went wrong.', 'warning');
          });
        }
      });
    },
    submitDataPriv: function submitDataPriv() {
      var self = this;
      self.$Progress.start();
      axios.post('/method/access/user-privileges', {
        'privilege': self.privilege
      }).then(function (response) {
        Toast.fire({
          type: 'success',
          title: 'Data saved successfully'
        });
        self.$Progress.finish();
        Fire.$emit('AfterModified');
        $("#modalFormPrivilege").modal("hide");
      })["catch"](function (error) {
        Swal.fire('Alert!', 'Something went wrong.', 'warning');
        self.$Progress.fail();
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/access/UserType.vue?vue&type=template&id=241a0aef&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/access/UserType.vue?vue&type=template&id=241a0aef& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "content-header" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row mb-2" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "col-sm-6" }, [
            _c("ol", { staticClass: "breadcrumb float-sm-right" }, [
              _c("li", { staticClass: "breadcrumb-item" }, [
                _c("a", { attrs: { href: "javascript:;" } }, [
                  _vm._v(_vm._s(_vm.globalVar.appName))
                ])
              ]),
              _vm._v(" "),
              _vm._m(1),
              _vm._v(" "),
              _c("li", { staticClass: "breadcrumb-item active" }, [
                _vm._v("Tipe Pengguna")
              ])
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("section", { staticClass: "content" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12" }, [
            _c("div", { staticClass: "card" }, [
              _c("div", { staticClass: "card-header" }, [
                _c("h3", { staticClass: "card-title" }),
                _vm._v(" "),
                _c("div", { staticClass: "card-tools" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-primary",
                      attrs: { type: "button" },
                      on: { click: _vm.openModal }
                    },
                    [
                      _c("i", { staticClass: "fas fa-plus-circle" }),
                      _vm._v("   Tambah Data\n                            ")
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("br")
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "card-body dataTables_wrapper scroll" },
                [
                  _c(
                    "div",
                    {
                      staticClass: "dataTables_length",
                      attrs: { id: "tableData_length" }
                    },
                    [
                      _c("label", [
                        _vm._v("Show\n                                "),
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.tableData.length,
                                expression: "tableData.length"
                              }
                            ],
                            on: {
                              change: [
                                function($event) {
                                  var $$selectedVal = Array.prototype.filter
                                    .call($event.target.options, function(o) {
                                      return o.selected
                                    })
                                    .map(function(o) {
                                      var val =
                                        "_value" in o ? o._value : o.value
                                      return val
                                    })
                                  _vm.$set(
                                    _vm.tableData,
                                    "length",
                                    $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  )
                                },
                                function($event) {
                                  return _vm.loadData()
                                }
                              ]
                            }
                          },
                          _vm._l(_vm.perPage, function(records, index) {
                            return _c(
                              "option",
                              { key: index, domProps: { value: records } },
                              [_vm._v(_vm._s(records))]
                            )
                          }),
                          0
                        ),
                        _vm._v("\n                                entries")
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "dataTables_filter",
                      attrs: { id: "tableData_filter" }
                    },
                    [
                      _c("label", [
                        _vm._v("Search:\n                                "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.tableData.search,
                              expression: "tableData.search"
                            }
                          ],
                          attrs: {
                            type: "text",
                            placeholder: "",
                            "aria-controls": "tableData"
                          },
                          domProps: { value: _vm.tableData.search },
                          on: {
                            input: [
                              function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.tableData,
                                  "search",
                                  $event.target.value
                                )
                              },
                              function($event) {
                                return _vm.loadData()
                              }
                            ]
                          }
                        })
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "datatable",
                    {
                      attrs: {
                        columns: _vm.columns,
                        sortKey: _vm.sortKey,
                        sortOrders: _vm.sortOrders
                      },
                      on: { sort: _vm.sortBy }
                    },
                    [
                      _vm.dataTables.length != 0
                        ? _c(
                            "tbody",
                            _vm._l(_vm.dataTables, function(dataTable, index) {
                              return _c("tr", { key: index }, [
                                _c("td", [_vm._v(_vm._s(index + 1))]),
                                _vm._v(" "),
                                _c("td", [_vm._v(_vm._s(dataTable.name))]),
                                _vm._v(" "),
                                _c("td", { staticClass: "text-center" }, [
                                  _c(
                                    "button",
                                    {
                                      staticClass: "btn btn-primary btn-sm",
                                      attrs: {
                                        type: "button",
                                        title: "Ubah Data"
                                      },
                                      on: {
                                        click: function($event) {
                                          return _vm.openModal(dataTable)
                                        }
                                      }
                                    },
                                    [_c("i", { staticClass: "fas fa-edit" })]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "button",
                                    {
                                      staticClass: "btn btn-primary btn-sm",
                                      attrs: {
                                        type: "button",
                                        title: "Ubah Akses"
                                      },
                                      on: {
                                        click: function($event) {
                                          return _vm.openModalPrivilege(
                                            dataTable
                                          )
                                        }
                                      }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "fas fa-user-secret"
                                      })
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "button",
                                    {
                                      staticClass: "btn btn-danger btn-sm",
                                      attrs: {
                                        type: "button",
                                        title: "Hapus Data"
                                      },
                                      on: {
                                        click: function($event) {
                                          return _vm.deleteData(dataTable)
                                        }
                                      }
                                    },
                                    [_c("i", { staticClass: "fas fa-ban" })]
                                  )
                                ])
                              ])
                            }),
                            0
                          )
                        : _c("tbody", [
                            _c("tr", [
                              _c(
                                "td",
                                {
                                  staticClass: "text-center",
                                  attrs: { colspan: "3" }
                                },
                                [
                                  _vm._v(
                                    "\n                                      Tidak ada data yang cocok ditemukan\n                                    "
                                  )
                                ]
                              )
                            ])
                          ])
                    ]
                  ),
                  _vm._v(" "),
                  _c("pagination", {
                    attrs: { pagination: _vm.pagination },
                    on: {
                      prev: function($event) {
                        return _vm.loadData(_vm.pagination.prevPageUrl)
                      },
                      next: function($event) {
                        return _vm.loadData(_vm.pagination.nextPageUrl)
                      }
                    }
                  })
                ],
                1
              )
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "modal fade bd-example-modal-lg",
        attrs: {
          id: "modalForm",
          role: "dialog",
          "aria-labelledby": "modalFormLabel",
          "aria-hidden": "true"
        }
      },
      [
        _c(
          "div",
          { staticClass: "modal-dialog modal-lg", attrs: { role: "document" } },
          [
            _c("div", { staticClass: "modal-content" }, [
              _vm._m(2),
              _vm._v(" "),
              _c(
                "form",
                {
                  attrs: { id: "formAdd" },
                  on: {
                    submit: function($event) {
                      $event.preventDefault()
                      return _vm.submitData($event)
                    }
                  }
                },
                [
                  _c(
                    "div",
                    { staticClass: "modal-body" },
                    [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.form.id,
                            expression: "form.id"
                          }
                        ],
                        staticClass: "d-none",
                        class: { "is-invalid": _vm.form.errors.has("id") },
                        attrs: { type: "text", name: "id", id: "id" },
                        domProps: { value: _vm.form.id },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.form, "id", $event.target.value)
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("has-error", {
                        attrs: { form: _vm.form, field: "id" }
                      }),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group row" }, [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-4 col-form-label",
                            attrs: { for: "name" }
                          },
                          [
                            _vm._v(
                              "\n                    Nama\n                "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-8" },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.name,
                                  expression: "form.name"
                                }
                              ],
                              ref: "name",
                              staticClass: "form-control",
                              class: {
                                "is-invalid": _vm.form.errors.has("name")
                              },
                              attrs: {
                                type: "text",
                                name: "name",
                                id: "name",
                                placeholder: "Nama",
                                autocomplete: "off",
                                required: ""
                              },
                              domProps: { value: _vm.form.name },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.form,
                                    "name",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: { form: _vm.form, field: "name" }
                            })
                          ],
                          1
                        )
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _vm._m(3)
                ]
              )
            ])
          ]
        )
      ]
    ),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "modal fade bd-example-modal-lg",
        attrs: {
          id: "modalFormPrivilege",
          role: "dialog",
          "aria-labelledby": "modalFormPrivilegeLabel",
          "aria-hidden": "true"
        }
      },
      [
        _c(
          "div",
          { staticClass: "modal-dialog modal-lg", attrs: { role: "document" } },
          [
            _c("div", { staticClass: "modal-content" }, [
              _vm._m(4),
              _vm._v(" "),
              _c(
                "form",
                {
                  attrs: { id: "formAddPriv" },
                  on: {
                    submit: function($event) {
                      $event.preventDefault()
                      return _vm.submitDataPriv($event)
                    }
                  }
                },
                [
                  _c("div", { staticClass: "modal-body" }, [
                    _c(
                      "table",
                      {
                        staticClass:
                          "table table-striped table-bordered table-hover table-checkable order-column table-scroll"
                      },
                      [
                        _vm._m(5),
                        _vm._v(" "),
                        _c(
                          "tbody",
                          _vm._l(_vm.privilege, function(menu, index) {
                            return _c("tr", { key: index }, [
                              _c("td", { attrs: { width: "80%" } }, [
                                _c(
                                  "label",
                                  {
                                    staticClass: "form-check-label",
                                    style:
                                      menu.parent != 0
                                        ? "margin-left: 20px; text-transform: uppercase;"
                                        : "font-weight: bold; text-transform: uppercase;",
                                    attrs: { for: "checkbox" + menu.menuId }
                                  },
                                  [
                                    _vm._v(
                                      "\n                                  " +
                                        _vm._s(menu.name) +
                                        "\n                              "
                                    )
                                  ]
                                )
                              ]),
                              _vm._v(" "),
                              _c(
                                "td",
                                {
                                  staticClass: "text-center",
                                  attrs: { width: "20%" }
                                },
                                [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: menu.checked,
                                        expression: "menu.checked"
                                      }
                                    ],
                                    staticClass: "form-check-input",
                                    attrs: {
                                      type: "checkbox",
                                      id: "checkbox" + menu.menuId
                                    },
                                    domProps: {
                                      checked: menu.checked == 1 ? true : false,
                                      checked: Array.isArray(menu.checked)
                                        ? _vm._i(menu.checked, null) > -1
                                        : menu.checked
                                    },
                                    on: {
                                      change: function($event) {
                                        var $$a = menu.checked,
                                          $$el = $event.target,
                                          $$c = $$el.checked ? true : false
                                        if (Array.isArray($$a)) {
                                          var $$v = null,
                                            $$i = _vm._i($$a, $$v)
                                          if ($$el.checked) {
                                            $$i < 0 &&
                                              _vm.$set(
                                                menu,
                                                "checked",
                                                $$a.concat([$$v])
                                              )
                                          } else {
                                            $$i > -1 &&
                                              _vm.$set(
                                                menu,
                                                "checked",
                                                $$a
                                                  .slice(0, $$i)
                                                  .concat($$a.slice($$i + 1))
                                              )
                                          }
                                        } else {
                                          _vm.$set(menu, "checked", $$c)
                                        }
                                      }
                                    }
                                  })
                                ]
                              )
                            ])
                          }),
                          0
                        )
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _vm._m(6)
                ]
              )
            ])
          ]
        )
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-sm-6" }, [
      _c("h1", { staticClass: "m-0 text-dark" }, [_vm._v("Tipe Pengguna")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "breadcrumb-item" }, [
      _c("a", { attrs: { href: "javascript:;" } }, [_vm._v("Akses")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "h5",
        { staticClass: "modal-title", attrs: { id: "modalFormLabel" } },
        [_vm._v("\n              Modal Form\n          ")]
      ),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-footer text-right" }, [
      _c(
        "button",
        {
          staticClass: "btn btn-secondary",
          attrs: { type: "button", "data-dismiss": "modal" }
        },
        [_vm._v("\n                  Batal\n              ")]
      ),
      _vm._v(" "),
      _c(
        "button",
        { staticClass: "btn btn-success", attrs: { type: "submit" } },
        [_vm._v("\n                  Simpan\n              ")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "h5",
        {
          staticClass: "modal-title",
          attrs: { id: "modalFormPrivilegeLabel" }
        },
        [_vm._v("\n              Modal Form Privilege\n          ")]
      ),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { attrs: { width: "80%" } }, [_vm._v(" Menu ")]),
        _vm._v(" "),
        _c("th", { attrs: { width: "20%" } }, [_vm._v(" Akses ")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-footer text-right" }, [
      _c(
        "button",
        {
          staticClass: "btn btn-secondary",
          attrs: { type: "button", "data-dismiss": "modal" }
        },
        [_vm._v("\n                  Batal\n              ")]
      ),
      _vm._v(" "),
      _c(
        "button",
        { staticClass: "btn btn-success", attrs: { type: "submit" } },
        [_vm._v("\n                  Simpan\n              ")]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backoffice/access/UserType.vue":
/*!****************************************************************!*\
  !*** ./resources/js/components/backoffice/access/UserType.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _UserType_vue_vue_type_template_id_241a0aef___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./UserType.vue?vue&type=template&id=241a0aef& */ "./resources/js/components/backoffice/access/UserType.vue?vue&type=template&id=241a0aef&");
/* harmony import */ var _UserType_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UserType.vue?vue&type=script&lang=js& */ "./resources/js/components/backoffice/access/UserType.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _UserType_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _UserType_vue_vue_type_template_id_241a0aef___WEBPACK_IMPORTED_MODULE_0__["render"],
  _UserType_vue_vue_type_template_id_241a0aef___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backoffice/access/UserType.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backoffice/access/UserType.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/backoffice/access/UserType.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UserType_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./UserType.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/access/UserType.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UserType_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backoffice/access/UserType.vue?vue&type=template&id=241a0aef&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/components/backoffice/access/UserType.vue?vue&type=template&id=241a0aef& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserType_vue_vue_type_template_id_241a0aef___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./UserType.vue?vue&type=template&id=241a0aef& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/access/UserType.vue?vue&type=template&id=241a0aef&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserType_vue_vue_type_template_id_241a0aef___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserType_vue_vue_type_template_id_241a0aef___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);